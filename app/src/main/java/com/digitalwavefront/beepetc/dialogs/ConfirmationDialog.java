package com.digitalwavefront.beepetc.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;

/**
 * Created by Wayne on 11/1/15.
 */
public class ConfirmationDialog extends DialogFragment {
    private String title, prompt;
    private ConfirmationListener listener;

    public interface ConfirmationListener {
        void onConfirm();
        void onDeny();
    }
    public static ConfirmationDialog newInstance(String title, String prompt, ConfirmationListener listener) {
        ConfirmationDialog dialog = new ConfirmationDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("prompt", prompt);
        dialog.setArguments(args);
        dialog.setListener(listener);
        return dialog;
    }

    private void setListener(ConfirmationListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.title = getArguments().getString("title");
        this.prompt = getArguments().getString("prompt");
        setRetainInstance(true);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.dialog_confirmation, null);
        TextView dialogTitle = (TextView) dialogView.findViewById(R.id.dialog_title);
        dialogTitle.setText(title);
        TextView dialogPrompt = (TextView) dialogView.findViewById(R.id.dialog_text);
        dialogPrompt.setText(prompt);

        // Set fonts
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
        dialogTitle.setTypeface(lightTypeface);
        dialogPrompt.setTypeface(lightTypeface);

        builder.setView(dialogView)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onConfirm();
                        ConfirmationDialog.this.getDialog().cancel();
                    }
                }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onDeny();
                ConfirmationDialog.this.getDialog().cancel();
            }
        });

        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }
}

