package com.digitalwavefront.beepetc.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;

/**
 * Created by Wayne on 11/13/15.
 */
public class EditTextDialog extends DialogFragment {
    private static final String EXTRA_DIALOG_TITLE = "com.waynebjackson.becon.dialog_title";
    private static final String EXTRA_TEXT_TO_EDIT = "com.waynebjackson.becon.text_to_edit";
    private static final String EXTRA_HINT = "com.waynebjackson.becon.hint";
    private static final String EXTRA_INPUT_TYPE = "com.waynebjackson.becon.input_type";

    private EditTextDialogListener listener;

    public EditTextDialog() {}

    public static EditTextDialog newInstance(String dialogTitle, String textToEdit, String hint,
                                             int inputType, EditTextDialogListener listener) {
        EditTextDialog dialog = new EditTextDialog();
        Bundle args = new Bundle();
        args.putString(EXTRA_DIALOG_TITLE, dialogTitle);
        args.putString(EXTRA_TEXT_TO_EDIT, textToEdit);
        args.putString(EXTRA_HINT, hint);
        args.putInt(EXTRA_INPUT_TYPE, inputType);
        dialog.setArguments(args);
        dialog.setListener(listener);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Get arguments
        String dialogTitle = getArguments().getString(EXTRA_DIALOG_TITLE);
        String textToEdit = getArguments().getString(EXTRA_TEXT_TO_EDIT);
        String hint = getArguments().getString(EXTRA_HINT);
        int inputType = getArguments().getInt(EXTRA_INPUT_TYPE);

        // Initialize views
        View dialogView = inflater.inflate(R.layout.dialog_edit_text, null);
        TextView dialogTitleTextView = (TextView) dialogView.findViewById(R.id.dialog_title_text_view);
        final EditText editText = (EditText) dialogView.findViewById(R.id.edit_text);

        // Set fonts
        final Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
        dialogTitleTextView.setTypeface(lightTypeface);
        editText.setTypeface(lightTypeface);

        // Set view properties
        dialogTitleTextView.setText(dialogTitle);
        editText.setHint(hint);
        editText.setInputType(inputType);
        editText.setText(textToEdit);
        editText.setSelection(textToEdit.length());

        // Open the keyboard
        openKeyboard();

        builder.setView(dialogView)
               .setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {

                   @Override
                   public void onClick(DialogInterface dialog, int which) {
                       closeKeyboard(editText);
                       String editedText = editText.getText().toString().trim();
                       listener.onEdit(editedText);
                   }
               })
               .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                   @Override
                   public void onClick(DialogInterface dialog, int which) {
                       closeKeyboard(editText);
                       dialog.cancel();
                   }
               });

        return builder.create();
    }

    private void openKeyboard() {
        InputMethodManager imm =(InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    private void closeKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    private void setListener(EditTextDialogListener listener) {
        this.listener = listener;
    }

    public interface EditTextDialogListener {
        void onEdit(String editedText);
    }
}
