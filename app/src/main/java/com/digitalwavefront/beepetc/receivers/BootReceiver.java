package com.digitalwavefront.beepetc.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.digitalwavefront.beepetc.BeConApp;
import com.digitalwavefront.beepetc.utils.BluetoothUtils;

/**
 * Created by Wayne on 9/27/15.
 */
public class BootReceiver extends BroadcastReceiver {
    private static final String TAG = "BootReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (BluetoothUtils.bluetoothIsEnabled() || BluetoothUtils.bluetoothIsTurningOn()) {
            Log.d(TAG, "Bluetooth is enabled or connecting starting beacon service");
            ((BeConApp) context.getApplicationContext()).startBeaconMonitoringIfNecessary();
        } else {
            Log.d(TAG, "Bluetooth is not enabled or turning on");
        }
    }
}
