package com.digitalwavefront.beepetc.receivers;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.digitalwavefront.beepetc.BeConApp;

/**
 * Created by Wayne on 9/27/15.
 */
public class BluetoothConnectionReceiver extends BroadcastReceiver {

    private static final String TAG = "BluetoothConnReceiver";

    public BluetoothConnectionReceiver() {}

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        try{
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                int bluetoothState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);

                switch (bluetoothState) {
                    case BluetoothAdapter.STATE_OFF:
                        Log.d(TAG, "Bluetooth turned off");
                        ((BeConApp) context.getApplicationContext()).stopBeaconMonitoringService();
                        break;
                    case BluetoothAdapter.STATE_ON:
                        Log.d(TAG, "Bluetooth turned on");
                        ((BeConApp) context.getApplicationContext()).startBeaconMonitoringIfNecessary();
                        break;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
