package com.digitalwavefront.beepetc.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.SaveCallback;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.utils.NotificationUtils;

/**
 * Created by Wayne on 10/11/15.
 */
public class SaveNotificationReceiver extends BroadcastReceiver {
    public static final String EXTRA_NOTIFICATION_ID = "notificationId";
    public static final String EXTRA_COMPANY_ID = "companyId";
    public static final String EXTRA_NOTIFICATION_CANCEL_ID = "notificationCancelId";
    private static final int INVALID_CANCEL_ID = -1;

    @Override
    public void onReceive(final Context context, Intent intent) {
        BeConUser currentUser = BeConUser.getCurrentUser();
        String notificationId = intent.getStringExtra(EXTRA_NOTIFICATION_ID);
        String companyId = intent.getStringExtra(EXTRA_COMPANY_ID);
        int notificationCancelId = intent.getIntExtra(EXTRA_NOTIFICATION_CANCEL_ID, INVALID_CANCEL_ID);

        // Dismiss the notification
        NotificationUtils.dismissNotification(context, notificationCancelId);

        if (currentUser != null) {
            currentUser.saveNotification(companyId, notificationId, new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Toast.makeText(context, R.string.saved_exclaim, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.error_saving, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
