package com.digitalwavefront.beepetc.receivers;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.digitalwavefront.beepetc.login.LoginType;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.services.BeaconMonitoringService;

/**
 * Created by Wayne on 9/27/15.
 */
public class BeaconMonitoringAlarm extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!isBeaconMonitoringServiceRunning(context)) {

            // Only start the service if this is an attendee login
            if (BeConUser.getCurrentUser().getLoginType(context) == LoginType.ATTENDEE) {
                Intent beaconMonitoringService = new Intent(context, BeaconMonitoringService.class);
                context.startService(beaconMonitoringService);
            }
        }
    }

    private boolean isBeaconMonitoringServiceRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (BeaconMonitoringService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
