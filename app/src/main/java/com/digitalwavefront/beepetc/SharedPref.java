package com.digitalwavefront.beepetc;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Tarul on 09/09/2016.
 */
public class SharedPref {
    private final SharedPreferences mPrefs;

    public SharedPref(Context context) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    private String PREF_Key = "Key";
    private String PREF_MenuItems = "menuItems";

    public String getKey() {
        String str = mPrefs.getString(PREF_Key, "");
        return str;
    }

    public void setKey(String pREF_Key) {
        SharedPreferences.Editor mEditor = mPrefs.edit();
        mEditor.putString(PREF_Key, pREF_Key);
        mEditor.commit();
    }

    public boolean getVal(String key) {
        boolean str = mPrefs.getBoolean(key, false);
        return str;
    }

    public void setValKey(String pREF_Key, boolean val) {
        SharedPreferences.Editor mEditor = mPrefs.edit();
        mEditor.putBoolean(pREF_Key, val);
        mEditor.commit();
    }
/*
    public Set<String> getMenuItems() {
        Set<String> items = mPrefs.getStringSet(PREF_MenuItems,"");
        return items;
    }

    public void setMenuItems(Set<String> pREF_menuItems) {
        SharedPreferences.Editor mEditor = mPrefs.edit();
        mEditor.putStringSet(PREF_MenuItems,pREF_menuItems);
        mEditor.commit();
    }*/
}
