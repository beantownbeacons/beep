package com.digitalwavefront.beepetc.interfaces;

import com.digitalwavefront.beepetc.parse.models.BeConNotification;

/**
 * Created by Wayne on 10/11/15.
 */
public interface SavedNotificationActionListener {
    void onPdfClicked(BeConNotification notification);
    void onWebClicked(BeConNotification notification);
    void onShare(BeConNotification notification);
    void onDeleteClicked(BeConNotification notification);
    void onSaveClicked(BeConNotification notification);

    //void onDeleteClicked(BeConNotification notification);
}
