package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.BeConBeacon;

import java.util.List;

/**
 * Created by Wayne on 9/27/15.
 */
public interface BeaconsLoadedListener {
    void onBeaconsLoaded(@Nullable List<BeConBeacon> beacons);
}
