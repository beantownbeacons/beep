package com.digitalwavefront.beepetc.interfaces;


import com.digitalwavefront.beepetc.parse.models.Exhibitor;

/**
 * Created by Tarul on 05/10/2016.
 */
public interface ExhibitorActionListener {
    void onWebClicked(Exhibitor exhibitor);
}
