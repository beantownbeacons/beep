package com.digitalwavefront.beepetc.interfaces;

import com.digitalwavefront.beepetc.parse.models.Speaker;

/**
 * Created by Wayne on 11/17/15.
 */
public interface SpeakerOptionClickListener {
    void onLinkedInClicked(Speaker speaker);
    void onTwitterClicked(Speaker speaker);

}
