package com.digitalwavefront.beepetc.interfaces;

import com.digitalwavefront.beepetc.parse.models.SurveyQuestion;

/**
 * Created by Wayne on 10/18/15.
 */
public interface SurveyQuestionAnsweredListener {
    void onSurveyQuestionAnswered(SurveyQuestion question);
}
