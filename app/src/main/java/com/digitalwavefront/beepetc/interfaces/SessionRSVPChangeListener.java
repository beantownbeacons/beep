package com.digitalwavefront.beepetc.interfaces;

import com.digitalwavefront.beepetc.parse.models.Session;

/**
 * Created by Wayne on 10/10/15.
 */
public interface SessionRSVPChangeListener {
    void onRSVPChange(Session session, boolean isGoing);
}
