package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.NavConfigurator;

/**
 * Created by User on 9/19/17.
 */

public interface NavConfLoadedListener {

    void onNavConfLoaded(@Nullable NavConfigurator navConf);

}
