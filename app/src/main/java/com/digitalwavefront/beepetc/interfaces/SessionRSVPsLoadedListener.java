package com.digitalwavefront.beepetc.interfaces;

import com.digitalwavefront.beepetc.parse.models.SessionRSVP;

import java.util.List;

/**
 * Created by Wayne on 10/13/15.
 */
public interface SessionRSVPsLoadedListener {
    void onSessionRSVPSLoaded(List<SessionRSVP> sessionRSVPs);
}
