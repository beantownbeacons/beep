package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.Speaker;

/**
 * Created by Wayne on 9/18/15.
 */
public interface SpeakerLoadedListener {
    void onSpeakerLoaded(@Nullable Speaker speaker);
}
