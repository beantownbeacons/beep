package com.digitalwavefront.beepetc.interfaces;

import android.graphics.Bitmap;

/**
 * Created by Wayne on 9/16/15.
 */
public interface BitmapLoadedListener {
    void onBitmapLoaded(Bitmap bitmap);
}
