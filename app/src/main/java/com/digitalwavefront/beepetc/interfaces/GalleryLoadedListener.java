package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.EventGallery;

import java.util.List;

/**
 * Created by Praneet on 01/07/2016.
 */
public interface GalleryLoadedListener {
    void onGalleryLoaded(@Nullable List<EventGallery> gallery);
}
