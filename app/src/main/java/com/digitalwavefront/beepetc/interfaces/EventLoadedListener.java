package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.Event;

/**
 * Created by Wayne on 9/16/15.
 */
public interface EventLoadedListener {
    void onEventLoaded(@Nullable Event event);
}
