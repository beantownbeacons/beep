package com.digitalwavefront.beepetc.interfaces;

import com.parse.ParseException;

/**
 * Created by Wayne on 11/22/15.
 */
public interface SignInListener {
    void onSignIn();
    void onError(ParseException e);
}
