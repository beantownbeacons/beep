package com.digitalwavefront.beepetc.interfaces;

import com.parse.ParseException;
import com.digitalwavefront.beepetc.parse.models.SurveyResponse;

/**
 * Created by Wayne on 10/16/15.
 */
public interface SurveyResponseCallback {
    void onResponseRecorded(SurveyResponse response);
    void onResponseError(ParseException e);
}
