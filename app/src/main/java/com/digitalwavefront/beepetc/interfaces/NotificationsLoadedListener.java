package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.BeConNotification;

import java.util.List;

/**
 * Created by Wayne on 9/28/15.
 */
public interface NotificationsLoadedListener {
    void onNotificationsLoaded(@Nullable List<BeConNotification> notifications);
}
