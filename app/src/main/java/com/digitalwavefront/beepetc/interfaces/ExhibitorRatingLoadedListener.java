package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.ExhibitorRating;

import java.util.List;

/**
 * Created by sanjay on 02/06/16.
 */
public interface ExhibitorRatingLoadedListener {

    void onExhibitorRatingLoaded(@Nullable List<ExhibitorRating> exhibitorRatingList);
}
