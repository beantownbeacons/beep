package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.UserPreferences;

/**
 * Created by User on 1/11/18.
 */

public interface PrefLoadedListener {

    void onPrefLoadedListener(@Nullable UserPreferences preferences);
}
