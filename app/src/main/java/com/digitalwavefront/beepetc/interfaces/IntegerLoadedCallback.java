package com.digitalwavefront.beepetc.interfaces;

/**
 * Created by Wayne on 10/18/15.
 */
public interface IntegerLoadedCallback {
    void onIntegerLoaded(int value);
}
