package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.BeaconInteraction;

import java.util.List;

/**
 * Created by Wayne on 10/18/15.
 */
public interface BeaconInteractionsLoadedListener {
    void onInteractionsLoaded(@Nullable List<BeaconInteraction> interactions);
}
