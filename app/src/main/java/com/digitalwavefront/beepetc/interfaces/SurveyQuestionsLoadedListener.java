package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.SurveyQuestion;

import java.util.List;

/**
 * Created by Wayne on 10/12/15.
 */
public interface SurveyQuestionsLoadedListener {
    void onSurveyQuestionsLoaded(@Nullable List<SurveyQuestion> questions);
}
