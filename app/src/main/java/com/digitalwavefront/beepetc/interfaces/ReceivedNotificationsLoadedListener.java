package com.digitalwavefront.beepetc.interfaces;

import com.digitalwavefront.beepetc.parse.models.ReceivedNotification;

import java.util.List;

/**
 * Created by Wayne on 10/13/15.
 */
public interface ReceivedNotificationsLoadedListener {
    void onReceivedNotificationsLoaded(List<ReceivedNotification> notifications);
}
