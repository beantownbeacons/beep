package com.digitalwavefront.beepetc.interfaces;

import com.digitalwavefront.beepetc.parse.models.BeConUser;

/**
 * Created by Tarul on 13/09/2016.
 */
public interface AttendeeOptionClickListener {
    void onLinkedInClicked(BeConUser attendee);
    void onTwitterClicked(BeConUser attendee);
    void onA2AConnClicked(BeConUser attendee);
}
