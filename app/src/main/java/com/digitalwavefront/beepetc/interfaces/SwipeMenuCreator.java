package com.digitalwavefront.beepetc.interfaces;


import com.digitalwavefront.beepetc.views.SwipeMenu;

/**
 * Added by Supra-Chaitali on 12/04/2016
 */
public interface SwipeMenuCreator {

	void create(SwipeMenu menu);
}
