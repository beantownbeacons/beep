package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.Speaker;

import java.util.List;

/**
 * Created by Wayne on 9/18/15.
 */
public interface SpeakersLoadedListener {
    void onSpeakersLoaded(@Nullable List<Speaker> speakers);
}
