package com.digitalwavefront.beepetc.interfaces;

import com.digitalwavefront.beepetc.parse.models.SavedNotification;

import java.util.List;

/**
 * Created by Wayne on 10/13/15.
 */
public interface SavedNotificationsLoadedListener {
    void onSavedNotificationsLoaded(List<SavedNotification> notifications);
}
