package com.digitalwavefront.beepetc.interfaces;

/**
 * Created by Wayne on 11/29/15.
 */
public interface BooleanCallback {
    void onValueRetrieved(boolean value);
}
