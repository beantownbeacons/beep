package com.digitalwavefront.beepetc.interfaces;

/**
 * Created by Wayne on 10/18/15.
 */
public interface FloatLoadedCallback {
    void onFloatLoaded(float value);
}
