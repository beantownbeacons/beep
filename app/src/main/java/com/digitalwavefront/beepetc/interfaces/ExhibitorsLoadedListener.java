package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.Exhibitor;

import java.util.List;

/**
 * Created by sanjay on 02/06/16.
 */
public interface ExhibitorsLoadedListener {

    void onExhibitorsLoaded(@Nullable List<Exhibitor> exhibitors);
}
