package com.digitalwavefront.beepetc.interfaces;

import com.digitalwavefront.beepetc.parse.models.Content;

/**
 * Created by User on 8/14/17.
 */

public interface ContentLibraryActionListener {
    void onPdfClicked(Content content);
    void onWebClicked(Content content);
    void onShare(Content content);
}
