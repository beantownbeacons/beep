package com.digitalwavefront.beepetc.interfaces;

import com.digitalwavefront.beepetc.parse.models.BeConUser;

/**
 * Created by Wayne on 11/1/15.
 */
public interface UserVerificationListener {
    void onUserVerified(BeConUser user);
    void onVerificationInvalid(BeConUser user);
}
