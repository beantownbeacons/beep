package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.EventAccessLog;

import java.util.List;

/**
 * Created by Sarika on 9/25/17.
 */

public interface EventAccessLogLoadedListener {

    void onEventAccessLoaded(@Nullable List<EventAccessLog> evtLogs);
}
