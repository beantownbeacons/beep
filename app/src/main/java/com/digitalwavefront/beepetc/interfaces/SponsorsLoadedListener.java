package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.Sponsor;

import java.util.List;

/**
 * Created by Tarul on 06/10/2016.
 */
public interface SponsorsLoadedListener {
    void onSponsorsLoaded(@Nullable List<Sponsor> sponsors);
}
