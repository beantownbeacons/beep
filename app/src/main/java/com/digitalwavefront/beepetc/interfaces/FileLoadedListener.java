package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import java.io.File;

/**
 * Created by Wayne on 10/11/15.
 */
public interface FileLoadedListener {
    void onFileLoaded(@Nullable File file);
}
