package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.SurveyResponse;

import java.util.List;

/**
 * Created by Wayne on 10/16/15.
 */
public interface SurveyResponseLoadedListener {
    void onSurveyResponsesLoaded(@Nullable List<SurveyResponse> responses);
}
