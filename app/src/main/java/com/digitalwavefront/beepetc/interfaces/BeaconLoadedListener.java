package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.BeConBeacon;

/**
 * Created by Wayne on 9/27/15.
 */
public interface BeaconLoadedListener {
    void onBeaconLoaded(@Nullable BeConBeacon beacon);
}
