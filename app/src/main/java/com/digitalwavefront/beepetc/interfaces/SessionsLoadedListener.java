package com.digitalwavefront.beepetc.interfaces;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.parse.models.Session;

import java.util.List;

/**
 * Created by Wayne on 9/18/15.
 */
public interface SessionsLoadedListener {
    void onSessionsLoaded(@Nullable List<Session> sessions);
}
