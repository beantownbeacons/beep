package com.digitalwavefront.beepetc.adapters;

import android.content.Intent;
import android.graphics.drawable.Drawable;

/**
 * Created by user on 23-Apr-16.
 */
public class CropingOption {
    public CharSequence title;
    public Drawable icon;
    public Intent appIntent;
}
