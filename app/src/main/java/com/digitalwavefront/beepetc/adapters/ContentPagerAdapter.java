package com.digitalwavefront.beepetc.adapters;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.digitalwavefront.beepetc.fragments.ContentFragment;
import com.digitalwavefront.beepetc.fragments.ContentLibraryFragment;
import com.digitalwavefront.beepetc.fragments.LeftoutContentFragment;
import com.digitalwavefront.beepetc.interfaces.IResetadapter;

/**
 * Created by User on 1/6/18.
 */

public class ContentPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs=3;

    IResetadapter ir;
    public ContentPagerAdapter(FragmentManager fm, int NumOfTabs,IResetadapter ir) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.ir=ir;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                ContentFragment tab1 = new ContentFragment();

                return tab1;
            case 1:
                ContentLibraryFragment tab2 = new ContentLibraryFragment();

                return tab2;
            case 2:
                LeftoutContentFragment tab3 = new LeftoutContentFragment();
                tab3.ir=ir;

                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }


}
