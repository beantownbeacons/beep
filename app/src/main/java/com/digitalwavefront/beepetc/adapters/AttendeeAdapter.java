package com.digitalwavefront.beepetc.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.interfaces.AttendeeOptionClickListener;
import com.digitalwavefront.beepetc.interfaces.BitmapLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.views.RoundImageView;

import java.util.List;

/**
 * Created by supra-chaitali on 23/03/2016.
 */
/**
 * Updated by Tarul on 9/8/16.
 */
public class AttendeeAdapter extends BaseAdapter {
    private Context contxt;
    private List<BeConUser> attendee;
    private LayoutInflater inflater;
    private AttendeeOptionClickListener attendeeOptionClickListener;

    public AttendeeAdapter(Context context, List<BeConUser> attendeelist, AttendeeOptionClickListener attendeeOptionClickListener) {
        this.contxt = context;
        this.attendee = attendeelist;
        this.inflater = LayoutInflater.from(contxt);
        this.attendeeOptionClickListener = attendeeOptionClickListener;
    }

    @Override
    public int getCount() {
        return attendee.size();
    }

    @Override
    public BeConUser getItem(int position) {
        return attendee.get(position);
    }

    private class ViewHolder {
        private RoundImageView attendeePhotoView;
        private ImageView attendeePhotoView1;
        private TextView txt_name, txt_company;//, txt_title;
        private Button btn_linkedin, btn_twitter,  btn_A2AConn;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final BeConUser attendee = getItem(position);
        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.attendee_list_item, null);
            holder.attendeePhotoView = (RoundImageView) convertView.findViewById(R.id.attendee_photo_view);
            holder.attendeePhotoView1 = (ImageView) convertView.findViewById(R.id.attendee_photo_view1);
            holder.txt_name = (TextView) convertView.findViewById(R.id.txt_attendee_name);
            holder.txt_company = (TextView) convertView.findViewById(R.id.txt_attendee_company);
            holder.btn_linkedin = (Button) convertView.findViewById(R.id.linkedin_btn);
            holder.btn_twitter = (Button) convertView.findViewById(R.id.twitterbtn);
            //holder.btn_A2AConn = (Button) convertView.findViewById(R.id.A2AConn_btn);
            //  holder.txt_title = (TextView) convertView.findViewById(R.id.txt_attendee_title);

            //set fonts
            Typeface lightTypeface = TypefaceUtils.getLightTypeface(contxt);
            Typeface boldTypeface = TypefaceUtils.getSemiBoldTypeface(contxt);
            holder.txt_name.setTypeface(boldTypeface);
            holder.txt_company.setTypeface(lightTypeface);
            //holder.txt_title.setTypeface(lightTypeface);

            //set stroke color to roundphotoview
            //   holder.attendeePhotoView.setBorderColor(ContextCompat.getColor(contxt, R.color.tw__light_gray));
            convertView.setTag(holder);
        }

        final ViewHolder holder = (ViewHolder) convertView.getTag();

        holder.txt_name.setText(attendee.getName());
        if(attendee.getCompany()!=null && attendee.getTitle()!=null) {
            holder.txt_company.setText(attendee.getCompany() + System.getProperty("line.separator") + attendee.getTitle());
        } else if(attendee.getCompany()!=null){
            holder.txt_company.setText(attendee.getCompany());
        }
        //holder.txt_title.setText(attendee.getTitle());

        holder.attendeePhotoView1.setVisibility(View.VISIBLE);
        holder.attendeePhotoView.setVisibility(View.GONE);
        holder.attendeePhotoView1.setImageResource(R.drawable.ic_account_circle);

        attendee.getProfilePhoto(new BitmapLoadedListener() {
            @Override
            public void onBitmapLoaded(Bitmap profile_img) {
                if (profile_img != null) {
                    holder.attendeePhotoView1.setVisibility(View.GONE);
                    holder.attendeePhotoView.setVisibility(View.VISIBLE);
                    holder.attendeePhotoView.setImageBitmap(profile_img);
                } else {
                    holder.attendeePhotoView.setImageResource(R.drawable.ic_person);
                }
            }
        });


        // Setup linked-in action
        if (!attendee.hasLinkedIn()) {
            holder.btn_linkedin.setVisibility(View.GONE);
        } else {
            holder.btn_linkedin.setVisibility(View.VISIBLE);
            holder.btn_linkedin.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    attendeeOptionClickListener.onLinkedInClicked(attendee);
                }
            });
        }

        // Setup twitter action
        if (!attendee.hasTwitter()) {
            holder.btn_twitter.setVisibility(View.GONE);
        } else {
            holder.btn_twitter.setVisibility(View.VISIBLE);
            holder.btn_twitter.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    attendeeOptionClickListener.onTwitterClicked(attendee);
                }
            });
        }

        /*
        holder.btn_A2AConn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attendeeOptionClickListener.onA2AConnClicked(attendee);
            }
        });*/
        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
