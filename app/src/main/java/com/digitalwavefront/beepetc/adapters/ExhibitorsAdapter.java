package com.digitalwavefront.beepetc.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.bumptech.glide.Glide;
import com.digitalwavefront.beepetc.BeConApp;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.interfaces.ExhibitorActionListener;
import com.digitalwavefront.beepetc.interfaces.SessionRSVPChangeListener;
import com.digitalwavefront.beepetc.interfaces.SpeakerLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Exhibitor;
import com.digitalwavefront.beepetc.parse.models.ExhibitorRating;
import com.digitalwavefront.beepetc.utils.StringUtils;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.views.CircularImageView;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sanjay on 02/06/16.
 */

/**
 * Updated by Tarul on 15/05/2016
 */
public class ExhibitorsAdapter extends BaseAdapter {

    private Context context;
    private List<Exhibitor> exhibitors;
    private List<Exhibitor> filteredexhibitors;
    private LayoutInflater inflater;
    private SessionRSVPChangeListener rsvpChangeListener;
    public Activity activity;
    private SpeakerLoadedListener speakerLoadedListener;
    private Bitmap speaker_photoview;
    private ExhibitorActionListener exhibitorActionListener;
    String url;

    // Row Types
    private static final int ROW_TYPE_NORMAL = 0;
    private static final int ROW_TYPE_EXPANDED = 1;
    private static final int ROW_TYPE_COUNT = 2;

    private HashMap<String, Integer> exRatingHash, tempExRatingHash = new HashMap<String, Integer>() {
    };

    // Expanded map
    private HashMap<Integer, Boolean> expandedMap;
    ProgressDialog progressDialog;

    /*public ExhibitorsAdapter(Context context, Activity act, List<Exhibitor> exhibitors, HashMap<String, Integer> exRatingHash) {
        this.context = context;
        this.exhibitors = exhibitors;
        this.filteredexhibitors = exhibitors;
        this.inflater = LayoutInflater.from(context);
        this.expandedMap = new HashMap<>();
        this.activity = act;
        this.exRatingHash = exRatingHash;


        // Initialize the expanded map
        for (int i = 0; i < exhibitors.size(); i++) {
            expandedMap.put(i, false);
        }
    }*/


    public ExhibitorsAdapter(Context context, Activity act, List<Exhibitor> exhibitors) {
        this.context = context;
        this.exhibitors = exhibitors;
        this.filteredexhibitors = exhibitors;
        this.inflater = LayoutInflater.from(context);
        this.expandedMap = new HashMap<>();
        this.activity = act;

        // Initialize the expanded map
        for (int i = 0; i < exhibitors.size(); i++) {
            expandedMap.put(i, false);
        }
    }

    @Override
    public int getCount() {
        return exhibitors.size();
    }

    @Override
    public ParseObject getItem(int position) {
        return exhibitors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return ROW_TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        if (expandedMap.get(position)) {
            return ROW_TYPE_EXPANDED;
        }
        return ROW_TYPE_NORMAL;
    }

    @Override
    public void notifyDataSetChanged() {

        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //final Exhibitor exhibitor = (Exhibitor)getItem(position);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        final ParseObject exhibitor = getItem(position);


        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.exhibitor_list_item, null);
            holder.mainContent = (RelativeLayout) convertView.findViewById(R.id.main_content);
            holder.secondaryContent = (RelativeLayout) convertView.findViewById(R.id.secondary_content);

            holder.titleView = (TextView) convertView.findViewById(R.id.exhibitor_name);
            //holder.photoLayout = (ImageView) convertView.findViewById(R.id.exhibitor_photo_view);
            holder.sessionSpeakerPhotoView = (CircularImageView) convertView.findViewById(R.id.exhibitor_photo_view);
            holder.imgurl = (ImageView) convertView.findViewById(R.id.imgurl);
            holder.txt_desc = (TextView) convertView.findViewById(R.id.txt_description);
            holder.boothname = (TextView) convertView.findViewById(R.id.boothname);
            //holder.imgstar = (ImageView)convertView.findViewById(R.id.imgstar);
            //holder.ratingBar = (RatingBar)convertView.findViewById(R.id.ratingBar);
            //holder.btnrate = (Button)convertView.findViewById(R.id.btnrate);

            /*holder.ratingStarOne = (CheckBox) convertView.findViewById(R.id.rating_star_one);
            holder.ratingStarTwo = (CheckBox) convertView.findViewById(R.id.rating_star_two);
            holder.ratingStarThree = (CheckBox) convertView.findViewById(R.id.rating_star_three);
            holder.ratingStarFour = (CheckBox) convertView.findViewById(R.id.rating_star_four);
            holder.ratingStarFive = (CheckBox) convertView.findViewById(R.id.rating_star_five);*/


            // Set Fonts
            Typeface lightTypeface = TypefaceUtils.getLightTypeface(context);
            Typeface regularTypeface = TypefaceUtils.getRegularTypeface(context);

            holder.titleView.setTypeface(regularTypeface);
            holder.txt_desc.setTypeface(regularTypeface);


            convertView.setTag(holder);
        }

        final ViewHolder holder = (ViewHolder) convertView.getTag();
        int viewType = getItemViewType(position);

        Log.e("exhibitor: ", "exhibitor name: " + exhibitor.getString("name"));
        holder.titleView.setText(exhibitor.getString("name"));

        String booth = exhibitor.getString("booth");
        if (StringUtils.isNullOrEmpty(booth)){
            booth = "";
        }
        holder.boothname.setText(booth);

        ParseFile logo = exhibitor.getParseFile("logo");
        //Log.e("exhibitor: ", "logo" + logo);
        if (logo != null) {

            //Picasso.with(context).load(logo.getUrl()).into(holder.photoLayout);
            //###### by Sarika ###### [07/10/17]
            Picasso.with(context).load(logo.getUrl()).into(holder.sessionSpeakerPhotoView);

            /*holder.photoLayout.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Bitmap bitmap = getBitmapFromURL(logo.getUrl());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 10, stream);
            holder.photoLayout.setImageBitmap(bitmap);*/

            /*Bitmap bitmap = getBitmapFromURL(logo.getUrl());
            Bitmap bitmap1 = getResizedBitmap(bitmap, 60);
            holder.photoLayout.setImageBitmap(bitmap1);*/

            /*Picasso.with(context)
                    .load(logo.getUrl())
                    .transform(new RoundedTransformation(100, 0))
                    .fit()
                    .into(holder.photoLayout);
*/
            /*Bitmap bitmap = getBitmapFromURL(logo.getUrl());
            Log.e("Bitmap", "" + bitmap);
            //Canvas canvas = new Canvas(bitmap);
            RoundedBitmapDrawable roundDrawable = RoundedBitmapDrawableFactory.create(Resources.getSystem(), bitmap);
            roundDrawable.setCircular(true);
            holder.photoLayout.setImageDrawable(roundDrawable);*/
            /*roundDrawable.setBounds(5, 5, 5, 5);
            roundDrawable.draw(canvas);*/

            /*Bitmap bitmap = BitmapFactory.decodeFile(logo.getUrl());
            holder.photoLayout.setImageBitmap(bitmap);*/

            //Picasso.with(context).load(logo.getUrl()).centerCrop().resize(50,50).into(holder.photoLayout);
            //Glide.with(context).load(logo.getUrl()).into(holder.photoLayout);
            /*
            logo.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] bytes, ParseException e) {
                    if (bytes != null) {
                        Bitmap avatar = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                        holder.photoLayout.setImageBitmap(avatar);
                    }
                }
            });*/
        } else {
            //holder.photoLayout.setImageResource(R.drawable.ic_profile);
            //###### by Sarika ###### [07/10/17]
            holder.sessionSpeakerPhotoView.setImageResource(R.drawable.ic_profile);
        }

        String description = exhibitor.getString("description");
        Log.e("Description", "" + description);
        if (description != null) {
            holder.txt_desc.setText(description);
        }

        final String url = exhibitor.getString("website");

        if (url == null) {
            holder.imgurl.setVisibility(View.INVISIBLE);
        } else if (url.equals("")) {
            holder.imgurl.setVisibility(View.INVISIBLE);
        } else {
            holder.imgurl.setVisibility(View.VISIBLE);
            holder.imgurl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BeConApp) context.getApplicationContext()).openWebPage(activity, url);
                }
            });
        }


        /*int rating = 0;
        if (exRatingHash.containsKey(exhibitor.getObjectId())) {
            rating = exRatingHash.get(exhibitor.getObjectId());//exhibitor.getInt("rating");
            Log.e("exhibitor: ", "rating: " + rating);
        }


        if (rating > 0) {
            holder.imgstar.setImageResource(R.mipmap.rating_star_selected);
            holder.btnrate.setBackgroundResource(R.drawable.checkblue);
            //holder.ratingBar.setEnabled(false);
            holder.ratingBar.setIsIndicator(true);
            holder.btnrate.setEnabled(false);

        } else {
            holder.imgstar.setImageResource(R.mipmap.rating_star);
            holder.btnrate.setBackgroundResource(R.drawable.checkgrey);
            //holder.ratingBar.setEnabled(true);
            holder.ratingBar.setIsIndicator(false);
            holder.btnrate.setEnabled(true);
        }*/
        if (viewType == ROW_TYPE_EXPANDED) {
            holder.secondaryContent.setVisibility(View.VISIBLE);
        } else if (viewType == ROW_TYPE_NORMAL) {
            holder.secondaryContent.setVisibility(View.GONE);
        }


        /*ArrayList<CompoundButton> btnlist = new ArrayList<CompoundButton>(){};
        btnlist.add(holder.ratingStarOne);
        btnlist.add(holder.ratingStarTwo);
        btnlist.add(holder.ratingStarThree);
        btnlist.add(holder.ratingStarFour);
        btnlist.add(holder.ratingStarFive);

        HashMap<Integer,ArrayList<CompoundButton>> btnMap = new HashMap<Integer,ArrayList<CompoundButton>>(){};
        btnMap.put(position,btnlist);

        holder.ratingStarOne.setTag(position);

        holder.ratingStarOne.setOnCheckedChangeListener(new chklistener(position,1,btnlist));
        holder.ratingStarTwo.setOnCheckedChangeListener(new chklistener(position,2,btnlist));
        holder.ratingStarThree.setOnCheckedChangeListener(new chklistener(position,3,btnlist));
        holder.ratingStarFour.setOnCheckedChangeListener(new chklistener(position,4,btnlist));
        holder.ratingStarFive.setOnCheckedChangeListener(new chklistener(position,5,btnlist));*/

        //holder.btnrate.setOnClickListener(null);
        //holder.btnrate.setOnClickListener(new clicklistener(position));

        //holder.ratingBar.setOnRatingBarChangeListener(null);
        //holder.ratingBar.setOnRatingBarChangeListener(onRatingChangedListener(holder, position));

        //holder.ratingBar.setTag(position);
        //holder.ratingBar.setRating(rating);

        return convertView;
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void toggleItem(int position) {
        boolean expanded = !expandedMap.get(position);
        Log.e("expanded", "map: " + expanded);

        // Collapse others if they are expanded
        if (expanded) {
            for (int i = 0; i < getCount(); i++) {
                if (i != position && expandedMap.get(i)) {
                    expandedMap.put(i, false);
                }
            }
        }

        expandedMap.put(position, expanded);
        notifyDataSetChanged();
    }


    public void showAllSessions() {
        collapseAllSessions();
        filteredexhibitors = exhibitors;
        notifyDataSetChanged();
    }

    private void collapseAllSessions() {
        for (int i = 0; i < exhibitors.size(); i++) {
            if (expandedMap.get(i)) {
                expandedMap.put(i, false);
            }
        }
    }

    /*private RatingBar.OnRatingBarChangeListener onRatingChangedListener(final ViewHolder holder, final int position) {
        return new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {

                ParseObject exhibitor = getItem(position);
                Log.e("Adapter", "star: " + v + ", position: " + position);
                int rate = Math.round(v);
                Log.e("Adapter", "rate: " + rate + ", exhibitor: " + exhibitor + ", ex objectId: " + exhibitor.getObjectId());
                //exRatingHash.put(exhibitor.getObjectId(), rate);
                tempExRatingHash.put(exhibitor.getObjectId(), rate);

            }
        };
    }*/

    private class ViewHolder {
        TextView startEndTimeView, titleView, speakerNameView, sessionDetailsView, txt_desc, boothname; //goingTextView,
        CheckBox goingCheckBox;
        // RoundImageView sessionSpeakerPhotoView;
        CircularImageView sessionSpeakerPhotoView;
        //LinearLayout mainContent;
        RelativeLayout secondaryContent, mainContent;
        //CircularImageView photoLayout;
        ImageView imgstar, imgurl, photoLayout;
        RatingBar ratingBar;
        Button btnrate;
        CheckBox ratingStarOne, ratingStarTwo, ratingStarThree, ratingStarFour, ratingStarFive;
    }

    private class chklistener implements CompoundButton.OnCheckedChangeListener {
        int pos, btnpos;
        //HashMap<Integer,ArrayList<CompoundButton>> btnMap;

        ArrayList<CompoundButton> btnList;

        private chklistener(int pos, int btnpos, ArrayList<CompoundButton> btnList) {

            this.pos = pos;
            this.btnpos = btnpos;
            this.btnList = btnList;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            LinearLayout lay1 = (LinearLayout) buttonView.getParent();

            CompoundButton btn1 = (CompoundButton) lay1.findViewById(R.id.rating_star_one); //btnList.get(0);
            CompoundButton btn2 = (CompoundButton) lay1.findViewById(R.id.rating_star_two); //btnList.get(1);
            CompoundButton btn3 = (CompoundButton) lay1.findViewById(R.id.rating_star_three); //btnList.get(2);
            CompoundButton btn4 = (CompoundButton) lay1.findViewById(R.id.rating_star_four); //btnList.get(3);
            CompoundButton btn5 = (CompoundButton) lay1.findViewById(R.id.rating_star_five); //btnList.get(4);

            switch (buttonView.getId()) {
                case R.id.rating_star_one:

                    btn2.setChecked(false);
                    btn3.setChecked(false);
                    btn4.setChecked(false);
                    btn5.setChecked(false);

                    if (isChecked) {
                        btn1.setChecked(true);
                    }
                    break;
                case R.id.rating_star_two:

                    btn3.setChecked(false);
                    btn4.setChecked(false);
                    btn5.setChecked(false);

                    if (isChecked) {
                        btn1.setChecked(true);
                        btn2.setChecked(true);
                    }
                    break;
                case R.id.rating_star_three:

                    btn4.setChecked(false);
                    btn5.setChecked(false);

                    if (isChecked) {
                        btn3.setChecked(true);
                        btn2.setChecked(true);
                        btn1.setChecked(true);
                    }
                    break;
                case R.id.rating_star_four:

                    btn5.setChecked(false);

                    if (isChecked) {
                        btn1.setChecked(true);
                        btn2.setChecked(true);
                        btn3.setChecked(true);
                        btn4.setChecked(true);
                    }
                    break;
                case R.id.rating_star_five:

                    if (isChecked) {
                        btn1.setChecked(true);
                        btn2.setChecked(true);
                        btn3.setChecked(true);
                        btn4.setChecked(true);
                        btn5.setChecked(true);
                    }
                    break;
            }
        }
    }

    private class clicklistener implements View.OnClickListener {
        int pos;

        private clicklistener(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {


            ParseObject exhibitor = getItem(pos);
            int rating = tempExRatingHash.get(exhibitor.getObjectId());
            exRatingHash.put(exhibitor.getObjectId(), rating);

            Log.e("onClick", "exhibitor objectId, rating: " + exhibitor.getObjectId() + ", " + rating);

            if (rating > 0) {

                progressDialog = new ProgressDialog(activity);
                progressDialog.setTitle("Loading...");
                progressDialog.setMessage(activity.getString(R.string.one_moment_please));
                progressDialog.show();

                BeConUser currentUser = BeConUser.getCurrentUser();

                ParseObject exhibitorRating = ParseObject.create(ExhibitorRating.class);
                exhibitorRating.put("UserId", currentUser.getObjectId());
                exhibitorRating.put("ExhibitorId", exhibitor.getObjectId());
                exhibitorRating.put("Rating", rating);

                exhibitorRating.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {

                        progressDialog.dismiss();

                        notifyDataSetChanged();
                    }
                });
            } else {
                Toast.makeText(context, "Please select rating", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class RoundedTransformation implements com.squareup.picasso.Transformation {
        private final int radius;
        private final int margin;  // dp

        // radius is corner radii in dp
        // margin is the board in dp
        public RoundedTransformation(final int radius, final int margin) {
            this.radius = radius;
            this.margin = margin;
        }

        @Override
        public Bitmap transform(final Bitmap source) {
            final Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(new BitmapShader(source, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));

            Bitmap output = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);
            canvas.drawRoundRect(new RectF(margin, margin, source.getWidth() - margin, source.getHeight() - margin), radius, radius, paint);

            if (source != output) {
                source.recycle();
            }

            return output;
        }

        @Override
        public String key() {
            return "rounded";
        }
    }
    public void refreshlist(List<Exhibitor> exb)
    {
        this.exhibitors=exb;
        this.notifyDataSetChanged();
    }
}
