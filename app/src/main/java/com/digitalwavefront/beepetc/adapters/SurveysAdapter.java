package com.digitalwavefront.beepetc.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.interfaces.SpeakerLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Session;
import com.digitalwavefront.beepetc.parse.models.Speaker;
import com.digitalwavefront.beepetc.parse.models.Survey;
import com.digitalwavefront.beepetc.parse.models.SurveyQuestion;
import com.digitalwavefront.beepetc.parse.models.SurveyResponse;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.views.RoundImageView;
import com.parse.CountCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Wayne on 10/11/15.
 */
public class SurveysAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Survey> surveys;
    private List<SurveyQuestion> surveyQuestions;
    private List<Session> sessions;
    private List<String> speakerIds;
    private HashMap<String,Speaker> speakers;
    private HashMap<String,Integer> surveyResponse;
    private HashMap<Session,String> sessSpeakers;
    private List<Integer> percentList;
    private LayoutInflater inflater;
    int completeCount;

    public SurveysAdapter(Context context, ArrayList<Survey> surveys) {
        this.context = context;
        this.surveys = surveys;
        this.inflater = LayoutInflater.from(context);
    }

    //################### by Sarika [27/09/2017]############################
    public SurveysAdapter(Context context, ArrayList<Session> sessions, List<SurveyQuestion> questions) {
        this.context = context;
        this.sessions = sessions;
        this.surveyQuestions = questions;
        this.inflater = LayoutInflater.from(context);
    }

    public SurveysAdapter(Context context, ArrayList<Session> sessions, List<SurveyQuestion> questions, List<String> spkrIds, List<Integer> percentArr) {
        this.context = context;
        this.sessions = sessions;
        this.surveyQuestions = questions;
        this.speakerIds = spkrIds;
        this.speakers = new HashMap<>();
        this.sessSpeakers = new HashMap<>();
        this.surveyResponse = new HashMap<>();
        this.percentList = percentArr;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return sessions.size();
        //return surveys.size();
    }


    public void refreshSessionlist(ArrayList<Session> sessions, List<String> spkrIds, List<Integer> percentArr)
    {
        this.sessions=sessions;
        this.speakerIds = spkrIds;
        this.percentList = percentArr;

        this.notifyDataSetChanged();
        System.out.println("refreshlist");
    }
    /*
    @Override
    public Survey getItem(int position) {
        return surveys.get(position);
    }
    */
    @Override
    public Session getItem(int position) {
        return sessions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Survey survey = getItem(position);
        final Session session = getItem(position);
        BeConUser currentUser = BeConUser.getCurrentUser();

        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.survey_list_item, null);
            holder.titleTextView = (TextView) convertView.findViewById(R.id.title_text_view);
            holder.statusTextView = (TextView) convertView.findViewById(R.id.status_text_view);
            holder.surveyPhotoView = (RoundImageView) convertView.findViewById(R.id.survey_photoview);
            holder.surveyPhotoView1 = (ImageView) convertView.findViewById(R.id.survey_photoview1);
            holder.speaker_text_view = (TextView) convertView.findViewById(R.id.speaker_text_view);

            // Set fonts
            Typeface lightTypeface = TypefaceUtils.getLightTypeface(context);
            holder.titleTextView.setTypeface(lightTypeface);
            holder.statusTextView.setTypeface(lightTypeface);
            //holder.speaker_text_view.setTypeface(lightTypeface);

            convertView.setTag(holder);
        }

        final ViewHolder holder = (ViewHolder) convertView.getTag();
        //holder.surveyPhotoView1.setVisibility(View.VISIBLE);
        //holder.surveyPhotoView.setVisibility(View.GONE);
        //holder.surveyPhotoView1.setImageResource(R.drawable.ic_account_circle);



        final String speakerId = speakerIds.get(position); //session.getSessSpeakerId(); //survey.getSpeakerId();


        String userId = BeConUser.getCurrentUser().getObjectId();



        //if (speakerId == null) {
        holder.surveyPhotoView.setImageResource(R.drawable.ic_person);
        holder.speaker_text_view.setText("");

        Speaker speaker = null;
        try {
            speaker = speakers.get(speakerId);
        }catch (Exception e) {

        }
            if (speaker != null) {

                holder.speaker_text_view.setText(speaker.getFirstName() + " " + speaker.getLastName());

                try {

                    ParseFile avatarFile = speaker.getParseFile("avatar");
                    if (avatarFile != null) {
                        //System.out.println("avatarFile.getUrl(): " + avatarFile.getUrl());

                        Picasso.with(context).load(avatarFile.getUrl()).into(holder.surveyPhotoView);
                    } else {
                        holder.surveyPhotoView.setImageResource(R.drawable.ic_person);
                    }
                } catch (Exception e) {
                    holder.surveyPhotoView.setImageResource(R.drawable.ic_person);
                }
            } else {
                if (speakerIds != null && speakerIds.size() > 0) {
                    String spkId = speakerIds.get(position);
                    session.setSpeakerId(spkId);
                    sessSpeakers.put(session,spkId);
                    //System.out.println("spkrId: " + position + ", " + spkId);
                    Speaker.getSpeakerByObjectId(spkId, new SpeakerLoadedListener() {
                        @Override
                        public void onSpeakerLoaded(@Nullable Speaker speaker) {
                            if (speaker != null) {

                                //speakers.add(speaker);
                                speakers.put(speaker.getObjectId(),speaker);
                                notifyDataSetChanged();
                            }
                        }
                    });
                }
            }



        //}
       // holder.titleTextView.setText(survey.getTitle());
        holder.titleTextView.setText(session.getTitle());

        // by Sarika 3 Feb 2018
        Integer response = null;

        try {
            response = surveyResponse.get(session.getObjectId()+speakerId);
        }catch (Exception e) {

        }
        if (response == null) {

            ParseQuery<SurveyResponse> query = ParseQuery.getQuery(SurveyResponse.class);
            //query.whereEqualTo("questionId", quesId);
            query.whereEqualTo("userId", currentUser.getObjectId());
            query.whereEqualTo("sessionId", session.getObjectId());
            query.whereEqualTo("speakerId", speakerId);
            try {
                query.countInBackground(new CountCallback() {
                    @Override
                    public void done(int count, ParseException e) {

                        if (e == null) {
                            completeCount = count;
                        } else {
                            completeCount = 0;
                        }
                        int percent = (completeCount * 100) / surveyQuestions.size();
                        surveyResponse.put(session.getObjectId()+speakerId, percent);
                    }
                });
            }
            catch (Exception e) {

            }



        } else {

            //################### by Sarika [27/09/2017]############################
            if (surveyQuestions != null && !surveyQuestions.isEmpty()) {
                //final int questionCount = surveyQuestions.size();
                //completeCount = 0;

                try {
                    String percentCompleteString = String.format("%d%%", response);//session.getPercent());
                    holder.statusTextView.setText(percentCompleteString);

                }catch (Exception ex){}
            }
        }


        return convertView;
    }

    private class ViewHolder {
        TextView titleTextView, statusTextView, speaker_text_view;
        RoundImageView surveyPhotoView;
        ImageView surveyPhotoView1;
    }
}
