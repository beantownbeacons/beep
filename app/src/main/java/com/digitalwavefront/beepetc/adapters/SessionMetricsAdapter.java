package com.digitalwavefront.beepetc.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.interfaces.BitmapLoadedListener;
import com.digitalwavefront.beepetc.interfaces.FloatLoadedCallback;
import com.digitalwavefront.beepetc.interfaces.IntegerLoadedCallback;
import com.digitalwavefront.beepetc.parse.models.Speaker;
import com.digitalwavefront.beepetc.utils.StringUtils;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.views.RoundImageView;

import java.util.List;

/**
 * Created by Wayne on 10/18/15.
 */
public class SessionMetricsAdapter extends BaseAdapter {

    private Context context;
    private List<Speaker> speakers;
    private LayoutInflater inflater;

    public SessionMetricsAdapter(Context context, List<Speaker> speakers) {
        this.context = context;
        this.speakers = speakers;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return speakers.size();
    }

    @Override
    public Speaker getItem(int position) {
        return speakers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Speaker speaker = getItem(position);

        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.session_metric_list_item, null);
            holder.speakerPhotoView = (RoundImageView) convertView.findViewById(R.id.speaker_photo_view);
            holder.speakerNameView = (TextView) convertView.findViewById(R.id.speaker_name_view);
            holder.attendeesTextView = (TextView) convertView.findViewById(R.id.attendees_text_view);
            holder.attendeesTitleTextView = (TextView) convertView.findViewById(R.id.attendees_title_text_view);
            holder.averageTimeTextView = (TextView) convertView.findViewById(R.id.average_time_text_view);
            holder.averageTimeTitleTextView = (TextView) convertView.findViewById(R.id.average_time_title_text_view);
            holder.surveysCompletedTextView = (TextView) convertView.findViewById(R.id.surveys_completed_text_view);
            holder.surveysCompletedTitleTextView = (TextView) convertView.findViewById(R.id.surveys_completed_title_text_view);
            holder.averageScoreTextView = (TextView) convertView.findViewById(R.id.average_score_text_view);
            holder.averageScoreTitleTextView = (TextView) convertView.findViewById(R.id.average_score_title_text_view);

            // Set border color of photo view
            holder.speakerPhotoView.setBorderColor(ContextCompat.getColor(context, R.color.colorAccent));

            // Set fonts
            Typeface boldTypeface = TypefaceUtils.getBoldTypeface(context);
            Typeface lightTypeface = TypefaceUtils.getLightTypeface(context);
            holder.speakerNameView.setTypeface(lightTypeface);
            holder.attendeesTextView.setTypeface(boldTypeface);
            holder.attendeesTitleTextView.setTypeface(lightTypeface);
            holder.averageTimeTextView.setTypeface(boldTypeface);
            holder.averageTimeTitleTextView.setTypeface(lightTypeface);
            holder.surveysCompletedTextView.setTypeface(boldTypeface);
            holder.surveysCompletedTitleTextView.setTypeface(lightTypeface);
            holder.averageScoreTextView.setTypeface(boldTypeface);
            holder.averageScoreTitleTextView.setTypeface(lightTypeface);

            convertView.setTag(holder);
        }

        final ViewHolder holder = (ViewHolder) convertView.getTag();

        // Load avatar
        speaker.getAvatar(new BitmapLoadedListener() {

            @Override
            public void onBitmapLoaded(Bitmap bitmap) {
                if (bitmap != null) {
                    holder.speakerPhotoView.setImageBitmap(bitmap);
                } else {
                    // TODO: Handle no bitmap
                }
            }
        });

        holder.speakerNameView.setText(speaker.getFullName());

        speaker.getAttendeeCount(new IntegerLoadedCallback() {

            @Override
            public void onIntegerLoaded(int attendeeCount) {
                holder.attendeesTextView.setText(String.valueOf(attendeeCount));
            }
        });

        speaker.getAverageUserSessionTimeInSeconds(new FloatLoadedCallback() {

            @Override
            public void onFloatLoaded(float averageUserTimeSeconds) {
                holder.averageTimeTextView.setText(StringUtils.getMinuteSecondStringFromSeconds(averageUserTimeSeconds));
            }
        });

        speaker.getSurveyCompleteCount(new IntegerLoadedCallback() {

            @Override
            public void onIntegerLoaded(int surveysCompletedCount) {
                holder.surveysCompletedTextView.setText(String.valueOf(surveysCompletedCount));
            }
        });

        speaker.calculateAverageScore(new FloatLoadedCallback() {

            @Override
            public void onFloatLoaded(float averageScore) {
                holder.averageScoreTextView.setText(String.format("%.2f", averageScore));
            }
        });

        return convertView;
    }

    private class ViewHolder {
        private RoundImageView speakerPhotoView;
        private TextView speakerNameView, attendeesTextView, attendeesTitleTextView,
        averageTimeTextView, averageTimeTitleTextView, surveysCompletedTextView,
        surveysCompletedTitleTextView, averageScoreTextView, averageScoreTitleTextView;
    }
}
