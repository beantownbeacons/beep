package com.digitalwavefront.beepetc.adapters;

import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import com.digitalwavefront.beepetc.fragments.SelectedImageFragment;
import com.digitalwavefront.beepetc.parse.models.EventGallery;

import java.util.List;

/**
 * Created by Tarul on 08/10/2016.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    List<EventGallery> galleries;
    Cursor cursor;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);

    }

    public ViewPagerAdapter(FragmentManager fm, List<EventGallery> galleries) {
        super(fm);
        this.galleries = galleries;
        Log.e("GallerySize", "" + getCount());
    }

    @Override
    public int getCount() {
        return galleries.size();
    }

    /*@Override
    public Object instantiateItem(ViewGroup container, int position) {
        SelectedImageFragment imageFragment = SelectedImageFragment.newInstance();
        Bundle args = new Bundle();

        EventGallery gallery = getItem(position);
        String pictureId = gallery.getObjectId();
        args.putString("pictureId", pictureId);
        args.putString("ImageURL", gallery.getImageUrl());
        args.putString("UserName", gallery.getUploaderName());


        imageFragment.setArguments(args);
        Log.e("PictureId", pictureId);
        Log.e("ImageURL", gallery.getImageUrl());
        Log.e("Username", gallery.getUploaderName());

        //container.addView(imageFragment);

        return imageFragment;
    }*/

    @Override
    public Fragment getItem(int position) {
        Log.e("FragmentAdapter", "Position" + position);
        SelectedImageFragment f = SelectedImageFragment.newInstance();
        f.setPosition(galleries.get(position));
        Log.e("Galleries Position", "" + position);
        return f;
    }

    /*@Override
    public boolean isViewFromObject(View view, Object object) {
        return false;
    }*/

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }

   /* public EventGallery getItem(int position) {
        Log.e("Event", "in getItem");
        return galleries.get(position);
    }*/
}
