package com.digitalwavefront.beepetc.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.interfaces.ContentLibraryActionListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Content;
import com.digitalwavefront.beepetc.parse.models.ContentTracking;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.SaveCallback;

import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Tarul on 08/09/2016.
 */
public class ContentAdapter extends BaseAdapter {
    private Context context;
    private List<Content> contentList;
    private LayoutInflater inflater;
    private ContentLibraryActionListener actionListener;
    ViewHolder holder;
    private static final int ROW_TYPE_NORMAL = 0;
    private static final int ROW_TYPE_EXPANDED = 1;
    private static final int ROW_TYPE_COUNT = 2;

    BeConUser currentUser = BeConUser.getCurrentUser();
    String userId = currentUser.getObjectId();

    private HashMap<Integer, Boolean> expandedMap;

    public ContentAdapter(Context context, List<Content> contentList, ContentLibraryActionListener actionListener) {
        this.context = context;
        this.contentList = contentList;
        this.actionListener = actionListener;
        inflater = LayoutInflater.from(context);
        this.expandedMap = new HashMap<>();

        for (int i = 0; i < contentList.size(); i++) {
            expandedMap.put(i, false);
        }
    }

    @Override
    public int getCount() {
        return contentList.size();
    }

    @Override
    public Content getItem(int position) {
        return contentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public int getItemViewType(int position) {
        if (expandedMap.get(position)) {
            return ROW_TYPE_EXPANDED;
        }
        return ROW_TYPE_NORMAL;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Content content = getItem(position);


        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.saved_notification_list_item, null);
            holder.mainContent = (LinearLayout) convertView.findViewById(R.id.main_content);
            holder.notificationTitleTextView = (TextView) convertView.findViewById(R.id.notification_title_text_view);
            /*
            holder.viewPdfBtn = (Button) convertView.findViewById(R.id.view_pdf_btn);
            holder.viewURLBtn = (Button) convertView.findViewById(R.id.view_url_btn);
            holder.btn_share = (ImageButton) convertView.findViewById(R.id.btn_share);
            holder.btn_share.setVisibility(View.VISIBLE);
            */
            holder.btn_options = (ImageButton) convertView.findViewById(R.id.btn_options);
            holder.btn_options.setVisibility(View.VISIBLE);
            holder.secondaryContent = (LinearLayout) convertView.findViewById(R.id.secondary_content);
            holder.notificationDetail = (TextView) convertView.findViewById(R.id.notification_detail);
            //holder.deleteBtn = (Button) convertView.findViewById(R.id.delete_btn);

            // Set fonts
            Typeface lightTypeface = TypefaceUtils.getLightTypeface(context);
            holder.notificationTitleTextView.setTypeface(lightTypeface);
            holder.notificationDetail.setTypeface(lightTypeface);

            convertView.setTag(holder);
        }
        holder = (ViewHolder) convertView.getTag();
        int viewType = getItemViewType(position);
        holder.notificationTitleTextView.setText(content.getTitle());
        holder.notificationDetail.setText(content.getMessage());
        Log.e("Title", content.getTitle());

        String pdfpath = content.getPdfFilePath(context);
        System.out.println("pdfpath "+pdfpath);
        File pdfFile = new File(pdfpath);
/*
        ParseFile parsefile = content.getPdfFile();
//        Log.e("PDF File", content.getPdfFile().toString());
        if (parsefile == null) {
            holder.viewPdfBtn.setVisibility(View.INVISIBLE);
            holder.viewPdfBtn.setOnClickListener(null);
            Log.e("InVisible", "PDF");
        } else {
            holder.viewPdfBtn.setVisibility(View.VISIBLE);
            Log.e("Visible", "PDF");
            holder.viewPdfBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentTracking contentTracking = new ContentTracking();
                    contentTracking.setContentId(content.getObjectId());
                    contentTracking.setContentType("PDF");
                    contentTracking.setUserId(userId);
                    contentTracking.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            //   Log.e("ParseException", e.toString());
                            Log.e("ContentId", content.getObjectId());
                            Log.e("ContentType", "PDF");
                            Log.e("UserId", userId);
                        }
                    });
                    actionListener.onPdfClicked(content);
                }
            });
        }


*/

        if (viewType == ROW_TYPE_EXPANDED) {
            holder.secondaryContent.setVisibility(View.VISIBLE);
        } else if (viewType == ROW_TYPE_NORMAL) {
            holder.secondaryContent.setVisibility(View.GONE);
        }

        holder.btn_options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               showmenu(v,content);
            }
        });
        return convertView;
    }

    private void showmenu(View view,final Content content)
    {
        //actionListener.onShare(content);
        PopupMenu popup = new PopupMenu(context, view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.content_options_menu, popup.getMenu());
        MenuItem urlitem=(MenuItem)popup.getMenu().findItem(R.id.menu_item_url);
        MenuItem pdfitem=(MenuItem)popup.getMenu().findItem(R.id.menu_item_pdf);
        MenuItem shareitem=(MenuItem)popup.getMenu().findItem(R.id.menu_item_share);
        MenuItem deleteitem=(MenuItem)popup.getMenu().findItem(R.id.menu_item_delete);
        
        deleteitem.setVisible(false);

        MenuItem saveitem=(MenuItem)popup.getMenu().findItem(R.id.menu_item_save);
        saveitem.setVisible(false);

        String pdfpath = content.getPdfFilePath(context);
        File pdfFile = new File(pdfpath);
        ParseFile parsefile = content.getPdfFile();


        String url = content.getUrl();
        Log.e("URL", "U"+content.getUrl()+"rl");

        if (url == null || url.equals("")) {
            urlitem.setVisible(false);
            Log.e("InVisible", "URL");
        }
        else {
            urlitem.setVisible(true);
            Log.e("Visible", "URL");

        }

        if (parsefile == null) {
            pdfitem.setVisible(false);
            Log.e("InVisible", "PDF");
        } else
        {
            pdfitem.setVisible(true);
            Log.e("Visible", "PDF");
        }
        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                if(item.getItemId()==R.id.menu_item_url){
                    ContentTracking contentTracking = new ContentTracking();
                    contentTracking.setContentId(content.getObjectId());
                    contentTracking.setContentType("URL");
                    contentTracking.setUserId(userId);
                    contentTracking.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            //   Log.e("ParseException", e.toString());
                            Log.e("ContentId", content.getObjectId());
                            Log.e("ContentType", "URL");
                            Log.e("UserId", userId);
                        }
                    });
                    actionListener.onWebClicked(content);
                }

                else if(item.getItemId()==R.id.menu_item_pdf){
                    ContentTracking contentTracking = new ContentTracking();
                    contentTracking.setContentId(content.getObjectId());
                    contentTracking.setContentType("PDF");
                    contentTracking.setUserId(userId);
                    contentTracking.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            //   Log.e("ParseException", e.toString());
                            Log.e("ContentId", content.getObjectId());
                            Log.e("ContentType", "PDF");
                            Log.e("UserId", userId);
                        }
                    });
                    actionListener.onPdfClicked(content);
                }

                else if(item.getItemId()==R.id.menu_item_share){

                    actionListener.onShare(content);
                }

                else if(item.getItemId()==R.id.menu_item_delete){

                }
                return true;
            }
        });

        popup.show();//showing popup menu
    }
    public void toggleItem(int position) {
        boolean expanded = !expandedMap.get(position);
        Log.e("expanded", "map: " + expanded);

        // Collapse others if they are expanded
        if (expanded) {
            for (int i = 0; i < getCount(); i++) {
                if (i != position && expandedMap.get(i)) {
                    expandedMap.put(i, false);
                }
            }
        }

        expandedMap.put(position, expanded);
        notifyDataSetChanged();
    }

    private class ViewHolder {
        TextView notificationTitleTextView;
        //Button viewPdfBtn, viewURLBtn;
        ImageButton btn_options;
        LinearLayout mainContent;
        LinearLayout secondaryContent;
        TextView notificationDetail;
    }

}
