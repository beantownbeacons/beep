package com.digitalwavefront.beepetc.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.twitter.BeepTweet;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;

import java.util.List;

/**
 * Created by Wayne on 12/2/15.
 */
public class TweetAdapter extends BaseAdapter {

    private Context context;
    private List<BeepTweet> tweets;
    private LayoutInflater inflater;

    public TweetAdapter(Context context, List<BeepTweet> tweets) {
        this.context = context;
        this.tweets = tweets;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return tweets.size();
    }

    @Override
    public BeepTweet getItem(int position) {
        return tweets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BeepTweet tweet = getItem(position);

        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.tweet_list_item, null);
            holder.userNameTextView = (TextView) convertView.findViewById(R.id.user_name_text_view);
            holder.messageTextView = (TextView) convertView.findViewById(R.id.message_text_view);
            holder.tweetImage = (ImageView) convertView.findViewById(R.id.tweet_image);

            // Set fonts
            Typeface lightTypeface = TypefaceUtils.getLightTypeface(context);
            holder.userNameTextView.setTypeface(lightTypeface);
            holder.messageTextView.setTypeface(lightTypeface);
            convertView.setTag(holder);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.userNameTextView.setText(tweet.getUserName());
        holder.messageTextView.setText(tweet.getMessage());

        if (tweet.getImageUrl() != null) {
            holder.tweetImage.setVisibility(View.VISIBLE);
            Picasso.with(context).load(tweet.getImageUrl()).into(holder.tweetImage);
        } else {
            holder.tweetImage.setVisibility(View.GONE);
            holder.tweetImage.setImageBitmap(null);
        }
        return convertView;
    }

    private class ViewHolder {
        private TextView userNameTextView, messageTextView;
        private ImageView tweetImage;
    }
}
