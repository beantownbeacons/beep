package com.digitalwavefront.beepetc.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.interfaces.BitmapLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SpeakerOptionClickListener;
import com.digitalwavefront.beepetc.parse.models.Speaker;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.views.CircularImageView;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Wayne on 9/21/15.
 */
public class SpeakersAdapter extends BaseAdapter {

    private Context context;
    private List<Speaker> speakers;
    private LayoutInflater inflater;
    private SpeakerOptionClickListener speakerOptionClickListener;
    Activity acitvity;

      // Row Types
    private static final int ROW_TYPE_NORMAL = 0;
    private static final int ROW_TYPE_EXPANDED = 1;
    private static final int ROW_TYPE_COUNT = 2;

    // Expanded map
    private HashMap<Integer, Boolean> expandedMap;

    public SpeakersAdapter(Context context, Activity act, List<Speaker> speakers, SpeakerOptionClickListener speakerOptionClickListener) {
        this.context = context;
        this.speakers = speakers;
        this.inflater = LayoutInflater.from(context);
        this.speakerOptionClickListener = speakerOptionClickListener;
        this.acitvity = act;
        this.expandedMap = new HashMap<>();

        sortSpeakersByName();

        // Initialize the expanded map
        for (int i = 0; i < speakers.size(); i++) {
            expandedMap.put(i, false);
        }
    }

    @Override
    public int getCount() {
        return speakers.size();
    }

    @Override
    public Speaker getItem(int position) {
        return speakers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return ROW_TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        try {
            if (expandedMap.get(position)) {
                return ROW_TYPE_EXPANDED;
            }
        }catch (Exception v){}
        return ROW_TYPE_NORMAL;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Speaker speaker = getItem(position);

        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.speaker_list_item, null);
            holder.speakerPhotoView = (CircularImageView) convertView.findViewById(R.id.speaker_photo_view);
            holder.speakerNameView = (TextView) convertView.findViewById(R.id.speaker_name_view);
            holder.speakerCompanyView = (TextView) convertView.findViewById(R.id.speaker_compnyview);
            holder.speakerAboutView = (TextView) convertView.findViewById(R.id.speaker_about_view);
            holder.mainContent = (LinearLayout) convertView.findViewById(R.id.main_content);
            holder.secondaryContent = (LinearLayout) convertView.findViewById(R.id.secondary_content);
            holder.linkedInBtn = (Button) convertView.findViewById(R.id.linked_in_btn);
            holder.twitterBtn = (Button) convertView.findViewById(R.id.twitter_btn);

            // Set Fonts
            Typeface lightTypeface = TypefaceUtils.getLightTypeface(context);
            Typeface boldTypeface = TypefaceUtils.getSemiBoldTypeface(context);
            holder.speakerNameView.setTypeface(boldTypeface);
            holder.speakerCompanyView.setTypeface(lightTypeface);
            holder.speakerAboutView.setTypeface(lightTypeface);
            convertView.setTag(holder);
        }

        final ViewHolder holder = (ViewHolder) convertView.getTag();
        int viewType = getItemViewType(position);

        holder.speakerNameView.setText(speaker.getFullName());
        holder.speakerCompanyView.setText(speaker.getCompany()+" "+System.getProperty("line.separator")+speaker.getTitle());
        holder.speakerAboutView.setText(speaker.getAbout());

        /*
        speaker.getAvatar(new BitmapLoadedListener() {

            @Override
            public void onBitmapLoaded(Bitmap avatar) {

                if (avatar!=null){

                    holder.speakerPhotoView.setVisibility(View.VISIBLE);
                    holder.speakerPhotoView.setImageBitmap(avatar);
                }else {

                    holder.speakerPhotoView.setImageResource(R.drawable.ic_person);
                }
            }
        });
        */

        //holder.speakerPhotoView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        try {
            ParseFile avatarFile = speaker.getParseFile("avatar");
            if (avatarFile != null) {
                System.out.println("avatarFile.getUrl(): "+avatarFile.getUrl());

                Picasso.with(context).load(avatarFile.getUrl()).into(holder.speakerPhotoView);
            } else {
                holder.speakerPhotoView.setImageResource(R.drawable.ic_person);
            }
        }catch (Exception e) {
            holder.speakerPhotoView.setImageResource(R.drawable.ic_person);
        }

        if (viewType == ROW_TYPE_EXPANDED) {
            holder.secondaryContent.setVisibility(View.VISIBLE);
        } else if(viewType == ROW_TYPE_NORMAL) {
            holder.secondaryContent.setVisibility(View.GONE);
        }
        // Setup linked-in action
        if (!speaker.hasLinkedIn()) {
            holder.linkedInBtn.setVisibility(View.GONE);
        } else {
            holder.linkedInBtn.setVisibility(View.VISIBLE);
            holder.linkedInBtn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    speakerOptionClickListener.onLinkedInClicked(speaker);
                }
            });
        }

        // Setup twitter action
        if (!speaker.hasTwitter()) {
            holder.twitterBtn.setVisibility(View.GONE);
        } else {
            holder.twitterBtn.setVisibility(View.VISIBLE);
            holder.twitterBtn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    speakerOptionClickListener.onTwitterClicked(speaker);
                }
            });
        }

        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        sortSpeakersByName();
        super.notifyDataSetChanged();
    }

    public void toggleItem(int position) {
        boolean expanded = !expandedMap.get(position);

        // Collapse others if they are expanded
        try {
            if (expanded) {
                for (int i = 0; i < getCount(); i++) {
                    if (i != position && expandedMap.get(i)) {
                        expandedMap.put(i, false);
                    }
                }
            }
        }catch (Exception cx){}
        expandedMap.put(position, expanded);
        notifyDataSetChanged();
    }

    private void sortSpeakersByName() {
        // Sort the speakers by their name
        Collections.sort(speakers, new Comparator<Speaker>() {

            @Override
            public int compare(Speaker o1, Speaker o2) {
                return o1.getFullName().compareTo(o2.getFullName());

            }
        });
    }

    private class ViewHolder {
        //RoundImageView speakerPhotoView;
        CircularImageView speakerPhotoView;
        TextView speakerNameView, speakerAboutView, speakerCompanyView;
        LinearLayout mainContent, secondaryContent;
        Button linkedInBtn, twitterBtn;
    }

    public void refreshlist(List<Speaker> speakers)
    {
        this.speakers=speakers;
        this.expandedMap = new HashMap<>();


        // Initialize the expanded map
        for (int i = 0; i < speakers.size(); i++) {
            expandedMap.put(i, false);
        }
        this.notifyDataSetChanged();
    }
}
