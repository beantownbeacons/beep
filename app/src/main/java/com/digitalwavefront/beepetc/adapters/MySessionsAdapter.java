package com.digitalwavefront.beepetc.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Session;
import com.digitalwavefront.beepetc.utils.EmojiconSpan;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.views.CircularImageView;
import com.parse.ParseFile;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Wayne on 10/10/15.
 */
public class MySessionsAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<Session> mySessions;
    private List<Session> filteredSessions;
    private LayoutInflater inflater;
    private Activity activity;

    // Row Types
    private static final int ROW_TYPE_NORMAL = 0;
    private static final int ROW_TYPE_EXPANDED = 1;
    private static final int ROW_TYPE_COUNT = 2;

    // Expanded map
    private HashMap<Integer, Boolean> expandedMap;
    private HashMap<String, HashMap<String, ParseFile>> sessSpkDic;
    private HashMap<String, HashMap<String, String>> sessSpkNameDic;
    private int cntr = 0;
    private HashMap<String, Integer> spkIdDic;

    public MySessionsAdapter(Context context, List<Session> mySessions) {
        this.context = context;
        this.mySessions = mySessions;
        filteredSessions = mySessions;
        this.inflater = LayoutInflater.from(context);

        this.expandedMap = new HashMap<>();

        sortSessionsByStartTime();

        // Initialize the expanded map
        for (int i = 0; i < mySessions.size(); i++) {
            expandedMap.put(i, false);
        }
    }
    public MySessionsAdapter(Context context, Activity act, List<Session> sessions, HashMap<String, HashMap<String, ParseFile>> sessSpkDic, HashMap<String, Integer> spkIdDic, HashMap<String, HashMap<String, String>> sessSpkNameDic) {
        this.context = context;
        this.mySessions = sessions;
        this.filteredSessions = sessions;
        this.inflater = LayoutInflater.from(context);
        this.expandedMap = new HashMap<>();
        this.activity = act;
        this.sessSpkDic = sessSpkDic;
        this.spkIdDic = spkIdDic;
        this.sessSpkNameDic = sessSpkNameDic;

        Log.e("sessSpkDic", "sessSpkDic size:" + sessSpkDic.size());
        sortSessionsByStartTime();

        // Initialize the expanded map
        for (int i = 0; i < sessions.size(); i++) {
            expandedMap.put(i, false);
        }
    }

    @Override
    public int getCount() {
        return filteredSessions.size();
    }

    @Override
    public Session getItem(int position) {
        return filteredSessions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return ROW_TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        if (expandedMap.get(position)) {
            return ROW_TYPE_EXPANDED;
        }
        return ROW_TYPE_NORMAL;
    }

    @Override
    public void notifyDataSetChanged() {
        sortSessionsByStartTime();
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Session session = getItem(position);

        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.my_session_list_item, null);
            holder.mainContent = (LinearLayout) convertView.findViewById(R.id.main_content);
            holder.secondaryContent = (LinearLayout) convertView.findViewById(R.id.secondary_content);
            //holder.startEndTimeView = (TextView) convertView.findViewById(R.id.start_end_time_view);
            holder.start_time_view = (TextView) convertView.findViewById(R.id.start_time_view);
            holder.end_time_view = (TextView) convertView.findViewById(R.id.end_time_view);
            holder.titleView = (TextView) convertView.findViewById(R.id.title_view);
            holder.speakerNameView = (TextView) convertView.findViewById(R.id.speaker_name_view);
            holder.photo_layout = (LinearLayout) convertView.findViewById(R.id.myagenda_photo_layout);
            holder.sessionDetailsView = (TextView) convertView.findViewById(R.id.session_details_view);
            holder.sessionSpeakerPhotoView = (CircularImageView) convertView.findViewById(R.id.session_speaker_photo_view);
            holder.spkName = (TextView) convertView.findViewById(R.id.spkName);

            holder.btnprev = (Button) convertView.findViewById(R.id.btnprev);
            holder.btnext = (Button) convertView.findViewById(R.id.btnnext);
            // Set Fonts
            Typeface lightTypeface = TypefaceUtils.getLightTypeface(context);
            Typeface regularTypeface = TypefaceUtils.getRegularTypeface(context);
            //holder.startEndTimeView.setTypeface(lightTypeface);
            holder.titleView.setTypeface(regularTypeface);
            holder.speakerNameView.setTypeface(lightTypeface);
            holder.sessionDetailsView.setTypeface(lightTypeface);

            // Set stroke color for round image view
      //      holder.sessionSpeakerPhotoView.setBorderColor(ContextCompat.getColor(context, R.color.colorAccent));
            convertView.setTag(holder);
        }

        final ViewHolder holder = (ViewHolder) convertView.getTag();
        int viewType = getItemViewType(position);
        holder.photo_layout.setVisibility(View.INVISIBLE);

        holder.titleView.setText(session.getTitle());
        //holder.startEndTimeView.setText(session.getTimeRangeString());
        holder.start_time_view.setText(session.getFormattedTime("startTime"));
        holder.end_time_view.setText("");
        String strDots = "";
        if (session.getNumOfDots() > 0) {

            for(int i=0; i < session.getNumOfDots(); i++){
                strDots = strDots + "\u26AB";
            }
            SpannableStringBuilder builder = new SpannableStringBuilder(strDots);
            setspannedtext(builder, (int) holder.end_time_view.getTextSize());
            holder.end_time_view.setText(builder);
        } else {
            holder.end_time_view.setText(session.getEndTime1());
        }

        holder.spkName.setText("");
        List<String> spkIds = session.getSpeakerIds();
        ArrayList<String> listflds = new ArrayList<String>();
        listflds.add("firstName");
        listflds.add("lastName");
        /*
        if (spkIds != null && spkIds.size() > 0) {
            if (session.getSpeaker() == null) {
                Speaker.getSpeakerField(spkIds.get(0), listflds, new SpeakerLoadedListener() {
                    @Override
                    public void onSpeakerLoaded(@Nullable Speaker speaker) {

                        if (speaker != null) {
                            String spkname = speaker.getFirstName() + " " + speaker.getLastName();
                            if (session.getRoomName().length() > 0) {
                                spkname = spkname + ", " + session.getRoomName();
                            }

                            session.setSpeaker(speaker);
                            notifyDataSetChanged();

                        }
                    }
                });

            }

        }

        if (session.getSpeaker() != null) {
            String spkname = session.getSpeaker().getFirstName() + " " + session.getSpeaker().getLastName();
            if (session.getRoomName().length() > 0) {
                spkname = spkname + ", " + session.getRoomName();
            }
            holder.spkName.setText(spkname);
        }*/

        try {


            HashMap<String, ParseFile> imgMap = sessSpkDic.get(session.getObjectId());
            HashMap<String, String> nameMap = sessSpkNameDic.get(session.getObjectId());
            cntr = spkIdDic.get(session.getObjectId());

            if (spkIds != null && spkIds.size() > 0) {

                /*Log.e("SpkIdSize", "" + spkIds.size() + " " + session.getTitle());

                String value = String.valueOf(imgMap.get(spkIds.get(cntr)));
                if(value.equals("null")) {
                    holder.sessionSpeakerPhotoView.setImageResource(R.drawable.ic_account_circle);
                }*/

                Log.e("SpkSize", "" + spkIds.size());




                ParseFile pfile = imgMap.get(spkIds.get(cntr));
                String name = nameMap.get(spkIds.get(cntr));

                String value= String.valueOf(pfile);
                if(value.equals("null")) {
                    holder.sessionSpeakerPhotoView.setImageResource(R.drawable.ic_account_circle);
                    holder.speakerNameView.setText(name);
                }
                Log.e("pfile", "url: " + pfile.getUrl() + ", name ====== " + name);
                if (pfile != null) {
                    Log.e("Not Null", "Profile");
                    holder.photo_layout.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(pfile.getUrl()).resize(80, 80).into(holder.sessionSpeakerPhotoView);
                } else {
                    Log.e("Null", "Profile");
                    holder.sessionSpeakerPhotoView.setImageResource(R.drawable.ic_account_circle);
                    /*holder.btnext.setVisibility(View.INVISIBLE);
                    holder.btnprev.setVisibility(View.INVISIBLE);*/
                }

                try {

                    holder.speakerNameView.setText(name);
                    String spkname = session.getSpkNames();
                    if (session.getRoomName().length() > 0) {
                        spkname = spkname + ", " + session.getRoomName();
                    }
                    holder.spkName.setText(spkname);

                } catch (Exception e) {

                }
                if (spkIds.size() <= 1) {
                    Log.e("Invisible", "");
                    holder.btnext.setVisibility(View.INVISIBLE);
                    holder.btnprev.setVisibility(View.INVISIBLE);
                } else {
                    Log.e("Visible", "");
                    holder.btnext.setVisibility(View.VISIBLE);
                    holder.btnprev.setVisibility(View.VISIBLE);
                }

            } else {
                holder.sessionSpeakerPhotoView.setImageBitmap(null);
                holder.photo_layout.setVisibility(View.INVISIBLE);
            }

        } catch (Exception e) {

        }
        //holder.startEndTimeView.setText("\\u2022 Bullet");
        holder.sessionDetailsView.setText(session.getDetails());


        holder.btnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("session", "session title:" + session.getTitle());
                cntr = spkIdDic.get(session.getObjectId());
                HashMap<String, ParseFile> imgMap = sessSpkDic.get(session.getObjectId());
                HashMap<String, String> nameMap = sessSpkNameDic.get(session.getObjectId());

                List<String> spkIds = session.getSpeakerIds();
                cntr++;
                if (cntr >= spkIds.size()) {
                    cntr = 0;
                }

                Log.e("cntr btnext", "cntr: " + cntr + ", " + spkIds.size() + ", " + spkIds);
                ParseFile pfile = imgMap.get(spkIds.get(cntr));
                String name = nameMap.get(spkIds.get(cntr));

                //Log.e("pfile","next url: "+pfile.getUrl());
                if (pfile != null) {
                    //holder.photoLayout.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(pfile.getUrl()).into(holder.sessionSpeakerPhotoView);
                } else {
                    holder.sessionSpeakerPhotoView.setImageResource(R.drawable.ic_account_circle);
                }
                try {

                    holder.speakerNameView.setText(name);

                } catch (Exception e) {

                }

                spkIdDic.put(session.getObjectId(), cntr);
            }
        });
        holder.btnprev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("session", "session title:" + session.getTitle());
                cntr = spkIdDic.get(session.getObjectId());
                HashMap<String, ParseFile> imgMap = sessSpkDic.get(session.getObjectId());
                HashMap<String, String> nameMap = sessSpkNameDic.get(session.getObjectId());

                List<String> spkIds = session.getSpeakerIds();
                cntr--;
                if (cntr < 0) {
                    cntr = spkIds.size() - 1;
                }

                Log.e("cntr btnext", "cntr: " + cntr + ", " + spkIds.size() + ", " + spkIds);
                ParseFile pfile = imgMap.get(spkIds.get(cntr));
                String name = nameMap.get(spkIds.get(cntr));
                //Log.e("pfile","prev url: "+pfile.getUrl());

                if (pfile != null) {
                    //holder.photoLayout.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(pfile.getUrl()).placeholder(R.drawable.ic_account_circle).into(holder.sessionSpeakerPhotoView);
                } else {
                    holder.sessionSpeakerPhotoView.setImageResource(R.drawable.ic_account_circle);
                }
                try {

                    holder.speakerNameView.setText(name);

                } catch (Exception e) {

                }

                spkIdDic.put(session.getObjectId(), cntr);
            }
        });
        /*
        session.getSpeaker(new SpeakerLoadedListener() {

            @Override
            public void onSpeakerLoaded(@Nullable Speaker speaker) {
                if(speaker!=null){
                    if(speaker.getFullName()!=null){
                        holder.speakerNameView.setVisibility(View.VISIBLE);
                        holder.speakerNameView.setText(speaker.getFullName());
                    }else {
                        holder.speakerNameView.setVisibility(View.INVISIBLE);
                    }


                    holder.photo_layout.setVisibility(View.VISIBLE);
                }else {
                        holder.photo_layout.setVisibility(View.INVISIBLE);
                }
            }
        });*/

      /*  // Unregister setup
        holder.unregisterBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                removeSession(session);
                BeConUser.getCurrentUser().unregisterForSession(session, null);
            }
        });*/

        if (viewType == ROW_TYPE_EXPANDED) {
            holder.secondaryContent.setVisibility(View.VISIBLE);
        } else if(viewType == ROW_TYPE_NORMAL) {
            holder.secondaryContent.setVisibility(View.GONE);
        }
        return convertView;
    }

    // Filter
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();

                // If no constraint or empty string return all shopping items
                if (constraint == null || constraint.length() == 0) {
                    results.count = mySessions.size();
                    results.values = mySessions;
                } else {
                    List<Session> filteredSessions = new ArrayList<>();

                    for (int i = 0; i < mySessions.size(); i++) {
                        Session session = mySessions.get(i);
                        if (session.getRoomName()!=null &&  session.getRoomName().equalsIgnoreCase(constraint.toString())) {
                            filteredSessions.add(session);
                        }
                    }

                    results.count = filteredSessions.size();
                    results.values = filteredSessions;
                }

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredSessions = (List<Session>) results.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }

    public void removeSession(Session session) {
        // TODO: Update filter spinner and show empty view
        Log.e("removeSession", "session = " + session);

        Log.e("removeSession","mySessions: "+mySessions+", filteredSessions: "+filteredSessions);
        try {
            if (mySessions != null) {
                mySessions.remove(session);
            }
            if (filteredSessions != null) {
                filteredSessions.remove(session);
            }
            BeConUser.getCurrentUser().unregisterForSession(session, null);
            notifyDataSetChanged();
        }catch (Exception e){

        }
    }

    private void sortSessionsByStartTime() {
        // Sort the sessions by start time
        Collections.sort(filteredSessions, new Comparator<Session>() {

            @Override
            public int compare(Session o1, Session o2) {
                return o1.getStartTime().compareTo(o2.getStartTime());

            }
        });
    }

    public void toggleItem(int position) {
        boolean expanded = !expandedMap.get(position);

        // Collapse others if they are expanded
        if (expanded) {
            for (int i = 0; i < getCount(); i++) {
                if (i != position && expandedMap.get(i)) {
                    expandedMap.put(i, false);
                }
            }
        }

        expandedMap.put(position, expanded);
        notifyDataSetChanged();
    }

    public void filterSessionsByRoomTitle(String roomTitle) {
        collapseAllSessions();
        getFilter().filter(roomTitle);
    }

    public void showAllSessions() {
        collapseAllSessions();
        filteredSessions = mySessions;
        notifyDataSetChanged();
    }

    private void collapseAllSessions() {
        for (int i = 0; i < mySessions.size(); i++) {
            if (expandedMap.get(i)) {
                expandedMap.put(i, false);
            }
        }
    }

    private class ViewHolder {
        TextView startEndTimeView, titleView, speakerNameView, sessionDetailsView,start_time_view,end_time_view,spkName;
        //Button unregisterBtn;
        //RoundImageView sessionSpeakerPhotoView;
        CircularImageView sessionSpeakerPhotoView;
        LinearLayout mainContent, secondaryContent, photo_layout;
        LinearLayout photoLayout;
        Button btnprev, btnext;
    }

    private void setspannedtext(SpannableStringBuilder text, int textsize)
    {
        System.out.println("spanned text "+text);
        int textLength = text.length();
        int resid=0;
        int skip;
        for (int i = 0; i < textLength; i += skip) {
            skip = 0;
            resid = 0;

            if (resid == 0 && i + skip < textLength) {
                int unicode = Character.codePointAt(text, i + skip);
                skip = Character.charCount(unicode);

                resid=R.drawable.circle_1;

            }
            if(resid>0)
            {
                text.setSpan(new EmojiconSpan(context, resid, textsize),i, i+skip, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

        }
    }
}
