package com.digitalwavefront.beepetc.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.SpeakerViewActivity;
import com.digitalwavefront.beepetc.fragments.AgendaFragment;
import com.digitalwavefront.beepetc.interfaces.SessionRSVPChangeListener;
import com.digitalwavefront.beepetc.interfaces.SpeakerLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Session;
import com.digitalwavefront.beepetc.utils.EmojiconSpan;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.views.CircularImageView;
import com.parse.ParseFile;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Wayne on 9/18/15.
 */
public class SessionsAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<Session> sessions;
    private List<Session> filteredSessions;
    private LayoutInflater inflater;
    private SessionRSVPChangeListener rsvpChangeListener;
    private Activity activity;
    private SpeakerLoadedListener speakerLoadedListener;
    private Bitmap speaker_photoview;

    // Row Types
    private static final int ROW_TYPE_NORMAL = 0;
    private static final int ROW_TYPE_EXPANDED = 1;
    private static final int ROW_TYPE_COUNT = 2;

    // Expanded map
    private HashMap<Integer, Boolean> expandedMap;
    private HashMap<String, HashMap<String, ParseFile>> sessSpkDic;
    private HashMap<String, HashMap<String, String>> sessSpkNameDic;
    private HashMap<String, Integer> spkIdDic;

    private int cntr = 0;

    public SessionsAdapter(Context context, Activity act, List<Session> sessions, SessionRSVPChangeListener rsvpChangeListener, HashMap<String, HashMap<String, ParseFile>> sessSpkDic, HashMap<String, Integer> spkIdDic, HashMap<String, HashMap<String, String>> sessSpkNameDic) {
        this.context = context;
        this.sessions = sessions;
        this.rsvpChangeListener = rsvpChangeListener;
        this.filteredSessions = sessions;
        this.inflater = LayoutInflater.from(context);
        this.expandedMap = new HashMap<>();
        this.activity = act;
        this.sessSpkDic = sessSpkDic;
        this.spkIdDic = spkIdDic;
        this.sessSpkNameDic = sessSpkNameDic;

        Log.e("sessSpkDic", "sessSpkDic size:" + sessSpkDic.size());
        sortSessionsByStartTime();

        // Initialize the expanded map
        for (int i = 0; i < sessions.size(); i++) {
            expandedMap.put(i, false);
        }
    }

    @Override
    public int getCount() {
        return filteredSessions.size();
    }

    @Override
    public Session getItem(int position) {
        return filteredSessions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return ROW_TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        if (expandedMap.get(position)) {
            return ROW_TYPE_EXPANDED;
        }
        return ROW_TYPE_NORMAL;
    }

    @Override
    public void notifyDataSetChanged() {
        sortSessionsByStartTime();
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Session session = getItem(position);

        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.session_list_item_bck, null);
            holder.mainContent = (LinearLayout) convertView.findViewById(R.id.main_content);
            holder.secondaryContent = (LinearLayout) convertView.findViewById(R.id.secondary_content);
            //holder.startEndTimeView = (TextView) convertView.findViewById(R.id.start_end_time_view);
            holder.start_time_view = (TextView) convertView.findViewById(R.id.start_time_view);
            holder.end_time_view = (TextView) convertView.findViewById(R.id.end_time_view);
            holder.titleView = (TextView) convertView.findViewById(R.id.title_view);
            holder.photoLayout = (LinearLayout) convertView.findViewById(R.id.session_photo_layout);
            holder.speakerNameView = (TextView) convertView.findViewById(R.id.speaker_name_view);
            holder.sessionDetailsView = (TextView) convertView.findViewById(R.id.session_details_view);
            holder.sessionSpeakerPhotoView = (CircularImageView) convertView.findViewById(R.id.session_speaker_photo_view);
            holder.goingCheckBox = (CheckBox) convertView.findViewById(R.id.going_checkbox);
            holder.spkName = (TextView) convertView.findViewById(R.id.spkName);

            holder.btnprev = (Button) convertView.findViewById(R.id.btnprev);
            holder.btnext = (Button) convertView.findViewById(R.id.btnnext);

            // Set Fonts
            Typeface lightTypeface = TypefaceUtils.getLightTypeface(context);
            Typeface regularTypeface = TypefaceUtils.getRegularTypeface(context);
            //holder.startEndTimeView.setTypeface(lightTypeface);
            //holder.start_time_view.setTypeface(lightTypeface);
            //holder.end_time_view.setTypeface(lightTypeface);
            holder.titleView.setTypeface(regularTypeface);
            holder.speakerNameView.setTypeface(lightTypeface);
            holder.sessionDetailsView.setTypeface(lightTypeface);
            convertView.setTag(holder);
        }

        final ViewHolder holder = (ViewHolder) convertView.getTag();
        int viewType = getItemViewType(position);

        holder.titleView.setText(session.getTitle());
        //holder.startEndTimeView.setText(session.getTimeRangeString());
        holder.start_time_view.setText(session.getFormattedTime("startTime"));

        String strDots = "";
        holder.end_time_view.setText("");
        if (session.getNumOfDots() > 0) {

            for(int i=0; i < session.getNumOfDots(); i++){
                strDots = strDots + "\u26AB";
            }

            SpannableStringBuilder builder = new SpannableStringBuilder(strDots);
            setspannedtext(builder, (int) holder.end_time_view.getTextSize());
            holder.end_time_view.setText(builder);
        } else {
            holder.end_time_view.setText(session.getEndTime1());
        }
        holder.spkName.setText("");
        List<String> spkIds = session.getSpeakerIds();
        ArrayList<String> listflds = new ArrayList<String>();
        listflds.add("firstName");
        listflds.add("lastName");

/*
        if (spkIds != null && spkIds.size() > 0) {
            if (session.getSpeaker() == null) {
                Speaker.getSpeakerField(spkIds.get(0), listflds, new SpeakerLoadedListener() {
                    @Override
                    public void onSpeakerLoaded(@Nullable Speaker speaker) {

                        if (speaker != null) {
                            String spkname = speaker.getFirstName() + " " + speaker.getLastName();
                            if (session.getRoomName().length() > 0) {
                                spkname = spkname + ", " + session.getRoomName();
                            }
                            session.setSpeaker(speaker);
                            notifyDataSetChanged();

                        }
                    }
                });

            }

        }

        if (session.getSpeaker() != null) {
            String spkname = session.getSpeaker().getFirstName() + " " + session.getSpeaker().getLastName();
            if (session.getRoomName().length() > 0) {
                spkname = spkname + ", " + session.getRoomName();
            }
            holder.spkName.setText(spkname);
            try {
                ParseFile pfile = session.getSpeaker().getParseFile("avatar");
                if (pfile != null) {
                    Log.e("Not Null", "Profile");
                    holder.photoLayout.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(pfile.getUrl()).into(holder.sessionSpeakerPhotoView);
                } else {
                    Log.e("Null", "Profile");
                    holder.sessionSpeakerPhotoView.setImageResource(R.drawable.ic_account_circle);

                }
            }catch (Exception cx){}
        }
*/

        //holder.startEndTimeView.setText("\u2022 \n"+session.getEndTime1());
        holder.sessionDetailsView.setText(session.getDetails());


        Log.e("startTime", "" + session.FormattedStartDate());

        //holder.photoLayout.setVisibility(View.INVISIBLE);

        spkIds = session.getSpeakerIds();

        try {


            HashMap<String, ParseFile> imgMap = sessSpkDic.get(session.getObjectId());
            HashMap<String, String> nameMap = sessSpkNameDic.get(session.getObjectId());
            cntr = spkIdDic.get(session.getObjectId());

            if (spkIds != null && spkIds.size() > 0) {



                Log.e("SpkSize", "" + spkIds.size());

                if (spkIds.size() <= 1) {
                    Log.e("Invisible", "");
                    holder.btnext.setVisibility(View.INVISIBLE);
                    holder.btnprev.setVisibility(View.INVISIBLE);
                } else {
                    Log.e("Visible", "");
                    holder.btnext.setVisibility(View.VISIBLE);
                    holder.btnprev.setVisibility(View.VISIBLE);
                }

                ParseFile pfile = imgMap.get(spkIds.get(cntr));
                String name = nameMap.get(spkIds.get(cntr));

                String value= String.valueOf(pfile);
                if(value.equals("null")) {
                    holder.sessionSpeakerPhotoView.setImageResource(R.drawable.ic_account_circle);
                    holder.speakerNameView.setText(name);
                }
                Log.e("pfile", "url: " + pfile.getUrl() + ", name ====== " + name);
                if (pfile != null) {
                    Log.e("Not Null", "Profile");
                    holder.photoLayout.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(pfile.getUrl()).into(holder.sessionSpeakerPhotoView);
                } else {
                    Log.e("Null", "Profile");
                    holder.sessionSpeakerPhotoView.setImageResource(R.drawable.ic_account_circle);

                }

                try {

                    holder.speakerNameView.setText(name);
                    String spkname = session.getSpkNames();
                    if (session.getRoomName().length() > 0) {
                        spkname = spkname + ", " + session.getRoomName();
                    }
                    holder.spkName.setText(spkname);

                } catch (Exception e) {

                }



            } else {
                holder.sessionSpeakerPhotoView.setImageBitmap(null);
                holder.photoLayout.setVisibility(View.INVISIBLE);
            }

        } catch (Exception e) {

        }


        holder.btnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("session", "session title:" + session.getTitle());
                cntr = spkIdDic.get(session.getObjectId());
                HashMap<String, ParseFile> imgMap = sessSpkDic.get(session.getObjectId());
                HashMap<String, String> nameMap = sessSpkNameDic.get(session.getObjectId());

                List<String> spkIds = session.getSpeakerIds();
                cntr++;
                if (cntr >= spkIds.size()) {
                    cntr = 0;
                }

                Log.e("cntr btnext", "cntr: " + cntr + ", " + spkIds.size() + ", " + spkIds);
                ParseFile pfile = imgMap.get(spkIds.get(cntr));
                String name = nameMap.get(spkIds.get(cntr));

                //Log.e("pfile","next url: "+pfile.getUrl());
                if (pfile != null) {
                    //holder.photoLayout.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(pfile.getUrl()).resize(80, 80).into(holder.sessionSpeakerPhotoView);
                } else {
                    holder.sessionSpeakerPhotoView.setImageResource(R.drawable.ic_account_circle);
                }
                try {

                    holder.speakerNameView.setText(name);

                } catch (Exception e) {

                }
                /*
                Bitmap bmp = imgMap.get(spkIds.get(cntr));

                if (bmp != null) {
                    holder.sessionSpeakerPhotoView.setImageBitmap(bmp);
                    holder.photoLayout.setVisibility(View.VISIBLE);
                }else {
                    holder.photoLayout.setVisibility(View.INVISIBLE);
                }*/
                spkIdDic.put(session.getObjectId(), cntr);
            }
        });
        holder.btnprev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("session", "session title:" + session.getTitle());
                cntr = spkIdDic.get(session.getObjectId());
                HashMap<String, ParseFile> imgMap = sessSpkDic.get(session.getObjectId());
                HashMap<String, String> nameMap = sessSpkNameDic.get(session.getObjectId());

                List<String> spkIds = session.getSpeakerIds();
                cntr--;
                if (cntr < 0) {
                    cntr = spkIds.size() - 1;
                }

                Log.e("cntr btnext", "cntr: " + cntr + ", " + spkIds.size() + ", " + spkIds);
                ParseFile pfile = imgMap.get(spkIds.get(cntr));
                String name = nameMap.get(spkIds.get(cntr));
                //Log.e("pfile","prev url: "+pfile.getUrl());

                if (pfile != null) {
                    holder.photoLayout.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(pfile.getUrl()).placeholder(R.drawable.ic_account_circle).resize(80, 80).into(holder.sessionSpeakerPhotoView);
                } else {
                    holder.sessionSpeakerPhotoView.setImageResource(R.drawable.ic_account_circle);
                }
                try {

                    holder.speakerNameView.setText(name);

                } catch (Exception e) {

                }
                /*
                Bitmap bmp = imgMap.get(spkIds.get(cntr));

                if (bmp != null) {
                    holder.sessionSpeakerPhotoView.setImageBitmap(bmp);
                    holder.photoLayout.setVisibility(View.VISIBLE);
                }else {
                    holder.photoLayout.setVisibility(View.INVISIBLE);
                }*/
                spkIdDic.put(session.getObjectId(), cntr);
            }
        });
        /*
            session.getSpeaker(new SpeakerLoadedListener() {

                @Override
                public void onSpeakerLoaded(@Nullable Speaker speaker) {
                    if(speaker!=null){
                        holder.speakerNameView.setText(speaker.getFullName());
                        try {
                            speaker.getAvatar(new BitmapLoadedListener() {

                                @Override
                                public void onBitmapLoaded(Bitmap avatar) {
                                    if (avatar!=null){
                                        holder.sessionSpeakerPhotoView.setImageBitmap(avatar);
                                    }else {
                                        holder.sessionSpeakerPhotoView.setImageResource(R.drawable.ic_person);
                                    }
                                }

                            });
                            holder.photoLayout.setVisibility(View.VISIBLE);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }else {
                        holder.photoLayout.setVisibility(View.INVISIBLE);
                    }
                }
            });
            */
        // Setup RSVP checkbox
        // Setup RSVP checkbox
        BeConUser currentUser = BeConUser.getCurrentUser();
        holder.goingCheckBox.setOnCheckedChangeListener(null);
        holder.goingCheckBox.setChecked(currentUser.isRegisteredForSession(session));
        holder.goingCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                Log.e("SessionAdapter", "isScrolling: " + AgendaFragment.isScrolling);
                if (!AgendaFragment.isScrolling) {
                    Log.e("SessionAdapter", "not isScrolling");
                    boolean isGoing = isChecked;
                    rsvpChangeListener.onRSVPChange(session, isGoing);
                }

            }
        });


        if (viewType == ROW_TYPE_EXPANDED) {
            holder.secondaryContent.setVisibility(View.VISIBLE);
        } else if (viewType == ROW_TYPE_NORMAL) {
            holder.secondaryContent.setVisibility(View.GONE);
        }
        if (holder.secondaryContent.getVisibility() == View.VISIBLE) {

            holder.sessionSpeakerPhotoView.setOnClickListener(
                        /*
                        new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        cntr = spkIdDic.get(session.getObjectId());

                        List<String> spkIds = session.getSpeakerIds();
                        String speakerId = spkIds.get(cntr);
                        if(session.getSpeakerId()!=null && !speakerId.isEmpty()){

                            Intent intent_speakerview = new Intent(context, SpeakerViewActivity.class);
                            intent_speakerview.putExtra("speakerId", speakerId);
                            context.startActivity(intent_speakerview);
                            activity.overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        }

                    }

                }
                */
                    new clicklistener(position)
            );
        }
        return convertView;
    }

    private void sortSessionsByStartTime() {
        // Sort the sessions by start time
        if (filteredSessions != null) {
            Collections.sort(filteredSessions, new Comparator<Session>() {

                @Override
                public int compare(Session o1, Session o2) {
                    if (o1.getStartTime() != null && o2.getStartTime() != null) {
                        return o1.getStartTime().compareTo(o2.getStartTime());
                    }
                    return 0;
                }
            });
        }

    }

    public void toggleItem(int position) {
        boolean expanded = !expandedMap.get(position);
        Log.e("expanded", "map: " + expanded);

        // Collapse others if they are expanded
        if (expanded) {
            for (int i = 0; i < getCount(); i++) {
                if (i != position && expandedMap.get(i)) {
                    expandedMap.put(i, false);
                }
            }
        }

        expandedMap.put(position, expanded);
        notifyDataSetChanged();
    }

    public void filterSessionsByRoomTitle(String roomTitle) {
        collapseAllSessions();
        getFilter().filter(roomTitle);
    }

    public void showAllSessions() {
        collapseAllSessions();
        filteredSessions = sessions;
        notifyDataSetChanged();
    }

    private void collapseAllSessions() {
        for (int i = 0; i < sessions.size(); i++) {
            if (expandedMap.get(i)) {
                expandedMap.put(i, false);
            }
        }
    }

    // Filter
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();

                // If no constraint or empty string return all shopping items
                if (constraint == null || constraint.length() == 0) {
                    Log.e("getfilter", "if......");
                    results.count = sessions.size();
                    results.values = sessions;
                } else {
                    Log.e("getfilter", "else......" + constraint);
                    Log.e("getfilter", "....." + sessions.size());
                    List<Session> filteredSessions1 = new ArrayList<>();

                    for (int i = 0; i < sessions.size(); i++) {
                        Session session = sessions.get(i);
                        Log.e("getfilter", "roomname, constraint....." + session.getRoomName() + " ## " + constraint.toString());
                        if (session.getRoomName() != null && session.getRoomName().equalsIgnoreCase(constraint.toString())) {

                            filteredSessions1.add(session);
                            Log.e("getfilter", "filteredSessions: >>" + filteredSessions1.size());
                        }
                    }

                    Log.e("getfilter", "filteredSessions: >>" + filteredSessions1);

                    results.count = filteredSessions1.size();
                    results.values = filteredSessions1;
                }

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                Log.e("getfilter", "filteredSessions >>....." + results.count);
                try {
                    filteredSessions = (List<Session>) results.values;

                    notifyDataSetChanged();
                } catch (Exception e) {

                }
            }
        };
        return filter;
    }

    private class ViewHolder {
        TextView startEndTimeView, titleView, speakerNameView, sessionDetailsView,start_time_view,end_time_view, spkName; //goingTextView,
        CheckBox goingCheckBox;
        // RoundImageView sessionSpeakerPhotoView;
        CircularImageView sessionSpeakerPhotoView;
        LinearLayout mainContent, secondaryContent;
        LinearLayout photoLayout;
        Button btnprev, btnext;
    }

    private class clicklistener implements View.OnClickListener {
        int pos;

        private clicklistener(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {

            Session session = getItem(pos);
            Log.e("session", "sessid: " + session.getObjectId());
            cntr = spkIdDic.get(session.getObjectId());

            List<String> spkIds = session.getSpeakerIds();
            String speakerId = spkIds.get(cntr);
            Log.e("session", "speakerId: " + speakerId);

            if (speakerId != null && !speakerId.isEmpty()) {

                Intent intent_speakerview = new Intent(context, SpeakerViewActivity.class);
                intent_speakerview.putExtra("speakerId", speakerId);
                context.startActivity(intent_speakerview);
                activity.overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }
        }
    }

    private void setspannedtext(SpannableStringBuilder text, int textsize)
    {
        System.out.println("spanned text "+text);
        int textLength = text.length();
        int resid=0;
        int skip;
        for (int i = 0; i < textLength; i += skip) {
            skip = 0;
            resid = 0;

            if (resid == 0 && i + skip < textLength) {
                int unicode = Character.codePointAt(text, i + skip);
                skip = Character.charCount(unicode);

                resid=R.drawable.circle_1;

            }
            if(resid>0)
            {
                text.setSpan(new EmojiconSpan(context, resid, textsize),i, i+skip, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

        }
    }
}
