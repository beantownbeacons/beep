package com.digitalwavefront.beepetc.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.login.LoginType;
import com.digitalwavefront.beepetc.ui.models.DashboardListItem;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wayne on 10/18/15.
 */
public class DashboardListAdapter extends BaseAdapter {

    // List Positions
    public static final int DASHBOARD_POSITION_BOOTH_METRICS = 0;
    public static final int DASHBOARD_POSITION_SESSION_METRICS = 1;
    public static final int DASHBOARD_POSITION_SPEAKERS_BY_RANK = 2;
    public static final int DASHBOARD_POSITION_EVENT_SNAPSHOT = 3;

    // Row Types
    private static final int ROW_TYPE_ICON = 0;
    private static final int ROW_TYPE_NO_ICON = 1;
    private static final int ROW_TYPE_COUNT = 2;

    private Context context;
    private List<DashboardListItem> dashboardListItems;
    private LayoutInflater inflater;
    private int loginType;

    public DashboardListAdapter(Context context, int loginType) {
        this.context = context;
        this.loginType = loginType;
        initDashboardItems();
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return dashboardListItems.size();
    }

    @Override
    public DashboardListItem getItem(int position) {
        return dashboardListItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DashboardListItem item = getItem(position);

        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.dashboard_list_item, null);
            holder.iconView = (ImageView) convertView.findViewById(R.id.icon_view);
            holder.titleTextView = (TextView) convertView.findViewById(R.id.title_text_view);

            // Set fonts
            Typeface lightTypeface = TypefaceUtils.getLightTypeface(context);
            holder.titleTextView.setTypeface(lightTypeface);
            convertView.setTag(holder);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();

        if (getItemViewType(position) == ROW_TYPE_ICON) {
            holder.iconView.setVisibility(View.VISIBLE);
            holder.iconView.setImageResource(item.getIconResourceId());
        } else {
            holder.iconView.setVisibility(View.GONE);
        }
        holder.titleTextView.setText(item.getTitle());
        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        DashboardListItem item = getItem(position);
        if (item.hasIcon()) {
            return ROW_TYPE_ICON;
        }
        return ROW_TYPE_NO_ICON;
    }

    @Override
    public int getViewTypeCount() {
        return ROW_TYPE_COUNT;
    }

    private void initDashboardItems() {
        dashboardListItems = new ArrayList<>();
        Resources res = context.getResources();

        // Get the values for the drawer items from the resources
        String[] dashboardTitles = res.getStringArray(R.array.dashboard_titles);
        TypedArray dashboardIcons = res.obtainTypedArray(R.array.dashboard_icons);

        // Add booth metrics because it is always present
        addBoothMetrics(dashboardTitles, dashboardIcons);

        // Add the others if this is not an exhibitor login
        if (loginType != LoginType.EXHIBITOR) {
            for (int i = 1; i < dashboardTitles.length; i++) {
                String title = dashboardTitles[i];

                int icon = dashboardIcons.getResourceId(i, DashboardListItem.NO_ICON);
                addDashboardItem(title, icon);
            }
        }
    }

    private void addBoothMetrics(String[] dashboardTitles, TypedArray dashboardIcons) {
        String boothMetricsTitle = dashboardTitles[DASHBOARD_POSITION_BOOTH_METRICS];
        int boothMetricsIcon = dashboardIcons.getResourceId(DASHBOARD_POSITION_BOOTH_METRICS,
                DashboardListItem.NO_ICON);
        addDashboardItem(boothMetricsTitle, boothMetricsIcon);
    }

    private void addDashboardItem(String title, int icon) {
        DashboardListItem item = new DashboardListItem(title, icon);
        dashboardListItems.add(item);
    }

    private class ViewHolder {
        ImageView iconView;
        TextView titleTextView;
    }
}
