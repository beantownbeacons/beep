package com.digitalwavefront.beepetc.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitalwavefront.beepetc.BeConApp;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.parse.models.Sponsor;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.views.CircularImageView;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Tarul on 06/10/2016.
 */
public class SponsorsAdapter extends BaseAdapter {

    private Context context;
    private Activity activity;
    private List<Sponsor> sponsors;
    private LayoutInflater inflater;
    private HashMap<Integer, Boolean> expandedMap;

    // Row Types
    private static final int ROW_TYPE_NORMAL = 0;
    private static final int ROW_TYPE_EXPANDED = 1;
    private static final int ROW_TYPE_COUNT = 2;

    public SponsorsAdapter(Context context, Activity activity, List<Sponsor> sponsors) {
        this.context = context;
        this.activity = activity;
        this.sponsors = sponsors;
        this.inflater = LayoutInflater.from(context);
        this.expandedMap = new HashMap<>();

        for (int i = 0; i < sponsors.size(); i++) {
            expandedMap.put(i, false);
        }
    }

    @Override
    public int getCount() {
        return sponsors.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public ParseObject getItem(int position) {
        return sponsors.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (expandedMap.get(position)) {
            return ROW_TYPE_EXPANDED;
        }
        return ROW_TYPE_NORMAL;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ParseObject sponsor = getItem(position);

        final Sponsor sponsorObj = (Sponsor) getItem(position);

        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.sponsor_list_item, null);
            holder.mainContent = (LinearLayout) convertView.findViewById(R.id.main_layout);
            holder.secondaryContent = (RelativeLayout) convertView.findViewById(R.id.secondary_layout);

            holder.titleView = (TextView) convertView.findViewById(R.id.sponsor_name);
            holder.photoLayout = (CircularImageView) convertView.findViewById(R.id.sponsor_photo_view);
            holder.imgurl = (ImageView) convertView.findViewById(R.id.img_url);
            holder.txt_desc = (TextView) convertView.findViewById(R.id.txt_desc);
            holder.level = (TextView) convertView.findViewById(R.id.level);

            // Set Fonts
            Typeface regularTypeface = TypefaceUtils.getRegularTypeface(context);

            holder.titleView.setTypeface(regularTypeface);
            holder.txt_desc.setTypeface(regularTypeface);

            convertView.setTag(holder);
        }

        final ViewHolder holder = (ViewHolder) convertView.getTag();
        int viewType = getItemViewType(position);

        Log.e("sponsor: ", "sponsor name: " + sponsor.getString("name"));
        holder.titleView.setText(sponsor.getString("name"));
        holder.level.setText(sponsorObj.getLevel());

        ParseFile logo = sponsor.getParseFile("logo");
        //Log.e("sponsor: ", "logo" + logo);
        if (logo != null) {

            Picasso.with(context).load(logo.getUrl()).into(holder.photoLayout);

        } else {
            holder.photoLayout.setImageResource(R.drawable.ic_profile);
        }

        String description = sponsor.getString("description");
        if (description != null) {
            holder.txt_desc.setText(description);
        }

        final String url = sponsor.getString("website");
        if (url == null) {
            holder.imgurl.setVisibility(View.INVISIBLE);
        } else if (url.equals("")) {
            holder.imgurl.setVisibility(View.INVISIBLE);
        } else {
            holder.imgurl.setVisibility(View.VISIBLE);
            holder.imgurl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BeConApp) context.getApplicationContext()).openWebPage(activity, url);
                }
            });
        }


        if (viewType == ROW_TYPE_EXPANDED) {
            holder.secondaryContent.setVisibility(View.VISIBLE);
        } else if (viewType == ROW_TYPE_NORMAL) {
            holder.secondaryContent.setVisibility(View.GONE);
        }


        return convertView;
    }

    public void toggleItem(int position) {
        boolean expanded = !expandedMap.get(position);
        Log.e("expanded", "map: " + expanded);

        // Collapse others if they are expanded
        if (expanded) {
            for (int i = 0; i < getCount(); i++) {
                if (i != position && expandedMap.get(i)) {
                    expandedMap.put(i, false);
                }
            }
        }

        expandedMap.put(position, expanded);
        notifyDataSetChanged();
    }

    private class ViewHolder {
        LinearLayout mainContent;
        RelativeLayout secondaryContent;
        CircularImageView photoLayout;
        ImageView imgurl;
        TextView titleView, txt_desc, level;
    }

    public void refreshlist(List<Sponsor> spons)
    {
        this.sponsors=spons;
        this.notifyDataSetChanged();
    }
}
