package com.digitalwavefront.beepetc.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.interfaces.BitmapLoadedListener;
import com.digitalwavefront.beepetc.interfaces.FloatLoadedCallback;
import com.digitalwavefront.beepetc.interfaces.IntegerLoadedCallback;
import com.digitalwavefront.beepetc.parse.models.Speaker;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.views.RoundImageView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Wayne on 10/18/15.
 */
public class SpeakerRankAdapter extends BaseAdapter {

    private Context context;
    private List<Speaker> speakers;
    private LayoutInflater inflater;

    public SpeakerRankAdapter(Context context, List<Speaker> speakers) {
        this.context = context;
        this.speakers = speakers;
        this.inflater = LayoutInflater.from(context);
        calculateAverageScores();
    }

    @Override
    public int getCount() {
        return speakers.size();
    }

    @Override
    public Speaker getItem(int position) {
        return speakers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Speaker speaker = getItem(position);

        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.speaker_rank_list_item, null);
            holder.speakerPhotoView = (RoundImageView) convertView.findViewById(R.id.speaker_photo_view);
            holder.speakerPhotoView.setBorderColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.speakerNameView = (TextView) convertView.findViewById(R.id.speaker_name_view);
            holder.attendeesSurveysTextView = (TextView) convertView.findViewById(R.id.attendees_surveys_text_view);
            holder.scoreTextView = (TextView) convertView.findViewById(R.id.score_text_view);

            // Set Fonts
            Typeface boldTypeface = TypefaceUtils.getBoldTypeface(context);
            Typeface lightTypeface = TypefaceUtils.getLightTypeface(context);

            holder.speakerNameView.setTypeface(lightTypeface);
            holder.attendeesSurveysTextView.setTypeface(lightTypeface);
            holder.scoreTextView.setTypeface(boldTypeface);
            convertView.setTag(holder);
        }

        final ViewHolder holder = (ViewHolder) convertView.getTag();
        speaker.getAvatar(new BitmapLoadedListener() {

            @Override
            public void onBitmapLoaded(Bitmap bitmap) {
                if (bitmap != null) {
                    holder.speakerPhotoView.setImageBitmap(bitmap);
                }
            }
        });

        holder.speakerNameView.setText(speaker.getFullName());
        speaker.getAttendeeCount(new IntegerLoadedCallback() {

            @Override
            public void onIntegerLoaded(final int attendeeCount) {
                // TODO: Optimize
                speaker.getSurveyCompleteCount(new IntegerLoadedCallback() {

                    @Override
                    public void onIntegerLoaded(int surveyCompleteCount) {
                        String attendeeSurveyString = String.format("%d attendee(s)%n%d survey(s) completed",
                                attendeeCount, surveyCompleteCount);
                        holder.attendeesSurveysTextView.setText(attendeeSurveyString);
                    }
                });
            }
        });

        float averageScore = 0f;
        if (speaker.hasCalculatedAverageScore()) {
            averageScore = speaker.getAverageScore();
        }
        holder.scoreTextView.setText(String.format("%.2f", averageScore));
        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        // Sort the items
        Collections.sort(speakers, new Comparator<Speaker>() {
            public int compare(Speaker speaker1, Speaker speaker2) {
                return Float.compare(speaker2.getAverageScore(), speaker1.getAverageScore());
            }
        });
        super.notifyDataSetChanged();
    }

    private void calculateAverageScores() {
        for (Speaker speaker : speakers) {
            speaker.calculateAverageScore(new FloatLoadedCallback() {
                @Override
                public void onFloatLoaded(float value) {
                    notifyDataSetChanged();
                }
            });
        }
    }

    private class ViewHolder {
        RoundImageView speakerPhotoView;
        TextView speakerNameView, attendeesSurveysTextView, scoreTextView;
    }
}
