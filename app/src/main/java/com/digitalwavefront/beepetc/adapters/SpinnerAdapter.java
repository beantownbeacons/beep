package com.digitalwavefront.beepetc.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;

import java.util.List;

/**
 * Created by Wayne on 10/9/15.
 */
public class SpinnerAdapter extends BaseAdapter {

    private List<String> titles;
    private Context context;
    private LayoutInflater inflater;

    public SpinnerAdapter(Context context, List<String> titles) {
        this.titles = titles;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return titles.size();
    }

    @Override
    public String getItem(int position) {
        return titles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String title = getItem(position);
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.spinner_item, null);
            holder.titleTextView = (TextView) convertView.findViewById(R.id.title_text_view);

            // Set fonts
            Typeface lightTypeface = TypefaceUtils.getLightTypeface(context);
            holder.titleTextView.setTypeface(lightTypeface);

            // Set holder as tag
            convertView.setTag(holder);
        }

        holder = (ViewHolder) convertView.getTag();
        holder.titleTextView.setText(title);
        return convertView;
    }

    private class ViewHolder {
        TextView titleTextView;
    }
}
