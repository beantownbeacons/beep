package com.digitalwavefront.beepetc.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.parse.models.UserPreferences;

import java.util.List;

/**
 * Created by User on 1/10/18.
 */

public class PreferencesAdapter extends BaseAdapter {

    private Context contxt;
    private List<UserPreferences> preferencesList;
    private LayoutInflater inflater;

    private class ViewHolder {

        private TextView title;
        private Switch prefval;
    }

    @Override
    public int getCount() {
        return preferencesList.size();
    }

    @Override
    public UserPreferences getItem(int i) {
        return preferencesList.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.pref_list_item, null);

            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.prefval = (Switch) convertView.findViewById(R.id.prefval);

            convertView.setTag(holder);
        }

        final ViewHolder holder = (ViewHolder) convertView.getTag();

        UserPreferences pref = getItem(i);
        holder.title.setText(pref.getTitle());
        holder.prefval.setChecked(pref.getPrefVal(pref.getPrefTitle()));
        holder.prefval.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            }
        });
        return convertView;
    }
}
