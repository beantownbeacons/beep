package com.digitalwavefront.beepetc.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.interfaces.SavedNotificationActionListener;
import com.digitalwavefront.beepetc.parse.models.BeConNotification;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.ContentTracking;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.SaveCallback;

import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Wayne on 10/11/15.
 */
public class SavedNotificationsAdapter extends BaseAdapter {

    private Context context;
    private List<BeConNotification> notifications;
    private SavedNotificationActionListener actionListener;
    private LayoutInflater inflater;
    public static BeConNotification note;

    private static final int ROW_TYPE_NORMAL = 0;
    private static final int ROW_TYPE_EXPANDED = 1;
    private static final int ROW_TYPE_COUNT = 2;

    BeConUser currentUser = BeConUser.getCurrentUser();
    String userId=currentUser.getObjectId();

    private HashMap<Integer, Boolean> expandedMap;

    private Boolean flagdismiss = false;

    public SavedNotificationsAdapter(Context context, List<BeConNotification> notifications, SavedNotificationActionListener actionListener) {
        this.context = context;
        this.notifications = notifications;
        this.actionListener = actionListener;
        inflater = LayoutInflater.from(context);
        this.expandedMap=new HashMap<>();

        // Initialize the expanded map
        for (int i = 0; i < notifications.size(); i++) {
            expandedMap.put(i, false);
        }
    }
    public SavedNotificationsAdapter(Context context, List<BeConNotification> notifications, boolean flagDismiss, SavedNotificationActionListener actionListener) {
        this.context = context;
        this.notifications = notifications;
        this.actionListener = actionListener;
        inflater = LayoutInflater.from(context);
        this.expandedMap=new HashMap<>();
        this.flagdismiss = flagDismiss;

        // Initialize the expanded map
        for (int i = 0; i < notifications.size(); i++) {
            expandedMap.put(i, false);
        }
    }

    @Override
    public int getCount() {
        return notifications.size();
    }

    @Override
    public BeConNotification getItem(int position) {
        return notifications.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (expandedMap.get(position)) {
            return ROW_TYPE_EXPANDED;
        }
        return ROW_TYPE_NORMAL;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final BeConNotification notification = getItem(position);
        note = notification;
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.saved_notification_list_item, null);
            holder.mainContent=(LinearLayout)convertView.findViewById(R.id.main_content);
            holder.notificationTitleTextView = (TextView) convertView.findViewById(R.id.notification_title_text_view);
            /*
            holder.viewPdfBtn = (Button) convertView.findViewById(R.id.view_pdf_btn);
            holder.viewURLBtn = (Button) convertView.findViewById(R.id.view_url_btn);
            */
            holder.btn_options = (ImageButton) convertView.findViewById(R.id.btn_options);
            holder.btn_options.setVisibility(View.VISIBLE);
            holder.secondaryContent=(LinearLayout)convertView.findViewById(R.id.secondary_content);
            holder.notificationDetail=(TextView)convertView.findViewById(R.id.notification_detail);
            //holder.deleteBtn = (Button) convertView.findViewById(R.id.delete_btn);

            // Set fonts
            Typeface lightTypeface = TypefaceUtils.getLightTypeface(context);
            holder.notificationTitleTextView.setTypeface(lightTypeface);
            holder.notificationDetail.setTypeface(lightTypeface);

            convertView.setTag(holder);
        }

        holder = (ViewHolder) convertView.getTag();
        int viewType = getItemViewType(position);
        holder.notificationTitleTextView.setText(notification.getTitle());
        holder.notificationDetail.setText(notification.getMessage());
        Log.e("praneet",notification.getTitle());

        String pdfpath = notification.getPdfFilePath(context);
        File pdfFile = new File(pdfpath);
/*
        ParseFile parsefile=notification.getPdfFile();

        if (parsefile==null) {
            holder.viewPdfBtn.setVisibility(View.INVISIBLE);
            holder.viewPdfBtn.setOnClickListener(null);
        }else {
            holder.viewPdfBtn.setVisibility(View.VISIBLE);
            holder.viewPdfBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NotificationTracking notificationTracking=new NotificationTracking();
                    notificationTracking.setNotificationId(notification.getObjectId());
                    notificationTracking.setNotificationType("PDF");
                    notificationTracking.setUserId(userId);
                    notificationTracking.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                         //   Log.e("ParseException", e.toString());
                            Log.e("NotificationId", notification.getObjectId());
                            Log.e("NotificationType", "PDF");
                            Log.e("UserId", userId);
                        }
                    });
                    actionListener.onPdfClicked(notification);
                }
            });
        }

        String url = notification.getUrl();

        if(url != null){
            holder.viewURLBtn.setVisibility(View.VISIBLE);
            holder.viewURLBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NotificationTracking notificationTracking=new NotificationTracking();
                    notificationTracking.setNotificationId(notification.getObjectId());
                    notificationTracking.setNotificationType("URL");
                    notificationTracking.setUserId(userId);
                    notificationTracking.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            //   Log.e("ParseException", e.toString());
                            Log.e("NotificationId", notification.getObjectId());
                            Log.e("NotificationType", "URL");
                            Log.e("UserId", userId);
                        }
                    });
                    actionListener.onWebClicked(notification);
                }
            });
        }else {
            holder.viewURLBtn.setVisibility(View.INVISIBLE);
            holder.viewURLBtn.setOnClickListener(null);
        }
*/
        if (viewType == ROW_TYPE_EXPANDED) {
            holder.secondaryContent.setVisibility(View.VISIBLE);
        } else if(viewType == ROW_TYPE_NORMAL) {
            holder.secondaryContent.setVisibility(View.GONE);
        }



      /*  holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionListener.onDeleteClicked(notification);
            }
        });*/

        holder.btn_options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showmenu(v,notification);
            }
        });

        return convertView;
    }

    public void toggleItem(int position) {
        boolean expanded = !expandedMap.get(position);
        Log.e("expanded","map: "+expanded);

        // Collapse others if they are expanded
        if (expanded) {
            for (int i = 0; i < getCount(); i++) {
                if (i != position && expandedMap.get(i)) {
                    expandedMap.put(i, false);
                }
            }
        }

        expandedMap.put(position, expanded);
        notifyDataSetChanged();
    }

/*
    public void showAllSessions() {
        collapseAllSessions();
        filteredexhibitors = exhibitors;
        notifyDataSetChanged();
    }

    private void collapseAllSessions() {
        for (int i = 0; i < exhibitors.size(); i++) {
            if (expandedMap.get(i)) {
                expandedMap.put(i, false);
            }
        }
    }*/

    public void deleteNotification(BeConNotification notification) {
        notifications.remove(notification);
        notifyDataSetChanged();
    }

    private void showmenu(View view,final BeConNotification notification)
    {
        //actionListener.onShare(content);
        PopupMenu popup = new PopupMenu(context, view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.content_options_menu, popup.getMenu());
        MenuItem urlitem=(MenuItem)popup.getMenu().findItem(R.id.menu_item_url);
        MenuItem pdfitem=(MenuItem)popup.getMenu().findItem(R.id.menu_item_pdf);
        MenuItem shareitem=(MenuItem)popup.getMenu().findItem(R.id.menu_item_share);
        MenuItem deleteitem=(MenuItem)popup.getMenu().findItem(R.id.menu_item_delete);
        MenuItem saveitem=(MenuItem)popup.getMenu().findItem(R.id.menu_item_save);

        deleteitem.setVisible(false);
        saveitem.setVisible(false);
        shareitem.setVisible(false);



        String pdfpath = notification.getPdfFilePath(context);
        File pdfFile = new File(pdfpath);
        ParseFile parsefile = notification.getPdfFile();


        String url = notification.getUrl();
        Log.e("URL", "U"+notification.getUrl()+"rl");

        if (url == null || url.equals("")) {
            urlitem.setVisible(false);
            Log.e("InVisible", "URL");
        }
        else {
            urlitem.setVisible(true);
            Log.e("Visible", "URL");

        }

        if (parsefile == null) {
            pdfitem.setVisible(false);
            Log.e("InVisible", "PDF");
        } else
        {
            pdfitem.setVisible(true);
            Log.e("Visible", "PDF");
        }
        if (flagdismiss)
        {
            pdfitem.setVisible(false);
            urlitem.setVisible(false);
            saveitem.setVisible(true);

        } else {
            shareitem.setVisible(true);
            deleteitem.setVisible(true);
        }
        //registering popup with OnMenuItemClickListener

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                if(item.getItemId()==R.id.menu_item_url){
                    ContentTracking contentTracking = new ContentTracking();
                    contentTracking.setContentId(notification.getObjectId());
                    contentTracking.setContentType("URL");
                    contentTracking.setUserId(userId);
                    contentTracking.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            //   Log.e("ParseException", e.toString());
                            Log.e("ContentId", notification.getObjectId());
                            Log.e("ContentType", "URL");
                            Log.e("UserId", userId);
                        }
                    });
                    actionListener.onWebClicked(notification);
                }

                else if(item.getItemId()==R.id.menu_item_pdf){
                    ContentTracking contentTracking = new ContentTracking();
                    contentTracking.setContentId(notification.getObjectId());
                    contentTracking.setContentType("PDF");
                    contentTracking.setUserId(userId);
                    contentTracking.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            //   Log.e("ParseException", e.toString());
                            Log.e("ContentId", notification.getObjectId());
                            Log.e("ContentType", "PDF");
                            Log.e("UserId", userId);
                        }
                    });
                    actionListener.onPdfClicked(notification);
                }

                else if(item.getItemId()==R.id.menu_item_share){

                    actionListener.onShare(notification);
                }

                else if(item.getItemId()==R.id.menu_item_delete){

                    actionListener.onDeleteClicked(notification);
                }

                else if(item.getItemId()==R.id.menu_item_save){

                    actionListener.onSaveClicked(notification);
                }
                return true;
            }
        });

        popup.show();//showing popup menu
    }
    private class ViewHolder {
        TextView notificationTitleTextView;
        //Button viewPdfBtn, viewURLBtn;//, deleteBtn;
        ImageButton btn_options;
        LinearLayout mainContent;
        LinearLayout secondaryContent;
        TextView notificationDetail;
    }
}
