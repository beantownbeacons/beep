package com.digitalwavefront.beepetc.activities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by user on 27-Apr-16.
 */
public class beep_pref {

    public static final String PREFS_NAME = "AOP_PREFS";
    SharedPreferences  settings;
    Context context;
    SharedPreferences.Editor editor;
    public beep_pref(Context context) {
        this.context = context;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor=settings.edit();

    }
    public void setFlag(boolean flag){
     editor.putBoolean("flag",false);
        editor.commit();
    }
    public boolean getFlag(){
        boolean flag=settings.getBoolean("flag",false);
        return flag;
    }


}
