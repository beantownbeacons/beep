package com.digitalwavefront.beepetc.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import com.crashlytics.android.Crashlytics;

import com.crashlytics.android.core.CrashlyticsCore;

import com.digitalwavefront.beepetc.BeConApp;
import com.digitalwavefront.beepetc.ConnectionDetector;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.SharedPref;
import com.digitalwavefront.beepetc.fragments.CodeVerificationFragment;
import com.digitalwavefront.beepetc.fragments.EventLoginFragment;
import com.digitalwavefront.beepetc.fragments.ExhibitorLoginFragment;
import com.digitalwavefront.beepetc.fragments.ExhibitorsFragment;
import com.digitalwavefront.beepetc.fragments.ForgotPasswordFragment;
import com.digitalwavefront.beepetc.fragments.LocationAccessFragment;
import com.digitalwavefront.beepetc.fragments.LoginFragment;
import com.digitalwavefront.beepetc.fragments.OrganizerLoginFragment;
import com.digitalwavefront.beepetc.fragments.RegistrationFragment;
import com.digitalwavefront.beepetc.fragments.ResetPasswordFragment;
import com.digitalwavefront.beepetc.fragments.VerifyCodeFragment;
import com.digitalwavefront.beepetc.interfaces.EventLoadedListener;
import com.digitalwavefront.beepetc.interfaces.NavConfLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SignInListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Event;
import com.digitalwavefront.beepetc.parse.models.EventAccessLog;
import com.digitalwavefront.beepetc.parse.models.NavConfigurator;
import com.digitalwavefront.beepetc.services.GcmRegisteringService;
import com.digitalwavefront.beepetc.utils.VersionUtils;
import com.digitalwavefront.beepetc.utils.ViewUtils;
import com.facebook.CallbackManager;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.SaveCallback;

import java.util.HashMap;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Wayne on 9/15/15.
 */
public class LoginActivity extends AppCompatActivity implements ExhibitorsFragment.OnFragmentInteractionListener {
    private static final String TAG = "LoginActivity";
    private ProgressDialog progressDialog;
    CallbackManager callbackManager;
    ConnectionDetector detector;
    private boolean isInternetPresent;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig)); //this is Application*/
        Fabric.with(this, new Crashlytics());
        /*final Fabric fabric = new Fabric.Builder(this)

        /*TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig)); //this is Application*/
        //Fabric.with(this, new Crashlytics());
        Fabric.with(this, new CrashlyticsCore(), new Crashlytics());
        /*final Fabric fabric = new Fabric.Builder(this)

                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);*/

        progressDialog = new ProgressDialog(this);
        setContentView(R.layout.activity_login);
        View loginRoot = findViewById(R.id.login_root);


        /*boolean fabricInitialized = Fabric.isInitialized();
        if (fabricInitialized) {
            Log.e("Initiated", "");
        }*/


        boolean fabricInitialized = Fabric.isInitialized();
        if (fabricInitialized) {
            Log.e("Initiated", "");
        }

        if (savedInstanceState == null) {
            ViewUtils.setBackgroundImageWithTextProtection(getResources(), loginRoot, R.drawable.bg_login);

            if (VersionUtils.isLollipopOrAbove()) {
                setBackgroundToFullscreen(loginRoot);
            }

            showDefaultFragment();
        }


        /*Bundle bundle = getIntent().getExtras();
        String object = bundle.getString(RegistrationFragment.OBJECT);
        Log.d(TAG, "object : : " + object);

        if (object.equals("subject")) {
            showRegistrationFragment();
        }
*/


       /* try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        }
        catch (PackageManager.NameNotFoundException e) {

        }
        catch (NoSuchAlgorithmException e) {

        }*/

        Intent i = new Intent(this, GcmRegisteringService.class); startService(i);

    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    public void clearBackStack() {
        FragmentManager fm = getFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    private void setBackgroundToFullscreen(View rootView) {
        int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
        getWindow().getDecorView().setSystemUiVisibility(flags);

        // Pad the top to make up for the status bar
        int statusBarHeight = getResources().getDimensionPixelOffset(R.dimen.status_bar_height);
        rootView.setPadding(0, statusBarHeight, 0, 0);
    }

    private void showDefaultFragment() {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        if (BeConUser.getCurrentUser() == null) {
            //fragmentTransaction.add(R.id.fragment_container, RegistrationFragment.newInstance());
            fragmentTransaction.add(R.id.fragment_container, LoginFragment.newInstance());
        } else {
            final BeConUser currentUser = BeConUser.getCurrentUser();

            if (!currentUser.isVerified()) {
                showVerificationCodeFragment(false);
            } else if (!currentUser.isSignedIntoEvent()) {
                showEventLoginFragment();
            } else {
                resumeEventSignIn(currentUser);
            }
        }
        fragmentTransaction.commit();
    }

    private void resumeEventSignIn(BeConUser currentUser) {
        showProgressDialog(getString(R.string.one_moment_please), getString(R.string.signing_in));
        final String eventId = currentUser.getCurrentEventId();
        final int loginType = currentUser.getLoginType(this);

        if(checkConnection()) {
            Event.getEventByObjectId(eventId, new EventLoadedListener() {

                @Override
                public void onEventLoaded(@Nullable Event event) {
                    hideProgressDialog();
                    if(event == null){
                        Toast.makeText(LoginActivity.this, R.string.error_signinEvent, Toast.LENGTH_SHORT).show();

                    }else {
                        ((BeConApp) getApplication()).onEventSignIn(event, loginType);
                    }

                }
            });
        } else {
            hideProgressDialog();
            Toast.makeText(LoginActivity.this, R.string.no_network, Toast.LENGTH_SHORT).show();
        }

    }

    public void showVerificationCodeFragment(boolean sendVerificationCode) {
        if (sendVerificationCode) {
            sendVerificationCode();
        }
        clearBackStack();
        performFragmentTransaction(VerifyCodeFragment.newInstance(), false);
    }

    public void showEventLoginFragment() {
        clearBackStack();
        performFragmentTransaction(EventLoginFragment.newInstance(), false);
    }

    public void showLocationAccessFragment() {
        clearBackStack();
        performFragmentTransaction(LocationAccessFragment.newInstance(), false);
    }

    public void showOrganizerLoginFragment() {
        performFragmentTransaction(OrganizerLoginFragment.newInstance(), true);
    }

    public void showExhibitorLoginFragment() {
        performFragmentTransaction(ExhibitorLoginFragment.newInstance(), true);
    }

    public void showRegistrationFragment() {
        performFragmentTransaction(RegistrationFragment.newInstance(), false);
    }

    public void showLoginFragment() {
        performFragmentTransaction(LoginFragment.newInstance(), true);
    }

    public void showForgotPasswordFragment() {
        performFragmentTransaction(ForgotPasswordFragment.newInstance(), true);
    }

    public void showVerificationFragment() {


        performFragmentTransaction(CodeVerificationFragment.newInstance(), true);
    }

    public void showResetFragment() {
        performFragmentTransaction(ResetPasswordFragment.newInstance(), true);
    }

    private void performFragmentTransaction(Fragment fragment, boolean addToBackStack) {

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }

        transaction.commit();
    }

    public void signIntoEvent(final BeConUser user, final Event event, final int loginType,
                              final SignInListener listener) {
        user.setCurrentEvent(event);
        user.saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("onEventSignIn: ", "onEventSignIn");

                    // NavConfigurator
                    NavConfigurator.getNavConfiguratorByEventId(event.getObjectId(), new NavConfLoadedListener() {
                        @Override
                        public void onNavConfLoaded(@Nullable NavConfigurator navConf) {

                            if (navConf != null) {

                                SharedPref pref = new SharedPref(getApplicationContext());
                                //pref.setValKey("exhibitors",navConf.geNavExhibitors);
                                pref.setValKey("contentlib",navConf.getNavContentLibrary());
                                pref.setValKey("myContent",navConf.getNavMyContent());
                                pref.setValKey("survey",navConf.geNavSurvey());
                                pref.setValKey("sponsors",navConf.geNavSponsors());
                                pref.setValKey("gallery",navConf.getNavGallery());
                                pref.setValKey("exhibitors",navConf.geNavExhibitors());
                                pref.setValKey("newcontentlib",navConf.getNewcontentlibrary());
                                pref.setValKey("attendees",navConf.geNavAttendees());
                            }else {
                                SharedPref pref = new SharedPref(getApplicationContext());
                                //pref.setValKey("exhibitors",navConf.geNavExhibitors);
                                pref.setValKey("contentlib",true);
                                pref.setValKey("myContent",true);
                                pref.setValKey("survey",true);
                                pref.setValKey("sponsors",true);
                                pref.setValKey("gallery",true);
                                pref.setValKey("exhibitors",true);
                                pref.setValKey("newcontentlib",true);
                                pref.setValKey("attendees",true);
                            }

                            listener.onSignIn();
                            ((BeConApp) getApplication()).onEventSignIn(event, loginType);
                        }
                    });


                } else {
                    Log.d("onEventSignIn: ", "exception");
                    listener.onError(e);
                }
            }
        });

        // Entry to EventAccessLog

        EventAccessLog EVL = new EventAccessLog();
        EVL.setUserId(user.getObjectId());
        EVL.setEventId(event.getObjectId());
        EVL.setUserType("ATTENDEE");
        EVL.saveInBackground();
    }

    public void sendVerificationCode() {
        String phoneNumber = BeConUser.getCurrentUser().getPhoneNumber();
        HashMap<String, Object> params = new HashMap<>();
        params.put("phoneNumber", phoneNumber);
        Log.e("verifyphone", "" + phoneNumber);

        ParseCloud.callFunctionInBackground("sendVerificationCode", params, new FunctionCallback<String>() {

            @Override
            public void done(String s, ParseException e) {
                if (e == null) {
                    Toast.makeText(getApplicationContext(), R.string.verification_code_on_the_way, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.error_sending_verification_code, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showProgressDialog(String title, String message) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    private void hideProgressDialog() {
        progressDialog.dismiss();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("loginactivity onactivityresul exception ");
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
        try {
            LoginFragment fragment = (LoginFragment) getFragmentManager().findFragmentById(R.id.fragment_container);
            fragment.onActivityResult(requestCode, resultCode, data);
        } catch (Exception v) {
            System.out.println("loginactivity onactivityresul exception " + v);
        }


    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    private boolean checkConnection() {
        detector = new ConnectionDetector(getApplicationContext());

        isInternetPresent = detector.isConnectingToInternet();
        if (!isInternetPresent) {
            //setContentView(R.layout.activity_no_conncetion);
            return false;
        }
        return true;
    }
}
