package com.digitalwavefront.beepetc.activities;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.interfaces.BitmapLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SpeakerLoadedListener;
import com.digitalwavefront.beepetc.parse.models.Speaker;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.views.RoundImageView;

/**
 * Created by Supra-Chaitali on 02/04/2016.
 */
public class SpeakerViewActivity extends AppCompatActivity{
    private RoundImageView speakerPhotoView;
    private TextView app_bar_title, speakername, description, description_title;
    private String name, detail, speakerId;
    private Bitmap bitmap_image;
    private Toolbar app_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speakerview);

        app_bar = (Toolbar) findViewById(R.id.app_bar);
        app_bar_title = (TextView) findViewById(R.id.actionBar_title_txt);

        app_bar_title.setText("Agenda");

        app_bar.setNavigationIcon(R.drawable.ic_back);
        app_bar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SpeakerViewActivity.this.finish();
                SpeakerViewActivity.this.overridePendingTransition(0, R.anim.slide_out_right);
            }
        });

        speakerPhotoView = (RoundImageView) findViewById(R.id.speakerview_photo);
        speakername = (TextView) findViewById(R.id.speakerview_name_txt);
        //description_title = (TextView) findViewById(R.id.speakerview_description_title);
        description = (TextView) findViewById(R.id.speakerview_detail_txt);

        //set fonts
        Typeface boldtypeface = TypefaceUtils.getBoldTypeface(SpeakerViewActivity.this);
        Typeface lighttypeface = TypefaceUtils.getLightTypeface(SpeakerViewActivity.this);

        speakername.setTypeface(boldtypeface);
        // description_title.setTypeface(boldtypeface);
        description.setTypeface(lighttypeface);

        speakerId = getIntent().getStringExtra("speakerId");
        Log.e("spaekerId",""+speakerId);

      /*  name = getIntent().getStringExtra("speaker_name");
        Log.d("intent_name",""+name);
        detail = getIntent().getStringExtra("about");
        Log.d("intent_detail",""+detail);
        Intent intent = getIntent();
        bitmap_image = intent.getParcelableExtra("bitmap_image");*/

        Speaker.getSpeakerByObjectId(speakerId, new SpeakerLoadedListener() {
            @Override
            public void onSpeakerLoaded(@Nullable Speaker speaker) {
                if (speaker != null) {
                    name = speaker.getFullName();
                    detail = speaker.getAbout();
                    speakername.setText("" + name);
                    if(detail == null){
                        description.setText("");
                    }else {
                        description.setText("" + detail);
                    }
                    speaker.getAvatar(new BitmapLoadedListener() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap) {
                            if (bitmap != null) {
                                Log.e("In", "If");
                                bitmap_image = bitmap;
                                speakerPhotoView.setImageBitmap(bitmap_image);
                            } else {
                                Log.e("In", "Else");
                                speakerPhotoView.setImageResource(R.drawable.ic_account_circle);
                            }
                        }
                    });
                }
            }
        });


        app_bar_title.setVisibility(View.VISIBLE);



        // Set stroke color for round image view
       // speakerPhotoView.setBorderColor(ContextCompat.getColor(this, R.color.colorAccent));

        /*speakername.setText("" + name);
        description.setText("" + detail);
      //  description.setMovementMethod(new ScrollingMovementMethod());
        speakerPhotoView.setImageBitmap(bitmap_image);*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        SpeakerViewActivity.this.overridePendingTransition(0, R.anim.slide_out_right);
    }
}
