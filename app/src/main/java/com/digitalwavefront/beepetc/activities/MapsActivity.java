package com.digitalwavefront.beepetc.activities;

import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.digitalwavefront.beepetc.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import java.io.IOException;
import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String intent_address;
    Double latitude;
    Double longitude;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        intent_address = getIntent().getStringExtra("address");
        Log.d("intent_address: ",""+intent_address);

        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera


        Geocoder coder = new Geocoder(this);
        try {
            ArrayList<Address> adresses = (ArrayList<Address>) coder.getFromLocationName(intent_address, 50);
            for(Address add : adresses){
                if (add!=null) {//Controls to ensure it is right address such as country etc.
                    latitude = add.getLatitude();
                    Log.d("lat",""+latitude);
                    longitude = add.getLongitude();
                    Log.d("longi",""+longitude);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

           /* Geocoder gc = new Geocoder(MapsActivity.this);
            List<Address> ad;
            try{
                ad = gc.getFromLocationName(intent_address, 5);
                Log.d("ad",""+ad);
                if (ad==null) {
                    Log.d("ad: ","null");
                }
                Address location=ad.get(0);
                latitude = location.getLatitude();
                Log.d("lat",""+latitude);
                longitude = location.getLongitude();

            }catch (Exception e){
                e.printStackTrace();
            }*/

        //LatLng sydney = new LatLng(-34, 151);
       // mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Event Address"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 10));
    }
}
