package com.digitalwavefront.beepetc.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.digitalwavefront.beepetc.R;

/**
 * Created by supra-chaitali on 23/03/2016.
 */
public class TermsConditionsActivity extends AppCompatActivity {
    private static final String TAG = TermsConditionsActivity.class.getSimpleName();
    private WebView terms_webview;
    private ProgressBar terms_progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        terms_webview = (WebView) findViewById(R.id.terms_webview);
        terms_progressbar = (ProgressBar) findViewById(R.id.terms_progressBar);
        terms_webview.loadUrl("http://beepetc.com/terms-conditions");
        TermsConditionsActivity.this.finish();        Log.e("oncreate", "oncreate");
        //makeRequest();
    }

    private void makeRequest() {
        Log.e("makerequest", "makeRequest: invoke");
        terms_progressbar.setVisibility(View.VISIBLE);
        terms_webview.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress < 100 && terms_progressbar.getVisibility() == View.GONE) {
                    terms_progressbar.setVisibility(View.VISIBLE);
                    Log.e("If","if");
                }
                terms_progressbar.setProgress(newProgress);
                if (newProgress == 100) {
                    terms_progressbar.setVisibility(View.GONE);
                    Log.e("if","if");
                }
            }
        });
        terms_progressbar.setVisibility(View.GONE);
        Log.e("endmakerequest", "end");
    }

  /*  @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.e("backpressed", "onBackPressed: invoke");
        //TermsConditionsActivity.this.overridePendingTransition(0, R.anim.slide_out_right);
        //terms_webview.destroy();
        //getFragmentManager().beginTransaction().replace(R.id.fragment_container, new RegistrationFragment());
        TermsConditionsActivity.this.finish();
    }*/

    @Override
    public void onBackPressed(){
        if(terms_webview.canGoBack() == true){
            terms_webview.goBack();
            Log.e("GoBack","");
        }else{
            finish();
            Log.e("Can'tGo", "");
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
