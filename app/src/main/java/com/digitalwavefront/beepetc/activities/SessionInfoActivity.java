package com.digitalwavefront.beepetc.activities;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.interfaces.SessionRSVPChangeListener;
import com.digitalwavefront.beepetc.interfaces.SessionsLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Session;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.gc.materialdesign.views.ButtonRectangle;
import com.parse.ParseException;
import com.parse.SaveCallback;

import java.util.List;

/**
 * Created by Supra-chaitali on 31/03/2016.
 */
public class SessionInfoActivity extends AppCompatActivity implements SessionRSVPChangeListener{
    Toolbar app_bar;
    ButtonRectangle btn_ok;
    String speakerId, fullname ;
    TextView speakername, role,strtime, details, toolbar_title, label;
    private SessionRSVPChangeListener rsvpChangeListener;
    CheckBox cb;
    ProgressDialog rsvpDialog;
    private BeConUser currentUser;
    TextView textView_no_agenda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sessioninfo);

        rsvpChangeListener = SessionInfoActivity.this;
        currentUser = BeConUser.getCurrentUser();

        rsvpDialog = new ProgressDialog(this);
        rsvpDialog.setTitle(R.string.modifying_rsvp);
        rsvpDialog.setMessage(getString(R.string.one_moment_please));
        rsvpDialog.setCanceledOnTouchOutside(false);

        app_bar = (Toolbar) findViewById(R.id.app_bar);
        toolbar_title = (TextView) findViewById(R.id.actionBar_title_txt);
        toolbar_title.setVisibility(View.VISIBLE);
        toolbar_title.setText("Speakers");

        app_bar.setNavigationIcon(R.drawable.ic_back);
        app_bar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionInfoActivity.this.finish();
                SessionInfoActivity.this.overridePendingTransition(0, R.anim.slide_out_right);
            }
        });

        role = (TextView) findViewById(R.id.sessionInfo_role_txt);
        strtime = (TextView) findViewById(R.id.sessionInfo_time_txt);
        details = (TextView) findViewById(R.id.sessionInfo_detail_txt);
        label = (TextView) findViewById(R.id.addtoagenda_title);
        cb = (CheckBox) findViewById(R.id.sessioninfo_checkbox);


        speakerId = getIntent().getStringExtra("speakerId");
        Log.d("speakerId",""+speakerId);
        fullname = getIntent().getStringExtra("fullname");
        Log.d("fullname",""+fullname);
        // Set fonts
        Typeface boldTypeface = TypefaceUtils.getSemiBoldTypeface(SessionInfoActivity.this);
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(SessionInfoActivity.this);

        textView_no_agenda = (TextView)findViewById(R.id.txt_no_agenda);

        textView_no_agenda.setTypeface(lightTypeface);

        role.setTypeface(boldTypeface);
        strtime.setTypeface(lightTypeface);
        details.setTypeface(lightTypeface);
        label.setTypeface(boldTypeface);
    try {
        Session.getSessionsBySpeakerId(speakerId, new SessionsLoadedListener() {

            @Override
            public void onSessionsLoaded(@Nullable final List<Session> sessions) {

                if(sessions != null) {
                    Log.e("session", "" + sessions.get(0).getTitle() + ": "
                            + sessions.get(0).getStartTime() + ": " + sessions.get(0).getEndTime());

                    role.setText(sessions.get(0).getTitle());
                    strtime.setText("" + sessions.get(0).getSpeakerTimeRangeString());
                    details.setText(sessions.get(0).getDetails());

                    // Setup RSVP checkbox

                    cb.setChecked(currentUser.isRegisteredForSession(sessions.get(0)));
                    cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            boolean isGoing = isChecked;
                            rsvpChangeListener.onRSVPChange(sessions.get(0), isGoing);
                        }
                    });
                }
                else {
                    /*role.setVisibility(View.GONE);
                    strtime.setVisibility(View.GONE);
                    details.setVisibility(View.GONE);
                    label.setVisibility(View.GONE);
                    cb.setVisibility(View.GONE);

                    textView_no_agenda.setVisibility(View.VISIBLE);*/

                    Session.getSessionsBySpeakerIds(speakerId, new SessionsLoadedListener() {
                        @Override
                        public void onSessionsLoaded(@Nullable final List<Session> sessions) {
                            if(sessions != null) {
                                role.setText(sessions.get(0).getTitle());
                                strtime.setText("" +  sessions.get(0).getSpeakerTimeRangeString());
                                details.setText(sessions.get(0).getDetails());

                                cb.setChecked(currentUser.isRegisteredForSession(sessions.get(0)));
                                cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                    @Override
                                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                        boolean isGoing = isChecked;
                                        rsvpChangeListener.onRSVPChange(sessions.get(0), isGoing);
                                    }
                                });
                            }
                        }
                    });

                }
            }
        });
    }catch (Exception e){
        e.printStackTrace();
    }

    }

    private void showRSVPDialog() {
        rsvpDialog.show();
    }

    private void hideRSVPDialog() {
        rsvpDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SessionInfoActivity.this.overridePendingTransition(0, R.anim.slide_out_right);
    }

    @Override
    public void onRSVPChange(Session session, boolean isGoing) {
        showRSVPDialog();
        SaveCallback callback = new SaveCallback() {

            @Override
            public void done(ParseException e) {
                hideRSVPDialog();
            }
        };

        if (isGoing) {
            currentUser.rsvpForSession(session, callback);
        } else {
            currentUser.unregisterForSession(session, callback);
        }
    }
}
