package com.digitalwavefront.beepetc.ui.models;


/**
 * Created by Wayne on 10/18/15.
 */
public class DashboardListItem {
    public static final int NO_ICON = -1;

    private String title;
    private int iconResId;

    public DashboardListItem(String title, int iconResId) {
        this.title = title;
        this.iconResId = iconResId;
    }

    public String getTitle() {
        return title;
    }

    public boolean hasIcon() {
        return getIconResourceId() != NO_ICON;
    }

    public int getIconResourceId() {
        return iconResId;
    }
}
