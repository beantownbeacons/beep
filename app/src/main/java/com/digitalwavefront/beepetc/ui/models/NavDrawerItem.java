package com.digitalwavefront.beepetc.ui.models;

/**
 * Created by Wayne on 9/16/15.
 */
public class NavDrawerItem {

    private String title;
    private int icon, iconSelected;

    public NavDrawerItem(String title, int icon, int iconSelected){
        this.title = title;
        this.icon = icon;
        this.iconSelected = iconSelected;
    }

    public String getTitle(){
        return this.title;
    }

    public int getIcon() {
        return this.icon;
    }

    public int getIconSelected() {
        return this.iconSelected;
    }
}
