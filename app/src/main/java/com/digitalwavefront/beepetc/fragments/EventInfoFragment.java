package com.digitalwavefront.beepetc.fragments;

import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.MainActivity;
import com.digitalwavefront.beepetc.interfaces.BitmapLoadedListener;
import com.digitalwavefront.beepetc.interfaces.EventLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Event;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;

/**
 * Created by Wayne on 9/16/15.
 */
public class EventInfoFragment extends Fragment {

    private ImageView eventLogoView;
    private TextView eventNameView, eventDateView, eventLocationView, eventAddressView;
    private ProgressDialog progressDialog;
    private String detailaddress;

    public EventInfoFragment() {
    }

    public static EventInfoFragment newInstance() {
        return new EventInfoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_event_info, container, false);
        eventLogoView = (ImageView) rootView.findViewById(R.id.event_logo_view);
        eventNameView = (TextView) rootView.findViewById(R.id.event_name_view);
        eventDateView = (TextView) rootView.findViewById(R.id.event_date_view);
        eventLocationView = (TextView) rootView.findViewById(R.id.event_location_view);
        eventAddressView = (TextView) rootView.findViewById(R.id.event_address_view);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(R.string.loading_event_info);
        progressDialog.setMessage(getString(R.string.one_moment_please));
        progressDialog.setCanceledOnTouchOutside(false);

        // Set fonts
        Typeface regularTypeface = TypefaceUtils.getRegularTypeface(getActivity());
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
        eventNameView.setTypeface(regularTypeface);
        eventDateView.setTypeface(lightTypeface);
        eventLocationView.setTypeface(lightTypeface);
        eventAddressView.setTypeface(lightTypeface);

        //logUser();
        // Load the event information
        loadEventInfo();

       /* eventAddressView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent map_intent = new Intent(getActivity(), MapsActivity.class);
                map_intent.putExtra("address", "" + detailaddress);
                startActivity(map_intent);
            }
        });*/

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.app_name));
        // NavigationItemPositionManager navigationItemPositionManager = ((BeConApp) getActivity().getApplication()).getNavigationItemPositionManager();
        // ((MainActivity) getActivity()).setSelectedNavigationPosition(navigationItemPositionManager.getEventInfoPosition());
    }

    // to identify user in crashlytics
    /*
    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Fabric.with(getActivity(), new Crashlytics());

        BeConUser user = BeConUser.getCurrentUser();
        Crashlytics.setUserIdentifier(user.getObjectId());
        Crashlytics.setUserName(user.getUserName());
    }
    */
    private void loadEventInfo() {
        showProgressDialog();
        BeConUser user = BeConUser.getCurrentUser();
        user.getCurrentEvent(new EventLoadedListener() {

            @Override
            public void onEventLoaded(@Nullable final Event event) {
                if (event == null) {
                    showEventNotFoundAndReturnToLogin();
                    hideProgressDialog();
                } else {
                    hideProgressDialog();
                    event.getLogo(new BitmapLoadedListener() {

                        @Override
                        public void onBitmapLoaded(Bitmap logo) {
                            if (logo != null)
                                eventLogoView.setImageBitmap(logo);
                        }
                    });
                    eventNameView.setText(event.getName());
                    eventDateView.setText(event.getStartDateString());
                    eventLocationView.setText(event.getLocationName());
                    detailaddress = event.getDetailedAddress();
                    Log.d("detailadd", "" + detailaddress);
                    eventAddressView.setText(event.getDetailedAddress());

                }
            }
        });


    }

    private void showProgressDialog() {
        progressDialog.show();
    }

    private void hideProgressDialog() {
        progressDialog.dismiss();
    }

    private void showEventNotFoundAndReturnToLogin() {
        Toast.makeText(getActivity(), R.string.event_not_found, Toast.LENGTH_SHORT).show();
        ((MainActivity) getActivity()).signOutOfEvent();
    }
}
