package com.digitalwavefront.beepetc.fragments;


import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.adapters.SpeakerRankAdapter;
import com.digitalwavefront.beepetc.interfaces.SpeakersLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Speaker;

import java.util.List;

public class SpeakersByRankFragment extends Fragment {

    private ListView speakerByRankListView;
    private SpeakerRankAdapter speakerRankAdapter;
    private ProgressDialog loadingDialog;

    public SpeakersByRankFragment() {}

    public static SpeakersByRankFragment newInstance() {
        return new SpeakersByRankFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_speakers_by_rank, container, false);
        speakerByRankListView = (ListView) rootView.findViewById(R.id.speaker_by_rank_list_view);
        loadingDialog = new ProgressDialog(getActivity());
        loadingDialog.setTitle(R.string.loading_data);
        loadingDialog.setMessage(getString(R.string.one_moment_please));
        loadingDialog.setCanceledOnTouchOutside(false);
        loadMetrics();
        return rootView;
    }

    private void loadMetrics() {
        loadingDialog.show();
        Speaker.getSpeakersByEventId(BeConUser.getCurrentUser().getCurrentEventId(), new SpeakersLoadedListener() {

            @Override
            public void onSpeakersLoaded(@Nullable List<Speaker> speakers) {
                loadingDialog.dismiss();

                if (speakers == null) {
                    showErrorMessage();
                } else {
                    speakerRankAdapter = new SpeakerRankAdapter(getActivity(), speakers);
                    speakerByRankListView.setAdapter(speakerRankAdapter);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.speakers_by_rank));
    //    ((MainActivity) getActivity()).setSelectedNavigationPosition(NavigationItemPositionManager.NO_POSITION);
    }

    private void showErrorMessage() {
        Toast.makeText(getActivity(), R.string.error_loading_data, Toast.LENGTH_SHORT).show();
    }
}
