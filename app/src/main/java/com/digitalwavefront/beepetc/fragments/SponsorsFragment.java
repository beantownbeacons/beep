package com.digitalwavefront.beepetc.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.adapters.SponsorsAdapter;
import com.digitalwavefront.beepetc.interfaces.SponsorsLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Sponsor;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tarul on 06/10/2016.
 */
public class SponsorsFragment extends Fragment {

    private ListView sponsorsListView;
    private TextView txtNoSponsors;
    private ProgressDialog progressDialog;
    private List<Sponsor> sponsor_list,sponsor_list2;
    private SponsorsAdapter sponsorsAdapter;
    LinearLayout lay_moreitems;
    int counter=0;
    public static SponsorsFragment newInstance() {
        return new SponsorsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sponsors, container, false);

        sponsorsListView = (ListView) rootView.findViewById(R.id.sponsor_list_view);
        View footerview=((LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.more_items,null,false);
        lay_moreitems= (LinearLayout) footerview.findViewById(R.id.lay_moreitems);
        lay_moreitems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                copysponsors(counter);
                sponsorsAdapter.refreshlist(sponsor_list2);
            }
        });
        sponsorsListView.addFooterView(footerview);
        sponsorsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sponsorsAdapter.toggleItem(position);
            }
        });

        txtNoSponsors = (TextView) rootView.findViewById(R.id.txt_no_sponsors);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(R.string.loading_sponsors);
        progressDialog.setMessage(getString(R.string.one_moment_please));

        loadSponsors();

        return rootView;
    }

    private void loadSponsors() {
        final BeConUser currentUser = BeConUser.getCurrentUser();
        String eventId = currentUser.getCurrentEventId();

        Sponsor.getSponsorsByEventId(eventId, new SponsorsLoadedListener() {
            @Override
            public void onSponsorsLoaded(@Nullable List<Sponsor> sponsors) {
                if (sponsors != null && sponsors.size() != 0) {

                    sponsor_list = new ArrayList<Sponsor>();
                    sponsor_list2 = new ArrayList<Sponsor>();
                    for (int i = 0; i < sponsors.size(); i++) {
                        sponsor_list.add(sponsors.get(i));
                    }
                    copysponsors(0);
                    setlistview();

                } else {
                    showNoSponsorsMessage();
                }
            }
        });
    }

    private void setlistview() {
        sponsorsAdapter = new SponsorsAdapter(getActivity(), getActivity(), sponsor_list);
        sponsorsListView.setAdapter(sponsorsAdapter);
        sponsorsAdapter.notifyDataSetChanged();
    }

    private void showNoSponsorsMessage() {
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
        txtNoSponsors.setVisibility(View.VISIBLE);
        sponsorsListView.setVisibility(View.GONE);
        txtNoSponsors.setTypeface(lightTypeface);
        //txtNoSponsors.setText(getResources().getString(R.string.no_sponsors));
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.sponsors));
    }
    private void copysponsors(int cnt)
    {
        int limit1=cnt+10;
        int limit;
        if(limit1>sponsor_list.size())
        {
            limit=sponsor_list.size();
        }
        else
        {
            limit=limit1;
        }
        for(int c=cnt;c<limit;c++)
        {
            sponsor_list2.add(sponsor_list.get(c));
            counter++;
        }

        if (counter >= sponsor_list.size()) {
            lay_moreitems.setVisibility(View.GONE);
        }
    }
}
