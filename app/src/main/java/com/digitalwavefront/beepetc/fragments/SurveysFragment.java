package com.digitalwavefront.beepetc.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.digitalwavefront.beepetc.interfaces.SpeakersLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SurveyQuestionsLoadedListener;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.MainActivity;
import com.digitalwavefront.beepetc.adapters.SurveysAdapter;
import com.digitalwavefront.beepetc.interfaces.SurveysLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Session;
import com.digitalwavefront.beepetc.parse.models.Speaker;
import com.digitalwavefront.beepetc.parse.models.Survey;
import com.digitalwavefront.beepetc.parse.models.SurveyQuestion;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;

public class SurveysFragment extends Fragment implements View.OnClickListener {

    private BeConUser currentUser;
    private ListView surveyListView;
    private SurveysAdapter surveysAdapter;
    private ProgressDialog loadingDialog;
    private TextView txtNoSurvey;
    LinearLayout lay_moreitems;


    ArrayList<Session> sessList,sessList1;
    ArrayList<String> spkrList;
    ArrayList<Integer>percentList;
    List<Session> sessions;
    List<Session> sessionsList;
    List<SurveyQuestion> questions;

    private HashMap<String, List<Session>> hash;
    private HashMap<String, List<String>> hashSpkrIds;
    private HashMap<String, List<Integer>> hashPercent;
    private ArrayList<String> start_dates;
    private SegmentedGroup segmented2;
    private LinearLayout ll;

    public static SurveysFragment newInstance() {
        SurveysFragment fragment = new SurveysFragment();
        return fragment;
    }

    public SurveysFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(getString(R.string.surveys));
        currentUser = BeConUser.getCurrentUser();
        sessList = new ArrayList<>();
        sessList1 = new ArrayList<>();
        spkrList = new ArrayList<>();
        percentList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_surveys, container, false);
        getActivity().setTitle(getString(R.string.surveys));
        surveyListView = (ListView) rootView.findViewById(R.id.survey_list_view);
        txtNoSurvey=(TextView)rootView.findViewById(R.id.txt_no_surveys);
        View footerview=((LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.more_items,null,false);
        lay_moreitems= (LinearLayout) footerview.findViewById(R.id.lay_moreitems);

        lay_moreitems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new async(questions, cnt, sessions,percentList).execute("");
            }
        });
        loadingDialog = new ProgressDialog(getActivity());
        loadingDialog.setTitle(R.string.loading_surveys);
        loadingDialog.setMessage(getString(R.string.one_moment_please));

        // Adding segment layout for dates
        ll = (LinearLayout) rootView.findViewById(R.id.segment_layout);
        segmented2 = new SegmentedGroup(getActivity());
        segmented2.setOrientation(LinearLayout.HORIZONTAL);
        segmented2.setTintColor(Color.parseColor("#00BCD4"));
        segmented2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
            }
        });

        surveyListView.addFooterView(lay_moreitems);
        lay_moreitems.setVisibility(View.GONE);
        surveyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //String surveyId = surveysAdapter.getItem(position).getObjectId();
                //((MainActivity) getActivity()).showSurveyQuestionsFragment(surveyId);
                String eventId = surveysAdapter.getItem(position).getEventId();
                String sessionId = surveysAdapter.getItem(position).getObjectId();
                String speakerId = surveysAdapter.getItem(position).getSessSpeakerId();
                ((MainActivity) getActivity()).showSurveyQuestionsFragment(eventId,sessionId,speakerId);
            }
        });

        //commented by Sarika [27/09/2017]
        //loadSurveys();
        //if(sessList.size() <= 0){
        sessList = new ArrayList<>();
        sessList1 = new ArrayList<>();
        spkrList = new ArrayList<>();
        percentList = new ArrayList<>();
            loadSurveyQues();
        //}

        return rootView;
    }

    public void getSegments(int size){
        final RadioButton[] rb = new RadioButton[size];
        for(int j=0; j<size; j++){
            rb[j] = new RadioButton(getActivity(), null, info.hoang8f.android.segmented.R.style.RadioButton);
            String sdt = start_dates.get(j);
            rb[j].setText(sdt);
            rb[j].setId(j);
            rb[j].setTextColor(Color.parseColor("#00BCD4"));
            rb[j].setGravity(Gravity.CENTER);
            rb[j].setLayoutParams(new RadioGroup.LayoutParams(ll.getWidth(), ll.getHeight(), 1f));
            rb[j].setOnClickListener(this);
            segmented2.addView(rb[j]); //the RadioButtons are added to the radioGroup instead of the layout
        }
        segmented2.check(0);
        segmented2.updateBackground();
        ll.addView(segmented2);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.surveys));
        /*NavigationItemPositionManager navigationItemPositionManager = ((BeConApp) getActivity().getApplication()).getNavigationItemPositionManager();
        ((MainActivity) getActivity()).setSelectedNavigationPosition(navigationItemPositionManager.getSurveysPosition());*/

    }

    // by Sarika [27/09/2017]
    //################### START ############################
    private void loadSurveyQues() {

        showLoadingDialog();
        //BeConUser user = BeConUser.getCurrentUser();
        String eventId = currentUser.getCurrentEventId();
        currentUser.getSurveyQues(new SurveyQuestionsLoadedListener() {
            @Override
            public void onSurveyQuestionsLoaded(@Nullable List<SurveyQuestion> questions) {

                if (questions == null || questions.size()==0) {
                    showErrorMessage();
                } else {
                    //surveysAdapter = new SurveysAdapter(getActivity(), questions);
                    //surveyListView.setAdapter(surveysAdapter);
                    surveysAdapter = new SurveysAdapter(getActivity(), sessList, questions,spkrList,percentList);
                    surveyListView.setAdapter(surveysAdapter);


                    loadSessions(questions);

                }
            }
        });
    }




// ###### by Sarika [29/09/17] ######
// fetch survey questions

    private void loadSessions(final List<SurveyQuestion> questions) {

        showLoadingDialog();
        //final BeConUser currentUser = BeConUser.getCurrentUser();
        String eventId = currentUser.getCurrentEventId();

        //List<Session> sessions;

        this.questions=questions;
        ParseQuery<Session> query1 = ParseQuery.getQuery(Session.class);
        query1.whereEqualTo("eventId", eventId);
        query1.orderByAscending("startTime");
        try {
            sessions = query1.find();
            sessList = new ArrayList<Session>();

            new async(questions, 0, sessions,percentList).execute("");

        }catch (Exception e) {

        }
        /*
        Session.getSessionsByEventId(eventId, new SessionsLoadedListener() {
            @Override
            public void onSessionsLoaded(@Nullable List<Session> sessions) {

                sessList = new ArrayList<Session>();
                int cntr = 0;
                for(Session session: sessions) {
                    List<String> speakers = session.getList("speakerIds");

                    if(speakers != null) {
                        for (String speakerId : speakers) {
                            //session.setSpeakerId(speakerId);
				
			// code to get survey percentage
			// create class variable percent in session class and set percent to it	
                            int completeCount = 0;
                            try {
                                ParseQuery<SurveyResponse> query = ParseQuery.getQuery(SurveyResponse.class);
                                //query.whereEqualTo("questionId", quesId);
                                query.whereEqualTo("userId", currentUser.getObjectId());
                                query.whereEqualTo("sessionId", session.getObjectId());
                                query.whereEqualTo("speakerId", speakerId);
                                completeCount = query.count();
                                int percent = (completeCount * 100) / questions.size();
                                session.setPercent(percent);

                            }catch (Exception ex){}

                            sessList.add(session);
                            spkrList.add(speakerId);

                            cntr++;

                            if (cntr % 10 == 0) {

                                new async(questions).execute("");
                            }
                        }

                    }
                    //new async().execute("",cntr);
                    //surveysAdapter = new SurveysAdapter(getActivity(), sessList, questions,spkrList);
                    //surveyListView.setAdapter(surveysAdapter);

                }



                hideLoadingDialog();


            }
        }, false);
        */
    }





    //#################### END ###########################

    private void loadSpeakers(List<SurveyQuestion> questions) {

        HashMap<String, List<SurveyQuestion>> surveylist = new HashMap<>();

        BeConUser currentUser = BeConUser.getCurrentUser();
        String eventId = currentUser.getCurrentEventId();

        Speaker.getSpeakersByEventIdNew(eventId, new SpeakersLoadedListener() {

            @Override
            public void onSpeakersLoaded(@Nullable final List<Speaker> speakers) {


            }
        });
    }
    private void loadSurveys() {
        showLoadingDialog();
        currentUser.getSurveys(new SurveysLoadedListener() {

            @Override
            public void onSurveysLoaded(@Nullable List<Survey> surveys) {
                hideLoadingDialog();
                if (surveys == null || surveys.size()==0) {
                    showErrorMessage();
                } else {
                    surveysAdapter = new SurveysAdapter(getActivity(), (ArrayList)surveys);
                    surveyListView.setAdapter(surveysAdapter);
                }
            }
        });
    }

    private void showLoadingDialog() {
        loadingDialog.show();
    }

    private void hideLoadingDialog() {
        loadingDialog.dismiss();
    }

    private void showErrorMessage() {
        //Toast.makeText(getActivity(), R.string.error_loading_surveys, Toast.LENGTH_SHORT).show();
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
        txtNoSurvey.setTypeface(lightTypeface);
        txtNoSurvey.setText(getResources().getString(R.string.error_loading_surveys));
        txtNoSurvey.setVisibility(View.VISIBLE);
        surveyListView.setVisibility(View.GONE);
    }
    int cnt, limit=10,c;
    class async extends AsyncTask<String,String,Integer>
    {

        List<SurveyQuestion> questions;
        List<Session> sessions;
        List<Integer> percentList;
        //int cnt, limit = 10,c;
        int cntr = 0;
        public async(List<SurveyQuestion> quest, int cntr, List<Session> listsess, List<Integer> listpercent) {
            questions = quest;
            this.cntr = cntr;
            sessions = listsess;
            percentList = listpercent;

        }

        @Override
        protected void onPreExecute() {

            showLoadingDialog();
        }

        @Override
        protected Integer doInBackground(String... lists) {


            String eventId = currentUser.getCurrentEventId();
            limit = cntr+10;
            c = sessions.size();
            /*if(limit>sessions.size())
                c=sessions.size();
            else
                c=limit;*/
            hash = new HashMap<String, List<Session>>();
            hashSpkrIds = new HashMap<String, List<String>>();
            hashPercent = new HashMap<String, List<Integer>>();

            start_dates = new ArrayList<String>();
            for (int i = cntr; i < c; i++) {

                    Session session = sessions.get(i);

                    String sdt = session.FormattedStartDate();

                    if (!start_dates.contains(session.FormattedStartDate())) {
                        start_dates.add(sdt);
                        ArrayList<Session> list = new ArrayList<Session>() {
                        };
                        hash.put(sdt, list);
                        ArrayList<String> listSpk = new ArrayList<String>() {
                        };
                        hashSpkrIds.put(sdt,listSpk);
                        ArrayList<Integer> listPercent = new ArrayList<Integer>() {
                        };
                        hashPercent.put(sdt,listPercent);
                    }

                    //for(Session session: sessions) {
                    List<String> speakers = session.getList("speakerIds");

                    if (speakers != null) {
                        for (String speakerId : speakers) {
                            //session.setSpeakerId(speakerId);

                            // code to get survey percentage
                            // create class variable percent in session class and set percent to it
                            int completeCount = 0;
                            try {
                                /*
                                ParseQuery<SurveyResponse> query = ParseQuery.getQuery(SurveyResponse.class);
                                //query.whereEqualTo("questionId", quesId);
                                query.whereEqualTo("userId", currentUser.getObjectId());
                                query.whereEqualTo("sessionId", session.getObjectId());
                                query.whereEqualTo("speakerId", speakerId);
                                completeCount = query.count();
                               */

                                int percent = (completeCount * 100) / questions.size();
                                session.setPercent(percent);
                                percentList.add(percent);
                                hashPercent.get(sdt).add(percent);

                            } catch (Exception ex) {
                            }

                            sessList.add(session);
                            hash.get(sdt).add(session);
                            hashSpkrIds.get(sdt).add(speakerId);

                            spkrList.add(speakerId);

                            Log.d("Survey Log:","speaker: "+speakerId+", session: "+session.getTitle());


                        }


                    }

                cntr++;



            }

            return cntr;
        }

        protected void onPostExecute(Integer res) {



            /*
                surveysAdapter.refreshSessionlist(sessList,spkrList,percentList);
                surveyListView.invalidateViews();
                if (res < sessions.size()) {
                    new async(questions, cnt, sessions,percentList).execute("");
                } else {
                    hideLoadingDialog();
                }
                */
            hideLoadingDialog();
            cnt=res;
/*
            if (start_dates.size() > 0) {
                getSegments(start_dates.size());
                Log.e("Start_dates_size", String.valueOf(start_dates.size()));
                sessionsList = hash.get(start_dates.get(0));
                List<String> spkrIds = hashSpkrIds.get(start_dates.get(0));
                List<Integer> percentArr = hashPercent.get(start_dates.get(0));
                Log.e("Start_dates", start_dates.get(0));

            }
            ArrayList<Session> sessArrList = new ArrayList<>();
            sessArrList.addAll(sessionsList);*/
            surveysAdapter.refreshSessionlist(sessList,spkrList,percentList);

            if (res < sessions.size()) {
                //if(surveyListView.getFooterViewsCount() < 1)
                //surveyListView.addFooterView(lay_moreitems);
                lay_moreitems.setVisibility(View.VISIBLE);
            }
            else {
                if(surveyListView.getFooterViewsCount() ==1)
                surveyListView.removeFooterView(lay_moreitems);
                //lay_moreitems.setVisibility(View.GONE);
            }



            System.out.println("cnt="+cnt+" session size="+sessions.size());

        }

        }

    @Override
    public void onClick(View v) {

        RadioButton rb = (RadioButton)v;
        sessions = hash.get(rb.getText().toString());
        List<String> spkrIds = hashSpkrIds.get(rb.getText().toString());
        List<Integer> percentArr = hashPercent.get(rb.getText().toString());
        if(sessions!=null){
            ArrayList<Session> sessArrList = new ArrayList<>();
            sessArrList.addAll(sessions);
            surveysAdapter.refreshSessionlist(sessArrList,spkrIds,percentArr);
        }

    }
    }