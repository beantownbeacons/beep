package com.digitalwavefront.beepetc.fragments;

import android.app.Activity;
//import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.MainActivity;
import com.digitalwavefront.beepetc.adapters.GalleryAdapter;
import com.digitalwavefront.beepetc.interfaces.GalleryLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.EventGallery;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Praneet on 08/07/2016.
 */

/**
 * Updated by Tarul on 08/10/2016
 */
public class GalleryFragment extends Fragment implements AdapterView.OnItemClickListener {


    Handler handler;
    //Hello
    GridView gridview;
    private ProgressDialog progressDialog;
    //FloatingActionButton floatingActionButton;
    Button floatingActionButton;
    EventGallery eventGallery;
    String mCurrentPhotoPath;
    static final int REQUEST_TAKE_PHOTO = 123;
    SwipeRefreshLayout swipeRefreshLayout;
    TextView txtNoGallery;
    private int screenWidth = MainActivity.screenWidth;
    Activity activity;
    ArrayList<EventGallery> galleries;


    public GalleryFragment() {

    }

    public static GalleryFragment newInstance() {
        return new GalleryFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        galleries=new ArrayList<>();


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gallery, container, false);

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadGalleryImages();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        swipeRefreshLayout.setEnabled(false);

        gridview = (GridView) rootView.findViewById(R.id.gridview);

        gridview.setOnItemClickListener(this);

        gridview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                Log.e("Praneet", "onScrollStateChanged" + scrollState);
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.e("Praneet", "onScroll firstVisibleItem" + firstVisibleItem);
                Log.e("Praneet", "onScroll visibleItemCount" + visibleItemCount);
                if (firstVisibleItem == 0) {
                    swipeRefreshLayout.setEnabled(true);
                } else {
                    swipeRefreshLayout.setEnabled(false);
                }
            }
        });


        //floatingActionButton = (FloatingActionButton) rootView.findViewById(R.id.fab);
        floatingActionButton = (Button) rootView.findViewById(R.id.fab);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(R.string.loading_gallery);
        progressDialog.setMessage(getString(R.string.one_moment_please));
        txtNoGallery = (TextView) rootView.findViewById(R.id.txt_no_gallery);
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
        txtNoGallery.setTypeface(lightTypeface);
        gridview.setEmptyView(txtNoGallery);

        galadp= new GalleryAdapter(getActivity(), R.layout.grid_list_item, galleries);
        gridview.setAdapter(galadp);
        //loadGallery();

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ButtonClicked", "Floating");
                dispatchTakePictureIntent();
            }
        });

        if(galleries.size()<=0)
        loadGalleryImages();
        return rootView;
    }

    GalleryAdapter galadp;
    private void loadGalleryImages() {

        showProgressDialog();
        BeConUser currentUser = BeConUser.getCurrentUser();
        String eventId = currentUser.getCurrentEventId();


        EventGallery.getImagesByEventId(eventId, new GalleryLoadedListener() {
            @Override
            public void onGalleryLoaded(@Nullable List<EventGallery> gallery) {

                hideProgressDialog();
                if (gallery != null && gallery.size() != 0) {

                    showProgressDialog();
                    galleries = (ArrayList)gallery;
                    for(int c=0;c<galleries.size();c++) {


                        new async().execute(galleries.get(c).getImageThumbnailUrl(),String.valueOf(c));

                    }
                   // galadp.notifyDataSetChanged();


                } else {

                }
            }
        });
    }


    class async extends AsyncTask<String,Void,Bitmap>
    {

        @Override
        protected Bitmap doInBackground(String... lists) {

            Bitmap bmp=null;
            try {


                    URL url = new URL(lists[0]);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap bitmap = BitmapFactory.decodeStream(input);
                    galleries.get(Integer.parseInt(lists[1])).bitmap = bitmap;

                System.out.println(url+" downloaded ");


            } catch (IOException e) {
                // Log exception
                System.out.println("exception downloading "+e);
            }
            return bmp;
        }

        protected void onPostExecute(Bitmap bmp) {

            galadp= new GalleryAdapter(getActivity(), R.layout.grid_list_item, galleries);
            gridview.setAdapter(galadp);

            try {
                hideProgressDialog();
            }catch (Exception e){}

        }

    }

    public Bitmap getBitmapFromURL(final String src) {
        final Bitmap [] bmp =new Bitmap[1];
       new Handler().post(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url = new URL(src);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    bmp[0] = BitmapFactory.decodeStream(input);

                } catch (IOException e) {
                    // Log exception

                }
            }
        });
        return bmp[0];

    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.gallery));
        //     NavigationItemPositionManager navigationItemPositionManager = ((BeConApp) getActivity().getApplication()).getNavigationItemPositionManager();
        //    ((MainActivity) getActivity()).setSelectedNavigationPosition(navigationItemPositionManager.getGalleryPosition());
        //loadGallery();
    }

    private void showProgressDialog() {
        progressDialog.show();
    }

    private void hideProgressDialog() {
        progressDialog.dismiss();
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        Log.e("Timestamp", "Date");
        String imageFileName = "Beep_" + timeStamp + "_";
        Log.e("ImageFile", imageFileName);
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        Log.e("Storage", "Directory");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        Log.e("Image File", image.toString());

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        Log.e("PhotoPath", mCurrentPhotoPath);
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Log.e("Intent", "Capture");
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            Log.e("Camera", "Activity");
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "com.digitalwavefront.beepetc.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                Log.e("PhotoURI", photoURI.toString());
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
       /* if (requestCode == REQUEST_TAKE_PHOTO && resultCode == getActivity().RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmapThumbnail = (Bitmap) extras.get("data");
            Log.e("Praneet", "in activity result"+imageBitmapThumbnail);
        }*/

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == activity.RESULT_OK) {
            Log.e("Praneet", "in activity result" + mCurrentPhotoPath);
            mCurrentPhotoPath = mCurrentPhotoPath.substring(5);
            Log.e("CurrentPhotoPath", mCurrentPhotoPath);
            Bitmap imageBitmapLarge = decodeSampledBitmapFromFile(800, 600, mCurrentPhotoPath);
            Bitmap imageBitmapThumbnail = decodeSampledBitmapFromFile(80, 60, mCurrentPhotoPath);


            Log.e("Praneet", "in activity result" + imageBitmapLarge);
            Log.e("Praneet", "in activity result" + imageBitmapThumbnail);


            ByteArrayOutputStream streamLarge = new ByteArrayOutputStream();
            imageBitmapLarge.compress(Bitmap.CompressFormat.JPEG, 100, streamLarge);
            byte[] bitmapBytesForLargeImage = streamLarge.toByteArray();

            final BeConUser currentUser = BeConUser.getCurrentUser();
            final String eventId = currentUser.getCurrentEventId();

            final ParseFile parseFileLargeImage = new ParseFile("event_photo_large" + Math.random() + ".jpg", bitmapBytesForLargeImage);
            ByteArrayOutputStream streamThumb = new ByteArrayOutputStream();
            imageBitmapThumbnail.compress(Bitmap.CompressFormat.JPEG, 100, streamThumb);
            byte[] bitmapBytesForThumbnailImage = streamThumb.toByteArray();
            // imageBitmapThumbnail= BitmapFactory.decodeByteArray(bitmapBytesForThumbnailImage,0,bitmapBytesForThumbnailImage.length);
            // imageBitmapThumbnail.compress(Bitmap.CompressFormat.JPEG, 40, stream);
            // bitmapBytesForThumbnailImage = stream.toByteArray();

            final ParseFile parseFileImageThumbnail = new ParseFile("event_photo_thumb" + Math.random() + ".jpg", bitmapBytesForThumbnailImage);

            parseFileLargeImage.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    eventGallery = new EventGallery();
                    eventGallery.setEventId(eventId);
                    eventGallery.setUserId(currentUser.getObjectId());
                    eventGallery.setImage(parseFileLargeImage);
                    eventGallery.setImageThumbnail(parseFileImageThumbnail);
                    eventGallery.setLikeCount(0);
                    eventGallery.setUploaderName(currentUser.getName());

                    eventGallery.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                hideProgressDialog();
                                Toast.makeText(getActivity(), "Photo updated successfully.", Toast.LENGTH_SHORT).show();
                                loadGalleryImages();

                            } else {
                                hideProgressDialog();
                                Toast.makeText(getActivity(), "Failed to update photo.", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });

                }
            });

        }
    }


    public static Bitmap decodeSampledBitmapFromFile(int reqWidth, int reqHeight, String path) { // BEST QUALITY MATCH

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }


    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
// Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 2;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        eventGallery = galleries.get(position);
        Log.e("EventGallery", ""+eventGallery);

        /*ViewPagerAdapter fragment2 = new ViewPagerAdapter();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment2);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();*/

        //Fragment viewPagerFragment = ViewPagerFragment.newInstance();
        //Fragment viewPagerFragment = ViewPagerFragment.newInstance(galleries);
        ViewPagerFragment.galleries=galleries;
        ViewPagerFragment.position=position;
        Fragment viewPagerFragment = new ViewPagerFragment();

        Bundle args = new Bundle();

        EventGallery gallery = getItem(position);
        String pictureId = gallery.getObjectId();
        args.putString("pictureId", pictureId);
        args.putString("ImageURL", gallery.getImageUrl());
        args.putString("UserName", gallery.getUploaderName());


        viewPagerFragment.setArguments(args);
        Log.e("PictureId", pictureId);
        Log.e("ImageURL", gallery.getImageUrl());
        Log.e("Username", gallery.getUploaderName());
        /*FragmentTransaction transaction = ((Activity) mContext).getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, viewPagerFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();*/
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, viewPagerFragment).addToBackStack(null).commit();
    }

    public EventGallery getItem(int position) {
        Log.e("Event", "in getItem");
        return galleries.get(position);
    }

}