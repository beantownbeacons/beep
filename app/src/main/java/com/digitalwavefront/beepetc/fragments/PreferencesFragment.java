package com.digitalwavefront.beepetc.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.UserPreferences;
import com.digitalwavefront.beepetc.interfaces.PrefLoadedListener;
import com.parse.ParseException;
import com.parse.SaveCallback;

import java.util.List;

/**

 * Use the {@link PreferencesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PreferencesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ListView prefListview;

    List<UserPreferences> prefList;

    private ProgressDialog loadingDialog, updateDialog;

    Switch prefval;
    UserPreferences pref;
    BeConUser currentUser;
    //private OnFragmentInteractionListener mListener;

    public PreferencesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PreferencesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PreferencesFragment newInstance(String param1, String param2) {
        PreferencesFragment fragment = new PreferencesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        currentUser = BeConUser.getCurrentUser();
        //pref=new UserPreferences();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_preferences, container, false);
        prefval=rootView.findViewById(R.id.prefval);

        loadingDialog = new ProgressDialog(getActivity());
        loadingDialog.setTitle(R.string.loading_pref);
        loadingDialog.setMessage(getString(R.string.one_moment_please));

        updateDialog = new ProgressDialog(getActivity());
        updateDialog.setTitle(R.string.updating_pref);
        updateDialog.setMessage(getString(R.string.one_moment_please));

        showLoadingDialog();

        loadUserPref();


        return rootView;
    }

    private void showLoadingDialog() {
        loadingDialog.show();
    }

    private void hideLoadingDialog() {
        loadingDialog.dismiss();
    }

    private void showUpdateDialog() {
        updateDialog.show();
    }

    private void hideUpdateDialog() {
        updateDialog.dismiss();
    }

    public void loadUserPref() {

        UserPreferences.getPrefByUserId(currentUser.getObjectId(), new PrefLoadedListener() {
            @Override
            public void onPrefLoadedListener(@Nullable UserPreferences preferences) {


                if (preferences != null) {
                    pref = preferences;
                    setUserPref();
                    hideLoadingDialog();
                } else {
                    hideLoadingDialog();
                }
            }
        });
    }

    public void setUserPref(){

        prefval.setChecked(pref.getPrefVal(prefval.getTag().toString()));
        prefval.setOnCheckedChangeListener(new switchlistener(prefval.getTag().toString()));

    }

    class switchlistener implements CompoundButton.OnCheckedChangeListener
    {

        String tag;
        public switchlistener(String t)
        {
            tag=t;
        }
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            //pref.put(tag,b);
            showUpdateDialog();
            pref.setPrefVal(tag,b);
            pref.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {

                    if (e == null) {
                        Toast.makeText(getActivity(), R.string.updated_pref_success, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                    hideUpdateDialog();
                }
            });
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        /*
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
        */
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        */
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    /*
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    */
}
