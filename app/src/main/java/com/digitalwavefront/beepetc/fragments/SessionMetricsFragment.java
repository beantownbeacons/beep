package com.digitalwavefront.beepetc.fragments;


import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.adapters.SessionMetricsAdapter;
import com.digitalwavefront.beepetc.interfaces.SpeakersLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Speaker;

import java.util.List;

public class SessionMetricsFragment extends Fragment {

    private ListView sessionMetricsListView;
    private SessionMetricsAdapter metricsAdapter;
    private ProgressDialog loadingDialog;

    public SessionMetricsFragment() {}

    public static SessionMetricsFragment newInstance() {
        return new SessionMetricsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_session_metrics, container, false);
        loadingDialog = new ProgressDialog(getActivity());
        loadingDialog.setTitle(R.string.loading_data);
        loadingDialog.setMessage(getString(R.string.one_moment_please));
        loadingDialog.setCanceledOnTouchOutside(false);
        sessionMetricsListView = (ListView) rootView.findViewById(R.id.session_metrics_list_view);
        loadSessionMetrics();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.session_metrics));
    //    ((MainActivity) getActivity()).setSelectedNavigationPosition(NavigationItemPositionManager.NO_POSITION);
    }

    private void loadSessionMetrics() {
        loadingDialog.show();
        String eventId = BeConUser.getCurrentUser().getCurrentEventId();
        Speaker.getSpeakersByEventId(eventId, new SpeakersLoadedListener() {

            @Override
            public void onSpeakersLoaded(@Nullable List<Speaker> speakers) {
                loadingDialog.dismiss();
                if (speakers == null) {
                    showErrorMessage();
                } else {
                    metricsAdapter = new SessionMetricsAdapter(getActivity(), speakers);
                    sessionMetricsListView.setAdapter(metricsAdapter);
                }
            }
        });
    }

    private void showErrorMessage() {
        Toast.makeText(getActivity(), R.string.error_loading_data, Toast.LENGTH_SHORT).show();
    }
}
