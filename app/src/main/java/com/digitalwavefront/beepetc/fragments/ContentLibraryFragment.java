package com.digitalwavefront.beepetc.fragments;


import android.content.DialogInterface;
import android.content.pm.ResolveInfo;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalwavefront.beepetc.BeConApp;
import com.digitalwavefront.beepetc.interfaces.SwipeMenuCreator;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.adapters.SavedNotificationsAdapter;
import com.digitalwavefront.beepetc.interfaces.FileLoadedListener;
import com.digitalwavefront.beepetc.interfaces.NotificationsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SavedNotificationActionListener;
import com.digitalwavefront.beepetc.parse.models.BeConNotification;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.SavedNotification;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.views.SwipeMenu;
import com.digitalwavefront.beepetc.views.SwipeMenuItem;
import com.digitalwavefront.beepetc.views.SwipeMenuListView;
import com.parse.DeleteCallback;
import com.parse.ParseException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ContentLibraryFragment extends Fragment implements SavedNotificationActionListener {
    private static final String TAG = "ContentLibraryFragment";

    private BeConUser currentUser;
    //private ListView savedNotificationsListView;
    private SwipeMenuListView savedNotificationsListView;
    private SavedNotificationsAdapter notificationsAdapter;
    private ProgressDialog loadingDialog;
    private ProgressDialog pdfProgressDialog;
    TextView txtNoContent;
    private static final int REQUEST_SHARE=314;

    public static ContentLibraryFragment newInstance() {
        ContentLibraryFragment fragment = new ContentLibraryFragment();
        return fragment;
    }

    public ContentLibraryFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentUser = BeConUser.getCurrentUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        System.out.println("prefered");
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_content_library, container, false);
      //  savedNotificationsListView = (ListView) rootView.findViewById(R.id.saved_notifications_list_view);
        savedNotificationsListView = (SwipeMenuListView) rootView.findViewById(R.id.saved_notifications_list_view);
        savedNotificationsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                notificationsAdapter.toggleItem(position);

            }
        });
/*
        savedNotificationsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                onShare(notificationsAdapter.getItem(position));
                return true;
            }
        });
        */
        loadingDialog = new ProgressDialog(getActivity());
        loadingDialog.setTitle(R.string.loading_content);
        loadingDialog.setMessage(getString(R.string.one_moment_please));

        pdfProgressDialog = new ProgressDialog(getActivity());
        pdfProgressDialog.setTitle(R.string.loading_pdf);
        pdfProgressDialog.setMessage(getString(R.string.one_moment_please));
        txtNoContent=(TextView)rootView.findViewById(R.id.txt_no_content);
        txtNoContent.setText(getString(R.string.no_pref));
        
        /*
		 * Swipe Listener
		 * Added by Supra-Chaitali on 12/04/2016
		 */
        // Commented by Sarika [6 Dec] to hide delete option
        //del_menu_create();
        
        loadSavedNotifications();

        return rootView;
    }
    /**
     *  Added by Supra-Chaitali on 12/04/2016
     */
    private void del_menu_create() {
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                if (menu.getViewType()==0){
                    SwipeMenuItem deleteItem = new SwipeMenuItem(getActivity());
                    //set item background

                    deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                            0x3F, 0x25)));
                    //set item width
                    deleteItem.setWidth(dp2px(90));
                    // set a icon
                    deleteItem.setTitle("Delete");
                    deleteItem.setTitleColor(Color.WHITE);
                    deleteItem.setTitleSize(15);
                    //add to menu
                    menu.addMenuItem(deleteItem);
                }else {
                    SwipeMenuItem deleteItem = new SwipeMenuItem(
                            getActivity());
                    deleteItem.setBackground(new ColorDrawable(Color.WHITE));
                    // set item width
                    deleteItem.setWidth(dp2px(0));
                    // set a icon
                    deleteItem.setTitle("Back");
                    deleteItem.setTitleColor(Color.BLACK);
                    //  deleteItem.setIcon(R.drawable.ic_delete);
                    deleteItem.setTitleSize(15);
                    menu.addMenuItem(deleteItem);
                }
            }
        };

        //set creator
        savedNotificationsListView.setMenuCreator(creator);
    }
    /**
     *  Dp to Pixel conversion
     * @param dp
     * @return
     * Added by Supra-Cahitali on 12/04/2016
     */
    private int dp2px(int dp) {

        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }


    @Override
    public void onResume() {
        super.onResume();


    //    NavigationItemPositionManager navigationItemPositionManager = ((BeConApp) getActivity().getApplication()).getNavigationItemPositionManager();
    //    ((MainActivity) getActivity()).setSelectedNavigationPosition(navigationItemPositionManager.getContentLibraryPosition());
    }

    private void loadSavedNotifications() {
        showLoadingDialog();
       boolean isNotificationSaved= currentUser.getSavedNotifications(new NotificationsLoadedListener() {

            @Override
            public void onNotificationsLoaded(@Nullable final List<BeConNotification> notifications) {
                hideLoadingDialog();

                if (notifications == null || notifications.size()==0) {
                    Log.e("Praneet","notification"+notifications);
                    showErrorMessage();
                } else {
                   // Log.e("loadSavedNotifications","notification title: "+notifications.size()+", "+notifications.get(0).getTitle());
                    notificationsAdapter = new SavedNotificationsAdapter(getActivity(),
                            notifications, ContentLibraryFragment.this);

                    savedNotificationsListView.setAdapter(notificationsAdapter);

                    //Added by Supra-Chaitali on 12-04-2016
                    savedNotificationsListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {
                        @Override
                        public void onSwipeStart(int position) {

                        }

                        @Override
                        public void onSwipeEnd(int position) {

                        }
                    });

                }
            }
        });
        if (!isNotificationSaved)
        {
            hideLoadingDialog();
            showErrorMessage();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE_MY_PICK) {
            if(data != null && data.getComponent() != null && !TextUtils.isEmpty(data.getComponent().flattenToShortString()) ) {
                String appName = data.getComponent().flattenToShortString();
                // Now you know the app being picked.
                // data is a copy of your launchIntent with this important extra info added.


                Bundle b=data.getBundleExtra("b1");

                data.removeExtra("b1");
                if(appName.equalsIgnoreCase("com.twitter.android/com.twitter.composer.ComposerShareActivity")){
                    data.putExtra(Intent.EXTRA_TEXT,  b.getString("url")+" - via @beepetc");
                }
                else if(appName.equalsIgnoreCase("com.google.android.gm/.ComposeActivityGmail"))
                {
                    data.putExtra(Intent.EXTRA_TEXT, "BEEP: " + b.getString("title") + "\n\n" + b.getString("msg") + "\n\n Visit: " + b.getString("url"));
                    data.putExtra(Intent.EXTRA_SUBJECT, "BEEP: " + b.getString("title"));
                }
                else {

                    data.putExtra(Intent.EXTRA_TEXT, "BEEP: " + b.getString("title") +  "\n\n Visit: " + b.getString("url"));
                    data.putExtra(Intent.EXTRA_SUBJECT, "BEEP: " + b.getString("title"));
                }
                // Start the selected activity
                startActivity(data);
            }
        }
        if(requestCode == REQUEST_CODE_MY_PICK_pdf) {
            if(data != null && data.getComponent() != null && !TextUtils.isEmpty(data.getComponent().flattenToShortString()) ) {
                String appName = data.getComponent().flattenToShortString();
                // Now you know the app being picked.
                // data is a copy of your launchIntent with this important extra info added.

                Bundle b=data.getBundleExtra("b1");
                data.removeExtra("b1");
                if(appName.equalsIgnoreCase("com.twitter.android/com.twitter.composer.ComposerShareActivity")){

                    data.putExtra(Intent.EXTRA_TEXT, b.getString("url") +" "+  b.getString("pdfurl") +" - via @beepetc");
                }
                else if(appName.equalsIgnoreCase("com.google.android.gm/.ComposeActivityGmail"))
                {
                    data.putExtra(Intent.EXTRA_TEXT, "BEEP: " + b.getString("title") + "\n\n" + b.getString("msg") + "\n\n Visit: " + b.getString("url") + "\n\n View: " + b.getString("pdfurl"));
                    data.putExtra(Intent.EXTRA_SUBJECT, "BEEP: " + b.getString("title"));
                }
                else {
                    data.putExtra(Intent.EXTRA_TEXT, "BEEP: " + b.getString("title") + "\n\n Visit: " + b.getString("url") + "\n\n View: " + b.getString("pdfurl"));
                    data.putExtra(Intent.EXTRA_SUBJECT, "BEEP: " + b.getString("title"));
                }
                // Start the selected activity
                startActivity(data);
            }
        }
    }



    public void onShare(final BeConNotification notification)
    {
        final String url = notification.getUrl();
        final ArrayList<Uri> uris=new ArrayList<>();
        final ArrayList<String> strArr = new ArrayList<>();
        strArr.add(url);

        if(notification.getPdfFile()!=null) {

            //startShareIntent_desc(url,notification.getPdfFile().getUrl(),notification.getTitle(),notification.getMessage());
            doSocialShare(url,notification.getTitle(),notification.getPdfFile().getUrl(),notification.getMessage());
            /*
            showPdfProgressDialog();
            content.getPdf(getActivity(), new FileLoadedListener() {
                @Override
                public void onFileLoaded(@Nullable File pdfFile) {
                    hidePdfProgressDialog();

                    if (pdfFile != null) {
                        uris.add(Uri.fromFile(pdfFile));
                        startShareIntent(uris, strArr,content.getTitle());
                    } else {
                        showErrorPdfMessage();
                    }
                }
            });
            */

        }
        else {
            //startShareIntent_desc(url,notification.getTitle(),notification.getMessage());
            doSocialShare(url,notification.getTitle(),notification.getMessage());
        }

    }

    private void startShareIntent(String str,String title) {
        //shareDialog.show();
        Intent shareintent=new Intent(Intent.ACTION_SEND);
        shareintent.setType("text/plain");
        shareintent.putExtra(Intent.EXTRA_TEXT,"BEEP: "+title+"\n"+ str);
        shareintent.putExtra(Intent.EXTRA_SUBJECT, "BEEP: "+title);

        System.out.println("str="+str);

        startActivityForResult(Intent.createChooser(shareintent, "Share With"),REQUEST_SHARE);
    }
    private void startShareIntent(ArrayList<Uri> uri, ArrayList<String> str,String title) {
        //shareDialog.show();
        Intent shareintent=new Intent(Intent.ACTION_SEND);
        shareintent.setType("application/pdf");
        shareintent.putExtra(Intent.EXTRA_TEXT, "BEEP: "+title+"\n"+str.get(0));
        shareintent.putExtra(Intent.EXTRA_STREAM, uri.get(0));
        shareintent.putExtra(Intent.EXTRA_SUBJECT, "BEEP: "+title);

        System.out.println("str="+str);
        System.out.println("uri="+uri);
        startActivityForResult(Intent.createChooser(shareintent, "Share With"),REQUEST_SHARE);
    }
    private void startShareIntent(String str,String pdfurl,String title) {
        //shareDialog.show();
        Intent shareintent=new Intent(Intent.ACTION_SEND);
        shareintent.setType("text/plain");
        shareintent.putExtra(Intent.EXTRA_TEXT,"BEEP: "+title+"\n"+ str+"\n PDF: \n"+ pdfurl);
        shareintent.putExtra(Intent.EXTRA_SUBJECT, "BEEP: "+title);

        System.out.println("str="+str);

        startActivityForResult(Intent.createChooser(shareintent, "Share With"),REQUEST_SHARE);
    }
    private void startShareIntent_desc(String str,String title,String msg) {
        //shareDialog.show();
        Intent shareintent=new Intent(Intent.ACTION_SEND);
        shareintent.setType("text/plain");
        shareintent.putExtra(Intent.EXTRA_TEXT,"BEEP: "+title+"\n\n"+ msg+"\n\n Visit: "+ str);
        shareintent.putExtra(Intent.EXTRA_SUBJECT, "BEEP: "+title);

        System.out.println("str="+str);

        startActivityForResult(Intent.createChooser(shareintent, "Share With"),REQUEST_SHARE);
    }
    private void startShareIntent_desc(String str,String pdfurl,String title, String msg) {
        //shareDialog.show();
        Intent shareintent=new Intent(Intent.ACTION_SEND);
        shareintent.setType("text/plain");
        shareintent.putExtra(Intent.EXTRA_TEXT,"BEEP: "+title+"\n\n"+ msg+"\n\n Visit: "+ str+"\n\n View: "+ pdfurl);
        shareintent.putExtra(Intent.EXTRA_SUBJECT, "BEEP: "+title);

        System.out.println("str="+str);

        startActivityForResult(Intent.createChooser(shareintent, "Share With"),REQUEST_SHARE);
    }


    public void doSocialShare(String url, String title, String msg){
        // First search for compatible apps with sharing (Intent.ACTION_SEND)
        List<Intent> targetedShareIntents = new ArrayList<Intent>();
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        // Set title and text to share when the user selects an option.
        /*
        shareIntent.putExtra(Intent.EXTRA_TITLE, title);
        shareIntent.putExtra(Intent.EXTRA_TEXT, url);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        */
        Bundle b=new Bundle();
        b.putString("title",title);
        b.putString("url",url);
        b.putString("msg",msg);
        shareIntent.putExtra("b1",b);
        List<ResolveInfo> resInfo = getActivity().getPackageManager().queryIntentActivities(shareIntent, 0);
        if (!resInfo.isEmpty()) {
            for (ResolveInfo info : resInfo) {
                Intent targetedShare = new Intent(Intent.ACTION_SEND);
                targetedShare.setType("text/plain"); // put here your mime type
                targetedShare.setPackage(info.activityInfo.packageName.toLowerCase());
                targetedShareIntents.add(targetedShare);
            }
            // Then show the ACTION_PICK_ACTIVITY to let the user select it
            Intent intentPick = new Intent();
            intentPick.setAction(Intent.ACTION_PICK_ACTIVITY);
            // Set the title of the dialog
            intentPick.putExtra(Intent.EXTRA_TITLE, "Share with ");
            intentPick.putExtra(Intent.EXTRA_INTENT, shareIntent);
            intentPick.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray());
            // Call StartActivityForResult so we can get the app name selected by the user
            this.startActivityForResult(intentPick, REQUEST_CODE_MY_PICK);
        }
    }

    public void doSocialShare(String url, String title,String pdfurl, String msg){
        // First search for compatible apps with sharing (Intent.ACTION_SEND)
        List<Intent> targetedShareIntents = new ArrayList<Intent>();
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        // Set title and text to share when the user selects an option.
        /*
        shareIntent.putExtra(Intent.EXTRA_TITLE, title);
        shareIntent.putExtra(Intent.EXTRA_TEXT, url);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        */
        Bundle b=new Bundle();
        b.putString("title",title);
        b.putString("url",url);
        b.putString("pdfurl",pdfurl);
        b.putString("msg",msg);

        shareIntent.putExtra("b1",b);

        List<ResolveInfo> resInfo = getActivity().getPackageManager().queryIntentActivities(shareIntent, 0);
        if (!resInfo.isEmpty()) {
            for (ResolveInfo info : resInfo) {
                Intent targetedShare = new Intent(Intent.ACTION_SEND);
                targetedShare.setType("text/plain"); // put here your mime type
                targetedShare.setPackage(info.activityInfo.packageName.toLowerCase());
                targetedShareIntents.add(targetedShare);
            }
            // Then show the ACTION_PICK_ACTIVITY to let the user select it
            Intent intentPick = new Intent();
            intentPick.setAction(Intent.ACTION_PICK_ACTIVITY);
            // Set the title of the dialog
            intentPick.putExtra(Intent.EXTRA_TITLE, "Share with ");
            intentPick.putExtra(Intent.EXTRA_INTENT, shareIntent);
            intentPick.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray());
            // Call StartActivityForResult so we can get the app name selected by the user
            this.startActivityForResult(intentPick, REQUEST_CODE_MY_PICK_pdf);
        }
    }
    int REQUEST_CODE_MY_PICK=2020;
    int REQUEST_CODE_MY_PICK_pdf=2021;


    // Saved Notification Actions
    @Override
    public void onPdfClicked(BeConNotification notification) {
        showPdfProgressDialog();


        notification.getPdf(getActivity(), new FileLoadedListener() {
            @Override
            public void onFileLoaded(@Nullable File pdfFile) {
                hidePdfProgressDialog();

                if (pdfFile != null) {
                    showPdfViewerChooser(pdfFile);
                } else {
                    showErrorPdfMessage();
                }
            }
        });
    }

    private void showPdfViewerChooser(File pdfFile) {
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(pdfFile), "application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getActivity(), R.string.please_install_pdf_viewer, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onWebClicked(BeConNotification notification) {
        String url = notification.getUrl();
        ((BeConApp) getActivity().getApplication()).openWebPage(getActivity(), url);
    }

    @Override
    public void onDeleteClicked(final BeConNotification notification) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setTitle("");
        builder.setMessage("Do you really want to delete the content?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                deleteContent(notification);
            }
        });
        builder.setNegativeButton("No",null);
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {
            }

        });
        builder.show();

    }

    private void deleteContent(BeConNotification notification) {

        File pdfFile = new File(notification.getPdfFilePath(getActivity()));
        if (pdfFile.exists()) {
            pdfFile.delete();
        }

        SavedNotification.deleteSavedNotification(notification, currentUser, new DeleteCallback() {
            @Override
            public void done(ParseException e) {
                loadSavedNotifications();
            }
        });
    }
    private void deleteNotificationPdfFile(BeConNotification notification) {
        File pdfFile = new File(notification.getPdfFilePath(getActivity()));
        if (pdfFile.exists()) {
            pdfFile.delete();
        }
    }

    private void showLoadingDialog() {
        loadingDialog.show();
    }

    private void hideLoadingDialog() {
        loadingDialog.dismiss();
    }

    private void showPdfProgressDialog() {
        pdfProgressDialog.show();
    }

    private void hidePdfProgressDialog() {
        pdfProgressDialog.dismiss();
    }

    private void showErrorMessage() {
       // Toast.makeText(getActivity(), R.string.error_loading_content, Toast.LENGTH_LONG).show();
        savedNotificationsListView.setVisibility(View.GONE);
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
        txtNoContent.setTypeface(lightTypeface);
        txtNoContent.setVisibility(View.VISIBLE);
    }

    private void showErrorPdfMessage() {
        Toast.makeText(getActivity(), R.string.pdf_error, Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onSaveClicked(BeConNotification notification) {

    }
}
