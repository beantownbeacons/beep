package com.digitalwavefront.beepetc.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.adapters.ExhibitorsAdapter;
import com.digitalwavefront.beepetc.interfaces.ExhibitorsLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Exhibitor;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ExhibitorsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ExhibitorsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExhibitorsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ListView exhibitorListView;
    private ProgressDialog progressDialog;

    private List<Exhibitor> exhibitor_list,exhibitor_list2;
    private OnFragmentInteractionListener mListener;
    private ExhibitorsAdapter exhibitorsAdapter;
    HashMap<String, Integer> exRatingHash;
    private TextView txtNoExhibotor;
    LinearLayout lay_moreitems;
    int counter=0;

    public static ExhibitorsFragment newInstance() {
        return new ExhibitorsFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_exhibitors, container, false);
        //speakerListView = (ListView) rootView.findViewById(R.id.speaker_list_view);
        exhibitorListView = (ListView) rootView.findViewById(R.id.exhibitor_list_view);

        View footerview=((LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.more_items,null,false);
        lay_moreitems= (LinearLayout) footerview.findViewById(R.id.lay_moreitems);
        lay_moreitems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                copyexhibitors(counter);
                exhibitorsAdapter.refreshlist(exhibitor_list2);
            }
        });
        exhibitorListView.addFooterView(footerview);
        exhibitorListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                exhibitorsAdapter.toggleItem(position);
            }
        });

        txtNoExhibotor = (TextView) rootView.findViewById(R.id.txt_no_exhibitors);
        txtNoExhibotor.setVisibility(View.GONE);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(R.string.loading_speakers);
        progressDialog.setMessage(getString(R.string.one_moment_please));

        Fabric.with(getActivity().getApplicationContext(), new Crashlytics());

        //logUser();

        loadExhibitors();
        return rootView;
    }

    // to identify user in crashlytics
    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods

        BeConUser user = BeConUser.getCurrentUser();
        Crashlytics.setUserIdentifier(user.getObjectId());
        Crashlytics.setUserName(user.getUserName());
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

   /* @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void loadExhibitors() {

        final BeConUser currentUser = BeConUser.getCurrentUser();
        String eventId = currentUser.getCurrentEventId();

        Exhibitor.getExhibitorByEventId(eventId, new ExhibitorsLoadedListener() {
            @Override
            public void onExhibitorsLoaded(@Nullable List<Exhibitor> exhibitors) {

                if (exhibitors != null && exhibitors.size() != 0) {
                    exhibitor_list = new ArrayList<Exhibitor>();
                    exhibitor_list2 = new ArrayList<Exhibitor>();
                    for (int i = 0; i < exhibitors.size(); i++) {
                        exhibitor_list.add(exhibitors.get(i));
                    }
                    /*exRatingHash = new HashMap<String, Integer>(){};

                    for (int i = 0; i < exhibitors.size(); i++) {
                        exhibitor_list.add(exhibitors.get(i));

                        ParseObject exObj = exhibitors.get(i);
                        exRatingHash.put(exObj.getObjectId(), 0);

                        ParseQuery<ExhibitorRating> query = ParseQuery.getQuery(ExhibitorRating.class);
                        query.whereEqualTo("ExhibitorId", exObj.getObjectId());
                        query.whereEqualTo("UserId", currentUser.getObjectId());
                        query.findInBackground(new FindCallback<ExhibitorRating>() {
                            @Override
                            public void done(List<ExhibitorRating> list, ParseException e) {

                                if (list != null && list.size() > 0) {
                                    ParseObject exhibitorRating = list.get(0);
                                    if (exhibitorRating != null) {
                                        exRatingHash.put(exhibitorRating.getString("ExhibitorId"), exhibitorRating.getInt("Rating"));
                                        exhibitorsAdapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        });

                    }*/

                    Log.e("exhibitors", "exhibitors size: " + exhibitors.size());
                    //exhibitor_list = exhibitors;
                    copyexhibitors(0);
                    setlistview();

                } else {
                    showNoExhibitorsMessage();
                }
            }
        });

    }

    private void setlistview() {
        exhibitorsAdapter = new ExhibitorsAdapter(getActivity(), getActivity(), exhibitor_list2);
        exhibitorListView.setAdapter(exhibitorsAdapter);
        exhibitorsAdapter.notifyDataSetChanged();
    }

    private void showNoExhibitorsMessage() {

        //  Toast.makeText(getActivity(), R.string.no_exhibitors, Toast.LENGTH_SHORT).show();
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
        txtNoExhibotor.setVisibility(View.VISIBLE);
        exhibitorListView.setVisibility(View.GONE);
        txtNoExhibotor.setTypeface(lightTypeface);
        txtNoExhibotor.setText(getResources().getString(R.string.no_exhibitors));
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.exhibitors));
        //  NavigationItemPositionManager navigationItemPositionManager = ((BeConApp) getActivity().getApplication()).getNavigationItemPositionManager();
        //  ((MainActivity) getActivity()).setSelectedNavigationPosition(navigationItemPositionManager.getExhibitorPosition());
    }

    private void copyexhibitors(int cnt)
    {
        int limit1=cnt+10;
        int limit;
        if(limit1>exhibitor_list.size())
        {
            limit=exhibitor_list.size();
        }
        else
        {
            limit=limit1;
        }
        for(int c=cnt;c<limit;c++)
        {
            exhibitor_list2.add(exhibitor_list.get(c));
            counter++;
        }

        if (counter >= exhibitor_list.size()) {
            lay_moreitems.setVisibility(View.GONE);
        }
    }
}
