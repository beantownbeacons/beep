package com.digitalwavefront.beepetc.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.LoginActivity;
import com.digitalwavefront.beepetc.utils.StringUtils;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.utils.ViewUtils;
import com.gc.materialdesign.views.ButtonRectangle;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Praneet on 26/08/2016.
 */
public class CodeVerificationFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {

    ButtonRectangle verify;
    TextInputLayout verify_layout;
    TextInputEditText verify_text;
    LinearLayout root_layout;
    SharedPreferences sharedpreferences;
    String Phoneno;
    TextView cancel;

    public static CodeVerificationFragment newInstance() {  return new CodeVerificationFragment(); }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_code_verification, container, false);

        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());

        root_layout = (LinearLayout) rootView.findViewById(R.id.verify_layout);
        verify_layout = (TextInputLayout) rootView.findViewById(R.id.verify_textlayout);
        verify_text = (TextInputEditText) rootView.findViewById(R.id.verify_field);
        verify_layout.setTypeface(lightTypeface);
        verify_text.setTypeface(lightTypeface);
        ViewUtils.tintViews(getActivity(), android.R.color.white, verify_text);
        verify = (ButtonRectangle) rootView.findViewById(R.id.btn_verify);
        verify.setOnClickListener(this);
        cancel=(TextView) rootView.findViewById(R.id.textView_cancel);
        cancel.setPaintFlags(cancel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        cancel.setOnClickListener(this);
        /*Bundle bn=this.getArguments();
        if(bn!=null){
         String phonenumber=bn.getString("Phno");
            Log.d("phno",phonenumber);
            *//*Toast.makeText(getActivity(), phonenumber, Toast.LENGTH_LONG).show();*//*
        }*/
        sharedpreferences = getActivity().getSharedPreferences("Mypref", Context.MODE_PRIVATE);
         Phoneno = sharedpreferences.getString("phno", null);
        if(Phoneno!=null){
            //Toast.makeText(getActivity(), Phoneno, Toast.LENGTH_SHORT).show();
            SharedPreferences.Editor editor = sharedpreferences.edit();
           // editor.clear().commit();

        }

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_verify:
            {
                callparseclould();
               // showResetFragment();
            }
            break;

            case R.id.textView_cancel:
            Toast.makeText(getActivity(), "Your password change request was cancelled.", Toast.LENGTH_SHORT).show();
            showLoginFragment();
            break;
        }

    }

    private void showLoginFragment() {
        ((LoginActivity) getActivity()).showLoginFragment();
    }

    private void showResetFragment() {
        ((LoginActivity) getActivity()).showResetFragment();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()){
            case R.id.verify_layout:
                root_layout.requestFocus();
                StringUtils.hideSoftKeyboard(getActivity());
                break;
        }
        return false;
    }
    public void callparseclould(){
       String code= verify_text.getText().toString();
        if(code.length()==0) {

            Toast.makeText(getActivity(), R.string.empty_fields, Toast.LENGTH_LONG).show();

        }
        else{
            Map<String, String> params = new HashMap<String, String>();
            params.put("phoneVerificationCode",code);
            params.put("phoneNumber",Phoneno);
            ParseCloud.callFunctionInBackground("validateUserPhoneVerificationCode", params, new FunctionCallback<Object>() {
                private String msg;
                private String code;

                @Override
                public void done(Object objects, ParseException exc) {
                    if (exc == null) {
                        String result = "";
                        result = objects.toString();
                        System.out.println(result);
                        Toast.makeText(getActivity(), "Phone number verified!", Toast.LENGTH_LONG).show();
                        showResetFragment();

                    } else {
                        System.out.println(exc);
                        Toast.makeText(getActivity(), "Enter valid verification code", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }
}
