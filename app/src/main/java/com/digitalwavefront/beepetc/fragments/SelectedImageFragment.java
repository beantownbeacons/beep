package com.digitalwavefront.beepetc.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.EventGallery;
import com.digitalwavefront.beepetc.parse.models.EventGalleryLikes;
import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by Praneet on 30/06/2016.
 */

/**
 * Updated by Tarul on 08/10/2016
 */
public class SelectedImageFragment extends android.support.v4.app.Fragment {

    int finalHeight, finalWidth;
    ImageView selectedImage, likeImage;
    ProgressDialog progress;
    LinearLayout linearLayout;
    TextView userName;
    String pictureId;
    BeConUser currentUser = BeConUser.getCurrentUser();
    String userId = currentUser.getObjectId();
    Dialog dialog;
    EventGallery current_position;

    ProgressDialog progressDialog;

    public SelectedImageFragment() {
    }

    public static SelectedImageFragment newInstance() {
        return new SelectedImageFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_selected_image, container, false);
        Log.e("gaurav", "in SelectedImageFragment");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //  dialog = super.onCreateDialog(savedInstanceState);
        dialog = new Dialog(getActivity());
        dialog.setTitle(R.string.loading_image);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Downloading image please wait...");
        selectedImage = (ImageView) rootView.findViewById(R.id.imageView);
        linearLayout = (LinearLayout) rootView.findViewById(R.id.ll);
        ViewTreeObserver vto = selectedImage.getViewTreeObserver();
        userName = (TextView) rootView.findViewById(R.id.username);
        likeImage = (ImageView) rootView.findViewById(R.id.imagelike);


        currentUser = BeConUser.getCurrentUser();
        userId = currentUser.getObjectId();


        setLargeImage();
        return rootView;
    }

    private void setLargeImage() {
        pictureId = current_position.getObjectId();

        String imageURL = current_position.getImageUrl();

        String user = current_position.getUploaderName();

        userName.setText("@" + user);

        if(current_position.largebitmap!=null)
        selectedImage.setImageBitmap(current_position.largebitmap);
        else {
            new asynclargebitmap().execute(current_position.getImageUrl());
        }

        likeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                likeImage.setImageResource(R.drawable.heart_big);

                EventGalleryLikes eventGalleryLikes = new EventGalleryLikes();
                eventGalleryLikes.setUserId(userId);
                eventGalleryLikes.setPictureId(pictureId);
                eventGalleryLikes.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        Log.e("Praneet", "" + e);
                        Log.e("Praneet", "" + userId);
                        Log.e("Praneet", "" + pictureId);
                    }
                });
                likeImage.setEnabled(false);



                //  EventGallery eventGallery
                ParseQuery<EventGallery> qry = ParseQuery.getQuery(EventGallery.class);
                qry.whereEqualTo("objectId", pictureId);
                Log.e("Praneet", "pictureid:::" + pictureId);
                qry.findInBackground(new FindCallback<EventGallery>() {
                    @Override
                    public void done(List<EventGallery> galleryObjects, ParseException e) {
                        Log.e("Praneet", "Praneet" + e);
                        Log.e("Praneet", "Praneet" + galleryObjects.toString());
                        EventGallery eventGallery = (EventGallery) galleryObjects.get(0);
                        Log.e("Praneet", "praneet" + eventGallery);
                        int likeCount = eventGallery.getLikeCount();
                        likeCount = likeCount + 1;
                        eventGallery.setLikeCount(likeCount);
                        eventGallery.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                Log.e("Praneet", "Like count incremented" + e);
                            }
                        });
                    }


                });
            }
        });

        setLikeImageResource();
    }


    void setLikeImageResource() {
        Log.e("Praneet", "setLikeImageResource pictureId " + pictureId);
        Log.e("Praneet", "setLikeImageResource userId " + userId);
        ParseQuery<EventGalleryLikes> query = ParseQuery.getQuery(EventGalleryLikes.class);
        query.whereEqualTo("pictureId", pictureId);
        query.whereEqualTo("userId", userId);
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int count, ParseException e) {
                Log.e("Praneet", "setLikeImageResource" + e);
                Log.e("Praneet", "setLikeImageResource count" + count);
                if (count != 0) {
                    likeImage.setImageResource(R.drawable.heart_big);
                    dialog.dismiss();
                    likeImage.setEnabled(false);
                } else {
                    likeImage.setImageResource(R.drawable.heart_big_unsel);
                    dialog.dismiss();
                    likeImage.setEnabled(true);
                }
            }
        });

    }

    public void refreshSelectedImage(EventGallery current)
    {
        this.current_position = current;
        setLargeImage();

    }
    public void setPosition(EventGallery current_position) {
        this.current_position = current_position;
    }

    class asynclargebitmap extends AsyncTask<String,Void,Bitmap>
    {

        @Override
        protected Bitmap doInBackground(String... lists) {

            Bitmap bmp=null;
            try {


                URL url = new URL(lists[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap bitmap = BitmapFactory.decodeStream(input);
                current_position.largebitmap = bitmap;

                System.out.println(url+" downloaded ");


            } catch (IOException e) {
                // Log exception
                System.out.println("exception downloading "+e);
            }
            return bmp;
        }

        protected void onPostExecute(Bitmap bmp) {


            selectedImage.setImageBitmap(current_position.largebitmap);
            try {

            }catch (Exception e){}



        }

    }
}

