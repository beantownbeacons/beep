package com.digitalwavefront.beepetc.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.adapters.ViewPagerAdapter;
import com.digitalwavefront.beepetc.parse.models.EventGallery;

import java.util.List;

/**
 * Created by Tarul on 08/10/2016.
 */
public class ViewPagerFragment extends android.support.v4.app.Fragment implements ViewPager.OnPageChangeListener {

    ViewPager viewPager;
    ViewPagerAdapter pagerAdapter;
    static List<EventGallery> galleries;
    static int position;

    public ViewPagerFragment() {

    }

    /*
    public ViewPagerFragment(List<EventGallery> galleries, int position) {
        this.galleries = galleries;
        this.position = position;
    }
    */

    /*public static ViewPagerFragment newInstance(List<EventGallery> galleries) {
        this.galleries=galleries;
        return new ViewPagerFragment();
    }*/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_view_pager, container, false);

        viewPager = (ViewPager) rootView.findViewById(R.id.view_pager);
        pagerAdapter = new ViewPagerAdapter(getFragmentManager(), galleries);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOnPageChangeListener(this);
        viewPager.setCurrentItem(position);

        return rootView;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        Log.e("OnPageScrolled", "" + position);
    }

    @Override
    public void onPageSelected(int position) {
        this.position = position;
        Log.e("ViewPager", "Position" + position);
        //viewPager.setCurrentItem(position);
        viewPager.setOffscreenPageLimit(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
