package com.digitalwavefront.beepetc.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseTwitterUtils;
import com.parse.ParseUser;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.LoginActivity;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.utils.StringUtils;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.utils.ViewUtils;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
//import java.security.Signature;
import java.util.ArrayList;
import java.util.List;

import static com.twilio.client.impl.useragent.AndroidDevices.getDeviceName;


/**
 * A simple {@link Fragment} subclass.
 */
/*
 * Updated by Tarul on 10/26/16.
 */
public class LoginFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {
    private static final String TAG = "LoginFragment";

    private TextView titleText, btnreg, tv_frgtpassword;
    private TextInputEditText emailField, passwordField;
    /*
    private ButtonRectangle loginButton;
    private Button loginButtonfb;
    private Button loginButtontw;
    private Button linkedinlogin;
    */

    private Button loginButton;
    private LinearLayout loginButtonfb;
    private LinearLayout loginButtontw;
    private LinearLayout linkedinlogin;

    private ProgressDialog progressDialog;
    private TextInputLayout email_layout, password_layout;
    private RelativeLayout root_layout;
    public String deviceDetails, os, screen;
    public static final List<String> permissions = new ArrayList<String>() {{
        add("public_profile");
        add("email");
        //add("name");
        //add("picture");
    }};

    private boolean LIFlag = false, FBFlag = false;
    private String linkedinProfURL;

    private Target target;
    public LoginFragment() {
        // Required empty public constructor


    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        return model;
    }

    public static String getScreenSize(Activity activity) {

        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        //display.getSize(size);


        int height = display.getHeight();
        int width = display.getWidth();

        //int width = size.x;
        //int height = size.y;

        String screenSize = width + "x" + height;
        return screenSize;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);

        deviceDetails = getDeviceName();
        os = Build.VERSION.RELEASE;
        screen = getScreenSize(getActivity());

        root_layout = (RelativeLayout) rootView.findViewById(R.id.login_layout);
        titleText = (TextView) rootView.findViewById(R.id.title_text);

        email_layout = (TextInputLayout) rootView.findViewById(R.id.login_email_textlayout);
        password_layout = (TextInputLayout) rootView.findViewById(R.id.login_password_textlayout);

        emailField = (TextInputEditText) rootView.findViewById(R.id.email_field);
        passwordField = (TextInputEditText) rootView.findViewById(R.id.password_field);
        loginButton = (Button) rootView.findViewById(R.id.login_btn);
        loginButton.setOnClickListener(this);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(R.string.logging_in);
        progressDialog.setMessage(getString(R.string.one_moment_please));
        progressDialog.setCanceledOnTouchOutside(false);

        root_layout.setOnTouchListener(this);

        // Set fonts
        Typeface boldTypeface = TypefaceUtils.getBoldTypeface(getActivity());
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());

        titleText.setTypeface(boldTypeface);
        email_layout.setTypeface(lightTypeface);
        password_layout.setTypeface(lightTypeface);
        emailField.setTypeface(lightTypeface);
        passwordField.setTypeface(lightTypeface);


        ParseTwitterUtils.initialize("wOhJOYlwke0kgSdhMTKiUrz3R", "FaFhm9p0byv3SBAdyE0Dalnc2adbQHxKF9StGiAEbN22dNA68g");

        // Tint fields
        ViewUtils.tintViews(getActivity(), android.R.color.white, emailField, passwordField);
        linkedinlogin = (LinearLayout) rootView.findViewById(R.id.linkedin_login);
        linkedinlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login(view);
            }
        });
        loginButtonfb = (LinearLayout) rootView.findViewById(R.id.fb_login);
        //loginButtonfb.setFragment(this);
        loginButtonfb.setOnClickListener(this);

        loginButtontw = (LinearLayout) rootView.findViewById(R.id.twitter_login_button);
        loginButtontw.setOnClickListener(this);

        btnreg = (TextView) rootView.findViewById(R.id.btnreg);
        btnreg.setPaintFlags(btnreg.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        btnreg.setOnClickListener(this);

        tv_frgtpassword = (TextView) rootView.findViewById(R.id.tv_forgot);
        tv_frgtpassword.setPaintFlags(tv_frgtpassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tv_frgtpassword.setOnClickListener(this);
        /*loginButtontw.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls
            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
            }
        });*/


        // Add code to print out the key hash
        try {
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(
                    "com.digitalwavefront.beepetc",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_btn:
                attemptLogin();
                break;

            case R.id.fb_login: {
                FBFlag = true;
                Log.d("Facebooklogin", "Facebook!");
                ParseFacebookUtils.logInWithReadPermissionsInBackground(getActivity(), permissions, new LogInCallback() {
                    @Override
                    public void done(ParseUser user, ParseException err) {
                        if (user == null) {
                            Log.d("MyApp", "Uh oh. The user cancelled the Facebook login. ===== "+err.getLocalizedMessage());
                            Toast.makeText(getActivity(), "Error logging through Facebook. Please try again later", Toast.LENGTH_SHORT).show();
                        } else if (user.isNew()) {
                            Log.d("MyApp", "User signed up and logged in through Facebook!");


                            user.put("isVerified", true);
                            user.saveInBackground();

                            getUserFBDetails();

                            updateDeviceDetails();


                            //showEventLoginFragment();


                        } else {
                            Log.d("MyApp", "User logged in through Facebook!");
                            getUserFBDetails();
                            updateDeviceDetails();
                            showEventLoginFragment();
                        }
                    }
                });

            }
            break;

            case R.id.twitter_login_button: {
                Log.d("Twitterlogin", "Twitter!");
                ParseTwitterUtils.logIn(getActivity(), new LogInCallback() {
                    @Override
                    public void done(ParseUser user, ParseException err) {
                        if (user == null) {
                            Log.d("MyApp", "Uh oh. The user cancelled the Twitter login.");
                            // Toast.makeText(getActivity(),err.toString(),Toast.LENGTH_LONG).show();
                        } else if (user.isNew()) {
                            Log.d("MyApp", "User signed up and logged in through Twitter!");
                            //showEventLoginFragment();

                            String name = ParseTwitterUtils.getTwitter().getScreenName();
                            Log.d("MyAppTw", "name : " + name);

                            user.put("name", name);
                            user.put("seed", false);
                            user.put("isVerified", true);
                            user.put("deviceDetails", deviceDetails);
                            user.put("deviceOS", os);
                            user.put("screenSize", screen);
                            user.saveInBackground();

                            showProgressDialog();
                            new twinfo().execute(new String[]{});
                            //showEventLoginFragment();

                            updateDeviceDetails();
                            //showEventLoginFragment();


                        } else {
                            Log.d("MyApp", "User logged in through Twitter!");
                            showProgressDialog();
                            new twinfo().execute(new String[]{});
                            updateDeviceDetails();
                            //showEventLoginFragment();
                        }
                    }
                });
            }
            break;

            case R.id.btnreg: {
                showRegFragment();
            }
            break;

            case R.id.tv_forgot: {
                showForgotFragment();
            }
            break;

        }
    }

    private void getUserFBDetails(){

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        // Application code
                        try {
                            Log.d("in callback", "callback");
                            //showEventLoginFragment();
                            String name = "", email = "", fbId = "", picurl = "";
                            if (object.getString("name") != null) {
                                name = object.getString("name");
                                Log.e("FBName", ""+name);
                            }

                            if (object.getString("email") != null) {
                                email = object.getString("email");
                                Log.e("FBEmail", ""+email);
                            }

                            if (object.getString("id") != null) {
                                fbId = object.getString("id");
                                Log.e("FBId", ""+fbId);
                            }

                            if (object.getJSONObject("picture").getJSONObject("data").getString("url") != null) {
                                picurl = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                Log.e("FBPicture", ""+picurl);
                            }
                            addFBUserInfo(name, email, fbId, picurl);
                        } catch (Exception e) {

                        }
                    }
                });


        Bundle parameters = new Bundle();
        parameters.putString("fields", "email,name,picture");
        request.setParameters(parameters);
        Log.d("before executeAsyync", "executeAsyync");
        request.executeAsync();
    }


    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS);
    }
    // Authenticate with linkedin and intialize Session.

    public void login(View view){
        LIFlag = true;
        LISessionManager.getInstance(getActivity())
                .init(getActivity(), buildScope(), new AuthListener() {
                    @Override
                    public void onAuthSuccess() {
                        System.out.println("Linkedin auth success ");
                        /*
                        Toast.makeText(getActivity(), "success" +
                                        LISessionManager
                                                .getInstance(getActivity())
                                                .getSession().getAccessToken().toString(),
                                Toast.LENGTH_LONG).show();
                        */
                    }

                    @Override
                    public void onAuthError(LIAuthError error) {
                        System.out.println("Linkedin auth error ");
                        Toast.makeText(getActivity(), "failed "
                                        + error.toString(),
                                Toast.LENGTH_LONG).show();
                    }
                }, true);
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
       // super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);

        LISessionManager.getInstance(getActivity()).onActivityResult(getActivity(),requestCode, resultCode, data);
        System.out.println("on activity result ");
        if (!FBFlag) {
            getLinkedinDetails();
            updateDeviceDetails();
        }
        //showEventLoginFragment();
    }

    private void getLinkedinDetails()
    {
        System.out.println("getting Linkedin details ");
        String url = "https://api.linkedin.com/v1/people/~:(id,email-address,formatted-name,phone-numbers,public-profile-url,picture-url)";

        APIHelper apiHelper = APIHelper.getInstance(getActivity());
        apiHelper.getRequest(getActivity(), url, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse) {

                System.out.println("Linkedin api response success "+apiResponse.getResponseDataAsString());
                addLinkedinUserInfo(apiResponse.getResponseDataAsJson());
            }

            @Override
            public void onApiError(LIApiError LIApiError) {
                System.out.println("Linkedin api error ");
            }
        });
    }

    private void addLinkedinUserInfo(JSONObject response) {


    try {

    final String email = response.get("emailAddress").toString();
    final String liId = response.get("id").toString();
    final String password = liId + liId;
    final String name = response.get("formattedName").toString();
    linkedinProfURL = response.getString("pictureUrl");
    final String linkedInProfile = response.get("publicProfileUrl").toString();
    final BeConUser user = new BeConUser();

    showProgressDialog();
    // Check that the user exists
    BeConUser.getUsersByUsername(liId, new FindCallback<BeConUser>() {

        @Override
        public void done(List<BeConUser> users, ParseException e) {

            hideProgressDialog();
            if (e == null) {

                if (users.isEmpty()) {

                    user.setName(name);user.put("seed", false);
                    user.setUsername(liId);
                    user.setPassword(password);
                    user.setIsVerified(true);
                    user.setLinkedInProfile(linkedInProfile);
                    user.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                saveLIProfileImg(linkedinProfURL,user);
                                showEventLoginFragment();
                            }else {
                                handleParseException(e);
                            }
                        }
                    });

                } else {

                    finishLogin(liId, password);
                }
            } else {
                handleParseException(e);
            }
        }
    });











}catch (Exception vc){System.out.println("Linkedin error "+vc);}
    }


    private void saveLIProfileImg(String picURL, ParseUser user1) {

        final ParseUser user = user1;
        Log.d("MyApp", "picURL = " + picURL);


        if (picURL.length() > 0) {

            Target target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {


                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    // Compress image to lower quality scale 1 - 100
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] image = stream.toByteArray();

                    Log.d("MyApp", "user.getObjectId() = " + user.getObjectId());

                    String filename = String.format("photo_%s.png", user.getObjectId());

                    final ParseFile file = new ParseFile(filename, image);

                    file.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                            Log.d("MyApp", "ParseFile saved");
                            if (e == null){
                                final BeConUser beConUser = BeConUser.getCurrentUser();
                                try {
                                    beConUser.setProfilephoto(file);
                                    beConUser.save();
                                }catch (Exception ex){}
                            }else {

                            }



                        }
                    });
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                    Log.d("MyApp", "onBitmapFailed.......");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                    Log.d("MyApp", "onPrepareLoad.......");

                }
            };
            Picasso.with(getActivity()).load(picURL).into(target);
        }

    }

    private void attemptLogin() {
        String email = emailField.getText().toString().trim();
        String password = passwordField.getText().toString().trim();

        if (fieldsValid(email, password)) {
            checkForEmailExistence(email, password);
        }
    }

    private boolean fieldsValid(String email, String password) {
        if (StringUtils.isNullOrEmpty(email, password)) {
            Toast.makeText(getActivity(), R.string.empty_fields, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private class twinfo extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... data) {

            //Log.d("doInBackground","data: "+data[0]);
            String url = "https://api.twitter.com/1.1/account/verify_credentials.json";
            HttpClient client = new DefaultHttpClient();
            HttpGet verifyGet = new HttpGet(url);
            ParseTwitterUtils.getTwitter().signRequest(verifyGet);

            String line = null;
            String strres = "";
            StringBuilder sb = new StringBuilder();
            try {
                HttpResponse response = client.execute(verifyGet);
                //String responseStr = EntityUtils.toString(response.getEntity());


                try {
                    BufferedReader reader =
                            new BufferedReader(new InputStreamReader(response.getEntity().getContent()), 65728);


                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }

                    strres = sb.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("Twitter response", "line : " + line);
            return strres;
        }

        @Override
        protected void onPostExecute(String result) {

            Log.e("Twitter response", "responseStr: " + result);
            String name = "", username = "", fbId = "", picurl = "";
            try {
                JSONObject jObj = new JSONObject(result);

                if (jObj.getString("name") != null) {
                    name = jObj.getString("name");
                }
                if (jObj.getString("screen_name") != null) {
                    username = jObj.getString("screen_name");
                }
                if (jObj.getString("profile_image_url_https") != null) {
                    picurl = jObj.getString("profile_image_url_https");
                }
            } catch (Exception e) {

            }
            hideProgressDialog();
            addTWUserInfo(name, username, picurl);

        }

    }

    private void checkForEmailExistence(final String email, final String password) {
        showProgressDialog();

        // Check that the user exists
        BeConUser.getUsersByEmail(email, new FindCallback<BeConUser>() {

            @Override
            public void done(List<BeConUser> users, ParseException e) {
                if (e == null) {

                    if (users.isEmpty()) {
                        handleEmailDoesNotExist();
                    } else {
                        finishLogin(email, password);
                    }
                } else {
                    handleParseException(e);
                }
            }
        });
    }

    private void finishLogin(String email, String password) {
        ParseUser.logInInBackground(email, password, new LogInCallback() {
            public void done(ParseUser user, ParseException e) {

                if (e == null) {
                    hideProgressDialog();
                    try {
                        if (linkedinProfURL != null) {
                            saveLIProfileImg(linkedinProfURL, user);
                        }
                    }catch (Exception ex ) {}

                    boolean userIsVerified = ((BeConUser) user).isVerified();

                    if (!userIsVerified) {
                        updateDeviceDetails();
                        Log.e("VerifyCode", "");
                        showVerificationCodeFragment();
                    } else {
                        updateDeviceDetails();
                        Log.e("EventLogin", "");

                        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            StringUtils.hideSoftKeyboard(getActivity());
                            //showLocationAccessFragment();
                        } else {
                            showEventLoginFragment();
                        }*/

                        //####### by Sarika ########
                        // don't need location settings screen
                        showEventLoginFragment();

                        /*
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            StringUtils.hideSoftKeyboard(getActivity());
                            //showLocationAccessFragment();
                        } else {
                            showEventLoginFragment();
                        }
                    */

                    }
                } else {
                    handleParseException(e);
                }
            }
        });
    }

    private void updateDeviceDetails() {

        final BeConUser beConUser = BeConUser.getCurrentUser();
        Log.e("CurrentBeconuser", "" + beConUser.getObjectId());

        ParseQuery<BeConUser> query = ParseQuery.getQuery(BeConUser.class);
        query.whereEqualTo("objectId", beConUser.getObjectId());
        query.findInBackground(new FindCallback<BeConUser>() {
            @Override
            public void done(List<BeConUser> objects, ParseException e) {
                if (e == null) {
                    //ParseObject object = objects.get(0);
                    BeConUser beConUser1 = objects.get(0);
                    Log.e("CurrentBeconuser", "" + beConUser1.getObjectId());
                    beConUser1.put("deviceDetails", deviceDetails);
                    beConUser1.put("deviceOS", os);
                    beConUser1.put("screenSize", screen);
                    beConUser1.put("seed", false);
                    try {
                        beConUser1.save();
                    }catch (ParseException ex) {

                    }

                }
            }
        });
    }

    private void handleParseException(ParseException e) {
        Log.e(TAG, "Error logging into parse: " + e.getMessage(), e);
        hideProgressDialog();
        Toast.makeText(getActivity(), R.string.error_logging_in, Toast.LENGTH_SHORT).show();
    }

    private void handleEmailDoesNotExist() {
        hideProgressDialog();
        Toast.makeText(getActivity(), R.string.email_does_not_exist, Toast.LENGTH_SHORT).show();
    }

    private void showProgressDialog() {
        progressDialog.show();
    }

    private void hideProgressDialog() {
        progressDialog.dismiss();
    }

    private void showVerificationCodeFragment() {
        ((LoginActivity) getActivity()).showVerificationCodeFragment(true);
    }

    private void showEventLoginFragment() {
        ((LoginActivity) getActivity()).showEventLoginFragment();
    }

    private void showLocationAccessFragment() {
        ((LoginActivity) getActivity()).showLocationAccessFragment();
    }

    private void showForgotFragment() {
        ((LoginActivity) getActivity()).showForgotPasswordFragment();
    }

    private void showRegFragment() {
        ((LoginActivity) getActivity()).showRegistrationFragment();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.login_layout:
                root_layout.requestFocus();
                StringUtils.hideSoftKeyboard(getActivity());
                break;
        }
        return false;
    }

    private void addFBUserInfo(String name, String email, String fbId, String picURL) {

        Log.d("MyApp", "addFBUserInfo");

        final ParseUser user = ParseUser.getCurrentUser();
        user.put("name", name);
        user.put("seed", false);
        user.put("email", email);
        user.put("username", fbId);
        user.put("isVerified", true);


        picURL = String.format("https://graph.facebook.com/%s/picture?type=large", fbId);
        Log.d("MyApp", "picURL = " + picURL);

        if (picURL.length() > 0) {

            Log.d("MyApp", "picURL.length() > 0");

            target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    // Compress image to lower quality scale 1 - 100
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] image = stream.toByteArray();

                    Log.d("MyApp", "user.getObjectId() = " + user.getObjectId());

                    String filename = String.format("photo_%s.png", user.getObjectId());
                    final ParseFile file = new ParseFile(filename, image);

                    file.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                            Log.d("MyApp", "ParseFile saved");
                            if (e == null) {
                                user.put("profilePhoto", file);
                                user.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        //showEventLoginFragment();
                                    }
                                });
                            }else {
                                //showEventLoginFragment();
                            }
                        }
                    });
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                    Log.d("MyApp", "onBitmapFailed.......");

                    user.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            //showEventLoginFragment();
                        }
                    });
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                    Log.d("MyApp", "onPrepareLoad.......");

                }
            };
            Picasso.with(getActivity()).load(picURL).into(target);
            showEventLoginFragment();
        }
        else  {
            user.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    showEventLoginFragment();
                }
            });

        }

    }

    private void addTWUserInfo(String name, String username, String picURL) {

        final ParseUser user = ParseUser.getCurrentUser();
        user.put("name", name);
        user.put("seed", false);
        user.put("username", username);
        user.put("twitterHandle", "@"+username);
        user.put("isVerified", true);


        Log.d("MyApp", "picURL = " + picURL);

        if (picURL.length() > 0) {

            Log.d("MyApp", "picURL.length() > 0");

            Target target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    // Compress image to lower quality scale 1 - 100
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] image = stream.toByteArray();

                    Log.d("MyApp", "user.getObjectId() = " + user.getObjectId());

                    String filename = String.format("photo_%s.png", user.getObjectId());
                    final ParseFile file = new ParseFile(filename, image);

                    file.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                            Log.d("MyApp", "ParseFile saved");

                            user.put("profilePhoto", file);
                            user.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    showEventLoginFragment();
                                }
                            });
                        }
                    });
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                    Log.d("MyApp", "onBitmapFailed.......");

                    user.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            showEventLoginFragment();
                        }
                    });
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                    Log.d("MyApp", "onPrepareLoad.......");

                }
            };
            Picasso.with(getActivity()).load(picURL).into(target);
        }
        else  {
            user.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    showEventLoginFragment();
                }
            });

        }

    }

    private void addLinkedUserInfo(String name, String username, String picURL) {

        final ParseUser user = ParseUser.getCurrentUser();
        user.put("name", name);
        user.put("seed", false);
        user.put("username", username);

        user.put("isVerified", true);


        Log.d("MyApp", "picURL = " + picURL);

        if (picURL.length() > 0) {

            Log.d("MyApp", "picURL.length() > 0");

            Target target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    // Compress image to lower quality scale 1 - 100
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] image = stream.toByteArray();

                    Log.d("MyApp", "user.getObjectId() = " + user.getObjectId());

                    String filename = String.format("photo_%s.png", user.getObjectId());
                    final ParseFile file = new ParseFile(filename, image);

                    file.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                            Log.d("MyApp", "ParseFile saved");

                            user.put("profilePhoto", file);
                            user.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    showEventLoginFragment();
                                }
                            });
                        }
                    });
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                    Log.d("MyApp", "onBitmapFailed.......");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                    Log.d("MyApp", "onPrepareLoad.......");

                }
            };
            Picasso.with(getActivity()).load(picURL).into(target);
        }

    }
}
