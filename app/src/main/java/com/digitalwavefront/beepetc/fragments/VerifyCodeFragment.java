package com.digitalwavefront.beepetc.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonRectangle;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.LoginActivity;
import com.digitalwavefront.beepetc.dialogs.EditTextDialog;
import com.digitalwavefront.beepetc.dialogs.EditTextDialog.EditTextDialogListener;
import com.digitalwavefront.beepetc.interfaces.UserVerificationListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.utils.StringUtils;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.utils.ViewUtils;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Wayne on 9/15/15.
 */
/*
 * Updated by Tarul on 10/26/16.
 */
public class VerifyCodeFragment extends Fragment implements View.OnClickListener, UserVerificationListener, View.OnTouchListener {
    private static final String TAG = "VerifyCodeFragment";

    private TextView phoneNumberTextView,titleText, resendCodeView, editPhoneTextView, signOutTextView;
    private TextInputEditText codeField;
    private ButtonRectangle verifyBtn;
    private ProgressDialog progressDialog;
    private TextInputLayout codefield_layout;
    private LinearLayout root_layout;

    public VerifyCodeFragment() {}

    public static VerifyCodeFragment newInstance() {
        return new VerifyCodeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_verify_code, container, false);
        root_layout = (LinearLayout) rootView.findViewById(R.id.verifycode_layout);
        phoneNumberTextView = (TextView) rootView.findViewById(R.id.phone_number_text_view);
        refreshPhoneNumber();
        titleText = (TextView) rootView.findViewById(R.id.title_text);
        resendCodeView = (TextView) rootView.findViewById(R.id.resend_code_view);
        resendCodeView.setPaintFlags(resendCodeView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        resendCodeView.setOnClickListener(this);
        codefield_layout = (TextInputLayout) rootView.findViewById(R.id.code_field_textlayout);
        editPhoneTextView = (TextView) rootView.findViewById(R.id.edit_phone_text_view);
        editPhoneTextView.setPaintFlags(editPhoneTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        editPhoneTextView.setOnClickListener(this);
        signOutTextView = (TextView) rootView.findViewById(R.id.sign_out_text_view);
        signOutTextView.setPaintFlags(signOutTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        signOutTextView.setOnClickListener(this);
        codeField = (TextInputEditText) rootView.findViewById(R.id.code_field);
        verifyBtn = (ButtonRectangle) rootView.findViewById(R.id.verify_btn);
        root_layout.setOnTouchListener(this);
        verifyBtn.setOnClickListener(this);
        progressDialog = new ProgressDialog(getActivity());

        // Set fonts
        Typeface boldTypeface = TypefaceUtils.getBoldTypeface(getActivity());
        Typeface regularTypeface = TypefaceUtils.getRegularTypeface(getActivity());
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());

        phoneNumberTextView.setTypeface(regularTypeface);
        titleText.setTypeface(boldTypeface);
        resendCodeView.setTypeface(boldTypeface);
        editPhoneTextView.setTypeface(boldTypeface);
        signOutTextView.setTypeface(boldTypeface);
        codefield_layout.setTypeface(lightTypeface);
        codeField.setTypeface(lightTypeface);
        // Tint fields
        ViewUtils.tintViews(getActivity(), android.R.color.white, codeField);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.verify_btn:
                verifyCode();
                break;
            case R.id.resend_code_view:
                ((LoginActivity) getActivity()).sendVerificationCode();
                break;
            case R.id.edit_phone_text_view:
                editPhoneNumber();
                break;
            case R.id.sign_out_text_view:
                signOut();
                break;
        }
    }

    private void editPhoneNumber() {
        String dialogTag = "editPhoneDialog";
        String dialogTitle = getString(R.string.edit_phone_dialog_title);
        String textToEdit = BeConUser.getCurrentUser().getPhoneNumber();
        String hint = getString(R.string.phone_number_hint);
        int inputType = InputType.TYPE_CLASS_PHONE;

        if (getFragmentManager().findFragmentByTag(dialogTag) == null) {
            EditTextDialog.newInstance(dialogTitle, textToEdit, hint, inputType, new EditTextDialogListener() {
                @Override
                public void onEdit(String phoneNumber) {
                    verifyPhoneNumber(phoneNumber);
                }
            }).show(getFragmentManager(), dialogTag);
        }
    }

    private void verifyPhoneNumber(String phoneNumber) {
        if (StringUtils.isValidPhoneNumber(phoneNumber)) {

            phoneNumber = StringUtils.convertToNumericPhoneNumber(phoneNumber);

            if (phoneNumber.equals(BeConUser.getCurrentUser().getPhoneNumber())) {
                Toast.makeText(getActivity(), R.string.phone_unchanged, Toast.LENGTH_SHORT).show();
            } else {
                updatePhoneNumber(phoneNumber);
            }
        } else {
            Toast.makeText(getActivity(), R.string.invalid_phone_number, Toast.LENGTH_SHORT).show();
        }
    }

    private void updatePhoneNumber(final String phoneNumber) {
        showProgressDialog(getString(R.string.updating_phone), getString(R.string.one_moment_please));
        final BeConUser currentUser = BeConUser.getCurrentUser();
        // Make sure phone number is not in use
        BeConUser.getVerifiedUsersByPhoneNumber(phoneNumber, new FindCallback<BeConUser>() {

            @Override
            public void done(List<BeConUser> users, ParseException e) {
                if (e == null) {

                    if (users.isEmpty()) {
                        // Phone number is not in use!

                        // TODO: Do not reset the username here
                        // currentUser.setUsername(phoneNumber);
                        currentUser.setPhoneNumber(phoneNumber);
                        currentUser.saveInBackground(new SaveCallback() {

                            @Override
                            public void done(ParseException e) {
                                hideProgressDialog();

                                if (e == null) {
                                    // Phone number updated successfully
                                    Toast.makeText(getActivity(), R.string.phone_update_success, Toast.LENGTH_SHORT).show();

                                    // Resend verification code
                                    ((LoginActivity) getActivity()).sendVerificationCode();

                                    // Refresh the UI
                                    refreshPhoneNumber();
                                } else {
                                    showUpdatePhoneErrorMessage();
                                }
                            }
                        });
                    } else {
                        hideProgressDialog();
                        Toast.makeText(getActivity(), R.string.phone_number_taken, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e(TAG, "ParseException: " + e.getMessage());
                    hideProgressDialog();
                    showUpdatePhoneErrorMessage();
                }
            }
        });
    }

    private void showUpdatePhoneErrorMessage() {
        Toast.makeText(getActivity(), R.string.error_updating_phone, Toast.LENGTH_SHORT).show();
    }

    private void refreshPhoneNumber() {
        try {
            Log.e("User getPhoneNumber", ""+BeConUser.getCurrentUser().getPhoneNumber());
            String phn = StringUtils.getFormattedPhoneNumber(getActivity(), BeConUser.getCurrentUser().getPhoneNumber());
            if (phn.equals("0")){
                phoneNumberTextView.setText(String.format("Phone: %s", getResources().getString(R.string.invalid_phone_number)));
            }else {
                phoneNumberTextView.setText(String.format("Phone: %s", phn));
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void signOut() {
        ParseUser.logOut();
        ((LoginActivity) getActivity()).showRegistrationFragment();
    }

    private void verifyCode() {
        String code = codeField.getText().toString().trim();

        if (StringUtils.isNullOrEmpty(code)) {
            showEmptyCodeMessage();
        } else {
            showProgressDialog(getString(R.string.verifying_code), getString(R.string.one_moment_please));

            HashMap<String, Object> params = new HashMap<>();
            params.put("verificationCode", Integer.valueOf(code));

            ParseCloud.callFunctionInBackground("verifyUser", params, new FunctionCallback<Boolean>() {

                @Override
                public void done(Boolean verified, ParseException e) {
                    if (e == null) {
                        if (verified) {
                            onUserVerified(BeConUser.getCurrentUser());
                        } else {
                            onVerificationInvalid(BeConUser.getCurrentUser());
                        }
                    } else {
                        showErrorMessage();
                        hideProgressDialog();
                    }
                }
            });
        }
    }

    private void showEventLoginFragment() {
        ((LoginActivity) getActivity()).showEventLoginFragment();
    }

    private void showProgressDialog(String title, String message) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    private void hideProgressDialog() {
        progressDialog.dismiss();
    }

    private void showErrorMessage() {
        Toast.makeText(getActivity(), R.string.error_verifying_code, Toast.LENGTH_SHORT).show();
    }

    private void showEmptyCodeMessage() {
        Toast.makeText(getActivity(), R.string.no_code_entered, Toast.LENGTH_SHORT).show();
    }

    // User verification listener
    @Override
    public void onUserVerified(BeConUser user) {
        user.setIsVerified(true);
        user.saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException e) {
                hideProgressDialog();
                if (e == null) {

                    /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        showLocationAccessFragment();
                    } else {
                        showEventLoginFragment();
                    }*/
                    showEventLoginFragment();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        showLocationAccessFragment();
                    } else {
                        showEventLoginFragment();
                    }

                } else {
                    showErrorMessage();
                    Log.e(TAG, e.getMessage());
                }
            }
        });
    }

    private void showLocationAccessFragment() {
        ((LoginActivity) getActivity()).showLocationAccessFragment();
    }

    @Override
    public void onVerificationInvalid(BeConUser user) {
        hideProgressDialog();
        Toast.makeText(getActivity(), R.string.invalid_verification_code, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()){
            case R.id.verifycode_layout:
                root_layout.requestFocus();
                StringUtils.hideSoftKeyboard(getActivity());
                break;
        }
        return false;
    }
}
