package com.digitalwavefront.beepetc.fragments;

import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalwavefront.beepetc.interfaces.SpeakersLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SwipeMenuCreator;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.adapters.MySessionsAdapter;
import com.digitalwavefront.beepetc.adapters.SpinnerAdapter;
import com.digitalwavefront.beepetc.interfaces.SessionsLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Session;
import com.digitalwavefront.beepetc.parse.models.Speaker;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.views.SwipeMenu;
import com.digitalwavefront.beepetc.views.SwipeMenuItem;
import com.digitalwavefront.beepetc.views.SwipeMenuListView;
import com.parse.ParseFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Wayne on 10/10/15.
 */
public class MyAgendaFragment extends Fragment {

    private static final int ALL_ROOM_FILTER_POSITION = 0;

    private BeConUser currentUser;
    private TextView roomFilterTitleText;
    private Spinner roomFilterSpinner;
   // private ListView agendaListView;
    private SwipeMenuListView agendaListView;
    private SpinnerAdapter roomSpinnerAdapter;
    private MySessionsAdapter sessionsAdapter;
    private ProgressDialog progressDialog;
    private  List<Session> session;
    public MyAgendaFragment() {}
    private HashMap<String, List<Session>> hash;
    private HashMap<String,Integer> spkIdDic;
    private HashMap<String,HashMap<String,ParseFile>> sessSpkDic;
    private HashMap<String,HashMap<String,String>> sessSpkNameDic;
    private List<Session> session_list;

    public static MyAgendaFragment newInstance() {
        return new MyAgendaFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentUser = BeConUser.getCurrentUser();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_agenda, container, false);
        roomFilterTitleText = (TextView) rootView.findViewById(R.id.room_filter_title_text);
        roomFilterSpinner = (Spinner) rootView.findViewById(R.id.room_filter_spinner);
        roomFilterSpinner.setOnItemSelectedListener(roomFilterItemSelectedListener);
        //agendaListView = (ListView) rootView.findViewById(R.id.agenda_list_view);
        agendaListView = (SwipeMenuListView) rootView.findViewById(R.id.agenda_list_view);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(R.string.loading_sessions);
        progressDialog.setMessage(getString(R.string.one_moment_please));

        agendaListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (session_list.get(position).getDetails() != null && session_list.get(position).getDetails().length() > 0) {
                    if ((session_list.get(position).getSpeakerId() != null && session_list.get(position).getSpeakerId().length() > 0) || (session_list.get(position).getDetails() != null)) {
                        sessionsAdapter.toggleItem(position);
                    }
                }
            }
        });

        // set empty view
        TextView emptyView = (TextView)rootView.findViewById(R.id.emptyviewId);
        agendaListView.setEmptyView(emptyView);
        // Set fonts
        Typeface boldTypeface = TypefaceUtils.getBoldTypeface(getActivity());
        roomFilterTitleText.setTypeface(boldTypeface);

        create_delete_option();

        agendaListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {
            @Override
            public void onSwipeStart(int position) {

            }

            @Override
            public void onSwipeEnd(int position) {

            }
        });

        getMySessions();
        return rootView;
    }

    private void create_delete_option() {

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem delete = new SwipeMenuItem(getActivity());
                //set item background

                delete.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                //set item width
                delete.setWidth(dp2px(90));
                // set a icon
                delete.setTitle("Remove");
                delete.setTitleColor(Color.WHITE);
                delete.setTitleSize(15);
                //add to menu
                menu.addMenuItem(delete);
            }
        };

        agendaListView.setMenuCreator(creator);
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.my_agenda));
      //  ((MainActivity) getActivity()).setSelectedNavigationPosition(NavigationItemPositionManager.NO_POSITION);
    }

    private void getMySessions() {
        // Show progress dialog
        showProgressDialog();

        sessSpkDic = new HashMap<String,HashMap<String,ParseFile>>();
        sessSpkNameDic = new HashMap<String,HashMap<String,String>>();
        spkIdDic = new HashMap<String,Integer>();

        currentUser.getRegisteredSessions(new SessionsLoadedListener() {

            @Override
            public void onSessionsLoaded(@Nullable final List<Session> sessions) {
                hideProgressDialog();

                /*
                if (sessions != null) {
                    session = sessions;
                    sessionsAdapter = new MySessionsAdapter(getActivity(), sessions);
                    agendaListView.setAdapter(sessionsAdapter);
                    setupRoomFilterSpinner(sessions);

                    agendaListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                            Log.e("current session","pos="+position+", sessions size="+sessions.size()+", "+sessions.get(position).getSpeakerId());
                            sessionsAdapter.removeSession(sessions.get(position));
                            //BeConUser.getCurrentUser().unregisterForSession(sessions.get(position), null);
                            return false;
                        }
                    });
                } else {

                    roomFilterTitleText.setVisibility(View.GONE);
                    roomFilterSpinner.setVisibility(View.GONE);
                    showErrorMessage();
                }
*/

                if (sessions != null && sessions.size()!=0) {

                    Log.e("Praneet","if");
                    /// session_list = sessions;
                    hash = new HashMap<String, List<Session>>();

                    for (int i = 0; i < sessions.size(); i++) {

                        final Session sessObj = sessions.get(i);

                        String sdt = sessions.get(i).FormattedStartDate();
                        Log.e("Praneet","strat date:: "+sdt);


                        //added by Sarika [3/5/2016]
                        spkIdDic.put(sessObj.getObjectId(), 0);

                        final HashMap<String, ParseFile> imgMap = new HashMap<String, ParseFile>();
                        final HashMap<String, String> nameMap = new HashMap<String, String>();
                        // get speaker ids
                        List<String> spkIds = sessions.get(i).getSpeakerIds();

                        if (spkIds != null) {
                            Log.e("spkIds", "spkIds: " + spkIds);
                            Speaker.getSpeakersByObjectIds(spkIds, new SpeakersLoadedListener() {
                                @Override
                                public void onSpeakersLoaded(@Nullable List<Speaker> speakers) {

                                    if (speakers != null) {
                                        String spkNames = "";
                                        Log.e("speakers", "speakers size: " + speakers.size());

                                        for (int j = 0; j < speakers.size(); j++) {

                                            final Speaker speaker = speakers.get(j);
                                            ParseFile avatarFile = speaker.getParseFile("avatar");

                                            imgMap.put(speaker.getObjectId(), avatarFile);
                                            nameMap.put(speaker.getObjectId(), speaker.getFullName());
                                            sessSpkDic.put(sessObj.getObjectId(), imgMap);
                                            sessSpkNameDic.put(sessObj.getObjectId(), nameMap);
                                            spkNames = spkNames + speaker.getFullName();
                                            if (j < speakers.size()-1){
                                                spkNames = spkNames + ",";
                                            }
                                            sessionsAdapter.notifyDataSetChanged();


                                        }
                                        sessObj.setSpkeakerNames(spkNames);
                                    }

                                }
                            });
                        }

                    }

                    session_list = sessions;
                    if (sessions != null) {
                        session_list = sessions;
                        sessionsAdapter = new MySessionsAdapter(getActivity(), getActivity(), sessions, sessSpkDic, spkIdDic, sessSpkNameDic);
                        agendaListView.setAdapter(sessionsAdapter);
                        sessionsAdapter.notifyDataSetChanged();
                        setupRoomFilterSpinner(sessions);

                        agendaListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                                Log.e("current session","pos="+position+", sessions size="+sessions.size()+", "+sessions.get(position).getSpeakerId());
                                sessionsAdapter.removeSession(sessions.get(position));
                                //BeConUser.getCurrentUser().unregisterForSession(sessions.get(position), null);
                                return false;
                            }
                        });
                    }

                } else {

                    roomFilterTitleText.setVisibility(View.GONE);
                    roomFilterSpinner.setVisibility(View.GONE);
                    //showErrorMessage();
                }
            }
        });
    }

    private void setupRoomFilterSpinner(List<Session> sessions) {
        List<String> roomTitles = new ArrayList<>();

        // Add the string for all
        roomTitles.add(getString(R.string.all));
        for (Session session : sessions) {
            if (session.getRoomName() != null && session.getRoomName().length() > 0) {
                roomTitles.add(session.getRoomName());
            }
        }

        if (roomTitles.size() > 0) {
            roomSpinnerAdapter = new SpinnerAdapter(getActivity(), roomTitles);
            roomFilterSpinner.setAdapter(roomSpinnerAdapter);
        } else {
          roomFilterSpinner.setVisibility(View.GONE);
        }
    }

    private void showProgressDialog() {
        progressDialog.show();
    }

    private void hideProgressDialog() {
        progressDialog.dismiss();
    }

    private void showErrorMessage() {
        Toast.makeText(getActivity(), R.string.error_loading_sessions, Toast.LENGTH_SHORT).show();
    }

    private AdapterView.OnItemSelectedListener roomFilterItemSelectedListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            if (position == ALL_ROOM_FILTER_POSITION) {
                sessionsAdapter.showAllSessions();
            } else {
                String roomTitle = roomSpinnerAdapter.getItem(position);
                sessionsAdapter.filterSessionsByRoomTitle(roomTitle);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };
}
