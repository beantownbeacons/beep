package com.digitalwavefront.beepetc.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalwavefront.beepetc.BeConApp;
import com.digitalwavefront.beepetc.activities.SessionInfoActivity;
import com.digitalwavefront.beepetc.interfaces.SwipeMenuCreator;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.adapters.SpeakersAdapter;
import com.digitalwavefront.beepetc.interfaces.SpeakerOptionClickListener;
import com.digitalwavefront.beepetc.interfaces.SpeakersLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Speaker;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.views.SwipeMenu;
import com.digitalwavefront.beepetc.views.SwipeMenuItem;
import com.digitalwavefront.beepetc.views.SwipeMenuListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wayne on 9/21/15.
 */

/**
 * Updated by Tarul on 9/8/16.
 */
public class SpeakersFragment extends Fragment implements SpeakerOptionClickListener {
    private static final String TAG = "SpeakersFragment";

    // private ListView speakerListView;
    private SwipeMenuListView speakerListView;
    private SpeakersAdapter speakersAdapter;
    private ProgressDialog progressDialog;
    LinearLayout lay_moreitems;
    TextView textView_no_attendees;
    View viewContainer;

    List<Speaker> speakers1=new ArrayList<>();
    List<Speaker> speakers2=new ArrayList<>();
    int counter=0;
    public SpeakersFragment() {
    }

    public static SpeakersFragment newInstance() {
        return new SpeakersFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_speakers, container, false);
        //speakerListView = (ListView) rootView.findViewById(R.id.speaker_list_view);
        speakerListView = (SwipeMenuListView) rootView.findViewById(R.id.speaker_list_view);

        speakerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (speakersAdapter != null) {
                    speakersAdapter.toggleItem(position);
                }
            }
        });

        View footerview=((LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.more_items,null,false);
        lay_moreitems= (LinearLayout) footerview.findViewById(R.id.lay_moreitems);
        lay_moreitems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("speakers1="+speakers1.size()+" speakers2="+speakers2.size()+" counter="+counter);
                copyspeakers(counter);

                speakersAdapter.refreshlist(speakers2);
            }
        });
        //commented by Sarika
        speakerListView.addFooterView(lay_moreitems);

        textView_no_attendees = (TextView) rootView.findViewById(R.id.txt_no_speaker);
        viewContainer = (View) rootView.findViewById(R.id.view_container);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(R.string.loading_speakers);
        progressDialog.setMessage(getString(R.string.one_moment_please));

        sessionInfo_menu_creator();

        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
        textView_no_attendees.setTypeface(lightTypeface);

        //loadSpeakers();
        loadspeakers1();

        return rootView;
    }

    private void sessionInfo_menu_creator() {
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {


                Log.e("SwipeMenuCreator: ", "getViewType : " + menu.getViewType());

                SwipeMenuItem sessioninfoItem = new SwipeMenuItem(getActivity());
                sessioninfoItem.setBackground(new ColorDrawable(Color.rgb(5, 188, 212)));
                //set item width
                sessioninfoItem.setWidth(dp2px(120));
                // set a icon
                sessioninfoItem.setTitle("Session Info");
                sessioninfoItem.setTitleColor(Color.WHITE);
                sessioninfoItem.setTitleSize(15);
                //add to menu
                menu.addMenuItem(sessioninfoItem);
            }
        };

        speakerListView.setMenuCreator(creator);
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.speakers));
        //   NavigationItemPositionManager navigationItemPositionManager = ((BeConApp) getActivity().getApplication()).getNavigationItemPositionManager();
        //   ((MainActivity) getActivity()).setSelectedNavigationPosition(navigationItemPositionManager.getSpeakersPosition());
    }

    @Override
    public void onLinkedInClicked(Speaker speaker) {
        String linkedInURL = speaker.getLinkedInURL();
        if (!linkedInURL.contains("linkedin.com/in")) {
            linkedInURL = "http://www.linkedin.com/in/" + linkedInURL;
        }
        showWebPage(linkedInURL);
    }

    @Override
    public void onTwitterClicked(Speaker speaker) {
        String twitterURL = speaker.getTwitterURL();
        if (!twitterURL.contains("twitter.com")) {
            if (twitterURL.startsWith("@")) {
                //twitterURL.replace("@", "/");
                twitterURL = twitterURL.replace("@", "/");
                twitterURL = "http://www.twitter.com" + twitterURL;
            } else {
                twitterURL = "http://www.twitter.com/" + twitterURL;
            }

        }
        showWebPage(twitterURL);
    }

    private void showWebPage(String url) {
        ((BeConApp) getActivity().getApplication()).openWebPage(getActivity(), url);
    }

    private void setlistview()
    {
        viewContainer.setVisibility(View.VISIBLE);
        speakerListView.setVisibility(View.VISIBLE);
        textView_no_attendees.setVisibility(View.GONE);

        speakersAdapter = new SpeakersAdapter(getActivity(), getActivity(), speakers2, SpeakersFragment.this);
        speakerListView.setAdapter(speakersAdapter);
        speakerListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {


                Intent sessioninfo = new Intent(getActivity(), SessionInfoActivity.class);
                sessioninfo.putExtra("speakerId", speakers2.get(position).getObjectId());
                sessioninfo.putExtra("fullname", speakers2.get(position).getFullName());
                startActivity(sessioninfo);
                getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                return false;
            }
        });
        speakerListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {
            @Override
            public void onSwipeStart(int position) {

            }

            @Override
            public void onSwipeEnd(int position) {

            }
        });
    }
    private void loadspeakers1()
    {
        BeConUser currentUser = BeConUser.getCurrentUser();
        String eventId = currentUser.getCurrentEventId();

        showProgressDialog();
        Speaker.getSpeakersByEventIdNew(eventId, new SpeakersLoadedListener() {

            @Override
            public void onSpeakersLoaded(@Nullable final List<Speaker> speakers) {


                if (speakers != null && speakers.size() != 0) {

                    speakers1 = new ArrayList<>();
                    speakers2 = new ArrayList<>();

                    speakers1=speakers;

                    copyspeakers(0);
                    setlistview();
                    hideProgressDialog();

                } else {
                    showNoSpeakersMessage();
                }
            }
        });
    }

    private void copyspeakers(int cnt)
    {
        int limit1=cnt+10;
        int limit;
        if(limit1>speakers1.size())
        {
            limit=speakers1.size();
        }
        else
        {
            limit=limit1;
        }
        for(int c=cnt;c<limit;c++)
        {
            speakers2.add(speakers1.get(c));
            counter++;
        }

        if (counter >= speakers1.size()) {
            lay_moreitems.setVisibility(View.GONE);
        }
    }
    private void loadSpeakers() {
        showProgressDialog();

        /*BeConUser currentUser = BeConUser.getCurrentUser();
        String eventId = currentUser.getCurrentEventId();
        Session.getSessionsByEventId(eventId, new SessionsLoadedListener() {

            @Override
            public void onSessionsLoaded(@Nullable List<Session> sessions) {
                if (sessions != null) {
                    if (sessions.isEmpty()) {
                        showNoSpeakersMessage();
                    } else {
                        List<String> speakerIds = new ArrayList<>();
                        for (Session session : sessions) {
                            speakerIds.add(session.getSpeakerId());
                        }

                        Speaker.getSpeakersByObjectIds(speakerIds, new SpeakersLoadedListener() {

                            @Override
                            public void onSpeakersLoaded(@Nullable final List<Speaker> speakers) {
                                if (speakers != null) {
                                    speakersAdapter = new SpeakersAdapter(getActivity(), getActivity(), speakers, SpeakersFragment.this);
                                    speakerListView.setAdapter(speakersAdapter);
                                    hideProgressDialog();

                                    speakerListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                                        @Override
                                        public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                                            Intent sessioninfo = new Intent(getActivity(), SessionInfoActivity.class);
                                            sessioninfo.putExtra("speakerId", speakers.get(position).getObjectId());
                                            sessioninfo.putExtra("fullname", speakers.get(position).getFullName());
                                            startActivity(sessioninfo);
                                            getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                                            return false;
                                        }
                                    });

                                } else {
                                    showErrorMessage();
                                }
                            }
                        });
                    }
                } else {
                    showErrorMessage();
                }
            }
        }, false);

        */


        BeConUser currentUser = BeConUser.getCurrentUser();
        String eventId = currentUser.getCurrentEventId();

        Speaker.getSpeakersByEventIdNew(eventId, new SpeakersLoadedListener() {

            @Override
            public void onSpeakersLoaded(@Nullable final List<Speaker> speakers) {

                /*
                ParseObject.pinAllInBackground(speakers, new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        //Speaker.getSpeakersLoc();
                    }
                });

                */

                if (speakers != null && speakers.size() != 0) {

                    viewContainer.setVisibility(View.VISIBLE);
                    speakerListView.setVisibility(View.VISIBLE);
                    textView_no_attendees.setVisibility(View.GONE);

                    speakersAdapter = new SpeakersAdapter(getActivity(), getActivity(), speakers, SpeakersFragment.this);
                    speakerListView.setAdapter(speakersAdapter);
                    hideProgressDialog();


                    speakerListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {


                            Intent sessioninfo = new Intent(getActivity(), SessionInfoActivity.class);
                            sessioninfo.putExtra("speakerId", speakers.get(position).getObjectId());
                            sessioninfo.putExtra("fullname", speakers.get(position).getFullName());
                            startActivity(sessioninfo);
                            getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            return false;
                        }
                    });

                } else {
                    showNoSpeakersMessage();
                }
            }
        });

        speakerListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {
            @Override
            public void onSwipeStart(int position) {

            }

            @Override
            public void onSwipeEnd(int position) {

            }
        });


    }

    private void showNoSpeakersMessage() {
        hideProgressDialog();
        //Toast.makeText(getActivity(), R.string.no_speakers, Toast.LENGTH_SHORT).show();
        viewContainer.setVisibility(View.GONE);
        speakerListView.setVisibility(View.GONE);
        textView_no_attendees.setVisibility(View.VISIBLE);

        textView_no_attendees.setText(getResources().getString(R.string.no_speakers));
    }

    private void showErrorMessage() {
        hideProgressDialog();
        Toast.makeText(getActivity(), R.string.error_loading_speakers, Toast.LENGTH_SHORT).show();
    }

    private void showProgressDialog() {
        progressDialog.show();
    }

    private void hideProgressDialog() {
        progressDialog.dismiss();
    }


    int cnt, limit=10,c;
    class async extends AsyncTask<String,String,Integer>
    {

        int cntr = 0;
        public async() {


        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Integer doInBackground(String... lists) {



            return cntr;
        }

        protected void onPostExecute(Integer res) {



        }

    }
}
