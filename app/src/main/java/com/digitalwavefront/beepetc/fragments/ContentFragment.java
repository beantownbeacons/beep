package com.digitalwavefront.beepetc.fragments;

import android.content.pm.ResolveInfo;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalwavefront.beepetc.BeConApp;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.SharedPref;
import com.digitalwavefront.beepetc.adapters.ContentAdapter;
import com.digitalwavefront.beepetc.interfaces.ContentLibraryActionListener;
import com.digitalwavefront.beepetc.interfaces.FileLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Content;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tarul on 08/09/2016.
 */
public class ContentFragment extends Fragment implements ContentLibraryActionListener {

    private ListView contentListView;
    private TextView txtNoContent;
    private ContentAdapter contentAdapter;
    private ProgressDialog loadingDialog;
    private ProgressDialog pdfProgressDialog;
    private ProgressDialog shareDialog;
    BeConUser currentUser;
    String eventId;
    SharedPref sp;
    private static final int REQUEST_SHARE=314;

    public static ContentFragment newInstance() {
        return new ContentFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        System.out.println("public");
        View rootView = inflater.inflate(R.layout.fragment_content, container, false);
        currentUser = BeConUser.getCurrentUser();
        eventId = currentUser.getCurrentEventId();
        Log.e("EventId", eventId);

        sp=new SharedPref(getActivity());

        contentListView = (ListView) rootView.findViewById(R.id.content_list_view);
        contentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                contentAdapter.toggleItem(position);
            }
        });

        /*
        contentListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                //onShare(contentAdapter.getItem(position));
                return true;
            }
        });
        */
        loadingDialog = new ProgressDialog(getActivity());
        loadingDialog.setTitle(R.string.loading_content);
        loadingDialog.setMessage(getString(R.string.one_moment_please));

        pdfProgressDialog = new ProgressDialog(getActivity());
        pdfProgressDialog.setTitle(R.string.loading_pdf);
        pdfProgressDialog.setMessage(getString(R.string.one_moment_please));

        shareDialog = new ProgressDialog(getActivity());
        shareDialog.setTitle("Loading...");
        shareDialog.setMessage(getString(R.string.one_moment_please));


        txtNoContent = (TextView) rootView.findViewById(R.id.text_no_content);
       // eventCode = getEventCode(eventId);
       // Log.e("BeforeLoad", eventCode);
        loadContent();
        return rootView;
    }

    private void loadContent() {
        ParseQuery<Content> query = ParseQuery.getQuery(Content.class);
        query.whereEqualTo("EventId", eventId);
        //Log.e("EventId", EventLoginFragment.EventCode);
        query.findInBackground(new FindCallback<Content>() {
            @Override
            public void done(List<Content> objects, ParseException e) {
                if (e == null) {
                    if (objects.size() > 0) {
                        for (int count = 0; count < objects.size(); count++) {
                            ParseObject object = objects.get(count);
                            object.getString("url");
                            object.getParseFile("pdfFile");
                            Log.e("URL & PDF", object.getString("url") + "  " + object.getParseFile("pdfFile"));
                        }
                        Log.e("Objects", objects.toString());
                        contentAdapter = new ContentAdapter(getActivity(), objects, ContentFragment.this);
                        contentListView.setAdapter(contentAdapter);

                    } else {
                        // show no content available
                        showNoContent();
                    }
                } else {
                    // error - TextNoContent
                    showNoContent();
                }
            }
        });
    }

    private void showNoContent() {
        contentListView.setVisibility(View.INVISIBLE);
        txtNoContent.setText(R.string.no_content);
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
        txtNoContent.setTypeface(lightTypeface);
        txtNoContent.setVisibility(View.VISIBLE);
    }

    private void showLoadingDialog() {
        loadingDialog.show();
    }

    private void hideLoadingDialog() {
        loadingDialog.dismiss();
    }

    private void showPdfProgressDialog() {
        pdfProgressDialog.show();
    }

    private void hidePdfProgressDialog() {
        pdfProgressDialog.dismiss();
    }

    private void showErrorPdfMessage() {
        Toast.makeText(getActivity(), R.string.pdf_error, Toast.LENGTH_SHORT).show();
    }

    private void showPdfViewerChooser(File pdfFile) {
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(pdfFile), "application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getActivity(), R.string.please_install_pdf_viewer, Toast.LENGTH_SHORT).show();
        }
    }

    public void onResume() {
        super.onResume();

    //    NavigationItemPositionManager navigationItemPositionManager = ((BeConApp) getActivity().getApplication()).getNavigationItemPositionManager();
    //    ((MainActivity) getActivity()).setSelectedNavigationPosition(navigationItemPositionManager.getContentPosition());
    }


    public void onShare(final Content content)
    {
        final String url = content.getUrl();
        final ArrayList<Uri> uris=new ArrayList<>();
        final ArrayList<String> strArr = new ArrayList<>();
        strArr.add(url);

        if(content.getPdfFile()!=null) {


            //startShareIntent_desc(url,content.getPdfFile().getUrl(),content.getTitle(),content.getMessage());
            doSocialShare(url,content.getTitle(),content.getPdfFile().getUrl(),content.getMessage());
            /*
            showPdfProgressDialog();
            content.getPdf(getActivity(), new FileLoadedListener() {
                @Override
                public void onFileLoaded(@Nullable File pdfFile) {
                    hidePdfProgressDialog();

                    if (pdfFile != null) {
                        uris.add(Uri.fromFile(pdfFile));
                        startShareIntent(uris, strArr,content.getTitle());
                    } else {
                        showErrorPdfMessage();
                    }
                }
            });
            */

        }
        else {
            //startShareIntent_desc(url,content.getTitle(),content.getMessage());
            doSocialShare(url,content.getTitle(),content.getMessage());
        }




    }

    private void startShareIntent(String str,String title) {
        //shareDialog.show();
        Intent shareintent=new Intent(Intent.ACTION_SEND);
        shareintent.setType("text/plain");
        shareintent.putExtra(Intent.EXTRA_TEXT,"BEEP: "+title+"\n"+ str);
        shareintent.putExtra(Intent.EXTRA_SUBJECT, "BEEP: "+title);

        System.out.println("str="+str);

        startActivityForResult(Intent.createChooser(shareintent, "Share With"),REQUEST_SHARE);
    }
    private void startShareIntent(ArrayList<Uri> uri, ArrayList<String> str,String title) {
        //shareDialog.show();
        Intent shareintent=new Intent(Intent.ACTION_SEND);
        shareintent.setType("application/pdf");
        shareintent.putExtra(Intent.EXTRA_TEXT, "BEEP: "+title+"\n"+str.get(0));
        shareintent.putExtra(Intent.EXTRA_STREAM, uri.get(0));
        shareintent.putExtra(Intent.EXTRA_SUBJECT, "BEEP: "+title);

        System.out.println("str="+str);
        System.out.println("uri="+uri);
        startActivityForResult(Intent.createChooser(shareintent, "Share With"),REQUEST_SHARE);
    }
    private void startShareIntent(String str,String pdfurl,String title) {
        //shareDialog.show();
        Intent shareintent=new Intent(Intent.ACTION_SEND);
        shareintent.setType("text/plain");
        shareintent.putExtra(Intent.EXTRA_TEXT,"BEEP: "+title+"\n"+ str+"\n PDF: \n"+ pdfurl);
        shareintent.putExtra(Intent.EXTRA_SUBJECT, "BEEP: "+title);

        System.out.println("str="+str);

        startActivityForResult(Intent.createChooser(shareintent, "Share With"),REQUEST_SHARE);
    }
    private void startShareIntent_desc(String str,String title,String msg) {
        //shareDialog.show();
        Intent shareintent=new Intent(Intent.ACTION_SEND);
        shareintent.setType("text/plain");
        shareintent.putExtra(Intent.EXTRA_TEXT,"BEEP: "+title+"\n\n"+ msg+"\n\n Visit: "+ str);
        shareintent.putExtra(Intent.EXTRA_SUBJECT, "BEEP: "+title);

        System.out.println("str="+str);

        startActivityForResult(Intent.createChooser(shareintent, "Share With"),REQUEST_SHARE);
    }
    private void startShareIntent_desc(String str,String pdfurl,String title, String msg) {
        //shareDialog.show();
        Intent shareintent=new Intent(Intent.ACTION_SEND);
        shareintent.setType("text/plain");
        shareintent.putExtra(Intent.EXTRA_TEXT,"BEEP: "+title+"\n\n"+ msg+"\n\n Visit: "+ str+"\n\n View: "+ pdfurl);
        shareintent.putExtra(Intent.EXTRA_SUBJECT, "BEEP: "+title);

        System.out.println("str="+str);

        startActivityForResult(Intent.createChooser(shareintent, "Share With"),REQUEST_SHARE);
    }

    public void doSocialShare(String url, String title, String msg){
        // First search for compatible apps with sharing (Intent.ACTION_SEND)
        List<Intent> targetedShareIntents = new ArrayList<Intent>();
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        // Set title and text to share when the user selects an option.
        /*
        shareIntent.putExtra(Intent.EXTRA_TITLE, title);
        shareIntent.putExtra(Intent.EXTRA_TEXT, url);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        */
        Bundle b=new Bundle();
        b.putString("title",title);
        b.putString("url",url);
        b.putString("msg",msg);
        shareIntent.putExtra("b1",b);
        List<ResolveInfo> resInfo = getActivity().getPackageManager().queryIntentActivities(shareIntent, 0);
        if (!resInfo.isEmpty()) {
            for (ResolveInfo info : resInfo) {
                Intent targetedShare = new Intent(Intent.ACTION_SEND);
                targetedShare.setType("text/plain"); // put here your mime type
                targetedShare.setPackage(info.activityInfo.packageName.toLowerCase());
                targetedShareIntents.add(targetedShare);
            }
            // Then show the ACTION_PICK_ACTIVITY to let the user select it
            Intent intentPick = new Intent();
            intentPick.setAction(Intent.ACTION_PICK_ACTIVITY);
            // Set the title of the dialog
            intentPick.putExtra(Intent.EXTRA_TITLE, "Share with ");
            intentPick.putExtra(Intent.EXTRA_INTENT, shareIntent);
            intentPick.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray());
            // Call StartActivityForResult so we can get the app name selected by the user
            this.startActivityForResult(intentPick, REQUEST_CODE_MY_PICK);
        }
    }

    public void doSocialShare(String url, String title,String pdfurl, String msg){
        // First search for compatible apps with sharing (Intent.ACTION_SEND)
        List<Intent> targetedShareIntents = new ArrayList<Intent>();
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        // Set title and text to share when the user selects an option.
        /*
        shareIntent.putExtra(Intent.EXTRA_TITLE, title);
        shareIntent.putExtra(Intent.EXTRA_TEXT, url);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        */
        Bundle b=new Bundle();
        b.putString("title",title);
        b.putString("url",url);
        b.putString("pdfurl",pdfurl);
        b.putString("msg",msg);

        shareIntent.putExtra("b1",b);

        List<ResolveInfo> resInfo = getActivity().getPackageManager().queryIntentActivities(shareIntent, 0);
        if (!resInfo.isEmpty()) {
            for (ResolveInfo info : resInfo) {
                Intent targetedShare = new Intent(Intent.ACTION_SEND);
                targetedShare.setType("text/plain"); // put here your mime type
                targetedShare.setPackage(info.activityInfo.packageName.toLowerCase());
                targetedShareIntents.add(targetedShare);
            }
            // Then show the ACTION_PICK_ACTIVITY to let the user select it
            Intent intentPick = new Intent();
            intentPick.setAction(Intent.ACTION_PICK_ACTIVITY);
            // Set the title of the dialog
            intentPick.putExtra(Intent.EXTRA_TITLE, "Share with ");
            intentPick.putExtra(Intent.EXTRA_INTENT, shareIntent);
            intentPick.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray());
            // Call StartActivityForResult so we can get the app name selected by the user
            this.startActivityForResult(intentPick, REQUEST_CODE_MY_PICK_pdf);
        }
    }
    int REQUEST_CODE_MY_PICK=2020;
    int REQUEST_CODE_MY_PICK_pdf=2021;
    @Override
    public void onPdfClicked(Content content) {
        showPdfProgressDialog();
        content.getPdf(getActivity(), new FileLoadedListener() {
            @Override
            public void onFileLoaded(@Nullable File pdfFile) {
                hidePdfProgressDialog();

                if (pdfFile != null) {
                    showPdfViewerChooser(pdfFile);
                } else {
                    showErrorPdfMessage();
                }
            }
        });

    }

    @Override
    public void onWebClicked(Content content) {
        String url = content.getUrl();
//        Log.e("ContentFragmentUrl", url);
        ((BeConApp) getActivity().getApplication()).openWebPage(getActivity(), url);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE_MY_PICK) {
            if(data != null && data.getComponent() != null && !TextUtils.isEmpty(data.getComponent().flattenToShortString()) ) {
                String appName = data.getComponent().flattenToShortString();
                // Now you know the app being picked.
                // data is a copy of your launchIntent with this important extra info added.


                System.out.println("appname="+appName);
                Bundle b=data.getBundleExtra("b1");

                data.removeExtra("b1");
                if(appName.equalsIgnoreCase("com.twitter.android/com.twitter.composer.ComposerShareActivity")){
                    data.putExtra(Intent.EXTRA_TEXT,  b.getString("url")+" - via @beepetc");
                }
                else if(appName.equalsIgnoreCase("com.google.android.gm/.ComposeActivityGmail"))
                {
                    data.putExtra(Intent.EXTRA_TEXT, "BEEP: " + b.getString("title") + "\n\n" + b.getString("msg") + "\n\n Visit: " + b.getString("url"));
                    data.putExtra(Intent.EXTRA_SUBJECT, "BEEP: " + b.getString("title"));
                }
                else {

                    data.putExtra(Intent.EXTRA_TEXT, "BEEP: " + b.getString("title")  + "\n\n Visit: " + b.getString("url"));
                    data.putExtra(Intent.EXTRA_SUBJECT, "BEEP: " + b.getString("title"));
                }
                // Start the selected activity
                startActivity(data);
            }
        }
        if(requestCode == REQUEST_CODE_MY_PICK_pdf) {
            if(data != null && data.getComponent() != null && !TextUtils.isEmpty(data.getComponent().flattenToShortString()) ) {
                String appName = data.getComponent().flattenToShortString();
                // Now you know the app being picked.
                // data is a copy of your launchIntent with this important extra info added.
                System.out.println("appname="+appName);
                Bundle b=data.getBundleExtra("b1");
                data.removeExtra("b1");
                if(appName.equalsIgnoreCase("com.twitter.android/com.twitter.composer.ComposerShareActivity")){

                    data.putExtra(Intent.EXTRA_TEXT, b.getString("url") +" " + b.getString("pdfurl") +" - via @beepetc");
                }
                else if(appName.equalsIgnoreCase("com.google.android.gm/.ComposeActivityGmail"))
                {
                    data.putExtra(Intent.EXTRA_TEXT, "BEEP: " + b.getString("title") + "\n\n" + b.getString("msg") + "\n\n Visit: " + b.getString("url") + "\n\n View: " + b.getString("pdfurl"));
                    data.putExtra(Intent.EXTRA_SUBJECT, "BEEP: " + b.getString("title"));
                }
                else {
                    data.putExtra(Intent.EXTRA_TEXT, "BEEP: " + b.getString("title")  + "\n\n Visit: " + b.getString("url") + "\n\n View: " + b.getString("pdfurl"));
                    data.putExtra(Intent.EXTRA_SUBJECT, "BEEP: " + b.getString("title"));
                }
                // Start the selected activity
                startActivity(data);
            }
        }
    }
}
