package com.digitalwavefront.beepetc.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.LoginActivity;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.gc.materialdesign.views.ButtonRectangle;

/**
 * Created by Tarul on 26/10/2016.
 */
public class LocationAccessFragment extends Fragment implements View.OnClickListener {

    private TextView txt_loc1, txt_loc2;
    ButtonRectangle btn_settings, btn_ok;

    public static LocationAccessFragment newInstance() {
        return new LocationAccessFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_location_access, container, false);
        txt_loc1 = (TextView) rootView.findViewById(R.id.txt_loc1);
        txt_loc2 = (TextView) rootView.findViewById(R.id.txt_loc2);
        btn_settings = (ButtonRectangle) rootView.findViewById(R.id.btn_settings);
        btn_ok = (ButtonRectangle) rootView.findViewById(R.id.btn_ok);

        Typeface regularTypeface = TypefaceUtils.getRegularTypeface(getActivity());
        txt_loc1.setTypeface(regularTypeface);
        txt_loc2.setTypeface(regularTypeface);
        btn_settings.setOnClickListener(this);
        btn_ok.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_settings:
                startActivityForResult(new Intent(Settings.ACTION_APPLICATION_SETTINGS), 0);
                showEventLoginFragment();
                break;

            case R.id.btn_ok:
                showEventLoginFragment();
                break;
        }
    }

    void showEventLoginFragment() {
        ((LoginActivity) getActivity()).showEventLoginFragment();
    }
}
