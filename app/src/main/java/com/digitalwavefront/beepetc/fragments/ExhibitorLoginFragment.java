package com.digitalwavefront.beepetc.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonRectangle;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.LoginActivity;
import com.digitalwavefront.beepetc.interfaces.EventLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SignInListener;
import com.digitalwavefront.beepetc.login.LoginType;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Company;
import com.digitalwavefront.beepetc.parse.models.Event;
import com.digitalwavefront.beepetc.utils.StringUtils;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.utils.ViewUtils;

import java.util.List;

/**
 * Created by Wayne on 11/22/15.
 */
public class ExhibitorLoginFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "ExhibitorLoginFragment";

    private TextView titleText;
    private EditText eventCodeField, companyCodeField;
    private ButtonRectangle accessEventBtn;
    private ProgressDialog progressDialog;

    public ExhibitorLoginFragment() {}

    public static ExhibitorLoginFragment newInstance() {
        return new ExhibitorLoginFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_exhibitor_login, container, false);
        titleText = (TextView) rootView.findViewById(R.id.title_text);
        eventCodeField = (EditText) rootView.findViewById(R.id.event_code_field);
        companyCodeField = (EditText) rootView.findViewById(R.id.company_code_field);
        accessEventBtn = (ButtonRectangle) rootView.findViewById(R.id.access_event_btn);
        progressDialog = new ProgressDialog(getActivity());

        // Fonts
        Typeface boldTypeface = TypefaceUtils.getBoldTypeface(getActivity());
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
        titleText.setTypeface(boldTypeface);
        eventCodeField.setTypeface(lightTypeface);
        companyCodeField.setTypeface(lightTypeface);

        // Tint my views
        ViewUtils.tintViews(getActivity(), android.R.color.white, eventCodeField, companyCodeField);

        // Clicks
        accessEventBtn.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.access_event_btn:
                attemptExhibitorLogin();
                break;
        }
    }

    private void attemptExhibitorLogin() {
        final String eventCodeString = eventCodeField.getText().toString().trim();
        final String companyCode = companyCodeField.getText().toString().trim();
        if (verifyFields(eventCodeString, companyCode)) {

            // Show progress
            showProgressDialog(getString(R.string.one_moment_please),
                    getString(R.string.finding_event));

            // Check if company exists
            Company.getCompanyByCode(companyCode, new FindCallback<Company>() {

                @Override
                public void done(List<Company> list, ParseException e) {
                    if (e == null && list != null && !list.isEmpty()) {
                        final Company company = list.get(0);
                        int eventCode = Integer.valueOf(eventCodeString);

                        // Find the event with the given event code.
                        Event.getEventByEventCode(eventCodeString, new EventLoadedListener() {

                            @Override
                            public void onEventLoaded(@Nullable Event event) {

                                if (event != null) {
                                    // Sign into event
                                    String companyId = company.getObjectId();
                                    signIntoEventAsExhibitor(event, companyId);
                                } else {
                                    // Event not found
                                    hideProgressDialog();
                                    Toast.makeText(getActivity(), R.string.event_not_found, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    } else {
                        // Company not found
                        hideProgressDialog();
                        Toast.makeText(getActivity(), R.string.company_not_found, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }


    private boolean verifyFields(String eventCode, String companyCode) {
        if (StringUtils.isNullOrEmpty(eventCode, companyCode)) {
            Toast.makeText(getActivity(), R.string.empty_fields, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void signIntoEventAsExhibitor(Event event, String companyId) {
        final BeConUser user = BeConUser.getCurrentUser();
        user.setCompanyId(getActivity(), companyId);
        ((LoginActivity) getActivity()).signIntoEvent(user, event, LoginType.EXHIBITOR, new SignInListener() {

            @Override
            public void onSignIn() {
                hideProgressDialog();
            }

            @Override
            public void onError(ParseException e) {
                Log.e(TAG, "Error signing into event as exhibitor", e);
                hideProgressDialog();
                Toast.makeText(getActivity(), R.string.event_login_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showProgressDialog(String title, String message) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    private void hideProgressDialog() {
        progressDialog.dismiss();
    }
}
