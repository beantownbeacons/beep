package com.digitalwavefront.beepetc.fragments;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.beep_pref;
import com.digitalwavefront.beepetc.adapters.CropingOption;
import com.digitalwavefront.beepetc.adapters.CropingOptionAdapter;
import com.digitalwavefront.beepetc.interfaces.BitmapLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.utils.StringUtils;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.views.CircularImageView;
import com.gc.materialdesign.views.ButtonRectangle;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static android.provider.Settings.System.AIRPLANE_MODE_ON;

/**
 * Created by Supra-Chaitali on 16-Apr-16.
 */
public class ProfileFragment extends Fragment implements View.OnTouchListener, View.OnClickListener {

    private BeConUser currentUser;
    private TextInputEditText editField_company, editField_title, editField_linkedIn, editField_twitter;
    private TextView textField_name, textField_contact, textField_email;
    private TextInputLayout textInputLayout_company, textInputLayout_title, textInputLayout_linkedIn, textInputLayout_twitter;
    private ImageView imageview_camera;
    private Button btn_reset;
    private ButtonRectangle btn_save;
    private LinearLayout root_layout;
    private CircularImageView profile_circleview;
    int f1 = 0, f2 = 0;
    private Uri mImageCaptureUri;
    private static final int CAMERA_CODE = 101, GALLERY_CODE = 201, CROPING_CODE = 301;
    private File outPutFile = null;
    private CharSequence[] items;
    public beep_pref pref;
    Bitmap profphoto = null;
    View rootView;

    TextView settings, ok;

    private static final int PERMISSION = 5;
    String[] perms = {"android.Manifest.permission.WRITE_EXTERNAL_STORAGE", "android.Manifest.permission.CAMERA"};
    private static final String ACCESS_STORAGE = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private static final String CAMERA = android.Manifest.permission.CAMERA;


    public ProfileFragment() {
    }

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    static boolean isAirplaneModeOn(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        return Settings.System.getInt(contentResolver, AIRPLANE_MODE_ON, 0) != 0;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentUser = BeConUser.getCurrentUser();
        f1 = 0;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile_view, container, false);
        root_layout = (LinearLayout) rootView.findViewById(R.id.profile_root_layout);
        textInputLayout_company = (TextInputLayout) rootView.findViewById(R.id.profile_company_layout);
        textInputLayout_title = (TextInputLayout) rootView.findViewById(R.id.profile_title_layout);
        textInputLayout_linkedIn = (TextInputLayout) rootView.findViewById(R.id.profile_linkedIn_layout);
        textInputLayout_twitter = (TextInputLayout) rootView.findViewById(R.id.profile_twitter_layout);

        editField_company = (TextInputEditText) rootView.findViewById(R.id.profile_compny_txt);
        editField_title = (TextInputEditText) rootView.findViewById(R.id.profile_title_txt);
        editField_linkedIn = (TextInputEditText) rootView.findViewById(R.id.profile_linkedIn_txt);
        editField_twitter = (TextInputEditText) rootView.findViewById(R.id.profile_twitter_txt);

        textField_name = (TextView) rootView.findViewById(R.id.profile_txt_username);
        textField_contact = (TextView) rootView.findViewById(R.id.profile_contact_txt);
        textField_email = (TextView) rootView.findViewById(R.id.profile_email_txt);
        profile_circleview = (CircularImageView) rootView.findViewById(R.id.profile_circleView);
        pref = new beep_pref(getActivity());
        outPutFile = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
        imageview_camera = (ImageView) rootView.findViewById(R.id.profile_imageview_camera);
        f1 = 0;
        f2 = 0;
        //profile_circleview.setImageBitmap(null);
        imageview_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  dialog.show();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    //showAlert();
                    //Toast.makeText(getActivity(), "Please enable permissions for Camera and Storage to manage your profile photo.", Toast.LENGTH_LONG).show();
                    //createPermissions();
                    if(checkPermission()) {
                        selectImageOption();
                    }
                    else {
                        requestPermission();
                    }
                } else {
                    selectImageOption();

                    Toast.makeText(getActivity(), "Please enable permissions for Camera and Storage to manage your profile photo.", Toast.LENGTH_LONG).show();
                    startActivityForResult(new Intent(Settings.ACTION_APPLICATION_SETTINGS), 0);

                    showAlert();
                    //Toast.makeText(getActivity(), "Please enable permissions for Camera and Storage to manage your profile photo.", Toast.LENGTH_LONG).show();

                }

            }
        });

        btn_reset = (Button) rootView.findViewById(R.id.profile_btn_reset);
        btn_save = (ButtonRectangle) rootView.findViewById(R.id.profile_btn_save);

        init();

        return rootView;
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), ACCESS_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getActivity(), CAMERA);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_STORAGE, CAMERA}, PERMISSION);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void createPermissions(){
        String permission = android.Manifest.permission.READ_SMS;
        if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED){
            Log.e("Camera", "Permission");
            if(!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)){
                Log.e("Camera", "Permission");
                requestPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION);
            }
        }
    }

    void showAlert() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View alertDialogView = inflater.inflate(R.layout.profile_alert_marshmallow, null);
        alertDialog.setView(alertDialogView);
        alertDialog.create();
        final AlertDialog ad = alertDialog.show();

        settings = (TextView) alertDialogView.findViewById(R.id.txt_settings);
        ok = (TextView) alertDialogView.findViewById(R.id.txt_ok);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(Settings.ACTION_APPLICATION_SETTINGS), 0);
                ad.dismiss();
                selectImageOption();
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.dismiss();
                selectImageOption();
            }
        });

    }


    private void init() {

        //set font
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());

        textInputLayout_company.setTypeface(lightTypeface);
        textInputLayout_title.setTypeface(lightTypeface);
        textInputLayout_linkedIn.setTypeface(lightTypeface);
        textInputLayout_twitter.setTypeface(lightTypeface);
        editField_company.setTypeface(lightTypeface);
        editField_title.setTypeface(lightTypeface);
        editField_linkedIn.setTypeface(lightTypeface);
        editField_twitter.setTypeface(lightTypeface);

        textField_contact.setTypeface(lightTypeface);
        textField_email.setTypeface(lightTypeface);
        textField_name.setTypeface(lightTypeface);
        root_layout.setOnTouchListener(this);
        btn_save.setOnClickListener(this);
        btn_reset.setOnClickListener(this);

        pref.setFlag(false);
        setdata();
    }

    private void setdata() {
        textField_name.setText(currentUser.getName());

        if ((currentUser.getCompany() != null) && !currentUser.getCompany().equals("null")) {
            editField_company.setText(currentUser.getCompany().toString());
        }
        if ((currentUser.getTitle() != null) && !currentUser.getTitle().equals("null")) {
            editField_title.setText(currentUser.getTitle().toString());
        }
        if ((currentUser.getPhoneNumber() != null) && !currentUser.getPhoneNumber().equals("null")) {
            textField_contact.setText(currentUser.getPhoneNumber());
        }
        if ((currentUser.getEmail() != null) && !currentUser.getEmail().equals("null")) {
            textField_email.setText(currentUser.getEmail());
        }
        if ((currentUser.getLinkedInProfile() != null) && !currentUser.getLinkedInProfile().equals("null")) {
            editField_linkedIn.setText(currentUser.getLinkedInProfile());
        }
        if ((currentUser.getTwitterHandler() != null) && !currentUser.getTwitterHandler().equals("null")) {
            editField_twitter.setText(currentUser.getTwitterHandler());
        }
        if (f1 == 0) {
            currentUser.fetchInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {

                    if (e == null) {
                        BeConUser user = (BeConUser) parseObject;
                        profile_circleview.setImageResource(R.drawable.ic_account_circle);
                        f2 = 0;
                        user.getProfilePhoto(new BitmapLoadedListener() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap) {
                                Log.e("f1", "" + f1);
                                if (bitmap != null) {
                                    f2 = 1;
                                    profile_circleview.setImageBitmap(bitmap);
                                } else {
                                    f2 = 0;
                                    profile_circleview.setImageResource(R.drawable.ic_account_circle);
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Profile");
        currentUser = BeConUser.getCurrentUser();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.profile_root_layout:
                root_layout.requestFocus();
                StringUtils.hideSoftKeyboard(getActivity());
                break;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_btn_reset:
                f1 = 0;
                setdata();
                break;
            case R.id.profile_btn_save:

                if (!StringUtils.isNullOrEmpty(editField_company.getText().toString())) {
                    currentUser.setCompany(editField_company.getText().toString());
                } else {
                    currentUser.setCompany("");
                }
                if (!StringUtils.isNullOrEmpty(editField_title.getText().toString())) {
                    currentUser.setTitle(editField_title.getText().toString());
                } else {
                    currentUser.setTitle("");
                }
                if (!StringUtils.isNullOrEmpty(editField_linkedIn.getText().toString())) {
                    currentUser.setLinkedInProfile(editField_linkedIn.getText().toString());
                } else {
                    currentUser.setLinkedInProfile("");
                }
                if (!StringUtils.isNullOrEmpty(editField_twitter.getText().toString())) {
                    currentUser.setTwitterHandler(editField_twitter.getText().toString());
                } else {
                    currentUser.setTwitterHandler("");
                }
                if (currentUser.getCompany() != null && !currentUser.getCompany().equals("null")) {
                    editField_company.setText(currentUser.getCompany());
                }
                if (currentUser.getTitle() != null && !currentUser.getTitle().equals("null")) {
                    editField_title.setText(currentUser.getTitle());
                }
                if (currentUser.getLinkedInProfile() != null && !currentUser.getLinkedInProfile().equals("null")) {
                    editField_linkedIn.setText(currentUser.getLinkedInProfile());
                }
                if (currentUser.getTwitterHandler() != null && !currentUser.getTwitterHandler().equals("null")) {
                    editField_twitter.setText(currentUser.getTwitterHandler());
                }


                if (f1 == 1) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    // Compress image to lower quality scale 1 - 100
                    profphoto.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] image = stream.toByteArray();
                    // Create the ParseFile
                    final ParseFile file = new ParseFile(currentUser.getName() + "_photo_" + Math.random() + ".png", image);
                    file.saveInBackground(new SaveCallback() {
                                              @Override
                                              public void done(ParseException e) {
                                                  currentUser.put("profilePhoto", file);
                                                  currentUser.saveInBackground(new SaveCallback() {
                                                      @Override
                                                      public void done(ParseException e) {
                                                          profile_circleview.setImageBitmap(profphoto);

                                                      }
                                                  });
                                              }
                                          }
                    );
                }
                currentUser.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {

                        if (e == null) {
                            Toast.makeText(getActivity(), "Profile updated successfully.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "Failed to update Profile.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        f1 = 1;
        pref.setFlag(true);
        if (requestCode == GALLERY_CODE && resultCode == getActivity().RESULT_OK && null != data) {

            mImageCaptureUri = data.getData();
            System.out.println("Gallery Image URI : " + mImageCaptureUri);
            CropingIMG();

        } else if (requestCode == CAMERA_CODE && resultCode == getActivity().RESULT_OK) {

            System.out.println("Camera Image URI : " + mImageCaptureUri);
            CropingIMG();
        } else if (requestCode == CROPING_CODE) {

            try {
                if (outPutFile.exists()) {
                    Bitmap photo = decodeFile(outPutFile);
                    profphoto = photo;
                    Log.e("from camera", "" + photo);
                    profile_circleview.setImageBitmap(photo);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    // Compress image to lower quality scale 1 - 100
                    profphoto.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] image = stream.toByteArray();
                    // Create the ParseFile
                    currentUser.profile = photo;

                    final ParseFile file = new ParseFile("user_photo_" + Math.random() + ".png", image);
                    file.saveInBackground(new SaveCallback() {
                                              @Override
                                              public void done(ParseException e) {

                                                  currentUser.setProfilephoto(file);
                                                  currentUser.saveInBackground(new SaveCallback() {
                                                      @Override
                                                      public void done(ParseException e) {
                                                          if (e == null) {
                                                              Toast.makeText(getActivity(), "Profile Photo updated successfully.", Toast.LENGTH_SHORT).show();
                                                              profile_circleview.setImageBitmap(profphoto);
                                                          } else {
                                                              Toast.makeText(getActivity(), "Failed to update profile photo.", Toast.LENGTH_SHORT).show();
                                                          }

                                                      }
                                                  });
                                              }
                                          }
                    );
                } else {
                    Toast.makeText(getActivity(), "Error while save image", Toast.LENGTH_SHORT).show();
                }
                File f = new File(mImageCaptureUri.getPath());
                if (f.exists()) f.delete();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void selectImageOption() {
        final ProgressDialog pdialog = new ProgressDialog(getActivity());
        pdialog.setMessage("Removing profile photo.. Please Wait");
        pdialog.setCancelable(true);
        if (f2 == 1) {
            CharSequence[] item1 = {"Remove profile photo", "Take a Selfie!", "Select from gallery", "Cancel"};
            items = item1;
        } else {
            CharSequence[] item2 = {"Take a Selfie!", "Select from gallery", "Cancel"};
            items = item2;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose a method to pick your photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Remove profile photo")) {
                    dialog.dismiss();
                    pdialog.show();
                    // ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    // Compress image to lower quality scale 1 - 100
                    // profphoto.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] bm = new byte[0];// = stream.toByteArray();
                    currentUser.profile = null;
                    final ParseFile file = new ParseFile("null", bm);
                    file.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {

                            currentUser.put("profilePhoto", file);
                            currentUser.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    f2 = 0;
                                    profile_circleview.setImageResource(R.drawable.ic_account_circle);
                                    pdialog.dismiss();
                                }
                            });
                        }
                    });

                }
                if (items[item].equals("Take a Selfie!")) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp1.jpg");
                    mImageCaptureUri = Uri.fromFile(f);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                    startActivityForResult(intent, CAMERA_CODE);

                } else if (items[item].equals("Select from gallery")) {

                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, GALLERY_CODE);

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void CropingIMG() {

        final ArrayList<CropingOption> cropOptions = new ArrayList();

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        List<ResolveInfo> list = getActivity().getPackageManager().queryIntentActivities(intent, 0);
        int size = list.size();
        if (size == 0) {
            Toast.makeText(getActivity(), "Cann't find image croping app", Toast.LENGTH_SHORT).show();
            return;
        } else {
            intent.setData(mImageCaptureUri);
            intent.putExtra("outputX", 512);
            intent.putExtra("outputY", 512);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);

            //TODO: don't use return-data tag because it's not return large image data and crash not given any message
            //intent.putExtra("return-data", true);

            //Create output file here
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outPutFile));

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);

                i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));

                startActivityForResult(i, CROPING_CODE);
            } else {
                for (ResolveInfo res : list) {
                    final CropingOption co = new CropingOption();

                    co.title = getActivity().getPackageManager().getApplicationLabel(res.activityInfo.applicationInfo);
                    co.icon = getActivity().getPackageManager().getApplicationIcon(res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);
                    co.appIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                    cropOptions.add(co);
                }

                CropingOptionAdapter adapter = new CropingOptionAdapter(getActivity(), cropOptions);

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Choose Croping App");
                builder.setCancelable(false);
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        startActivityForResult(cropOptions.get(item).appIntent, CROPING_CODE);
                    }
                });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                        if (mImageCaptureUri != null) {
                            getActivity().getContentResolver().delete(mImageCaptureUri, null, null);
                            mImageCaptureUri = null;
                        }
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private Bitmap decodeFile(File f) {
        try {
            // decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 512;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

   /* @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case PERMISSION: {
                Log.e("In", "Case");
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Permission", "Granted");
                    selectImageOption();

                } else {
                    Log.e("Permission", "Not granted");
                }
                return;
            }

        }
    }*/

    /*@Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){

        Log.e("PermsRequest", "Code");
        switch(permsRequestCode){

            case 5:
                Log.e("In", "Case");
                boolean storageAccepted = grantResults[0]==PackageManager.PERMISSION_GRANTED;
                boolean cameraAccepted = grantResults[1]==PackageManager.PERMISSION_GRANTED;
                Log.e("Accepted", ""+storageAccepted+" "+cameraAccepted);
                if(storageAccepted && cameraAccepted) {
                    selectImageOption();
                }
                break;
        }

    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted && cameraAccepted)
                        selectImageOption();
                    else {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(ACCESS_STORAGE)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{ACCESS_STORAGE, CAMERA}, PERMISSION);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

}

