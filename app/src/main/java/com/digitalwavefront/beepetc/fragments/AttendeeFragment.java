package com.digitalwavefront.beepetc.fragments;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.digitalwavefront.beepetc.BeConApp;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.adapters.AttendeeAdapter;
import com.digitalwavefront.beepetc.interfaces.AttendeeOptionClickListener;
import com.digitalwavefront.beepetc.interfaces.BeaconInteractionsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.BeaconsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.EventAccessLogLoadedListener;
import com.digitalwavefront.beepetc.interfaces.EventLoadedListener;
import com.digitalwavefront.beepetc.parse.models.AttendeeChat;
import com.digitalwavefront.beepetc.parse.models.BeConBeacon;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.BeaconInteraction;
import com.digitalwavefront.beepetc.parse.models.Event;
import com.digitalwavefront.beepetc.parse.models.EventAccessLog;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by supra-chaitali on 23/03/2016.
 */

/**
 * Updated by Tarul on 9/8/16.
 */
public class AttendeeFragment extends Fragment implements AttendeeOptionClickListener {
    private static final String TAG = "AttendeeFragment";
    private ListView attendeeListview;
    List<BeConUser> attendeeList = new ArrayList<BeConUser>();
    AttendeeAdapter attendeeadapter;
    private ProgressDialog progressDialog;
    private TextView textView_no_attendees;

    List<String> userids;
    public AttendeeFragment() {
    }

    public static AttendeeFragment newInstance() {
        return new AttendeeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_attendee, container, false);

        attendeeListview = (ListView) rootview.findViewById(R.id.attendee_list_view);
        textView_no_attendees = (TextView) rootview.findViewById(R.id.txt_no_attendee);
        textView_no_attendees.setVisibility(View.GONE);

        attendeeListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });

        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
        textView_no_attendees.setTypeface(lightTypeface);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(R.string.loading_attendee);
        progressDialog.setMessage(getString(R.string.one_moment_please));

        loadattendees();
        return rootview;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Attendees");
        //  NavigationItemPositionManager navigationItemPositionManager = ((BeConApp) getActivity().getApplication()).getNavigationItemPositionManager();
        //  ((MainActivity) getActivity()).setSelectedNavigationPosition(navigationItemPositionManager.getAttendeePosition());
    }

    private void loadattendees() {
        showProgressDialog();
        BeConUser currentUser = BeConUser.getCurrentUser();
        Log.e("currentUser:  ", "" + currentUser.getUsername());
        currentUser.getCurrentEvent(new EventLoadedListener() {

            @Override
            public void onEventLoaded(@Nullable final Event event) {
                if (event == null) {
                    Log.e("NO event ", "");
                } else {

                    Log.e("event name : ", "" + event.getName());

                    //#### Modified by Sarika [25/09/17] fetch all becons for current evt

                    BeConBeacon.getBeaconsByEventId(event.getObjectId(), new BeaconsLoadedListener() {
                        @Override
                        public void onBeaconsLoaded(@Nullable List<BeConBeacon> beacons) {

                            if(beacons != null){

                                List<String> beconids = new ArrayList<String>();
                                for(BeConBeacon becon: beacons){
                                    beconids.add(becon.getObjectId());
                                }
                                BeaconInteraction.getUsersForInteractions(beconids, new BeaconInteractionsLoadedListener() {
                                    @Override
                                    public void onInteractionsLoaded(@Nullable List<BeaconInteraction> interactions) {
                                        if (interactions != null) {
                                            userids = new ArrayList<String>();

                                            for (BeaconInteraction interaction : interactions) {
                                                if (!userids.contains(interaction.getUserId())) {
                                                    Log.e("UserId: ", "" + interaction.getUserId());
                                                    userids.add(interaction.getUserId());
                                                }
                                            }

                                            getEVL(event.getObjectId());
                                            /*
                                            ParseQuery<BeConUser> query = ParseQuery.getQuery(BeConUser.class);
                                            query.whereContainedIn("objectId", userids);
                                            query.findInBackground(new FindCallback<BeConUser>() {
                                                @Override
                                                public void done(List<BeConUser> list, ParseException e) {
                                                    if (list != null && list.size() != 0) {
                                                        //   Log.e("attendeeName", "" + list.get(0).getUsername());
                                                        attendeeList = new ArrayList<BeConUser>();
                                                        attendeeList = list;
                                                        attendeeadapter = new AttendeeAdapter(getActivity(), attendeeList, AttendeeFragment.this);
                                                        attendeeListview.setAdapter(attendeeadapter);
                                                        // attendeeadapter.notifyDataSetChanged();
                                                        hideProgressDialog();
                                                    } else {
                                                        showNoAttendeeMessage();
                                                    }
                                                }
                                            });
                                            */
                                        } else {
                                            getEVL(event.getObjectId());
                                            showErrorMessage();
                                        }
                                    }
                                });
                            }else{
                                getEVL(event.getObjectId());
                            }
                        }
                    });

                    /*
                    List<String> triggerBeaconIds = event.getTriggerBeaconIds();

                    if (triggerBeaconIds != null) {
                        BeaconInteraction.getUsersForInteractions(triggerBeaconIds, new BeaconInteractionsLoadedListener() {
                            @Override
                            public void onInteractionsLoaded(@Nullable List<BeaconInteraction> interactions) {
                                if (interactions != null) {
                                    List<String> userids = new ArrayList<String>();

                                    for (BeaconInteraction interaction : interactions) {
                                        if (!userids.contains(interaction.getUserId())) {
                                            Log.e("UserId: ", "" + interaction.getUserId());
                                            userids.add(interaction.getUserId());
                                        }
                                    }
                                    ParseQuery<BeConUser> query = ParseQuery.getQuery(BeConUser.class);
                                    query.whereContainedIn("objectId", userids);
                                    query.findInBackground(new FindCallback<BeConUser>() {
                                        @Override
                                        public void done(List<BeConUser> list, ParseException e) {
                                            if (list != null && list.size() != 0) {
                                                //   Log.e("attendeeName", "" + list.get(0).getUsername());
                                                attendeeList = new ArrayList<BeConUser>();
                                                attendeeList = list;
                                                attendeeadapter = new AttendeeAdapter(getActivity(), attendeeList, AttendeeFragment.this);
                                                attendeeListview.setAdapter(attendeeadapter);
                                                // attendeeadapter.notifyDataSetChanged();
                                                hideProgressDialog();
                                            } else {
                                                showNoAttendeeMessage();
                                            }
                                        }
                                    });
                                } else {
                                    showErrorMessage();
                                }
                            }
                        });
                    } else {
                        showNoAttendeeMessage();
                    }
                    */
                }
            }
        });
    }

    private void getEVL(String evtId) {
        EventAccessLog.getUsersForEVL(evtId, new EventAccessLogLoadedListener() {
            @Override
            public void onEventAccessLoaded(@Nullable List<EventAccessLog> evtLogs) {

                if(evtLogs != null){

                    for(EventAccessLog evtlog: evtLogs){
                        userids.add(evtlog.getUserId());
                    }
                    fetchAttendee();
                }
            }
        });
    }
    private void fetchAttendee(){
        ParseQuery<BeConUser> query = ParseQuery.getQuery(BeConUser.class);
        query.whereContainedIn("objectId", userids);
        query.whereEqualTo("seed",false);
        query.findInBackground(new FindCallback<BeConUser>() {
            @Override
            public void done(List<BeConUser> list, ParseException e) {
                if (list != null && list.size() != 0) {
                    //   Log.e("attendeeName", "" + list.get(0).getUsername());
                    attendeeList = new ArrayList<BeConUser>();
                    attendeeList = list;
                    Collections.sort(attendeeList);
                    attendeeadapter = new AttendeeAdapter(getActivity(), attendeeList, AttendeeFragment.this);
                    attendeeListview.setAdapter(attendeeadapter);
                    // attendeeadapter.notifyDataSetChanged();
                    hideProgressDialog();
                } else {
                    showNoAttendeeMessage();
                }
            }
        });
    }
    private void showNoAttendeeMessage() {
        hideProgressDialog();
        // Toast.makeText(getActivity(), R.string.no_attendee, Toast.LENGTH_SHORT).show();
        textView_no_attendees.setVisibility(View.VISIBLE);
        textView_no_attendees.setText(getResources().getString(R.string.no_attendee));
    }

    private void showErrorMessage() {
        hideProgressDialog();
        //Toast.makeText(getActivity(), R.string.error_loading_attendee, Toast.LENGTH_SHORT).show();
        textView_no_attendees.setVisibility(View.VISIBLE);
        textView_no_attendees.setText(getResources().getString(R.string.error_loading_attendee));
    }

    private void showProgressDialog() {
        progressDialog.show();
    }

    private void hideProgressDialog() {
        progressDialog.dismiss();
    }

    // by Sarika for A2A conn
    public void onA2AConnClicked(BeConUser attendee){

        final BeConUser user = attendee;
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("");
        builder.setMessage("Send connection request to "+attendee.getName());
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                showProgressDialog();
                BeConUser currentUser = BeConUser.getCurrentUser();

                AttendeeChat attendeeChat = new AttendeeChat();
                attendeeChat.setSenderUserId(currentUser.getObjectId());
                attendeeChat.setReceiverUserId(user.getObjectId());
                attendeeChat.setStatus(false);

                try {
                    attendeeChat.save();
                }catch (ParseException ex) {

                }

                hideProgressDialog();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {
            }

        });
        builder.show();

    }
    public void onLinkedInClicked(BeConUser attendee) {
        String linkedInURL = attendee.getLinkedInProfile();
        Log.e("LinkedUrl", linkedInURL);
        if (!linkedInURL.contains("linkedin.com")) {
            linkedInURL = "http://www.linkedin.com/in/" + linkedInURL;
            Log.e("LinkedUrl", linkedInURL);
        }
        showWebPage(linkedInURL);
    }

    public void onTwitterClicked(BeConUser attendee) {
        String twitterURL = attendee.getTwitterHandler();
        Log.e("TwitterUrl", twitterURL);
        if (!twitterURL.contains("twitter.com")) {
            if (twitterURL.startsWith("@")) {
                twitterURL = twitterURL.replace("@", "/");
                twitterURL = "http://www.twitter.com" + twitterURL;
                Log.e("TwitterUrl@", twitterURL);
            } else {
                twitterURL = "http://www.twitter.com/" + twitterURL;
                Log.e("TwitterUrl!@", twitterURL);
            }
        }
        Log.e("TwitterUrl", twitterURL);
        showWebPage(twitterURL);
    }

    private void showWebPage(String url) {
        ((BeConApp) getActivity().getApplication()).openWebPage(getActivity(), url);
    }
}

