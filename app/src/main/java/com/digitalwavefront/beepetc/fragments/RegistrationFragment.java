package com.digitalwavefront.beepetc.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.LoginActivity;
import com.digitalwavefront.beepetc.activities.TermsConditionsActivity;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.utils.StringUtils;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.utils.ViewUtils;
import com.gc.materialdesign.views.ButtonRectangle;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.SignUpCallback;

import java.util.List;

/**
 * Created by Wayne on 9/15/15.
 */

/* Chages done by supra-chaitali from 21 March 16
 */
public class RegistrationFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {
    private static final String TAG = "RegistrationFragment";

    private static final int MINIMUM_PASSWORD_LENGTH = 6;

    private TextView titleText, verificationCodeText, alreadyAUserView, termtextview;
    private TextInputEditText nameField, emailField, passwordField, passwordConfirmationField,
            phoneField, companyField, titleField;
    private LinearLayout root_layout;
    private CheckBox termscheckbox;
    private ButtonRectangle registerBtn;
    private ProgressDialog progressDialog;
    private boolean ischecked=false;
    private TextInputLayout namelayout, emaillayout,passwordlayout, confirm_passwordlayout, phonelayout, companylayout, titlelayout;
    public String deviceDetails,os,screen;

    public RegistrationFragment() {}

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        return model;
    }

    public static String getScreenSize(Activity activity){

        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        //display.getSize(size);


        int height = display.getHeight();
        int width = display.getWidth();

        //int width = size.x;
        //int height = size.y;

        String screenSize = width+"x"+height;
        return screenSize;
    }

    public static RegistrationFragment newInstance() {
        return new RegistrationFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_registration, container, false);

        deviceDetails=getDeviceName();
        os= Build.VERSION.RELEASE;
        screen=getScreenSize(getActivity());

        root_layout = (LinearLayout) rootView.findViewById(R.id.registration_layout);
        titleText = (TextView) rootView.findViewById(R.id.title_text);
        verificationCodeText = (TextView) rootView.findViewById(R.id.verification_code_text);
        alreadyAUserView = (TextView) rootView.findViewById(R.id.already_a_user_view);

        //TextInputLayout Initialization
        namelayout = (TextInputLayout) rootView.findViewById(R.id.name_textlayout);
        emaillayout = (TextInputLayout) rootView.findViewById(R.id.email_textlayout);

        passwordlayout = (TextInputLayout) rootView.findViewById(R.id.password_textlayout);
        //confirm_passwordlayout = (TextInputLayout) rootView.findViewById(R.id.confirm_paswrd_textlayout);
        phonelayout = (TextInputLayout) rootView.findViewById(R.id.phone_textlayout);
        //companylayout = (TextInputLayout) rootView.findViewById(R.id.company_textlayout);
       // titlelayout = (TextInputLayout) rootView.findViewById(R.id.title_textlayout);

        nameField = (TextInputEditText) rootView.findViewById(R.id.name_field);
        emailField = (TextInputEditText) rootView.findViewById(R.id.email_field);

        passwordField = (TextInputEditText) rootView.findViewById(R.id.password_field);
        //passwordConfirmationField = (TextInputEditText) rootView.findViewById(R.id.password_confirmation_field);
        //companyField = (TextInputEditText) rootView.findViewById(R.id.company_field);
        //titleField = (TextInputEditText) rootView.findViewById(R.id.ttl_field);
        phoneField = (TextInputEditText) rootView.findViewById(R.id.phone_number_field);

        termtextview = (TextView) rootView.findViewById(R.id.terms_txt);
        termscheckbox = (CheckBox) rootView.findViewById(R.id.terms_checkbox);
        registerBtn = (ButtonRectangle) rootView.findViewById(R.id.register_btn);

        root_layout.setOnTouchListener(this);

        registerBtn.setOnClickListener(this);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(R.string.signing_in);
        progressDialog.setMessage(getString(R.string.one_moment_please));
        progressDialog.setCanceledOnTouchOutside(false);

        // Set fonts
        Typeface boldTypeface = TypefaceUtils.getBoldTypeface(getActivity());
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());

        titleText.setTypeface(boldTypeface);
        verificationCodeText.setTypeface(lightTypeface);
        alreadyAUserView.setTypeface(boldTypeface);

        namelayout.setTypeface(lightTypeface);
        emaillayout.setTypeface(lightTypeface);

        passwordlayout.setTypeface(lightTypeface);
        //confirm_passwordlayout.setTypeface(lightTypeface);
        phonelayout.setTypeface(lightTypeface);
        //companylayout.setTypeface(lightTypeface);
        //titlelayout.setTypeface(lightTypeface);

        nameField.setTypeface(lightTypeface);
        emailField.setTypeface(lightTypeface);

        passwordField.setTypeface(lightTypeface);
        //passwordConfirmationField.setTypeface(lightTypeface);
        phoneField.setTypeface(lightTypeface);
        //companyField.setTypeface(lightTypeface);
        //titleField.setTypeface(lightTypeface);
        termscheckbox.setTypeface(lightTypeface);
        termtextview.setTypeface(lightTypeface);

        // Tint all of the fields
        //ViewUtils.tintViews(getActivity(), android.R.color.white, nameField, emailField,passwordField, passwordConfirmationField, phoneField, companyField, titleField);

        ViewUtils.tintViews(getActivity(), android.R.color.white, nameField, emailField,
                passwordField, phoneField);

        alreadyAUserView.setOnClickListener(this);
        alreadyAUserView.setPaintFlags(alreadyAUserView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        termscheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ischecked = true;
                } else {
                    ischecked = false;
                }
            }
        });

        termtextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent terms_activity = new Intent(getActivity(), TermsConditionsActivity.class);
                startActivity(terms_activity);
                getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }
        });

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.register_btn:
                attemptRegister();
                break;
            case R.id.already_a_user_view:
                showLoginFragment();
                break;
        }
    }

    private void attemptRegister() {
        String name = nameField.getText().toString().trim();
        String email = emailField.getText().toString().trim();
        String password = passwordField.getText().toString().trim();
        //String passwordConfirmation = passwordConfirmationField.getText().toString().trim();
        String phone = phoneField.getText().toString().trim();
        //String company = companyField.getText().toString().trim();
        //String title = titleField.getText().toString().trim();

        if (fieldsValid(name, email, password, phone)) {
            if (ischecked){
               phone = StringUtils.getFormattedPhoneNumber(getActivity(), phone);
                Log.e("rphone", ""+phone);
                if(phone.equals("0")){
                    Toast.makeText(getActivity(), getResources().getString(R.string.invalid_phone_number), Toast.LENGTH_LONG).show();
                }else {
                    Log.e("verify user1","phone"+phone);
                    startRegistration(name, email, password, phone);
                    Log.e("start", "register");
                }

            }else {
                Toast.makeText(getActivity(), "Please accept Terms & Conditions", Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean fieldsValid(String name, String email, String password, String phone) {
        if (StringUtils.isNullOrEmpty(name, email, password, phone)) {
            Toast.makeText(getActivity(), R.string.empty_fields, Toast.LENGTH_SHORT).show();
            return false;
        } /*else if (!StringUtils.matches(password, passwordConfirmation)) {
            Toast.makeText(getActivity(), R.string.passwords_dont_match, Toast.LENGTH_SHORT).show();
            return false;
        }*/ else if (password.length() < MINIMUM_PASSWORD_LENGTH) {
            StringBuilder builder = new StringBuilder();
            builder.append(getString(R.string.password_must_be_at_least));
            builder.append(" " + MINIMUM_PASSWORD_LENGTH + " ");
            builder.append(getString(R.string.characters) + ".");
            Toast.makeText(getActivity(), builder.toString(), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!StringUtils.isValidPhoneNumber(phone)) {
            Toast.makeText(getActivity(), R.string.invalid_phone_number, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void startRegistration(String name, String email, String password, String phone,
                                   String company, String title) {
        showProgressDialog();
        Log.e("verify user2", "phone" + phone);
        verifyEmailAvailability(name, email, password, phone);
    }

    private void startRegistration(String name, String email, String password, String phone) {
        showProgressDialog();
        Log.e("verify user2", "phone" + phone);
        verifyEmailAvailability(name, email, password, phone);
    }

    private void verifyEmailAvailability(final String name, final String email,
                                         final String password, final String phone) {
        Log.e("verify user3","phone"+phone);
        // Check that the email is not in use
        BeConUser.getUsersByEmail(email, new FindCallback<BeConUser>() {
            @Override
            public void done(List<BeConUser> usersByEmail, ParseException e) {
                if (e == null) {
                    if (usersByEmail.isEmpty()) {
                        verifyPhoneNumberAvailability(name, phone, email, password);
                    } else {
                        handleTakenEmail();
                    }
                } else {
                    handleParseException(e);
                }
            }
        });
    }

    private void verifyPhoneNumberAvailability(final String name, String phone,
                                               final String email, final String password) {
        // Check that the phone number is not in use and verified
        Log.e("verify user4", "phone" + phone);
         phone = StringUtils.convertToNumericPhoneNumber(phone);
        Log.e("verify user4_1", "phone" + phone);

        final String finalPhone = phone;

        BeConUser.getVerifiedUsersByPhoneNumber(phone, new FindCallback<BeConUser>() {

            @Override
            public void done(List<BeConUser> users, ParseException e) {
                if (e == null) {

                    if (users.isEmpty()) {
                        Log.e("verify user5", "empty");
                        completeRegistration(name, email, password, finalPhone);
                    } else {
                        Log.e("verify user6", "duplicate");
                        handleTakenPhoneNumber();
                    }
                } else {
                    handleParseException(e);
                }
            }
        });
    }

    private void completeRegistration(String name, String email, String password, String phone) {
        final BeConUser user = new BeConUser();
        user.setName(name);
        user.setUsername(email);
        user.setPassword(password);
        user.setSeed(false);
      //  user.setEmail(email);
        //user.setCompany(company);
        user.setPhoneNumber(phone);
        //user.setTitle(title);
        user.setIsVerified(false);
        user.setDeviceDetails(deviceDetails);
        user.setDeviceOS(os);
        user.setDeviceScreen(screen);
        user.signUpInBackground(new SignUpCallback() {

            @Override
            public void done(ParseException e) {
                hideProgressDialog();
                if (e == null) {
                    showVerificationCodeFragment();
                } else {
                    handleParseException(e);
                }
            }
        });
    }

    private void handleTakenEmail() {
        hideProgressDialog();
        Toast.makeText(getActivity(), R.string.email_taken, Toast.LENGTH_SHORT).show();
    }

    private void handleTakenPhoneNumber() {
        hideProgressDialog();
        Toast.makeText(getActivity(), R.string.phone_number_taken, Toast.LENGTH_SHORT).show();
    }

    private void handleParseException(ParseException e) {
        Log.e(TAG, "Error registering with Parse: " + e.getMessage());
        hideProgressDialog();
        Toast.makeText(getActivity(), R.string.error_signing_up, Toast.LENGTH_SHORT).show();
    }

    private void showLoginFragment() {
        ((LoginActivity) getActivity()).showLoginFragment();
    }

    private void showVerificationCodeFragment() {
        ((LoginActivity) getActivity()).showVerificationCodeFragment(true);
    }

    private void showProgressDialog() {
        progressDialog.show();
    }

    private void hideProgressDialog() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()){
            case R.id.registration_layout:
                root_layout.requestFocus();
                StringUtils.hideSoftKeyboard(getActivity());
                break;
        }
        return false;
    }
}
