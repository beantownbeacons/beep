package com.digitalwavefront.beepetc.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.LoginActivity;
import com.digitalwavefront.beepetc.utils.StringUtils;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.utils.ViewUtils;
import com.gc.materialdesign.views.ButtonRectangle;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Praneet on 26/08/2016.
 */
public class ResetPasswordFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {

    private static final int MINIMUM_PASSWORD_LENGTH = 6;
    TextView cancel;
    TextInputLayout pass_layout, cpass_layout;
    TextInputEditText pass_text, cpass_text;
    LinearLayout root_layout;
  SharedPreferences sharedpreferences;
    String Phoneno;
    String pass,conpass;
    ButtonRectangle reset;

    public static ResetPasswordFragment newInstance() {  return new ResetPasswordFragment();  }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reset_password, container, false);

        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());

        root_layout = (LinearLayout) rootView.findViewById(R.id.reset_layout);
        pass_layout = (TextInputLayout) rootView.findViewById(R.id.pass_textlayout);
        cpass_layout = (TextInputLayout) rootView.findViewById(R.id.cpass_textlayout);
        pass_text = (TextInputEditText) rootView.findViewById(R.id.pass_field);
        cpass_text = (TextInputEditText) rootView.findViewById(R.id.cpass_field);
        reset=(ButtonRectangle) rootView.findViewById(R.id.reset_btn);
        reset.setOnClickListener(this);
        pass_layout.setTypeface(lightTypeface);
        cpass_layout.setTypeface(lightTypeface);
        pass_text.setTypeface(lightTypeface);
        cpass_text.setTypeface(lightTypeface);

        ViewUtils.tintViews(getActivity(), android.R.color.white, pass_text, cpass_text);
        cancel = (TextView) rootView.findViewById(R.id.text_cancel);
        cancel.setPaintFlags(cancel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        cancel.setOnClickListener(this);
        sharedpreferences = getActivity().getSharedPreferences("Mypref", Context.MODE_PRIVATE);
        Phoneno = sharedpreferences.getString("phno", null);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.text_cancel:
            Toast.makeText(getActivity(), "Your password change request was cancelled.", Toast.LENGTH_SHORT).show();
            showLoginFragment();
            break;


            case R.id.reset_btn:
                validatePassword();
                break;
        }

    }
    private void showLoginFragment() {
        ((LoginActivity) getActivity()).showLoginFragment();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()){
            case R.id.reset_layout:

                root_layout.requestFocus();
                StringUtils.hideSoftKeyboard(getActivity());

                break;
        }
        return false;
    }
    public void validatePassword(){
       pass= pass_text.getText().toString();
       conpass= cpass_text.getText().toString();
        if(pass.equals(conpass)){
            if(pass.length()==0 || conpass.length()==0){
                Toast.makeText(getActivity(), R.string.empty_fields, Toast.LENGTH_SHORT).show();
            }
            else if(pass.length() < MINIMUM_PASSWORD_LENGTH || conpass.length() < MINIMUM_PASSWORD_LENGTH){
                StringBuilder builder = new StringBuilder();
                builder.append(getString(R.string.password_must_be_at_least));
                builder.append(" " + MINIMUM_PASSWORD_LENGTH + " ");
                builder.append(getString(R.string.characters) + ".");
                Toast.makeText(getActivity(), builder.toString(), Toast.LENGTH_SHORT).show();
            }
            else {
                CallParseclould();
            }


        }
        else{
            Toast.makeText(getActivity(), R.string.passwords_dont_match, Toast.LENGTH_SHORT).show();
        }
    }
    public void CallParseclould(){
        Map<String, String> params = new HashMap<String, String>();
        if(Phoneno!=null){
            params.put("phoneNumber",Phoneno);
        }

        params.put("newPassword",conpass);
        ParseCloud.callFunctionInBackground("changeUserPassword", params, new FunctionCallback<Object>() {
            private String msg;
            private String code;

            @Override
            public void done(Object objects, ParseException exc) {
                if (exc == null) {
                    String result = "";
                    result = objects.toString();
                    System.out.println(result);
                    Toast.makeText(getActivity(), "Your password has been changed. Please login with your new password.", Toast.LENGTH_LONG).show();
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.clear().commit();
                    showLoginFragment();

                } else {
                    System.out.println(exc);
                    Toast.makeText(getActivity(), "Please retry.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
