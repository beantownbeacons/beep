package com.digitalwavefront.beepetc.fragments;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalwavefront.beepetc.BeConApp;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.interfaces.EventLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Event;
import com.digitalwavefront.beepetc.twitter.HashTagManager;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.tweetui.SearchTimeline;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;
import com.twitter.sdk.android.tweetui.UserTimeline;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Wayne on 11/29/15.
 */

/**
 * Changes Made by Supra-chaitali on 30/03/2016
 */
public class SocialChatterFragment extends Fragment {
    private ListView tweetListView;
    private static final String TAG = "SOCIAL_CHATTER";
    private HashTagManager hashTagManager;
    private ImageButton btn_prev, btn_next;
    private TextView hashTag_text, txtNoHashtag, txtNoTweets;
    private RelativeLayout tag_layout;
    int count = 0, count1;
    private List<String> hashlist;
    String hashtag;
    SwipeRefreshLayout swipeRefreshLayout;

    private TweetTimelineListAdapter adapter;

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "OfJzzYATRG9a3jA30Mz9ByBzB";
    private static final String TWITTER_SECRET = "LMFbGZ5q12fn99QAhvi3OMaceL9lMvT2fpZhmN6dztg9yyQhBU";
    private ProgressDialog progressDialog;

    public SocialChatterFragment() {
    }

    public static SocialChatterFragment newInstance() {
        return new SocialChatterFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hashTagManager = ((BeConApp) getActivity().getApplicationContext()).getHashTagManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_social_chatter, container, false);

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getHashTags();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        swipeRefreshLayout.setEnabled(false);

        tweetListView = (ListView) rootView.findViewById(R.id.tweet_list_view);
        tweetListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    swipeRefreshLayout.setEnabled(true);
                } else {
                    swipeRefreshLayout.setEnabled(false);
                }
            }
        });

        btn_next = (ImageButton) rootView.findViewById(R.id.btn_next);
        btn_prev = (ImageButton) rootView.findViewById(R.id.btn_previous);
        hashTag_text = (TextView) rootView.findViewById(R.id.hashtag_text);
        txtNoHashtag = (TextView) rootView.findViewById(R.id.txt_no_hashtag);
        txtNoTweets = (TextView) rootView.findViewById(R.id.no_recent_tweets);
        tag_layout = (RelativeLayout) rootView.findViewById(R.id.tag_layout);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.one_moment_please));
        progressDialog.setMessage(getString(R.string.loading_tweets));

        initFabric(getActivity());
        getHashTags();

        return rootView;
    }

    private void initFabric(Context context) {
        Log.d(TAG, "Initializing Fabric");
        if (!Fabric.isInitialized()) {
            TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
            Fabric.with(context, new Twitter(authConfig));
        } else {
            Log.d(TAG, "Fabric already initialized");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.social_chatter));
        //    NavigationItemPositionManager navigationItemPositionManager = ((BeConApp) getActivity().getApplication()).getNavigationItemPositionManager();
        //   ((MainActivity) getActivity()).setSelectedNavigationPosition(navigationItemPositionManager.getSocialChatterPosition());
    }


    private void getHashTags() {

        tag_layout.setVisibility(View.VISIBLE);

        BeConUser.getCurrentUser().getCurrentEvent(new EventLoadedListener() {
            @Override
            public void onEventLoaded(@Nullable Event event) {
                hashlist = event.getHash_Tag();

                if (hashlist == null || hashlist.isEmpty()) {
                    //  Toast.makeText(getActivity(), "The Organizer hasn't assigned a #hashtag to this event. If you have any suggestions, please feel free to reach out!", Toast.LENGTH_LONG).show();
                    tag_layout.setVisibility(View.GONE);
                    tweetListView.setVisibility(View.GONE);
                    Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
                    txtNoHashtag.setTypeface(lightTypeface);
                    txtNoHashtag.setVisibility(View.VISIBLE);
                } else {

                    progressDialog.show();
                    try {
                        Set<String> set = hashTagManager.getHashTagList();
                        final List<String> hashlist = new ArrayList<String>(set);
                        Log.e("HashTagList", "" + hashlist.size());
                        for (int i = 0; i < hashlist.size(); i++) {
                            Log.e("hashTaglist_item", " " + hashlist.get(i));
                        }
                        if (hashlist.size() == 0) {
                            tag_layout.setVisibility(View.GONE);
                            Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
                            txtNoTweets.setTypeface(lightTypeface);
                            txtNoTweets.setVisibility(View.VISIBLE);

                        } else {
                            if (hashlist.size() == 1) {
                                btn_next.setEnabled(false);
                                btn_prev.setEnabled(false);
                                btn_next.setVisibility(View.GONE);
                                btn_prev.setVisibility(View.GONE);

                            } else {
                                btn_next.setEnabled(true);
                                btn_prev.setEnabled(true);
                            }
                            hashTag_text.setText("#" + hashlist.get(0));
                            new TweetLoader("#" + hashlist.get(0)).execute();
                        }
                        btn_next.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                /*txtNoTweets.setVisibility(View.GONE);
                                tweetListView.setVisibility(View.VISIBLE);*/

                                if (count == hashlist.size() - 1) {
                                    count = 0;
                                } else {
                                    count++;
                                }
                                Log.e("count", "count" + count);

                                hashTag_text.setText("#" + hashlist.get(count));
                                hashtag = hashTag_text.getText().toString();
                                Log.e("tag_text1", "" + hashTag_text);

                                new TweetLoader("#" + hashtag).execute();

                                //checkTWLineForMsg();

                            }
                        });

                        // count1 = count;
                        Log.e("count1", "" + count1);

                        btn_prev.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                /*txtNoTweets.setVisibility(View.GONE);
                                tweetListView.setVisibility(View.VISIBLE);*/

                                if (count <= 0) {
                                    count++;
                                } else {
                                    count--;
                                }

                                hashTag_text.setText("#" + hashlist.get(count));
                                hashtag = hashTag_text.getText().toString();
                                Log.e("tag_text1", "" + hashTag_text);

                                new TweetLoader("#" + hashtag).execute();

                                //checkTWLineForMsg();
                                Log.e("count1", "" + count1);

                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Error loading tweets. Please try again.", Toast.LENGTH_LONG).show();
                        hideProgressDialog();

                    }
                }
            }
        });


    }

    public void checkTWLineForMsg(){

        // ###### by Sarika #####
        // to check if lis is empty
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //displayData();
                if(adapter.getCount() == 0) {
                    //tag_layout.setVisibility(View.GONE);
                    tweetListView.setVisibility(View.GONE);
                    Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
                    txtNoTweets.setTypeface(lightTypeface);
                    txtNoTweets.setVisibility(View.VISIBLE);
                } else{
                    tweetListView.setVisibility(View.VISIBLE);
                    txtNoTweets.setVisibility(View.GONE);
                }
            }
        }, 2000);
    }
    private class TweetLoader extends AsyncTask<Void, Void, Void> {

        String joinString;

        public TweetLoader(String joinedString) {
            this.joinString = joinedString;
            Log.e("asynckString", "" + joinString);
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Log.e("get", "Tweets method");
                adapter = retrieveTimeLineByHashtag(getActivity(), joinString);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void res) {
            Log.e("getTweet", "Post method");
            Log.e("Adapter", "" + adapter);
            tweetListView.setAdapter(adapter);
            /*Log.e("ListView", "" + tweetListView.getCount());
            if (tweetListView.getCount() == 0) {
                tweetListView.setVisibility(View.GONE);
                Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
                txtNoTweets.setTypeface(lightTypeface);
                txtNoTweets.setVisibility(View.VISIBLE);
                Log.e("Text", "NoTweets");
            }
            Log.e("IsShown", "" + tweetListView.isShown() + txtNoTweets.isShown());*/
            checkTWLineForMsg();


            hideProgressDialog();
        }
    }

    public static TweetTimelineListAdapter retrieveTimeLineByHashtag(Context context, String hashtag) {
        Log.d(TAG, "Loading tweets with hashtag " + hashtag);
        SearchTimeline searchTimeline = new SearchTimeline.Builder().query(hashtag).build();
        return new TweetTimelineListAdapter.Builder(context).setTimeline(searchTimeline).build();
    }

    public static TweetTimelineListAdapter retrieveTimeLineByAccount(Context context, String account) {
        Log.d(TAG, "Loading tweets from user " + account);
        UserTimeline userTimeline = new UserTimeline.Builder().screenName(account).build();
        return new TweetTimelineListAdapter.Builder(context).setTimeline(userTimeline).build();
    }

    private void hideProgressDialog() {
        progressDialog.dismiss();
    }
}
