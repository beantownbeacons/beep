package com.digitalwavefront.beepetc.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalwavefront.beepetc.SharedPref;
import com.digitalwavefront.beepetc.parse.models.UserPreferences;
import com.gc.materialdesign.views.ButtonRectangle;
import com.linkedin.platform.LISessionManager;
import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.digitalwavefront.beepetc.BeConApp;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.LoginActivity;
import com.digitalwavefront.beepetc.interfaces.EventLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SignInListener;
import com.digitalwavefront.beepetc.login.LoginType;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Event;
import com.digitalwavefront.beepetc.utils.StringUtils;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.utils.ViewUtils;

/**
 * Created by Wayne on 9/15/15.
 */
public class EventLoginFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {
    private static final String TAG = "EventLoginFragment";

    private TextView titleText, enterAccessCodeText;
    private TextInputEditText eventCodeField;
    private ButtonRectangle accessEventBtn;
    private ProgressDialog progressDialog,progressDialogLogout;
    private TextView organizerTextView, exhibitorTextView, logOutView;
    private TextInputLayout code_layout;
    private LinearLayout root_layout;
    public static String EventCode;
    public static final String PREFS_NAME = "EventCode";

    public Activity activity;
    public EventLoginFragment() {}

    public static EventLoginFragment newInstance() {
        return new EventLoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_event_login, container, false);
        root_layout = (LinearLayout) rootView.findViewById(R.id.eventlogin_layout);
        titleText = (TextView) rootView.findViewById(R.id.title_text);
        enterAccessCodeText = (TextView) rootView.findViewById(R.id.enter_access_code_text);
        code_layout = (TextInputLayout) rootView.findViewById(R.id.eventcode_textlayout);
        eventCodeField = (TextInputEditText) rootView.findViewById(R.id.event_code_field);
        accessEventBtn = (ButtonRectangle) rootView.findViewById(R.id.access_event_btn);
        organizerTextView = (TextView) rootView.findViewById(R.id.organizer_text_view);
        exhibitorTextView = (TextView) rootView.findViewById(R.id.exhibitor_text_view);
        logOutView = (TextView) rootView.findViewById(R.id.log_out_view);
        root_layout.setOnTouchListener(this);
        accessEventBtn.setOnClickListener(this);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(R.string.locating_event);
        progressDialog.setMessage(getString(R.string.one_moment_please));
        progressDialog.setCanceledOnTouchOutside(false);

        progressDialogLogout = new ProgressDialog(getActivity());
        progressDialogLogout.setTitle("Logging out");
        progressDialogLogout.setMessage(getString(R.string.one_moment_please));
        progressDialogLogout.setCanceledOnTouchOutside(false);

        // Set fonts
        Typeface boldTypeface = TypefaceUtils.getBoldTypeface(getActivity());
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());

        titleText.setTypeface(boldTypeface);
        enterAccessCodeText.setTypeface(lightTypeface);
        eventCodeField.setTypeface(lightTypeface);
        organizerTextView.setTypeface(boldTypeface);
        exhibitorTextView.setTypeface(boldTypeface);
        logOutView.setTypeface(boldTypeface);
        code_layout.setTypeface(lightTypeface);

        // Clicks
        organizerTextView.setOnClickListener(this);
        exhibitorTextView.setOnClickListener(this);
        logOutView.setOnClickListener(this);

        // Underline
        ViewUtils.underlineTextViews(organizerTextView, exhibitorTextView, logOutView);

        // Tint fields
        ViewUtils.tintViews(getActivity(), android.R.color.white, eventCodeField);

        activity = getActivity();
        if (!BeConApp.DEV_MODE) {
            hideExhibitorAndOrganizerView();
        }

        // add user preferences [11/01/2018]
        try {
            BeConUser currentUser = BeConUser.getCurrentUser();

            UserPreferences userpref = new UserPreferences();
            if (currentUser != null) {
                userpref.setPrefForUser(currentUser.getObjectId());
            }
        }catch (Exception e) {

        }
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.access_event_btn:
                verifyEventCode();
                break;
            case R.id.organizer_text_view:
                showOrganizerLogin();
                break;
            case R.id.exhibitor_text_view:
                showExhibitorLogin();
                break;
            case R.id.log_out_view:
                signOut();
                break;

        }
    }

    private void verifyEventCode() {
        String eventCode = eventCodeField.getText().toString().trim();

        SharedPref sp=new SharedPref(getActivity());
        sp.setKey(eventCode);

        if (StringUtils.isNullOrEmpty(eventCode)) {
            showEmptyCodeMessage();
        } else {
            showProgressDialog();
            Event.getEventByEventCode(eventCode, new EventLoadedListener() {

                @Override
                public void onEventLoaded(@Nullable Event event) {
                    if (event == null) {
                        hideProgressDialog();
                        showEventCouldNotBeFoundMessage();
                    } else {
                        signIntoEventAsAttendee(event);
                    }
                }
            });
        }
    }

    private void signIntoEventAsAttendee(Event event) {
        final BeConUser user = BeConUser.getCurrentUser();
        Log.d("signIntoEventAsAttendee","signIntoEventAsAttendee");

        ((LoginActivity) getActivity()).signIntoEvent(user, event, LoginType.ATTENDEE, new SignInListener() {
            @Override
            public void onSignIn() {
                hideProgressDialog();
            }

            @Override
            public void onError(ParseException e) {
                Log.e(TAG, "Error signing into event.", e);
                hideProgressDialog();
                showEventLoginError();
            }
        });


    }

    private void showOrganizerLogin() {
        ((LoginActivity) getActivity()).showOrganizerLoginFragment();
    }

    private void showExhibitorLogin() {
        ((LoginActivity) getActivity()).showExhibitorLoginFragment();
    }

    private void signOut() {
        showLogoutDialog();
        ((BeConApp) getActivity().getApplication()).clearNavigationManager();

        LISessionManager.getInstance(getActivity()).clearSession();
        ParseUser.logOutInBackground(new LogOutCallback() {
            @Override
            public void done(ParseException e) {
                hideProgressDialogLogout();
                ((LoginActivity) getActivity()).showLoginFragment();
            }
        });

    }

    private void showProgressDialog() {
        progressDialog.show();
    }
    private void showLogoutDialog(){
        progressDialogLogout.show();
    }

    private void hideProgressDialog() {
        progressDialog.dismiss();
    }
    private void hideProgressDialogLogout() {
        progressDialogLogout.dismiss();
    }

    private void showEventLoginError() {
        Toast.makeText(getActivity(), R.string.event_login_error, Toast.LENGTH_SHORT).show();
    }

    private void showEmptyCodeMessage() {
        Toast.makeText(getActivity(), R.string.no_event_code_entered, Toast.LENGTH_SHORT).show();
    }

    private void showEventCouldNotBeFoundMessage() {
        Toast.makeText(getActivity(), R.string.no_event_found, Toast.LENGTH_SHORT).show();
    }

    private void hideExhibitorAndOrganizerView() {
        organizerTextView.setVisibility(View.GONE);
        exhibitorTextView.setVisibility(View.GONE);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()){
            case R.id.eventlogin_layout:
                root_layout.requestFocus();
                StringUtils.hideSoftKeyboard(getActivity());
                break;
        }
        return false;
    }
}
