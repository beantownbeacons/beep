package com.digitalwavefront.beepetc.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.SharedPref;
import com.digitalwavefront.beepetc.adapters.ContentPagerAdapter;
import com.digitalwavefront.beepetc.interfaces.IResetadapter;
import com.digitalwavefront.beepetc.parse.models.BeConUser;

/**
 * Created by Tarul on 08/09/2016.
 */
public class ContentFragmentNew extends Fragment implements IResetadapter{


    BeConUser currentUser;
    String eventId;
    SharedPref sp;
    private static final int REQUEST_SHARE=314;


    TabLayout tabLayout;
    ViewPager viewPager;
    ContentPagerAdapter adapter;

    static boolean savedflag=false;
    public static ContentFragmentNew newInstance() {
        return new ContentFragmentNew();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_content_new, container, false);
        currentUser = BeConUser.getCurrentUser();
        eventId = currentUser.getCurrentEventId();

        tabLayout = (TabLayout) rootView.findViewById(R.id.tablayout_contentlib);
        tabLayout.addTab(tabLayout.newTab().setText("Public"));
        tabLayout.addTab(tabLayout.newTab().setText("Preferred"));
        tabLayout.addTab(tabLayout.newTab().setText("Dismissed"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) rootView.findViewById(R.id.content_pager);
        adapter = new ContentPagerAdapter(getActivity().getSupportFragmentManager(), tabLayout.getTabCount(),this);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return rootView;
    }

    public void resetadapter()
    {
        adapter = new ContentPagerAdapter(getActivity().getSupportFragmentManager(), tabLayout.getTabCount(),this);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(1);
    }

    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.content));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



    }
}
