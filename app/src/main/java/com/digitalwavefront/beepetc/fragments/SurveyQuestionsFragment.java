package com.digitalwavefront.beepetc.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalwavefront.beepetc.interfaces.BitmapLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SpeakerLoadedListener;
import com.digitalwavefront.beepetc.parse.models.Speaker;
import com.digitalwavefront.beepetc.views.CircularImageView;
import com.gc.materialdesign.views.ButtonRectangle;
import com.parse.ParseException;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.interfaces.SurveyQuestionAnsweredListener;
import com.digitalwavefront.beepetc.interfaces.SurveyQuestionsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SurveyResponseCallback;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Survey;
import com.digitalwavefront.beepetc.parse.models.SurveyQuestion;
import com.digitalwavefront.beepetc.parse.models.SurveyResponse;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;

import java.util.List;

/**
 * Created by Wayne on 10/12/15.
 */
public class SurveyQuestionsFragment extends Fragment implements SurveyQuestionAnsweredListener {

    private static final String EXTRA_SURVEY_ID = "survey_id";
    private static final String EXTRA_EVENT_ID = "event_id";
    private static final String EXTRA_SESSION_ID = "session_id";
    private static final String EXTRA_SPEAKER_ID = "speaker_id";

    private ViewPager surveyQuestionPager;
    private String surveyId, eventId, sessionId, speakerId;
    private List<SurveyQuestion> surveyQuestions;
    private ProgressDialog loadingDialog;

    public SurveyQuestionsFragment() {}

    /*
    public static SurveyQuestionsFragment newInstance(String surveyId) {
        SurveyQuestionsFragment fragment = new SurveyQuestionsFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_SURVEY_ID, surveyId);
        fragment.setArguments(args);
        return fragment;
    }
    */
    /*
    public static SurveyQuestionsFragment newInstance(List<SurveyQuestion> surveyQues) {
        SurveyQuestionsFragment fragment = new SurveyQuestionsFragment();
        Bundle args = new Bundle();
        //args.putString(EXTRA_SURVEY_ID, surveyId);

        fragment.setArguments(args);
        return fragment;
    }*/

    public static SurveyQuestionsFragment newInstance(String eventId, String sessionId, String speakerId) {
        SurveyQuestionsFragment fragment = new SurveyQuestionsFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_EVENT_ID, eventId);
        args.putString(EXTRA_SESSION_ID, sessionId);
        args.putString(EXTRA_SPEAKER_ID, speakerId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //surveyId = getArguments().getString(EXTRA_SURVEY_ID);
            eventId = getArguments().getString(EXTRA_EVENT_ID);
            sessionId = getArguments().getString(EXTRA_SESSION_ID);
            speakerId = getArguments().getString(EXTRA_SPEAKER_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_survey_questions, container, false);
        surveyQuestionPager = (ViewPager) rootView.findViewById(R.id.survey_question_pager);
        loadingDialog = new ProgressDialog(getActivity());
        loadingDialog.setTitle(R.string.loading_questions);
        loadingDialog.setMessage(getString(R.string.one_moment_please));
        loadingDialog.setCanceledOnTouchOutside(false);
        loadSurveyQuestions();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.survey));
        //((MainActivity) getActivity()).setSelectedNavigationPosition(NavigationItemPositionManager.NO_POSITION);
    }

    private void loadSurveyQuestions() {
        showLoadingDialog(R.string.loading_questions, getString(R.string.one_moment_please));
        /*
        SurveyQuestion.getSurveyQuestionsBySurveyId(surveyId, new SurveyQuestionsLoadedListener() {

            @Override
            public void onSurveyQuestionsLoaded(@Nullable final List<SurveyQuestion> questions) {
                hideProgressDialog();
                if (questions != null) {
                    surveyQuestions = questions;
                    surveyQuestionPager.setAdapter(new SurveyQuestionPagerAdapter(getChildFragmentManager()));
                    goToFirstUnansweredQuestion();
                } else {
                    showErrorMessage();
                }
            }
        }, true);
        */
        // by Sarika [27/09/2017]
        //################### START ############################

        SurveyQuestion.getSurveyQuestionsByEventId(eventId, speakerId, new SurveyQuestionsLoadedListener() {
            @Override
            public void onSurveyQuestionsLoaded(@Nullable List<SurveyQuestion> questions) {

                hideProgressDialog();
                if (questions != null) {
                    surveyQuestions = questions;
                    surveyQuestionPager.setAdapter(new SurveyQuestionPagerAdapter(getChildFragmentManager(),sessionId,speakerId));
                    goToFirstUnansweredQuestion();
                } else {
                    showErrorMessage();
                }
            }
        },true);
        //################### END ############################
    }

    private void showLoadingDialog(int titleResource, String message) {
        loadingDialog.setTitle(titleResource);
        loadingDialog.setMessage(message);
        loadingDialog.show();
    }

    private void hideProgressDialog() {
        loadingDialog.dismiss();
    }

    private void showErrorMessage() {
        Toast.makeText(getActivity(), R.string.error_loading_questions, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSurveyQuestionAnswered(SurveyQuestion question) {

        // Update the local question list with the response
        int position = question.getPosition() - 1;
        surveyQuestions.set(position, question);

        if (allQuestionsAnswered()) {
            thankUser();
            incrementSurveyCompleteCount();
            goBackToSurveys();
        } else {

            // Find the next question
            if (position == surveyQuestions.size() - 1) {
                surveyQuestionPager.setCurrentItem(0);
            } else {
                surveyQuestionPager.setCurrentItem(position + 1);
            }
        }
    }

    private void goToFirstUnansweredQuestion() {
        for (int i = 0; i < surveyQuestions.size(); i++) {
            SurveyQuestion question = surveyQuestions.get(i);
            if (!question.hasResponse()) {
                surveyQuestionPager.setCurrentItem(i);
                return;
            }
        }
    }

    private boolean allQuestionsAnswered() {
        for (SurveyQuestion question : surveyQuestions) {
            if (!question.hasResponse()) {
                return false;
            }
        }
        return true;
    }

    private void thankUser() {
        Toast.makeText(getActivity(), R.string.thank_you, Toast.LENGTH_SHORT).show();
    }

    private void incrementSurveyCompleteCount() {
        Survey.incrementSurveyCompleteCountById(surveyId);
    }

    private void goBackToSurveys() {
        getActivity().onBackPressed();
    }

    public class SurveyQuestionPagerAdapter extends android.support.v13.app.FragmentStatePagerAdapter {

        private String sessId, speakerId;
        public SurveyQuestionPagerAdapter(android.app.FragmentManager fm) {
            super(fm);
        }

        public SurveyQuestionPagerAdapter(android.app.FragmentManager fm, String sessionId, String speakerId) {
            super(fm);
            this.sessId = sessionId;
            this.speakerId = speakerId;
        }

        @Override
        public int getCount() {
            if (surveyQuestions != null) {
                return surveyQuestions.size();
            }
            return 0;
        }

        @Override
        public Fragment getItem(int position) {
            if (surveyQuestions != null) {
                return SurveyQuestionFragment.newInstance(surveyQuestions.get(position),sessionId,speakerId, surveyQuestions.size());
            }
            return null;
        }
    }

    public static class SurveyQuestionFragment extends Fragment implements OnCheckedChangeListener, View.OnClickListener {

        private static final String EXTRA_TOTAL_PAGES = "totalPages";
        private static final String TAG = "SurveyQuestionFragment";

        private SurveyQuestion question;
        private String sessionId, speakerId;
        private int totalPages;
        private TextView pageNumberTextView, questionTextView;
        private CheckBox ratingStarOne, ratingStarTwo, ratingStarThree, ratingStarFour, ratingStarFive;
        private ButtonRectangle saveBtn;
        private SurveyQuestionsFragment parentFragment;
        private BeConUser currentUser;
        private SurveyQuestionAnsweredListener questionAnsweredListener;
        private CircularImageView speaker_photo_view;

        public SurveyQuestionFragment() {}

        public static SurveyQuestionFragment newInstance(SurveyQuestion question, int totalPages) {
            SurveyQuestionFragment fragment = new SurveyQuestionFragment();
            fragment.setQuestion(question);
            Bundle args = new Bundle();
            args.putInt(EXTRA_TOTAL_PAGES, totalPages);
            fragment.setArguments(args);
            return fragment;
        }

        // by Sarika [27-09-2017]
        public static SurveyQuestionFragment newInstance(SurveyQuestion question, String sessionId, String speakerId, int totalPages) {
            SurveyQuestionFragment fragment = new SurveyQuestionFragment();
            fragment.setQuestion(question);
            fragment.setSessionId(sessionId);
            fragment.setSpeakerId(speakerId);
            Bundle args = new Bundle();
            args.putInt(EXTRA_TOTAL_PAGES, totalPages);
            args.putString(EXTRA_SPEAKER_ID,speakerId);
            args.putString(EXTRA_SESSION_ID,sessionId);

            fragment.setArguments(args);
            return fragment;
        }

        public void setQuestion(SurveyQuestion question) {
            this.question = question;
        }
        public void setSessionId(String sessionId) {
            this.sessionId = sessionId;
        }
        public void setSpeakerId(String sessionId) {
            this.speakerId = speakerId;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            parentFragment = (SurveyQuestionsFragment) getParentFragment();
            currentUser = BeConUser.getCurrentUser();
            if (getArguments() != null) {
                totalPages = getArguments().getInt(EXTRA_TOTAL_PAGES);
                speakerId = getArguments().getString(EXTRA_SPEAKER_ID);
                sessionId = getArguments().getString(EXTRA_SESSION_ID);
            }

            // Parent fragment need to implement the question answered listener
            questionAnsweredListener = (SurveyQuestionAnsweredListener) getParentFragment();
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_survey_question, container, false);
            pageNumberTextView = (TextView) rootView.findViewById(R.id.page_number_text_view);
            //pageNumberTextView.setText(String.format("Question %d / %d", question.getPosition(), totalPages));
            if(totalPages==7)
            {
                pageNumberTextView.setText(String.format("Question %d / %d", question.getPosition()+1, totalPages));
            }
            else
            {
                pageNumberTextView.setText(String.format("Question %d / %d", question.getPosition(), totalPages));
            }
            questionTextView = (TextView) rootView.findViewById(R.id.question_text_view);
            questionTextView.setText(question.getQuestion());

            speaker_photo_view = (CircularImageView) rootView.findViewById(R.id.speaker_photo_view);

            //load speaker photo
            Speaker.getSpeakerPhoto(speakerId, new SpeakerLoadedListener() {
                @Override
                public void onSpeakerLoaded(@Nullable Speaker speaker) {
                    if(speaker!=null){

                        speaker.getAvatar(new BitmapLoadedListener() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap) {
                                if (bitmap!=null){
                                    speaker_photo_view.setImageBitmap(bitmap);
                                }
                            }
                        });
                    }
                }
            });
            // Set fonts
            Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
            pageNumberTextView.setTypeface(lightTypeface);
            questionTextView.setTypeface(lightTypeface);

            ratingStarOne = (CheckBox) rootView.findViewById(R.id.rating_star_one);
            ratingStarTwo = (CheckBox) rootView.findViewById(R.id.rating_star_two);
            ratingStarThree = (CheckBox) rootView.findViewById(R.id.rating_star_three);
            ratingStarFour = (CheckBox) rootView.findViewById(R.id.rating_star_four);
            ratingStarFive = (CheckBox) rootView.findViewById(R.id.rating_star_five);

            ratingStarOne.setOnCheckedChangeListener(this);
            ratingStarTwo.setOnCheckedChangeListener(this);
            ratingStarThree.setOnCheckedChangeListener(this);
            ratingStarFour.setOnCheckedChangeListener(this);
            ratingStarFive.setOnCheckedChangeListener(this);

            saveBtn = (ButtonRectangle) rootView.findViewById(R.id.save_btn);
            saveBtn.setOnClickListener(this);

            if (question.hasResponse()) {
                disableRatings();
                setRating();
            }
            return rootView;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()) {
                case R.id.rating_star_one:
                    ratingStarTwo.setChecked(false);
                    ratingStarThree.setChecked(false);
                    ratingStarFour.setChecked(false);
                    ratingStarFive.setChecked(false);
                    break;
                case R.id.rating_star_two:
                    ratingStarThree.setChecked(false);
                    ratingStarFour.setChecked(false);
                    ratingStarFive.setChecked(false);

                    if (isChecked) {
                        ratingStarOne.setChecked(true);
                        ratingStarTwo.setChecked(true);
                    }
                    break;
                case R.id.rating_star_three:
                    ratingStarFour.setChecked(false);
                    ratingStarFive.setChecked(false);

                    if (isChecked) {
                        ratingStarThree.setChecked(true);
                        ratingStarTwo.setChecked(true);
                        ratingStarOne.setChecked(true);
                    }
                    break;
                case R.id.rating_star_four:
                    ratingStarFive.setChecked(false);

                    if (isChecked) {
                        ratingStarOne.setChecked(true);
                        ratingStarTwo.setChecked(true);
                        ratingStarThree.setChecked(true);
                        ratingStarFour.setChecked(true);
                    }
                    break;
                case R.id.rating_star_five:
                    if (isChecked) {
                        ratingStarOne.setChecked(true);
                        ratingStarTwo.setChecked(true);
                        ratingStarThree.setChecked(true);
                        ratingStarFour.setChecked(true);
                        ratingStarFive.setChecked(true);
                    }
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.save_btn:
                    if (!question.hasResponse()) {
                        saveRating();
                    } else {
                        Toast.makeText(getActivity(), R.string.rating_already_recorded, Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }

        private void saveRating() {
            int rating = getRating();

            if (rating == 0) {
                Toast.makeText(getActivity(), R.string.no_rating_given, Toast.LENGTH_SHORT).show();
            } else {
                parentFragment.showLoadingDialog(R.string.processing_response, getString(R.string.one_moment_please));
                /*
                currentUser.submitSurveyResponse(question, rating, new SurveyResponseCallback() {

                    @Override
                    public void onResponseRecorded(SurveyResponse response) {
                        parentFragment.hideProgressDialog();
                        question.setResponse(response);
                        disableRatings();
                        questionAnsweredListener.onSurveyQuestionAnswered(question);
                    }

                    @Override
                    public void onResponseError(ParseException e) {
                        Toast.makeText(getActivity(), "Error saving response", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, e.getMessage());
                        parentFragment.hideProgressDialog();
                    }
                });
                */

                // by Sarika [27-09-2017]
                currentUser.submitSurveyResponse(question, rating, sessionId, speakerId, new SurveyResponseCallback() {

                    @Override
                    public void onResponseRecorded(SurveyResponse response) {
                        parentFragment.hideProgressDialog();
                        question.setResponse(response);

                        disableRatings();
                        questionAnsweredListener.onSurveyQuestionAnswered(question);
                    }

                    @Override
                    public void onResponseError(ParseException e) {
                        Toast.makeText(getActivity(), "Error saving response", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, e.getMessage());
                        parentFragment.hideProgressDialog();
                    }
                });
            }
        }

        private int getRating() {
            if (ratingStarFive.isChecked()) {
                return 5;
            } else if (ratingStarFour.isChecked()) {
                return 4;
            } else if (ratingStarThree.isChecked()) {
                return 3;
            } else if (ratingStarTwo.isChecked()) {
                return 2;
            } else if (ratingStarOne.isChecked()) {
                return 1;
            }
            return 0;
        }

        private void setRating() {
            int rating = question.getResponse().getRating();

            if (rating >= 1) {
                ratingStarOne.setChecked(true);
            }

            if (rating >= 2) {
                ratingStarTwo.setChecked(true);
            }

            if (rating >= 3) {
                ratingStarThree.setChecked(true);
            }

            if (rating >= 4) {
                ratingStarFour.setChecked(true);
            }

            if (rating == 5) {
                ratingStarFive.setChecked(true);
            }
        }

        private void disableRatings() {
            ratingStarOne.setEnabled(false);
            ratingStarTwo.setEnabled(false);
            ratingStarThree.setEnabled(false);
            ratingStarFour.setEnabled(false);
            ratingStarFive.setEnabled(false);
        }
    }
}
