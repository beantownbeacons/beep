package com.digitalwavefront.beepetc.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.digitalwavefront.beepetc.BeConApp;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.MainActivity;
import com.digitalwavefront.beepetc.adapters.DashboardListAdapter;
import com.digitalwavefront.beepetc.navigation.NavigationItemPositionManager;
import com.digitalwavefront.beepetc.parse.models.BeConUser;

/**
 * Created by Wayne on 10/18/15.
 */
public class DashboardFragment extends Fragment {

    private BeConUser currentUser;
    private ListView dashboardListView;
    private DashboardListAdapter dashboardListAdapter;

    public DashboardFragment() {}

    public static DashboardFragment newInstance() {
        return new DashboardFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentUser = BeConUser.getCurrentUser();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        dashboardListView = (ListView) rootView.findViewById(R.id.dashboard_list_view);
        dashboardListAdapter = new DashboardListAdapter(getActivity(), currentUser.getLoginType(getActivity()));
        dashboardListView.setAdapter(dashboardListAdapter);

        dashboardListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectDashboardItem(position);
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.dashboard));
        NavigationItemPositionManager navigationItemPositionManager = ((BeConApp) getActivity().getApplication()).getNavigationItemPositionManager();
        ((MainActivity) getActivity()).setSelectedNavigationPosition(navigationItemPositionManager.getDashboardPosition());
    }

    private void selectDashboardItem(int position) {
        switch (position) {
            case DashboardListAdapter.DASHBOARD_POSITION_EVENT_SNAPSHOT:
                showEventSnapshot();
                break;
            case DashboardListAdapter.DASHBOARD_POSITION_SESSION_METRICS:
                showSessionMetrics();
                break;
            case DashboardListAdapter.DASHBOARD_POSITION_SPEAKERS_BY_RANK:
                showSpeakersByRank();
                break;
            case DashboardListAdapter.DASHBOARD_POSITION_BOOTH_METRICS:
                showBoothMetrics();
                break;
            default:
                break;
        }
    }

    private void showEventSnapshot() {
        ((MainActivity) getActivity()).showEventSnapshotFragment();
    }

    private void showSessionMetrics() {
        ((MainActivity) getActivity()).showShowSessionMetricsFragment();
    }

    private void showSpeakersByRank() {
        ((MainActivity) getActivity()).showShowSpeakersByRankFragment();
    }

    private void showBoothMetrics() {
        ((MainActivity) getActivity()).showBoothMetricsFragment();
    }
}
