package com.digitalwavefront.beepetc.fragments;


import android.support.v4.app.Fragment;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.interfaces.FloatLoadedCallback;
import com.digitalwavefront.beepetc.interfaces.IntegerLoadedCallback;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Event;
import com.digitalwavefront.beepetc.utils.StringUtils;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;

public class EventSnapshotFragment extends Fragment {

    private BeConUser currentUser;
    private int loginType;
    private String eventId;
    private TextView interactionsTextView, interactionsTitleTextView,
    averageTimeTextView, averageTimeTitleTextView, longestInteractionTextView,
    longestInteractionTitleTextView;

    public EventSnapshotFragment() {}

    public static EventSnapshotFragment newInstance() {
        EventSnapshotFragment fragment = new EventSnapshotFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentUser = BeConUser.getCurrentUser();
        eventId = currentUser.getCurrentEventId();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        loginType = currentUser.getLoginType(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_event_snapshot, container, false);
        interactionsTextView = (TextView) rootView.findViewById(R.id.interactions_text_view);
        interactionsTitleTextView = (TextView) rootView.findViewById(R.id.interactions_title_text_view);
        averageTimeTextView = (TextView) rootView.findViewById(R.id.average_time_text_view);
        averageTimeTitleTextView = (TextView) rootView.findViewById(R.id.average_time_title_text_view);
        longestInteractionTextView = (TextView) rootView.findViewById(R.id.longest_interaction_text_view);
        longestInteractionTitleTextView = (TextView) rootView.findViewById(R.id.longest_interaction_title_text_view);

        // Set fonts
        Typeface boldTypeface = TypefaceUtils.getBoldTypeface(getActivity());
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());

        interactionsTextView.setTypeface(boldTypeface);
        interactionsTitleTextView.setTypeface(lightTypeface);
        averageTimeTextView.setTypeface(boldTypeface);
        averageTimeTitleTextView.setTypeface(lightTypeface);
        longestInteractionTextView.setTypeface(boldTypeface);
        longestInteractionTitleTextView.setTypeface(lightTypeface);

        getMetrics();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.event_snapshot));
    //    ((MainActivity) getActivity()).setSelectedNavigationPosition(NavigationItemPositionManager.NO_POSITION);
    }

    private void getMetrics() {
        getInteractions();
        getAverageTimePerInteraction();
        getLongestMeasuredInteraction();
    }

    private void getInteractions() {
        Event.getTotalInteractionCount(getActivity(), eventId, new IntegerLoadedCallback() {
            @Override
            public void onIntegerLoaded(int count) {
                interactionsTextView.setText(String.valueOf(count));
            }
        });
    }

    private void getAverageTimePerInteraction() {
        Event.getAverageTimePerInteraction(getActivity(), eventId, new FloatLoadedCallback() {

            @Override
            public void onFloatLoaded(float averageSeconds) {
                String average = StringUtils.getMinuteSecondStringFromSeconds(averageSeconds);
                averageTimeTextView.setText(average);
            }
        });
    }

    private void getLongestMeasuredInteraction() {
        Event.getLongestInteractionForEvent(getActivity(), eventId, new IntegerLoadedCallback() {

            @Override
            public void onIntegerLoaded(int longestInteractionSeconds) {
                String longestInteraction = StringUtils.getMinuteSecondStringFromSeconds(longestInteractionSeconds);
                longestInteractionTextView.setText(longestInteraction);
            }
        });
    }
}
