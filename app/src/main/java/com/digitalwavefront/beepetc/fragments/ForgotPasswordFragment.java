package com.digitalwavefront.beepetc.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.LoginActivity;
import com.digitalwavefront.beepetc.utils.StringUtils;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.utils.ViewUtils;
import com.gc.materialdesign.views.ButtonRectangle;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Praneet on 26/08/2016.
 */
public class ForgotPasswordFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {

    TextInputLayout til_phoneno;
    TextInputEditText tiet_phoneno;
    ButtonRectangle proceed;
    TextView cancel;
    LinearLayout root_layout;
    String phno;
    SharedPreferences sharedpreferences;

    public static ForgotPasswordFragment newInstance() {
        return new ForgotPasswordFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_forgot_password, container, false);

        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());

        root_layout = (LinearLayout) rootView.findViewById(R.id.forpass_layout);
        til_phoneno = (TextInputLayout) rootView.findViewById(R.id.phoneno_textlayout);
        tiet_phoneno = (TextInputEditText) rootView.findViewById(R.id.phoneno_field);
        til_phoneno.setTypeface(lightTypeface);
        tiet_phoneno.setTypeface(lightTypeface);
        ViewUtils.tintViews(getActivity(), android.R.color.white, tiet_phoneno);
        proceed = (ButtonRectangle) rootView.findViewById(R.id.btn_proceed);
        proceed.setOnClickListener(this);
        cancel = (TextView) rootView.findViewById(R.id.tv_cancel);
        cancel.setPaintFlags(cancel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        cancel.setOnClickListener(this);
        sharedpreferences = getActivity().getSharedPreferences("Mypref", Context.MODE_PRIVATE);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_proceed:
                checkphonenumber();
                //showVerifyFragment();
                break;

            case R.id.tv_cancel:
                Toast.makeText(getActivity(), "Your password change request was cancelled.", Toast.LENGTH_SHORT).show();
                showLoginFragment();
                break;
        }
    }

    private void showVerifyFragment() {
        ((LoginActivity) getActivity()).showVerificationFragment();
    }

    private void showLoginFragment() {
        ((LoginActivity) getActivity()).showLoginFragment();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.forpass_layout:
                root_layout.requestFocus();
                StringUtils.hideSoftKeyboard(getActivity());
                break;
        }
        return false;
    }

    public void checkphonenumber() {
        phno = tiet_phoneno.getText().toString();
        if (phno.length() == 0) {
            Toast.makeText(getActivity(), R.string.empty_fields, Toast.LENGTH_LONG).show();
        } else {
            Map<String, String> params = new HashMap<String, String>();
            params.put("phoneNumber", phno);
            ParseCloud.callFunctionInBackground("sendVerificationCodeInForgotPassword", params, new FunctionCallback<Object>() {
                private String msg;
                private String code;

                @Override
                public void done(Object objects, ParseException exc) {
                    if (exc == null) {
                        String result = "";
                        result = objects.toString();
                        System.out.println(result);
                        Toast.makeText(getActivity(), "Message sent successfully!", Toast.LENGTH_LONG).show();
                        SharedPreferences.Editor editor = sharedpreferences.edit();

                        editor.putString("phno", phno);
                        editor.commit();
                        showVerifyFragment();
                    } else {
                        System.out.println(exc);

                        //Toast.makeText(getActivity(), "Phone number not found in our records. Please try again.", Toast.LENGTH_LONG).show();
                        Toast.makeText(getActivity(), exc.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    }

                }


            });

        }
    }
}
