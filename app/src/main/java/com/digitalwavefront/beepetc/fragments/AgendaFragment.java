package com.digitalwavefront.beepetc.fragments;


import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.MainActivity;
import com.digitalwavefront.beepetc.adapters.SessionsAdapter;
import com.digitalwavefront.beepetc.interfaces.SessionRSVPChangeListener;
import com.digitalwavefront.beepetc.interfaces.SessionsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SpeakersLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Session;
import com.digitalwavefront.beepetc.parse.models.Speaker;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import info.hoang8f.android.segmented.SegmentedGroup;

/**
 * Created by Wayne on 9/18/15.
 */

/**
 * Made changes by Supra-Chaitali on 25-04-2016.
 */
public class AgendaFragment extends Fragment implements SessionRSVPChangeListener, View.OnClickListener {

    private static final int ALL_ROOM_FILTER_POSITION = 0;

    private BeConUser currentUser;
   // private TextView roomFilterTitleText;
   // private Spinner roomFilterSpinner;
    private ListView agendaListView;
   // private SpinnerAdapter roomSpinnerAdapter;
    private SessionsAdapter sessionsAdapter;
    private ProgressDialog progressDialog;
    private ProgressDialog rsvpDialog;
    private ArrayList<String> start_dates;
    private Button offlinebtn, onlinebtn;
    private int onlineFlag;
    private LinearLayout ll;
    private SegmentedGroup segmented2;
    private List<Session> sessions;
    private List<Session> session_list;
    private HashMap<String, List<Session>> hash;
    private AlertDialog alert_Dialog;
    private int optionMenu_flag=0;
    public static boolean isScrolling=true;
    public AgendaFragment() {}
    private HashMap<String,HashMap<String,ParseFile>> sessSpkDic;
    private HashMap<String,HashMap<String,String>> sessSpkNameDic;
    private HashMap<String,Integer> spkIdDic;
    TextView textView_no_agenda;

    boolean userScrolled;
    public static AgendaFragment newInstance() {
        return new AgendaFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHasOptionsMenu(true);
        currentUser = BeConUser.getCurrentUser();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_agenda, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_my_agenda:
                showMyAgenda();
                break;
            case R.id.menu_item_filter:
                if (optionMenu_flag==1)
                {   setupRoomFilter(session_list);}
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupRoomFilter(List<Session> session_room_list) {
        getDialog(session_room_list);
    }

    void getDialog(final List<Session> session_dialog_list){
        final  int position = 0;
        int pos=0;
        final AlertDialog.Builder singlechoicedialog = new AlertDialog.Builder(getActivity());
        ArrayList<String> str_list = new ArrayList<>();
        str_list.add("All");
        for (Session session : session_dialog_list) {
            if (session.getRoomName()!=null && !str_list.contains(session.getRoomName())){
                str_list.add(session.getRoomName());
                Log.e("if", "loop room"+session.getRoomName());
            }else {
                Log.e("else","else loop");
            }
        }
        final String[] roomTitles = new String[str_list.size()];

        for (int i=0; i<str_list.size(); i++){
            roomTitles[i] = str_list.get(i);
        }
        singlechoicedialog.setTitle("Filter By Room");
        singlechoicedialog
                .setSingleChoiceItems(roomTitles, -1,
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface dialog,
                                    int item) {
                                if (item == ALL_ROOM_FILTER_POSITION) {
                                    sessionsAdapter.showAllSessions();
                                    alert_Dialog.getListView().setItemChecked(item, true);
                                } else {
                                    Log.e("if", "item room "+ roomTitles[item]);
                                    String roomTitle = roomTitles[item];
                                    sessionsAdapter.filterSessionsByRoomTitle(roomTitle);
                                    alert_Dialog.getListView().setItemChecked(item, true);
                                }
                                dialog.cancel();
                            }
                        });
        alert_Dialog = singlechoicedialog.create();
        alert_Dialog.show();
        alert_Dialog.getListView().setItemChecked(position, true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_agenda, container, false);

        agendaListView = (ListView) rootView.findViewById(R.id.agenda_list_view);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(R.string.loading_sessions);
        progressDialog.setMessage(getString(R.string.one_moment_please));
        progressDialog.setCanceledOnTouchOutside(false);

        rsvpDialog  = new ProgressDialog(getActivity());
        rsvpDialog.setTitle(R.string.modifying_rsvp);
        rsvpDialog.setMessage(getString(R.string.one_moment_please));
        rsvpDialog.setCanceledOnTouchOutside(false);

        ll = (LinearLayout) rootView.findViewById(R.id.segment_layout);
        segmented2 = new SegmentedGroup(getActivity());
        segmented2.setOrientation(LinearLayout.HORIZONTAL);
        segmented2.setTintColor(Color.parseColor("#00BCD4"));
        segmented2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
            }
        });

        agendaListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view == agendaListView && motionEvent.getAction() == MotionEvent.ACTION_SCROLL) {
                    userScrolled = true;
                } else {
                    //userScrolled = false;
                }

                return false;
            }
        });
        agendaListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                if (scrollState == SCROLL_STATE_IDLE) {
                    Log.e("onScroll", "onScrollStateChanged");
                    isScrolling = false;
                }

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                Log.e("onScroll", "onScroll");
                if (userScrolled) {
                    // && firstVisibleItem + visibleItemCount == totalItemCount) {

                    isScrolling = true;
                    agendaListView.setOnTouchListener(null);
                }
            }
        });

        agendaListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.e("Session_list", "SpeakerId ==== " + session_list.get(position).getSpeakerId());
                //isScrolling = false;
                //if (!isScrolling) {
                if (session_list.get(position).getDetails() != null && session_list.get(position).getDetails().length() > 0) {
                    if ((session_list.get(position).getSpeakerId() != null && session_list.get(position).getSpeakerId().length() > 0) || (session_list.get(position).getDetails() != null)) {
                        sessionsAdapter.toggleItem(position);
                    }
                }
                //}
            }
        });

        textView_no_agenda = (TextView) rootView.findViewById(R.id.txt_no_agenda);
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
        textView_no_agenda.setTypeface(lightTypeface);

        // Set fonts
        Typeface boldTypeface = TypefaceUtils.getBoldTypeface(getActivity());
       // roomFilterTitleText.setTypeface(boldTypeface);

        getSessions();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.agenda));
    //    NavigationItemPositionManager navigationItemPositionManager = ((BeConApp) getActivity().getApplication()).getNavigationItemPositionManager();
    //    ((MainActivity) getActivity()).setSelectedNavigationPosition(navigationItemPositionManager.getAgendaPosition());
    }

    private void getSessions() {
        // Show progress dialog
        showProgressDialog();

        sessSpkDic = new HashMap<String,HashMap<String,ParseFile>>();
        sessSpkNameDic = new HashMap<String,HashMap<String,String>>();
        spkIdDic = new HashMap<String,Integer>();
        String eventId = BeConUser.getCurrentUser().getCurrentEventId();
        Session.getSessionsByEventId(eventId, new SessionsLoadedListener() {

            @Override
            public void onSessionsLoaded(@Nullable List<Session> sessions) {

                Log.e("Praneet","sessions"+sessions.size());
                // Hide progress dialog
                hideProgressDialog();

                if (sessions != null && sessions.size()!=0) {

                    Log.e("Praneet","if");
                    /// session_list = sessions;
                    hash = new HashMap<String, List<Session>>();

                    start_dates = new ArrayList<String>();
                    for (int i = 0; i < sessions.size(); i++) {

                        try {
                            final Session sessObj = sessions.get(i);

                            String sdt = sessions.get(i).FormattedStartDate();
                            Log.e("Praneet", "strat date:: " + sdt);
                            if (!start_dates.contains(sessions.get(i).FormattedStartDate())) {
                                start_dates.add(sdt);
                                ArrayList<Session> list = new ArrayList<Session>() {
                                };
                                hash.put(sdt, list);
                            }
                            hash.get(sdt).add(sessions.get(i));

                            //added by Sarika [3/5/2016]
                            spkIdDic.put(sessObj.getObjectId(), 0);

                            final HashMap<String, ParseFile> imgMap = new HashMap<String, ParseFile>();
                            final HashMap<String, String> nameMap = new HashMap<String, String>();
                            // get speaker ids
                            List<String> spkIds = sessions.get(i).getSpeakerIds();

                            if (spkIds != null) {
                                Log.e("spkIds", "spkIds: " + spkIds);
                                Speaker.getSpeakersByObjectIds(spkIds, new SpeakersLoadedListener() {
                                    @Override
                                    public void onSpeakersLoaded(@Nullable List<Speaker> speakers) {

                                        if (speakers != null) {

                                            String spkNames = "";
                                            Log.e("speakers", "speakers size: " + speakers.size());

                                            for (int j = 0; j < speakers.size(); j++) {

                                                final Speaker speaker = speakers.get(j);
                                                ParseFile avatarFile = speaker.getParseFile("avatar");

                                                imgMap.put(speaker.getObjectId(), avatarFile);
                                                nameMap.put(speaker.getObjectId(), speaker.getFullName());
                                                sessSpkDic.put(sessObj.getObjectId(), imgMap);
                                                sessSpkNameDic.put(sessObj.getObjectId(), nameMap);
                                                spkNames = spkNames + speaker.getFullName();
                                                if (j < speakers.size()-1){
                                                    spkNames = spkNames + ",";
                                                }

                                                //sessionsAdapter.notifyDataSetChanged();

                                            /*
                                            try {
                                                Bitmap bitmap = Picasso.with(getActivity().getApplicationContext()).load(avatarFile.getUrl()).resize(80,80).get();
                                                if (bitmap != null) {
                                                    Log.e("speaker bitmap","bitmap: "+bitmap);
                                                    imgMap.put(speaker.getObjectId(), bitmap);
                                                    sessSpkDic.put(sessObj.getObjectId(), imgMap);

                                                    sessionsAdapter.notifyDataSetChanged();
                                                } else {

                                                }
                                            }catch (Exception e){

                                            }*/


                                            /*
                                            speaker.getAvatar(new BitmapLoadedListener() {
                                                @Override
                                                public void onBitmapLoaded(Bitmap bitmap) {

                                                    if (bitmap != null) {
                                                        Log.e("speaker bitmap","bitmap: "+bitmap);
                                                        imgMap.put(speaker.getObjectId(), bitmap);
                                                        sessSpkDic.put(sessObj.getObjectId(), imgMap);

                                                        sessionsAdapter.notifyDataSetChanged();
                                                    } else {

                                                    }
                                                }
                                            });
                                            */
                                            }
                                            sessObj.setSpkeakerNames(spkNames);
                                        }

                                    }
                                });
                            }
                        }catch (Exception e) {

                        }
                    }
                    if (start_dates.size() > 0) {
                        getSegments(start_dates.size());
                        Log.e("Start_dates_size", String.valueOf(start_dates.size()));
                        sessions = hash.get(start_dates.get(0));
                        Log.e("Start_dates", start_dates.get(0));
                    }
                    session_list = sessions;
                    if (sessions != null) {
                        session_list = sessions;
                        sessionsAdapter = new SessionsAdapter(getActivity(), getActivity(), sessions, AgendaFragment.this, sessSpkDic, spkIdDic, sessSpkNameDic);
                        agendaListView.setAdapter(sessionsAdapter);
                        sessionsAdapter.notifyDataSetChanged();
                    }
                    if (session_list != null) {
                        optionMenu_flag = 1;
                    } else {
                        optionMenu_flag = 0;
                    }
                } else {
                    Log.e("Praneet","else");
                    showErrorMessage();
                }
            }
        }, false);
    }


    private void showMyAgenda() {
        ((MainActivity) getActivity()).showMyAgendaFragment();
    }

    private void showProgressDialog() {
        progressDialog.show();
    }

    private void hideProgressDialog() {
        progressDialog.dismiss();
    }

    private void showRSVPDialog() {
        rsvpDialog.show();
    }

    private void hideRSVPDialog() {
        rsvpDialog.dismiss();
    }

    private void showErrorMessage() {
        //Toast.makeText(getActivity(), R.string.error_loading_sessions, Toast.LENGTH_SHORT).show();
        textView_no_agenda.setVisibility(View.VISIBLE);
        textView_no_agenda.setText(getResources().getString(R.string.no_agenda));
        agendaListView.setVisibility(View.GONE);
        ll.setVisibility(View.GONE);

    }
/*
    private OnItemSelectedListener roomFilterItemSelectedListener = new OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            if (position == ALL_ROOM_FILTER_POSITION) {
               sessionsAdapter.showAllSessions();
            } else {
                String roomTitle = roomSpinnerAdapter.getItem(position);
                sessionsAdapter.filterSessionsByRoomTitle(roomTitle);
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };*/
    // Session RSVP Change Listener
    @Override
    public void onRSVPChange(Session session, boolean isGoing) {
        showRSVPDialog();
        SaveCallback callback = new SaveCallback() {

            @Override
            public void done(ParseException e) {
                hideRSVPDialog();
            }
        };

        if (isGoing) {
            currentUser.rsvpForSession(session, callback);
        } else {
            currentUser.unregisterForSession(session, callback);
        }
    }
    public void getSegments(int size){
        final RadioButton[] rb = new RadioButton[size];
        for(int j=0; j<size; j++){
            rb[j] = new RadioButton(getActivity(), null, info.hoang8f.android.segmented.R.style.RadioButton);
            String sdt = start_dates.get(j);
            rb[j].setText(sdt);
            rb[j].setId(j);
            rb[j].setTextColor(Color.parseColor("#00BCD4"));
            rb[j].setGravity(Gravity.CENTER);
            rb[j].setLayoutParams(new RadioGroup.LayoutParams(ll.getWidth(), ll.getHeight(), 1f));
            rb[j].setOnClickListener(this);
            segmented2.addView(rb[j]); //the RadioButtons are added to the radioGroup instead of the layout
        }
        segmented2.check(0);
        segmented2.updateBackground();
        ll.addView(segmented2);
    }

    @Override
    public void onClick(View v) {

        RadioButton rb = (RadioButton)v;
        sessions = hash.get(rb.getText().toString());
        if(sessions!=null){
            session_list = sessions;
            sessionsAdapter = new SessionsAdapter(getActivity(), getActivity(), sessions, AgendaFragment.this,sessSpkDic,spkIdDic,sessSpkNameDic);
            agendaListView.setAdapter(sessionsAdapter);
            sessionsAdapter.notifyDataSetChanged();
        }

    }
}
