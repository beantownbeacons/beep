package com.digitalwavefront.beepetc.fragments;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonRectangle;
import com.parse.ParseException;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.LoginActivity;
import com.digitalwavefront.beepetc.interfaces.EventLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SignInListener;
import com.digitalwavefront.beepetc.login.LoginType;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Event;
import com.digitalwavefront.beepetc.utils.StringUtils;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;

public class OrganizerLoginFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "OrganizerLoginFragment";

    private TextView titleText, enterAccessCodeText;
    private EditText organizerCodeField;
    private ButtonRectangle accessEventBtn;
    private ProgressDialog progressDialog;

    public OrganizerLoginFragment() {}

    public static OrganizerLoginFragment newInstance() {
        OrganizerLoginFragment fragment = new OrganizerLoginFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_organizer_login, container, false);
        titleText = (TextView) rootView.findViewById(R.id.title_text);
        enterAccessCodeText = (TextView) rootView.findViewById(R.id.enter_access_code_text);
        organizerCodeField = (EditText) rootView.findViewById(R.id.organizer_code_field);
        accessEventBtn = (ButtonRectangle) rootView.findViewById(R.id.access_event_btn);
        progressDialog = new ProgressDialog(getActivity());

        // Fonts
        Typeface boldTypeface = TypefaceUtils.getBoldTypeface(getActivity());
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
        titleText.setTypeface(boldTypeface);
        enterAccessCodeText.setTypeface(lightTypeface);
        organizerCodeField.setTypeface(lightTypeface);

        // Clicks
        accessEventBtn.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.access_event_btn:
                attemptSignInAsOrganizer();
                break;
        }
    }

    private void attemptSignInAsOrganizer() {
        String organizerCode = organizerCodeField.getText().toString().trim();

        if (verifyFields(organizerCode)) {

            // Show progress
            showProgressDialog(getString(R.string.one_moment_please), getString(R.string.finding_event));

            // Get the event by the organizer code
            Event.getEventByOrganizerCode(organizerCode, new EventLoadedListener() {

                @Override
                public void onEventLoaded(@Nullable Event event) {

                    if (event == null) {
                        // Event not found
                        Toast.makeText(getActivity(), R.string.invalid_access_code, Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    } else {
                        // Event found, sign in as organizer.
                        signIntoEventAsOrganizer(event);
                    }
                }
            });
        }
    }

    private boolean verifyFields(String organizerCode) {
        if (StringUtils.isNullOrEmpty(organizerCode)) {
            Toast.makeText(getActivity(), R.string.empty_fields, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void signIntoEventAsOrganizer(Event event) {
        final BeConUser user = BeConUser.getCurrentUser();
        ((LoginActivity) getActivity()).signIntoEvent(user, event, LoginType.ORGANIZER, new SignInListener() {
            @Override
            public void onSignIn() {
                hideProgressDialog();
            }

            @Override
            public void onError(ParseException e) {
                Log.e(TAG, "Error signing into event as organizer", e);
                hideProgressDialog();
                Toast.makeText(getActivity(), R.string.event_login_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showProgressDialog(String title, String message) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    private void hideProgressDialog() {
        progressDialog.dismiss();
    }
}
