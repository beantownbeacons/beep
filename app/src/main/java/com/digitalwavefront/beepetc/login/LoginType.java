package com.digitalwavefront.beepetc.login;

/**
 * Created by Wayne on 11/22/15.
 */
public abstract class LoginType {
    public static final int INVALID_VALUE = -1;
    public static final int ATTENDEE = 1;
    public static final int ORGANIZER = 2;
    public static final int EXHIBITOR = 3;
}
