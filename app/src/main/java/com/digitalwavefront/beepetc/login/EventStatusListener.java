package com.digitalwavefront.beepetc.login;

import com.digitalwavefront.beepetc.parse.models.Event;

/**
 * Created by Wayne on 12/12/15.
 */
public interface EventStatusListener {
    void onEventSignIn(Event event, int loginType);
    void onEventSignOut();
}
