package com.digitalwavefront.beepetc.services;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by User on 1/23/18.
 */

public class TokenRefreshListenerService extends InstanceIDListenerService{

    @Override
    public void onTokenRefresh() {
        //super.onTokenRefresh();
        Intent i = new Intent(this, GcmRegisteringService.class); startService(i);
    }
}
