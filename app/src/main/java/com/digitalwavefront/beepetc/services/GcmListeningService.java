package com.digitalwavefront.beepetc.services;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;


import static android.content.ContentValues.TAG;

/**
 * Created by User on 1/23/18.
 */

public class GcmListeningService extends GcmListenerService{

    @Override
    public void onMessageReceived(String from, Bundle bundle) {
        String message = bundle.getString("message");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);


    }
}
