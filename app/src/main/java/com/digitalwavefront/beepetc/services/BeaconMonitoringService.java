package com.digitalwavefront.beepetc.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.digitalwavefront.beepetc.BeConApp;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.MainActivity;
import com.digitalwavefront.beepetc.interfaces.BeaconLoadedListener;
import com.digitalwavefront.beepetc.interfaces.BeaconsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.EventLoadedListener;
import com.digitalwavefront.beepetc.interfaces.NotificationsLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConBeacon;
import com.digitalwavefront.beepetc.parse.models.BeConNotification;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.BeaconInteraction;
import com.digitalwavefront.beepetc.parse.models.Event;
import com.digitalwavefront.beepetc.parse.models.Session;
import com.digitalwavefront.beepetc.receivers.SaveNotificationReceiver;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.BeaconManager.MonitoringListener;
import com.estimote.sdk.Region;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static android.support.v4.app.NotificationCompat.Action;

/**
 * Created by Wayne on 9/27/15.
 */

/**
 * Changed by supra on 02/04/2016
 */
public class BeaconMonitoringService extends Service implements MonitoringListener {

    private static final String TAG = "BeaconMonitoringService";
    private BeaconManager beaconManager;
    private List<Session> sessions;
    private List<BeConBeacon> beacons;
   /* private HashMap<Session, Handler> timerHandlerMap;
    private HashMap<Session, Runnable> timerRunnableMap;
    private HashMap<Session, Integer> timeInSessionMap;*/
   private HashMap<String, Handler> timerHandlerMap;
    private HashMap<String, Runnable> timerRunnableMap;
    private HashMap<String, Integer> timeInSessionMap;
    private HashMap<String, Date> timeEnteredMap;
    private BeConUser currentUser;

    // Timer Constants
    private static final int TIMER_INTERVAL_IN_SECONDS = 1;
    private static final int TIMER_INTERVAL_IN_MILLIS = TIMER_INTERVAL_IN_SECONDS * 1000;

    @Override
    public void onCreate() {
        Log.d(TAG, "Beacon Service Created");
        super.onCreate();

        currentUser = BeConUser.getCurrentUser();
        if (currentUser != null && currentUser.isSignedIntoEvent()) {
            beaconManager = new BeaconManager(getApplicationContext());
            beaconManager.setMonitoringListener(this);
            timerHandlerMap = new HashMap<>();
            timerRunnableMap = new HashMap<>();
            timeInSessionMap = new HashMap<>();
            timeEnteredMap = new HashMap<>();
            /*
            currentUser.getCurrentEvent(new EventLoadedListener() {

                @Override
                public void onEventLoaded(@Nullable Event event) {

                    if (event != null) {
                        event.getSessions( new SessionsLoadedListener() {

                            @Override
                            public void onSessionsLoaded(@Nullable List<Session> sessions) {

                                if (sessions != null && !sessions.isEmpty()) {
                                    Log.d(TAG, "Sessions fetched");
                                    BeaconMonitoringService.this.sessions = sessions;
                                    startMonitoringBeacons();
                                } else {
                                    // TODO: Handle no sessions
                                    Log.d(TAG, "No sessions found for this event");
                                    stopThisService();
                                }
                            }
                        }, true);
                    } else {
                        Log.d(TAG, "User is not signed into event");
                        stopThisService();
                    }

                }
            });

            */
            // get beacons by event for monitoring
            currentUser.getCurrentEvent(new EventLoadedListener() {
                @Override
                public void onEventLoaded(@Nullable Event event) {

                    if (event != null) {

                        BeConBeacon.getBeaconsByEventId(event.getObjectId(), new BeaconsLoadedListener() {
                            @Override
                            public void onBeaconsLoaded(@Nullable List<BeConBeacon> beacons) {
                                if (beacons != null && !beacons.isEmpty()){
                                    Log.d("beacon","loaded");
                                    BeaconMonitoringService.this.beacons = beacons;
                                    startMonitoringBeacons();
                                }else {
                                    Log.d(TAG, "No becon found for this event");
                                    stopThisService();
                                }

                            }
                        });

                        // get trigger beacons for monitoring (to catch attendee for event)

                        List<String> triggerbeaconIds;

                        triggerbeaconIds = event.getTriggerBeaconIds();
                       if(triggerbeaconIds != null){
                        if(triggerbeaconIds.isEmpty()){
                            Log.e("before null:","hiiii");
                            BeConBeacon.getBeaconsByIds(triggerbeaconIds, new BeaconsLoadedListener() {
                                @Override
                                public void onBeaconsLoaded(final @Nullable List<BeConBeacon> beacons) {

                                    beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
                                        @Override
                                        public void onServiceReady() {
                                            try {

                                                Log.d(TAG, "Beacon Manager Connected");
                                                for (BeConBeacon beacon : beacons) {
                                                    Log.d("trigger beconId: ", "" + beacon.getObjectId());
                                                    beacon.setIsTriggerBeacon(true);
                                                    BeaconMonitoringService.this.beacons.add(beacon);
                                                    beaconManager.startMonitoring(beacon.getRegion());
                                                    // Timer setup
                                                    timerHandlerMap.put(beacon.getObjectId(), new Handler());
                                                    timerRunnableMap.put(beacon.getObjectId(), getTimerRunnable(timerHandlerMap.get(beacon.getObjectId()), beacon));
                                                    timeInSessionMap.put(beacon.getObjectId(), 0);
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                Log.e(TAG, "Error starting monitoring " + e.getMessage());
                                            }
                                        }
                                    });

                                }
                            });
                        }
                       }else {
                           Log.d(TAG, "Beacon is not assigned to the event");
                           stopThisService();
                       }



                    }else {
                        Log.d(TAG, "User is not signed into event");
                        stopThisService();
                    }
                }
            });
        } else {
            Log.d(TAG, "User is not signed in");
            stopThisService();
        }

        // get trigger beacons for monitoring (to catch attendee for event)

       /* currentUser.getCurrentEvent(new EventLoadedListener() {
            @Override
            public void onEventLoaded(@Nullable Event event) {
                if(event!=null){
                    List<String> triggerbeaconIds;
                    triggerbeaconIds = event.getTriggerBeaconIds();
                    BeConBeacon.getBeaconsByIds(triggerbeaconIds, new BeaconsLoadedListener() {
                        @Override
                        public void onBeaconsLoaded(final @Nullable List<BeConBeacon> beacons) {


                                    beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
                                        @Override
                                        public void onServiceReady() {
                                            try {

                                                Log.d(TAG, "Beacon Manager Connected");
                                                for (BeConBeacon beacon : beacons) {
                                                    Log.d("trigger beconId: ", "" + beacon.getObjectId());
                                                    beacon.setIsTriggerBeacon(true);

                                                    beaconManager.startMonitoring(beacon.getRegion());
                                                    // Timer setup
                                                    timerHandlerMap.put(beacon.getObjectId(), new Handler());
                                                    timerRunnableMap.put(beacon.getObjectId(), getTimerRunnable(timerHandlerMap.get(beacon.getObjectId()), beacon));
                                                    timeInSessionMap.put(beacon.getObjectId(), 0);
                                                }
                                            }catch (RemoteException e){
                                                e.printStackTrace();
                                                Log.e(TAG, "Error starting monitoring " + e.getMessage());
                                            }
                                        }
                                    });

                        }
                    });

                }
            }
        });*/

       //getNotification();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Beacon Service Destroyed");
        if (beaconManager != null && beacons != null) {
            stopMonitoringBeacons();

            for (BeConBeacon beacon : beacons) {
                stopTiming(beacon);
            }
        }
        super.onDestroy();
    }

    // Kills this service and the alarm associated with it
    private void stopThisService() {
        Log.d(TAG, "Stopping beacons service internally");
        ((BeConApp) getApplication()).stopBeaconMonitoringService();
    }


    // Beacon Monitoring Methods
    private void startMonitoringBeacons() {
        Log.d(TAG, "Starting to monitor beacons");
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {

            @Override
            public void onServiceReady() {
                Log.d(TAG, "Beacon Manager Connected");
                try {
                    /*for (Session session : sessions) {
                        Log.d(TAG, "Monitoring time in session " + session.getTitle());
                        BeConBeacon beacon = session.getBeacon();
                        beaconManager.startMonitoring(beacon.getRegion());

                        // Timer setup
                        timerHandlerMap.put(session, new Handler());
                        timerRunnableMap.put(session, getTimerRunnable(timerHandlerMap.get(session), session));
                        timeInSessionMap.put(session, 0);
                    }*/

                    for (BeConBeacon beacon : beacons) {
                        Log.d("beconId: ",""+beacon.getObjectId());
                        try {
                            if (beacon.getUUID() != null && beacon.getUUID().toString().length() > 0) {
                                beaconManager.startMonitoring(beacon.getRegion());//(beacon.getRegionByUUID());
                                // Timer setup
                                timerHandlerMap.put(beacon.getObjectId(), new Handler());
                                timerRunnableMap.put(beacon.getObjectId(), getTimerRunnable(timerHandlerMap.get(beacon.getObjectId()), beacon));
                                timeInSessionMap.put(beacon.getObjectId(), 0);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "Error starting monitoring " + e.getMessage());
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "Error starting monitoring " + e.getMessage());
                }
            }
        });
    }

    private void stopMonitoringBeacons() {
        if (beacons != null) {
            try {
                for (BeConBeacon beacon : beacons) {
                    Log.d(TAG, "Stopping monitoring for session " + beacon.getObjectId());
                   // BeConBeacon beacon = session.getBeacon();
                    beaconManager.stopMonitoring(beacon.getRegion());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // Monitoring Listener
    @Override
    public void onEnteredRegion(Region region, List<Beacon> list) {
        //Session sessionsForRegion = getSessionForRegion(region);
/*
* proximityUUID=b9407f30-f5f8-466e-aff9-25556b57fe6d, major=1512, minor=7041
* proximityUUID=b9407f30-f5f8-466e-aff9-25556b57fe6d, major=62792, minor=6634, measuredPower=-72
* */
        BeConBeacon beaconForRegion = getBeaconForRegion(region);
        Log.d(TAG, "On Enter Region " + beaconForRegion.getObjectId());
        startTiming(beaconForRegion);

        //Log.d(TAG, "Enter event for session " + sessionsForRegion.getTitle());
    }

    @Override
    public void onExitedRegion(Region region) {
        BeConBeacon beaconForRegion = getBeaconForRegion(region);
        Log.d(TAG, "On Exit Region " + beaconForRegion.getObjectId());
        stopTiming(beaconForRegion);

       // Log.d(TAG, "Exit event for session " + sessionForRegion.getTitle());
    }


    // Timing Methods
    private void startTiming(BeConBeacon beacon) {
        Log.d(TAG, "Start timing for beacon " + beacon.getObjectId());
        Handler timer = timerHandlerMap.get(beacon.getObjectId());
        timer.postDelayed(timerRunnableMap.get(beacon.getObjectId()), TIMER_INTERVAL_IN_MILLIS);

        // Record the time entered for this session
        timeEnteredMap.put(beacon.getObjectId(), new Date());
    }

    private void stopTiming(BeConBeacon beacon) {
      //  Log.d(TAG, "Stop timing for session " + session.getTitle());
        Log.d(TAG, "Stop timing for beacon " + beacon.getObjectId());
        Handler timer = timerHandlerMap.get(beacon.getObjectId());
        timer.removeCallbacks(timerRunnableMap.get(beacon.getObjectId()));
        timeInSessionMap.put(beacon.getObjectId(), 0);

        if (beaconHasEnterEvent(beacon)) {
            // Record the enter and exit event
            Date enterTime = timeEnteredMap.get(beacon.getObjectId());
            Date exitTime = new Date();

            // Save the beacon interaction
            BeaconInteraction interaction = BeaconInteraction.newInstance(currentUser, beacon, enterTime, exitTime);
            interaction.saveInBackground();

            // Clear out the enter time
            timeEnteredMap.put(beacon.getObjectId(), null);
        }
    }
/*
    private boolean sessionHasEnterEvent(Session session) {
        return (timeEnteredMap.containsKey(session.getObjectId())) &&
                (timeEnteredMap.get(session.getObjectId()) != null);
    }*/

    private boolean beaconHasEnterEvent(BeConBeacon beacon) {
        return (timeEnteredMap.containsKey(beacon.getObjectId())) &&
                (timeEnteredMap.get(beacon.getObjectId()) != null);
    }

    // Convenience Methods
   /* private Session getSessionForRegion(Region region) {
        Session sessionForRegion = null;

        for (Session session : sessions) {
            BeConBeacon beacon = session.getBeacon();
            if (beacon.ownsRegion(region)) {
                sessionForRegion = session;
            }
        }
        return sessionForRegion;
    }*/

    private BeConBeacon getBeaconForRegion(Region region) {
        BeConBeacon beaconForRegion = null;

        for (BeConBeacon beacon : beacons) {
           // BeConBeacon beacon = session.getBeacon();
            if (beacon.ownsRegion(region)) {
                beaconForRegion = beacon;
            }
        }
        return beaconForRegion;
    }

    private Runnable getTimerRunnable(final Handler handler, final BeConBeacon beacon) {
        Runnable timerRunnable = new Runnable() {

            @Override
            public void run() {

                int timeInSession = timeInSessionMap.get(beacon.getObjectId());
                timeInSession += TIMER_INTERVAL_IN_SECONDS;
                timeInSessionMap.put(beacon.getObjectId(), timeInSession);
                Log.d("timeInSession",""+timeInSession);

                // Check to see if any notifications should fire for this amount of time
               // final BeConBeacon beacon = session.getBeacon();
                List<BeConNotification> notifications = beacon.getNotificationsForTriggerTime(timeInSession);
                if (notifications != null) {
                    for (BeConNotification notification : notifications) {

                        // Check if user has received the notification
                        if (!currentUser.hasReceivedNotification(notification)) {
                            Log.d(TAG, "Current user has not received notification");
                            Log.d(TAG, "triggerbeacon: "+beacon.isTriggerBeacon());
                            if(!beacon.isTriggerBeacon()){
                                fireBeConNotification(beacon, notification);
                            }
                        } else {
                            Log.d(TAG, "Current user has received notification");
                        }
                    }
                }

                handler.postDelayed(this, TIMER_INTERVAL_IN_MILLIS);
            }
        };

        return timerRunnable;
    }

    //for notification testing
    private void getNotification(){

        String[] notificationId = new String[1];
        notificationId[0] = "r8znTfiWNk";
        List<String> notificationIdList;
        notificationIdList = Arrays.asList(notificationId);
        BeConNotification.getNotificationsByIds(notificationIdList, new NotificationsLoadedListener() {
            @Override
            public void onNotificationsLoaded(@Nullable List<BeConNotification> notifications) {
                if (notifications != null) {
                    for (BeConNotification notification : notifications) {

                        // Check if user has received the notification
                        final BeConNotification notif = notification;
                        Log.d(TAG, "Current user has not received notification");
                        //Log.d(TAG, "triggerbeacon: "+beacon.isTriggerBeacon());

                        BeConBeacon.getBeaconById(notification.getBeaconId(), new BeaconLoadedListener() {
                            @Override
                            public void onBeaconLoaded(@Nullable BeConBeacon beacon) {
                                fireBeConNotification(beacon, notif);
                            }
                        });

                    }

                }
            }
        });

    }

    // Fires Notification Associated with Session and Time
    private void fireBeConNotification(BeConBeacon beacon, BeConNotification beaconNotification) {
        int notificationId = (int) beaconNotification.getCreatedAt().getTime();
        String title = beaconNotification.getTitle();
        String message = beaconNotification.getMessage();

        // Build and show notification
        Intent notifyIntent = new Intent(this, MainActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(this, 0,
                new Intent[]{notifyIntent}, PendingIntent.FLAG_UPDATE_CURRENT);

        // Create the save action for the notification
        String save = getString(R.string.save);
        Action saveAction = new Action.Builder(R.mipmap.ic_action_add, save,
                getSaveNotificationPendingIntent(beacon, beaconNotification, notificationId)).build();

        if (!MainActivity.isVisible){

            Notification notification = new NotificationCompat.Builder(this)
                    .setSmallIcon(android.R.drawable.ic_dialog_info)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .addAction(saveAction)
                    .build();

            notification.flags = Notification.FLAG_ONGOING_EVENT;
            notification.defaults |= Notification.DEFAULT_SOUND;
            NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(notificationId, notification);

    }else
    {
        MainActivity.print(title,message,beacon, beaconNotification, notificationId);
    }

        // Add this to the current users received notifications
        currentUser.addReceivedNotification(beacon, beaconNotification, null);

        // Add this notification Id to the global list
        ((BeConApp) getApplication()).addNotificationId(notificationId);
    }

    private PendingIntent getSaveNotificationPendingIntent(BeConBeacon beacon, BeConNotification notification, int notificationCancelId) {
        int pendingIntentRequestCode = (int) notification.getCreatedAt().getTime();
        String notificationId = notification.getObjectId();
        Intent saveNotificationIntent = new Intent(this, SaveNotificationReceiver.class);
        saveNotificationIntent.putExtra(SaveNotificationReceiver.EXTRA_COMPANY_ID, beacon.getCompanyId());
        saveNotificationIntent.putExtra(SaveNotificationReceiver.EXTRA_NOTIFICATION_ID, notificationId);
        saveNotificationIntent.putExtra(SaveNotificationReceiver.EXTRA_NOTIFICATION_CANCEL_ID, notificationCancelId);
        return PendingIntent.getBroadcast(this, pendingIntentRequestCode, saveNotificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
    }
}
