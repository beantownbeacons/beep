package com.digitalwavefront.beepetc.services;

import android.util.Log;

import com.digitalwavefront.beepetc.R;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.SaveCallback;

/**
 * Created by User on 3/8/18.
 */

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService{


    @Override
    public void onTokenRefresh()
    {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        Log.d("Firebase", "token "+ refreshedToken);

        ParseInstallation parseInstallation = ParseInstallation
                .getCurrentInstallation();

        parseInstallation.put("GCMSenderId",this.getString(R.string.gcm_project_number));
        //parseInstallation.put("deviceToken",refreshedToken);
        parseInstallation.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {

            }
        });
    }
}
