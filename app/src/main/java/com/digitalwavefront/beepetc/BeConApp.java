package com.digitalwavefront.beepetc;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

/*import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
*/
import com.crashlytics.android.Crashlytics;
import com.digitalwavefront.beepetc.parse.models.AttendeeChat;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.parse.models.Content;
import com.digitalwavefront.beepetc.parse.models.ContentTracking;
import com.digitalwavefront.beepetc.parse.models.EventAccessLog;
import com.digitalwavefront.beepetc.parse.models.EventGallery;
import com.digitalwavefront.beepetc.parse.models.EventGalleryLikes;
import com.digitalwavefront.beepetc.parse.models.NavConfigurator;
import com.digitalwavefront.beepetc.parse.models.NotificationTracking;
import com.digitalwavefront.beepetc.parse.models.Sponsor;
import com.digitalwavefront.beepetc.parse.models.UserPreferences;
import com.digitalwavefront.beepetc.services.BeaconMonitoringService;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.digitalwavefront.beepetc.activities.MainActivity;
import com.digitalwavefront.beepetc.login.EventStatusListener;
import com.digitalwavefront.beepetc.login.LoginType;
import com.digitalwavefront.beepetc.navigation.NavigationItemPositionManager;
import com.digitalwavefront.beepetc.parse.ParseAPICredentials;
import com.digitalwavefront.beepetc.parse.models.BeConBeacon;
import com.digitalwavefront.beepetc.parse.models.BeConNotification;
import com.digitalwavefront.beepetc.parse.models.BeaconInteraction;
import com.digitalwavefront.beepetc.parse.models.Company;
import com.digitalwavefront.beepetc.parse.models.Event;
import com.digitalwavefront.beepetc.parse.models.ReceivedNotification;
import com.digitalwavefront.beepetc.parse.models.SavedNotification;
import com.digitalwavefront.beepetc.parse.models.Session;
import com.digitalwavefront.beepetc.parse.models.SessionRSVP;
import com.digitalwavefront.beepetc.parse.models.Speaker;
import com.digitalwavefront.beepetc.parse.models.Survey;
import com.digitalwavefront.beepetc.parse.models.SurveyQuestion;
import com.digitalwavefront.beepetc.parse.models.SurveyResponse;
import com.digitalwavefront.beepetc.receivers.BeaconMonitoringAlarm;
import com.digitalwavefront.beepetc.twitter.HashTagManager;
//import com.parse.interceptors.ParseLogInterceptor;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;
/**
 * Created by Wayne on 9/15/15.
 */
/*
 * Updated by Tarul on 11/14/16
 */
public class BeConApp extends Application implements EventStatusListener {

    private static final String TAG = "BeConApp";
    public static boolean DEV_MODE = true;

    private List<Integer> notificationIds;
    private NavigationItemPositionManager navigationItemPositionManager;
    private HashTagManager hashTagManager;
    public static List<String> hashlist;
    public static String timeZone;
    public static Boolean inBackground = true;

    @Override
    public void onCreate() {
        super.onCreate();

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        //Fabric.with(this, new Crashlytics());
        //Consumer Key: tg2Nl8fIgV47I6c0m7vEIiKtC
        //CONSUMER SECRET: zQuJ28djkkMTnlcuQVyUclGmqROF47ph47akIJYcn6eeADU7HN

        TwitterAuthConfig authConfig = new TwitterAuthConfig("tg2Nl8fIgV47I6c0m7vEIiKtC", "zQuJ28djkkMTnlcuQVyUclGmqROF47ph47akIJYcn6eeADU7HN");
                //new TwitterAuthConfig("wOhJOYlwke0kgSdhMTKiUrz3R", "FaFhm9p0byv3SBAdyE0Dalnc2adbQHxKF9StGiAEbN22dNA68g");

        initializeParse();

        //HttpsTrustManager.allowAllSSL();
        ParseFacebookUtils.initialize(this);


        Fabric.Builder builder = new Fabric.Builder(this).kits(new Crashlytics(), new Twitter(authConfig));
        Fabric.with(builder.build());

        /*Fabric.with(this, new TwitterCore(authConfig), new TweetUi());

        Fabric fabric = new Fabric.Builder(this).debuggable(true).kits(new Crashlytics()).build();
        Fabric.with(fabric);*/
        //ParseTwitterUtils.initialize("wOhJOYlwke0kgSdhMTKiUrz3R", "FaFhm9p0byv3SBAdyE0Dalnc2adbQHxKF9StGiAEbN22dNA68g");

        if (BeConUser.getCurrentUser() != null) {
            BeConUser.getCurrentUser().fetchInBackground();
        }

        //initParsePushMessaging();
        //RegisterGCM();
/*
        String token = PreferenceManager.getDefaultSharedPreferences(this).getString(this.getString(R.string.gcm_pref_token), null );

        System.out.print("token = "+token);
        if(!TextUtils.isEmpty(token)) {

            new AWSCreateEndpointTask(this).execute(

                    "arn:aws:sns:us-west-2:912347879959:app/GCM/Beep",

            token,

                    "srr108@gmail.com");

        }
        */

        //startService(new Intent(this, MessageReceivingService.class));

        notificationIds = new ArrayList<>();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    private void initializeParse() {

        //Parse.enableLocalDatastore(getApplicationContext());

        ParseObject.registerSubclass(BeConUser.class);
        ParseObject.registerSubclass(BeConBeacon.class);
        ParseObject.registerSubclass(Event.class);
        ParseObject.registerSubclass(Session.class);
        ParseObject.registerSubclass(Speaker.class);
        ParseObject.registerSubclass(BeConNotification.class);
        ParseObject.registerSubclass(Survey.class);
        ParseObject.registerSubclass(SurveyQuestion.class);
        ParseObject.registerSubclass(SavedNotification.class);
        ParseObject.registerSubclass(ReceivedNotification.class);
        ParseObject.registerSubclass(SessionRSVP.class);
        ParseObject.registerSubclass(SurveyResponse.class);
        ParseObject.registerSubclass(BeaconInteraction.class);
        ParseObject.registerSubclass(Company.class);
        ParseObject.registerSubclass(EventGallery.class);
        ParseObject.registerSubclass(EventGalleryLikes.class);
        ParseObject.registerSubclass(NotificationTracking.class);
        ParseObject.registerSubclass(Content.class);
        ParseObject.registerSubclass(ContentTracking.class);
        ParseObject.registerSubclass(Sponsor.class);
        ParseObject.registerSubclass(EventAccessLog.class);
        ParseObject.registerSubclass(NavConfigurator.class);
        ParseObject.registerSubclass(AttendeeChat.class);
        ParseObject.registerSubclass(UserPreferences.class);
        //Parse.addParseNetworkInterceptor(new ParseLogInterceptor());
        //Parse.initialize(this, ParseAPICredentials.getApplicationId(), ParseAPICredentials.getClientKey());

        Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
                .applicationId(ParseAPICredentials.getApplicationId())
                .clientKey(ParseAPICredentials.getClientKey())
                .server(ParseAPICredentials.getServer())//.enableLocalDataStore()
                .build()
        );

        ParseInstallation.getCurrentInstallation().saveInBackground();
    }

    public void startBeaconMonitoringIfNecessary() {
        BeConUser currentUser = BeConUser.getCurrentUser();
        if (currentUser != null && currentUser.getLoginType(this) == LoginType.ATTENDEE) {
            Log.d("startBeaconMonitoring:"," startBeaconMonitoringIfNecessary ");
            // Runs every five minutes to ensure the beacon monitoring service stays alive
            AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            PendingIntent pendingIntent = getBeaconMonitoringAlarmIntent();
            am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
                    (5 * 60 * 1000), pendingIntent);
        }
    }

    public void stopBeaconMonitoringService() {
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent notificationService = new Intent(getApplicationContext(), BeaconMonitoringService.class);
        stopService(notificationService);

        // Cancel the repeating alarm the keeps service alive
        am.cancel(getBeaconMonitoringAlarmIntent());
    }

    private PendingIntent getBeaconMonitoringAlarmIntent() {
        Intent intent = new Intent(this, BeaconMonitoringAlarm.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);
        return pendingIntent;
    }

    public void addNotificationId(int notificationId) {
        notificationIds.add(notificationId);
    }

    private void cancelAllNotifications() {
        if (!notificationIds.isEmpty()) {
            NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            for (Integer notificationId : notificationIds) {
                nm.cancel(notificationId);
            }
            notificationIds.clear();
        }
    }

    public void openWebPage(Activity activity, String url) {
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            if (browserIntent.resolveActivity(activity.getPackageManager()) != null) {
                activity.startActivity(browserIntent);
            }
        } catch(ActivityNotFoundException e) {
            Log.e(TAG, "Error opening web page " + e.getMessage());
            Toast.makeText(this, R.string.error_opening_webpage, Toast.LENGTH_SHORT);
        }
    }


    // Navigation
    public NavigationItemPositionManager getNavigationItemPositionManager() {
        return navigationItemPositionManager;
    }

    public void initNavigationManager(int loginType) {
        navigationItemPositionManager = new NavigationItemPositionManager(loginType);
    }

    public void clearNavigationManager() {
        navigationItemPositionManager = null;
    }


    // HashTag Management
    public HashTagManager getHashTagManager() {
        return hashTagManager;
    }

    private void initHashTagManager(Event event) {
        if (event != null) {
            hashTagManager = new HashTagManager(getApplicationContext());
            //hashTagManager.storeHashTag(event.getHashTag());
            List<String> hashTag = event.getHash_Tag();
            //Log.e("Praneet", "initHashTagManager" + hashTag);
            if (hashTag != null)
                hashTagManager.storeHashList(event.getHash_Tag());
        }
    }

    private void clearHashTagManager() {
        hashTagManager = null;
    }


    // Event Status Listener
    @Override
    public void onEventSignIn(Event event, int loginType) {

        Log.d("","On Event Sign In");
        // Get the current user
        BeConUser user = BeConUser.getCurrentUser();

        // Set the login type first because it is needed
        user.setLoginType(getApplicationContext(), loginType);

        // Initialize the hash tag manager for Twitter

        try {
            hashlist = event.getHash_Tag();
            timeZone=event.getTimeZone();
            if (event != null) {
                initHashTagManager(event);
            }
        }catch (Exception cx){
            System.out.println(cx);
        }



        // Start monitoring beacons if requirements are met, see method.
       startBeaconMonitoringIfNecessary();



        // Start the main activity
       startMainActivity();
    }




    @Override
    public void onEventSignOut() {
        cancelAllNotifications();
        clearHashTagManager();
    }

    public void startMainActivity() {
        MainActivity.start(getApplicationContext());
    }



        /**
         * Initialize Push Messaging Service and subscribe to all-users channel
         */

        private void initParsePushMessaging() {
            ParseInstallation parseInstallation = ParseInstallation
                    .getCurrentInstallation();
            //You might skip this if
            if (ParseUser.getCurrentUser() != null) {
                parseInstallation.put("user",
                        BeConUser.getCurrentUser());
            }
            //if (parseInstallation.getObjectId() != null)

            parseInstallation.saveInBackground(new SaveCallback() {

                    @Override
                    public void done(ParseException e) {

                        ParsePush.subscribeInBackground("Beep", new SaveCallback() {
                            @Override
                            public void done(ParseException e) {

                            }
                        });
                        //PushService.subscribe(getApplicationContext(),"channel_name",MainHomeActivity.class);
                    }
                });


    }


    private void RegisterGCM() {

        System.out.print("register gcm");
        //let's register this app to the gcm if it hasn't been registered already.

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);

        String gcmToken = sp.getString(getString(R.string.gcm_pref_token), null);

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getApplicationContext());

        System.out.print("token = "+gcmToken);
        if(TextUtils.isEmpty(gcmToken)){

            new GCMRegisterTask(this, gcm).execute();

        }
        else
            {
                ParseInstallation parseInstallation = ParseInstallation
                        .getCurrentInstallation();

                parseInstallation.put("GCMSenderId",this.getString(R.string.gcm_project_number));
                //parseInstallation.put("deviceToken",gcmToken);
                parseInstallation.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {

                    }
                });
/*
                System.out.print("token exists calling awscreateendpoint ");
            new AWSCreateEndpointTask(this).execute(

                    "arn:aws:sns:us-west-2:912347879959:app/GCM/Beep",

                    gcmToken,

                    "srr108@gmail.com");
*/
        }
    }

    public class GCMRegisterTask extends AsyncTask<String, Void, Boolean> {

        private Context context;

        private GoogleCloudMessaging gcm;

        String token;

        public GCMRegisterTask(Context context, GoogleCloudMessaging gcm) {

            super();

            this.context = context;

            this.gcm = gcm;

        }

        @Override

        protected Boolean doInBackground(String... params) {



            try {

                token = gcm.register(context.getString(R.string.gcm_project_number));

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

                sp.edit().putString(context.getString(R.string.gcm_pref_token), token).apply();

                ParseInstallation parseInstallation = ParseInstallation
                        .getCurrentInstallation();

                parseInstallation.put("GCMSenderId",context.getString(R.string.gcm_project_number));
                //parseInstallation.put("deviceToken",token);
                parseInstallation.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {

                    }
                });
                return true;

            } catch (IOException e) {

                Log.i("Registration Error", e.getMessage());

            }

            return false;

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            /*
            if(!TextUtils.isEmpty(token)) {
                System.out.print("got token = "+token +" calling awscreateendpoint");
                new AWSCreateEndpointTask(context).execute(

                        "arn:aws:sns:us-west-2:912347879959:app/GCM/Beep",

                        token,

                        "srr108@gmail.com");

            }*/
        }
    }
/*
        private AmazonSNS getSNSClient() {
            AmazonSNS sns = null;
            try {
                AssetManager assetManager = this.getAssets();

                sns = new AmazonSNSClient(new PropertiesCredentials(
                        assetManager.open("AwsCredentials.properties")));

                sns.setEndpoint("https://sns.us-west-2.amazonaws.com");
            }catch (Exception e) {

            }
            return sns;
        }
*/
/*
public class AWSCreateEndpointTask extends AsyncTask<String, Void,  CreatePlatformEndpointResult> {

    Context context;

    public AWSCreateEndpointTask(Context context) {

        super();

        this.context = context;

    }

    @Override

    protected CreatePlatformEndpointResult doInBackground(String[] params) {

        if (params.length < 3) {

            return null;

        }

        String arn = params[0];

        String gcmToken = params[1];

        String userData = params[2];

        try {

            CreatePlatformEndpointRequest request = new CreatePlatformEndpointRequest();

            request.setCustomUserData(userData);

            request.setToken(gcmToken);

            request.setPlatformApplicationArn(arn);

            System.out.print("requesting aws endpoint  ");

            return getSNSClient().createPlatformEndpoint(request);

        } catch (Exception ex) {

            System.out.print("aws endpoint exception "+ ex);
            return null;

        }

    }

    @Override

    protected void onPostExecute(CreatePlatformEndpointResult result) {


        if (result != null) {


            SharedPreferences prefs = context.getSharedPreferences("my_prefs" , Context.MODE_PRIVATE);

            String endpointArn = result.getEndpointArn();

            prefs.edit().putString(context.getString(R.string.endpoint_arn), endpointArn).apply();

            System.out.print("aws endpoint = "+endpointArn);

        }
        else
            System.out.print("aws endpoint null ");

    }
    }
*/
}

