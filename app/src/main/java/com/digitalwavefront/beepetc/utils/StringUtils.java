package com.digitalwavefront.beepetc.utils;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

//import org.apache.commons.validator.routines.UrlValidator;

/**
 * Created by Wayne on 9/15/15.
 */
public class StringUtils {

    /**
     *
     * @param phoneNumber
     * @return true if valid phone number, otherwise false
     */
    public static boolean isValidPhoneNumber(String phoneNumber) {
        if (isNullOrEmpty(phoneNumber)) {
            return false;
        }

        //String expression = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";
        CharSequence inputStr = phoneNumber;
       // Pattern pattern = Pattern.compile(expression);
      //  Matcher matcher = pattern.matcher(inputStr);
      //  Log.e("matcher",""+matcher.matches());
        Log.e("matcher",""+android.util.Patterns.PHONE.matcher(inputStr).matches());
        //android.util.Patterns.PHONE.matcher(target).matches();
        //return matcher.matches();
        return android.util.Patterns.PHONE.matcher(inputStr).matches();
    }

    // Breakdown phone number to remove all non numeric characters
    public static String convertToNumericPhoneNumber(String phoneNumber) {
        return phoneNumber.replace("-", "")
                .replace("(", "")
                .replace(")", "")
                .trim();
    }

    //hide keyboard
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

//phone number validation
    public static String getFormattedPhoneNumber(Context context, String phone) {

        // Replace all characters other than numbers
       /*  String phoneNumber = StringUtils.convertToNumericPhoneNumber(getPhoneNumber());

        Log.e("converted_no2",""+phoneNumber);
        Log.e("no2_length",""+phoneNumber.length());
        // If phone number length is < 10 characters number is invalid

        if (phoneNumber.length()>=10) {

           // PhoneNumberUtils.formatNumberToE164(phoneNumber, PhoneNumberUtils.PhoneNumberFormat.E164);
            // Format the phone number should be in format(##########)
            String areaCode = phoneNumber.substring(0, 3);
            String firstThree = phoneNumber.substring(3, 6);
            String lastFour = phoneNumber.substring(6, phoneNumber.length());
            return String.format("(%s) %s-%s", areaCode, firstThree, lastFour);
        } else {

            // Inform user of invalid number
            return context.getString(R.string.invalid_phone_number);
        }
        ////////////////////////////////////*/

        // Replace all characters other than numbers
        String phoneNumber = StringUtils.convertToNumericPhoneNumber(phone);
        Log.e("converted_no1",""+phoneNumber);

        String  phonno="";
        if(phoneNumber.startsWith("+")){
            phonno = phoneNumber.replace("+","");
        }else{
            phonno = phoneNumber;
        }
        Log.e("converted_no2",""+phonno);
        Log.e("no2_length",""+phonno.length());
        if (phonno.length()>=10){
            switch (phonno.length()) {
                case 10:
                    return String.format("(%s)%s-%s", phonno.substring(0, 3), phonno.substring(3, 6), phonno.substring(6, 10));
                //break;
                case 11:
                    return String.format("+%s(%s)%s-%s", phonno.substring(0, 1), phonno.substring(1, 4), phonno.substring(4, 7), phonno.substring(7, 11));
                // break;
                case 12:
                    return String.format("+%s(%s)%s-%s", phonno.substring(0, 2), phonno.substring(2, 5), phonno.substring(5, 8), phonno.substring(8, 12));
                // break;
                case 13:
                    return String.format("+%s(%s)%s-%s", phonno.substring(0, 3), phonno.substring(3, 6), phonno.substring(6, 9), phonno.substring(9, 13));
                // break;
                case 14:
                    return String.format("+%s-%s(%s)%s-%s", phonno.substring(0, 1), phonno.substring(1, 4), phonno.substring(4, 7), phonno.substring(7, 10), phonno.subSequence(10, 14));
                //break;
                case 15:
                    return String.format("+%s(%s)%s-%s", phonno.substring(0, 4), phonno.substring(4, 7), phonno.substring(7, 10), phonno.substring(10, 15));
                //break;
                case 16:
                    return String.format("+%s-%s(%s)%s-%s", phonno.substring(0, 2), phonno.substring(2, 5), phonno.substring(5, 8), phonno.substring(8, 11), phonno.substring(11, 15));
                //break;
                default:
                    return null;
            }
        }else {

            // Inform user of invalid number
           // return context.getString(R.string.invalid_phone_number);
            return "0";
        }
    }


    // Checks if string is null or empty.
    public static boolean isNullOrEmpty(String string) {
        if (string == null) {
            return true;
        }

        if (string.trim().length() == 0) {
            return true;
        }

        return false;
    }

    // Checks if any strings in a series are null or empty
    public static boolean isNullOrEmpty(String... strings) {
        for (int i = 0; i < strings.length; i++) {
            String string = strings[i];

            if (isNullOrEmpty(string)) {
                return true;
            }
        }

        return false;
    }

    // Checks if email address is in valid format.
    public static boolean isValidEmail(String emailAddress) {
        return emailAddress.contains(" ") == false && emailAddress.matches(".+@.+\\.[a-z]+");
    }

    public static boolean matches(String stringOne, String stringTwo) {
        return stringOne.equals(stringTwo);
    }

    public static String getMinuteSecondStringFromSeconds(float seconds) {
        return getMinuteSecondStringFromSeconds((int) seconds);
    }

    public static String getMinuteSecondStringFromSeconds(int seconds) {
        int minutes = seconds / 60;
        int remainingSeconds = seconds % 60;
        return String.format("%02d:%02d", minutes, remainingSeconds);
    }

    /*
    public static UrlValidator getDefaultUrlValidator() {
        UrlValidator validator = new UrlValidator(new String[] {"http", "https"});
        return validator;
    }
    */
}
