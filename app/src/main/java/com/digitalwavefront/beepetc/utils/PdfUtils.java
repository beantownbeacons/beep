package com.digitalwavefront.beepetc.utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by Wayne on 10/11/15.
 */
public class PdfUtils {

    public static void savePdfFromByteArray(String filePath, byte[] data) throws IOException {
        File pdfFile = new File(filePath);
        FileUtils.writeByteArrayToFile(pdfFile, data);
    }
}
