package com.digitalwavefront.beepetc.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.View;
import android.widget.TextView;

import com.digitalwavefront.beepetc.views.GradientOverImageDrawable;

/**
 * Created by Wayne on 11/19/15.
 */
public class ViewUtils {

    // For background text gradient
    private static final int GRADIENT_START_ALPHA = 30;
    private static final int GRADIENT_END_ALPHA = 90;

    // Applies a background image with a subtle gradient to display lighter text more clearly
    public static void setBackgroundImageWithTextProtection(Resources resources, View view, int imageRes) {
        Bitmap background = BitmapFactory.decodeResource(resources, imageRes);

        // Apply a subtle gradient to image for text protection
        int gradientStartColor = Color.argb(GRADIENT_START_ALPHA, 0, 0, 0);
        int gradientEndColor = Color.argb(GRADIENT_END_ALPHA, 0, 0, 0);
        GradientOverImageDrawable gradientDrawable = new GradientOverImageDrawable(resources, background);
        gradientDrawable.setGradientColors(gradientStartColor, gradientEndColor);
        view.setBackground(gradientDrawable);
    }

    // Tints a list of widgets, or just one if that's your thing
    public static void tintViews(Context context, int color, View... views) {
        for (int i = 0; i < views.length; i++) {
            View view = views[i];
            Drawable wrappedDrawable = DrawableCompat.wrap(view.getBackground());
            DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(context, color));
            view.setBackground(wrappedDrawable);
        }
    }

    public static void underlineTextViews(TextView... textViews) {
        for (int i = 0; i < textViews.length; i++) {
            TextView textView = textViews[i];
            textView.setPaintFlags(textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }
    }


}
