

package com.digitalwavefront.beepetc.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.style.DynamicDrawableSpan;
import android.util.DisplayMetrics;

/**
 * @author Hieu Rocker (rockerhieu@gmail.com)
 */
public class EmojiconSpan extends DynamicDrawableSpan {
    private final Context mContext;
    private final int mResourceId;
    private final int mSize;
    private Drawable mDrawable;

    float fl;
    DisplayMetrics metrics;
    public EmojiconSpan(Context context, int resourceId, int size) {
        super();
        mContext = context;
        mResourceId = resourceId;
        mSize = size;
       
        
    }

    public Drawable getDrawable() {
        if (mDrawable == null) {
            try {
                mDrawable = mContext.getResources().getDrawable(mResourceId);
                int size = mSize;
                
                	mDrawable.setBounds(0, 0, size+20, size+20);
            } catch (Exception e) {
                // swallow
            }
            if (mDrawable == null)
            System.out.println("smiley drawable is null");
        }
        return mDrawable;
    }
}