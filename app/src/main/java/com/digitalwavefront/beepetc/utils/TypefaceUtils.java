package com.digitalwavefront.beepetc.utils;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Wayne on 9/15/15.
 */
public class TypefaceUtils {

    public static Typeface getLightTypeface(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Light.ttf");
    }

    public static Typeface getRegularTypeface(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Regular.ttf");
    }

    public static Typeface getSemiBoldTypeface(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-SemiBold.ttf");
    }

    public static Typeface getBoldTypeface(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Bold.ttf");
    }
}
