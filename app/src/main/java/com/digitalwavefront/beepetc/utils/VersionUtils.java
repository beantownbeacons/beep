package com.digitalwavefront.beepetc.utils;

import android.os.Build;

/**
 * Created by Wayne on 11/19/15.
 * Utility for determining Android API version.
 */
public class VersionUtils {

    public static boolean isLollipopOrAbove() {
        return android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }
}
