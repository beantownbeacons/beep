package com.digitalwavefront.beepetc.utils;

import android.app.NotificationManager;
import android.content.Context;

/**
 * Created by Wayne on 10/11/15.
 */
public class NotificationUtils {

    public static void dismissNotification(Context context, int notificationId) {
        NotificationManager manager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(notificationId);
    }
}
