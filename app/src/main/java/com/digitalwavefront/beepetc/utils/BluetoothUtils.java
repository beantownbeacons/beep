package com.digitalwavefront.beepetc.utils;

import android.bluetooth.BluetoothAdapter;

/**
 * Created by Wayne on 9/27/15.
 */
public class BluetoothUtils {

    public static boolean bluetoothIsTurningOn() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return bluetoothAdapter != null && bluetoothAdapter.getState() == BluetoothAdapter.STATE_TURNING_ON;
    }

    public static boolean bluetoothIsEnabled() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return bluetoothAdapter != null && bluetoothAdapter.isEnabled();
    }
}
