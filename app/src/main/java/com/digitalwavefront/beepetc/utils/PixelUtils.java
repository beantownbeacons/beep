package com.digitalwavefront.beepetc.utils;

import android.content.Context;

/**
 * Created by Wayne on 9/18/15.
 */
public class PixelUtils {

    // Converts DP units to pixels
    public static int dpsToPixels(Context context, int dps) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dps * scale + 0.5f);
    }
}
