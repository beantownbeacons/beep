package com.digitalwavefront.beepetc.navigation;

import com.digitalwavefront.beepetc.login.LoginType;

/**
 * Created by Wayne on 11/22/15.
 */

/**
 *Made changes by supra-chaitali on 22nd march
 */
public class NavigationItemPositionManager {
    public static final int NO_POSITION = -1;

    private int loginType;

    public NavigationItemPositionManager(int loginType) {
        this.loginType = loginType;
    }


    public int getHeaderPosition()  { return  0;}
    public int getEventInfoPosition() {
        return 1;
    }

    public int getAgendaPosition() {
        return 2;
    }

    public int getAttendeePosition(){ return 3; }
    public int getSpeakersPosition() {
        return 4;
    }
    public int getExhibitorPosition() {
        return 5;
    }


    public int getSurveysPosition() {
        if (loginType == LoginType.ATTENDEE) {
            return 6;
        }
        return NO_POSITION;
    }

    public int getContentLibraryPosition() {
        if (loginType == LoginType.ATTENDEE) {
            return 7;
        }
        return NO_POSITION;
    }

    public int getContentPosition() {
        if(loginType == LoginType.ATTENDEE) {
            return 8;
        }
        return NO_POSITION;
    }

    public int getDashboardPosition() {
        if (loginType == LoginType.EXHIBITOR || loginType == LoginType.ORGANIZER) {
            return 9;
        }
        return NO_POSITION;
    }

    public int getSocialChatterPosition() {
        if (loginType == LoginType.ATTENDEE) {
            return 9;
        } else {
            return 10;
        }
    }

    public int getGalleryPosition() {
        if (loginType == LoginType.ATTENDEE) {
            return 10;
        } else {
            return 11;
        }
    }

    public int getAboutUsPosition() {
        if (loginType == LoginType.ATTENDEE) {
            return 11;
        } else {
            return 12;
        }
    }

    public int getSignOutPosition() {
        if (loginType == LoginType.ATTENDEE) {
            return 12;
        } else {
            return 13;
        }
    }
}
