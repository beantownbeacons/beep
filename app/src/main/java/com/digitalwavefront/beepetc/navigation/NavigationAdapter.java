package com.digitalwavefront.beepetc.navigation;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitalwavefront.beepetc.BeConApp;
import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.activities.beep_pref;
import com.digitalwavefront.beepetc.interfaces.BitmapLoadedListener;
import com.digitalwavefront.beepetc.parse.models.BeConUser;
import com.digitalwavefront.beepetc.ui.models.NavDrawerItem;
import com.digitalwavefront.beepetc.utils.TypefaceUtils;
import com.digitalwavefront.beepetc.views.RoundImageView;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wayne on 9/16/15.
 */

/**
 * Made changes by supra-chaitali on 22nd march
 */
public class NavigationAdapter extends BaseAdapter {

    private static final String TAG = NavigationAdapter.class.getSimpleName();
    private Context context;
    private LayoutInflater inflater;
    private NavigationItemPositionManager navigationItemPositionManager;
    private int selectedPosition;
    private List<NavDrawerItem> drawerItems;
    BeConUser currentUser;
    beep_pref pref;


    private enum DrawerItemType {DEFAULT, SELECTED, DEFAULT_NO_ICON, SELECTED_NO_ICON}

    public NavigationAdapter(Context context) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.navigationItemPositionManager = ((BeConApp) context.getApplicationContext()).getNavigationItemPositionManager();
        this.selectedPosition = navigationItemPositionManager.getEventInfoPosition();

        // Get the drawer items
        initDrawerItems();

        currentUser = BeConUser.getCurrentUser();
        update_Data();
    }

    @Override
    public int getCount() {
        return drawerItems.size();
    }

    @Override
    public NavDrawerItem getItem(int position) {
        int pos = position;
        // if (position > 0)
        //{ pos = position-1;}
        return drawerItems.get(pos);
    }


    @Override
    public long getItemId(int position) {
        return position - 1;
    }

    @Override
    public int getViewTypeCount() {
        return DrawerItemType.values().length;
    }

    @Override
    public int getItemViewType(int position) {
        if (selectedPosition == position) {
            if (getItem(position).getIconSelected() != -1) {
                return DrawerItemType.SELECTED.ordinal();
            } else {
                return DrawerItemType.SELECTED_NO_ICON.ordinal();
            }
        }

        if (getItem(position).getIcon() != -1) {
            return DrawerItemType.DEFAULT.ordinal();
        } else {
            return DrawerItemType.DEFAULT_NO_ICON.ordinal();
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        NavDrawerItem drawerItem = getItem(position);

        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            if (position == 0) {
                convertView = inflater.inflate(R.layout.profile_item, parent, false);
                holder.header_profile_view = (ImageView) convertView.findViewById(R.id.profile_header);
                holder.header_setting_imgView = (ImageView) convertView.findViewById(R.id.profile_header_imageView);
                holder.header_username_text = (TextView) convertView.findViewById(R.id.profile_header_name);
                holder.profile_circle_view = (RoundImageView) convertView.findViewById(R.id.profile_circle_view);
            } else {
                convertView = inflater.inflate(R.layout.nav_drawer_item, parent, false);
                holder.iconView = (ImageView) convertView.findViewById(R.id.icon_view);
                holder.titleView = (TextView) convertView.findViewById(R.id.title_view);
                holder.titleView.setTypeface(TypefaceUtils.getLightTypeface(context));
            }

            convertView.setTag(holder);
        }
        final ViewHolder holder = (ViewHolder) convertView.getTag();
        if (position > 0) {


            int viewType = getItemViewType(position);

            // Set the color of the icon and text based on the selected state
            if (drawerItem != null) {
                if (viewType == DrawerItemType.DEFAULT.ordinal()) {
                    if (holder.iconView != null) {
                        holder.iconView.setImageResource(drawerItem.getIcon());
                    }
                    if (holder.titleView != null) {
                        holder.titleView.setTextColor(ContextCompat.getColor(context, R.color.colorSecondaryText));
                    }
                } else if (viewType == DrawerItemType.SELECTED.ordinal()) {
                    if (holder.iconView != null) {
                        holder.iconView.setImageResource(drawerItem.getIconSelected());
                    }
                    if (holder.titleView != null) {
                        holder.titleView.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    }
                } else if (viewType == DrawerItemType.SELECTED_NO_ICON.ordinal() || viewType == DrawerItemType.DEFAULT_NO_ICON.ordinal()) {
                    if (holder.iconView != null) {
                        holder.iconView.setVisibility(View.INVISIBLE);
                    }
                }
                if (holder.titleView != null) {
                    holder.titleView.setText(drawerItem.getTitle());
                }
            }
        } else {
            Typeface tf = TypefaceUtils.getSemiBoldTypeface(context);
            holder.header_username_text.setTypeface(tf);
            holder.header_username_text.setText(currentUser.getName());
            holder.header_profile_view.setVisibility(View.VISIBLE);
            holder.profile_circle_view.setVisibility(View.GONE);
            pref = new beep_pref(context);
            boolean flag = pref.getFlag();
            if (flag) {
                currentUser.fetchInBackground(new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        //   Log.e("profile pic:", "" + currentUser.getParseFile("profilePhoto").getUrl());
                        BeConUser user = (BeConUser) parseObject;

                        user.getProfilePhoto(new BitmapLoadedListener() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap) {
                                if (bitmap != null) {
                                    holder.header_profile_view.setVisibility(View.GONE);
                                    holder.profile_circle_view.setVisibility(View.VISIBLE);
                                    holder.profile_circle_view.setImageBitmap(bitmap);
                                } else {
                                    holder.header_profile_view.setImageResource(R.drawable.ic_account_circle);
                                }
                            }
                        });
                    }
                });
            } else {
                currentUser.getProfilePhoto(new BitmapLoadedListener() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap) {
                        if (bitmap != null) {
                            holder.header_profile_view.setVisibility(View.GONE);
                            holder.profile_circle_view.setVisibility(View.VISIBLE);
                            holder.profile_circle_view.setImageBitmap(bitmap);
                        } else {
                            holder.header_profile_view.setImageResource(R.drawable.ic_account_circle);
                        }
                    }
                });
            }
        }
        Log.e("size:", "" + drawerItems.size() + ", position: " + position);//+", title: "+drawerItem.getTitle());
        return convertView;
    }

    public void setSelectedPosition(int position) {
        this.selectedPosition = position;
        notifyDataSetChanged();
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    private void initDrawerItems() {
        drawerItems = new ArrayList<>();
        Resources res = context.getResources();

        // Header
        if (navigationItemPositionManager.getHeaderPosition() != NavigationItemPositionManager.NO_POSITION) {
            Log.d(TAG, "initDrawerItems: 0");
            int position1 = 0;
            String title1 = "";//context.getString(R.string.event_info);
            int icon1 = R.drawable.ic_event_info;
            int iconSelected1 = R.drawable.ic_event_info_selected;
            drawerItems.add(position1, new NavDrawerItem(title1, icon1, iconSelected1));

        }

        // Event Info
        if (navigationItemPositionManager.getEventInfoPosition() != NavigationItemPositionManager.NO_POSITION) {
            Log.d(TAG, "initDrawerItems: 1");
            int position = navigationItemPositionManager.getEventInfoPosition();
            String title = context.getString(R.string.event_info);
            int icon = R.drawable.ic_event_info;
            int iconSelected = R.drawable.ic_event_info_selected;
            drawerItems.add(position, new NavDrawerItem(title, icon, iconSelected));
        }

        // Agenda
        if (navigationItemPositionManager.getAgendaPosition() != NavigationItemPositionManager.NO_POSITION) {
            Log.d(TAG, "initDrawerItems: 2");
            int position = navigationItemPositionManager.getAgendaPosition();
            String title = context.getString(R.string.agenda);
            int icon = R.drawable.ic_agenda;
            int iconSelected = R.drawable.ic_agenda_selected;
            drawerItems.add(position, new NavDrawerItem(title, icon, iconSelected));
        }

        //Attendee
        if (navigationItemPositionManager.getAttendeePosition() != NavigationItemPositionManager.NO_POSITION) {
            Log.d(TAG, "initDrawerItems: 3");
            int position = navigationItemPositionManager.getAttendeePosition();
            String title = context.getString(R.string.attendee);
            int icon = R.drawable.ic_attendees;
            int iconSelected = R.drawable.ic_attendees_selected;
            drawerItems.add(position, new NavDrawerItem(title, icon, iconSelected));
        }

        // Speakers
        if (navigationItemPositionManager.getSpeakersPosition() != NavigationItemPositionManager.NO_POSITION) {
            Log.d(TAG, "initDrawerItems: 4");
            int position = navigationItemPositionManager.getSpeakersPosition();
            String title = context.getString(R.string.speakers);
            int icon = R.drawable.ic_speakers;
            int iconSelected = R.drawable.ic_speakers_selected;
            drawerItems.add(position, new NavDrawerItem(title, icon, iconSelected));
        }

        // Exhibitors
        if (navigationItemPositionManager.getExhibitorPosition() != NavigationItemPositionManager.NO_POSITION) {
            Log.d(TAG, "initDrawerItems: 5");
            int position = navigationItemPositionManager.getExhibitorPosition();
            String title = context.getString(R.string.exhibitors);
            int icon = R.drawable.ic_dashboard;
            int iconSelected = R.drawable.ic_dashboard_selected;
            drawerItems.add(position, new NavDrawerItem(title, icon, iconSelected));
        }
        // Surveys
        if (navigationItemPositionManager.getSurveysPosition() != NavigationItemPositionManager.NO_POSITION) {
            Log.d(TAG, "initDrawerItems: 6");
            int position = navigationItemPositionManager.getSurveysPosition();
            String title = context.getString(R.string.surveys);
            int icon = R.drawable.ic_surveys;
            int iconSelected = R.drawable.ic_surveys_selected;
            drawerItems.add(position, new NavDrawerItem(title, icon, iconSelected));
        }

        // Content Library
        if (navigationItemPositionManager.getContentLibraryPosition() != NavigationItemPositionManager.NO_POSITION) {
            Log.d(TAG, "initDrawerItems: 7");
            int position = navigationItemPositionManager.getContentLibraryPosition();
            String title = context.getString(R.string.content_library);
            int icon = R.drawable.ic_content_library;
            int iconSelected = R.drawable.ic_content_library_selected;
            drawerItems.add(position, new NavDrawerItem(title, icon, iconSelected));
        }

        //Content
        if (navigationItemPositionManager.getContentPosition() != NavigationItemPositionManager.NO_POSITION) {
            Log.d(TAG, "initDrawerItems: 8");
            int position = navigationItemPositionManager.getContentPosition();
            String title = context.getString(R.string.content);
            int icon = R.drawable.ic_content_library;
            int iconSelected = R.drawable.ic_content_library_selected;
            drawerItems.add(position, new NavDrawerItem(title, icon, iconSelected));
        }

        // Dashboard
        if (navigationItemPositionManager.getDashboardPosition() != NavigationItemPositionManager.NO_POSITION) {
            Log.d(TAG, "initDrawerItems: 9");
            int position = navigationItemPositionManager.getDashboardPosition();
            String title = context.getString(R.string.dashboard);
            int icon = R.drawable.ic_dashboard;
            int iconSelected = R.drawable.ic_dashboard_selected;
            drawerItems.add(position, new NavDrawerItem(title, icon, iconSelected));
        }

        // Social Chatter
        if (navigationItemPositionManager.getSocialChatterPosition() != NavigationItemPositionManager.NO_POSITION) {
            Log.d(TAG, "initDrawerItems: 9");
            int position = navigationItemPositionManager.getSocialChatterPosition();
            String title = context.getString(R.string.social_chatter);
            int icon = R.drawable.ic_social_chatter;
            int iconSelected = R.drawable.ic_social_chatter_selected;
            drawerItems.add(position, new NavDrawerItem(title, icon, iconSelected));
        }

        // Gallery
        if (navigationItemPositionManager.getGalleryPosition() != NavigationItemPositionManager.NO_POSITION) {
            Log.d(TAG, "initDrawerItems: 10");
            int position = navigationItemPositionManager.getGalleryPosition();
            String title = context.getString(R.string.gallery);
            int icon = R.drawable.ic_gallery;
            int iconSelected = R.drawable.ic_gallery_selected;
            drawerItems.add(position, new NavDrawerItem(title, icon, iconSelected));
        }

        // About us
        if (navigationItemPositionManager.getAboutUsPosition() != NavigationItemPositionManager.NO_POSITION) {
            Log.d(TAG, "initDrawerItems: 11");
            int position = navigationItemPositionManager.getAboutUsPosition();
            String title = context.getString(R.string.about_us);
            int icon = R.drawable.aboutus;
            int iconSelected = R.drawable.aboutus_selected;
            drawerItems.add(position, new NavDrawerItem(title, icon, iconSelected));
        }


        // Sign Out
        if (navigationItemPositionManager.getSignOutPosition() != NavigationItemPositionManager.NO_POSITION) {
            Log.d(TAG, "initDrawerItems: 12");
            int position = navigationItemPositionManager.getSignOutPosition();
            String title = context.getString(R.string.sign_out);
            int icon = R.drawable.ic_signout;
            int iconSelected = R.drawable.ic_signout_selected;
            drawerItems.add(position, new NavDrawerItem(title, icon, iconSelected));
            Log.e("POsition", String.valueOf(position));
        }
        Log.e("title: ", "" + drawerItems.get(7).getTitle());
    }


    public NavigationItemPositionManager getNavigationItemPositionManager() {
        return navigationItemPositionManager;
    }

    public void update_Data() {
        notifyDataSetChanged();
    }

    private class ViewHolder {
        public ImageView iconView;
        public TextView titleView;
        public ImageView header_profile_view;
        public ImageView profile_header_img, header_setting_imgView;
        public TextView header_username_text;
        public RoundImageView profile_circle_view;
    }
}