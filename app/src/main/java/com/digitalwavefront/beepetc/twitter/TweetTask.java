package com.digitalwavefront.beepetc.twitter;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.digitalwavefront.beepetc.BeConApp;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import twitter4j.MediaEntity;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by Wayne on 12/2/15.
 */
public class TweetTask extends AsyncTask<Void, Void, TweetResultBundle> {
    private static final String TAG = "TweetTask";

    private TweetListener tweetListener;
    private Twitter twitter;
    private HashTagManager hashTagManager;

    public TweetTask(Context context, TweetListener tweetListener) {
        this.tweetListener = tweetListener;
        this.hashTagManager = ((BeConApp) context.getApplicationContext()).getHashTagManager();
        twitter = prepareTwitter();
    }

    @Override
    protected TweetResultBundle doInBackground(Void... params) {

        // The hash-tag manager hash-tag should not be null
       /* if (StringUtils.isNullOrEmpty(hashTagManager.getHashTag())) {
            return new TweetResultBundle(null, new Exception("Error retrieving tweets for social chatter, " +
                    "hash-tag cannot be null or empty"));
        } */
        if (hashTagManager.getHashTagList().isEmpty()){
            return new TweetResultBundle(null, new Exception("Error retrieving tweets for social chatter, " +
                    "hash-tag cannot be null or empty"));
        }
        else {
            try {
                // Get authorization
                twitter.getOAuth2Token();

                List<BeepTweet> tweets = new ArrayList<>();

                Set<String> set = hashTagManager.getHashTagList();
                List<String> hashlist = new ArrayList<String>(set);
                for (int i=0; i<hashlist.size(); i++){
                    Log.e("hashTaglist_item"," "+hashlist.get(i));
                }

                String joinedString = TextUtils.join("+OR+",hashlist);
                Log.e("joinstring",""+joinedString);

             // Find all tweets of the required hash tag
                Query query = new Query(joinedString);
                Log.e("query"," "+query);
                QueryResult result = twitter.search(query);

                // Parse and add the tweets
                for (twitter4j.Status status : result.getTweets()) {
                    tweets.add(tweetFromStatus(status));
                }

                // Return the result
                return new TweetResultBundle(tweets, null);
            } catch (TwitterException e) {
                // There was an error getting tweets
                return new TweetResultBundle(null, e);
            }
        }
    }

    @Override
    protected void onPostExecute(TweetResultBundle resultBundle) {
        if (resultBundle.getTweets() != null) {
            tweetListener.onTweets(resultBundle.getTweets());
        } else {
            tweetListener.onTweetError(resultBundle.getException());
        }
    }

    private Twitter prepareTwitter() {
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setApplicationOnlyAuthEnabled(true);
        Twitter twitter = new TwitterFactory(configurationBuilder.build()).getInstance();
        twitter.setOAuthConsumer(TwitterAPICredentials.getConsumerKey(), TwitterAPICredentials.getConsumerSecret());
        return twitter;
    }

    private BeepTweet tweetFromStatus(twitter4j.Status status) {
        String screenName = status.getUser().getScreenName();
        String message = status.getText();

        // Get the image for the Tweet
        String imageUrl = null;

        if (status.getMediaEntities().length > 0) {
            MediaEntity mediaEntity = status.getMediaEntities()[0];
            imageUrl = mediaEntity.getMediaURL();
        }

        return new BeepTweet(screenName, message, imageUrl);
    }
}
