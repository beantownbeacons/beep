package com.digitalwavefront.beepetc.twitter;

import android.content.Context;

/**
 * Created by Wayne on 12/2/15.
 */
public class TwitterRequestManager {
    private static final String TAG = "TwitterRequestManager";

    private Context context;


    public TwitterRequestManager(Context context) {
        this.context = context;

    }

    public void getTweets(TweetListener tweetListener) {
        new TweetTask(context, tweetListener).execute();
    }
}
