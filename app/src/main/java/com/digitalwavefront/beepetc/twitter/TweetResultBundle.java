package com.digitalwavefront.beepetc.twitter;

import java.util.List;

/**
 * Created by Wayne on 12/2/15.
 */
public class TweetResultBundle {
    private List<BeepTweet> tweets;
    private Exception exception;

    public TweetResultBundle(List<BeepTweet> tweets, Exception exception) {
        this.tweets = tweets;
        this.exception = exception;
    }

    public List<BeepTweet> getTweets() {
        return tweets;
    }

    public Exception getException() {
        return exception;
    }
}
