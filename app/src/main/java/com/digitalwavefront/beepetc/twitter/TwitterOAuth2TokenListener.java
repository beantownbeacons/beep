package com.digitalwavefront.beepetc.twitter;

import twitter4j.TwitterException;
import twitter4j.auth.OAuth2Token;

/**
 * Created by Wayne on 12/2/15.
 */
public interface TwitterOAuth2TokenListener {
    void onToken(OAuth2Token token);
    void onTokenError(TwitterException e);
}
