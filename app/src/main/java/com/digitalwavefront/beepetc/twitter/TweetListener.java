package com.digitalwavefront.beepetc.twitter;

import java.util.List;

/**
 * Created by Wayne on 12/2/15.
 */
public interface TweetListener {
    void onTweets(List<BeepTweet> tweets);
    void onTweetError(Exception e);
}
