package com.digitalwavefront.beepetc.twitter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Wayne on 12/12/15.
 */

/**
 * Changes made by Supra-chaitali on 30/06/2016
 */
public class HashTagManager {

    private static final String PREF_HASH_TAG = "event_hash_tag";
    private static final String PREF_HASH_TAG_LIST = "event_hash_tag_list";
    private Context context;

    public HashTagManager(Context context) {
        this.context = context;
    }

    public void storeHashList(List<String> hashList){
        SharedPreferences.Editor editor = getPreferencesEditor();
        Set<String> set = new HashSet<String>();
        Log.e("store","hashlist");
        set.addAll(hashList);
        editor.putStringSet(PREF_HASH_TAG_LIST, set);
        editor.commit();
    }

    public Set<String> getHashTagList(){
        SharedPreferences pref = getSharedPreferences();
        Log.e("get","hashlist");
        return pref.getStringSet(PREF_HASH_TAG_LIST, null);
    }

   /* public void storeHashTag(String hashTag) {
        SharedPreferences.Editor editor = getPreferencesEditor();
        editor.putString(PREF_HASH_TAG, hashTag);
        editor.commit();
    }*/

    /*public String getHashTag() {
        SharedPreferences prefs = getSharedPreferences();
        return prefs.getString(PREF_HASH_TAG, null);
    }*/
    
    private SharedPreferences getSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    private SharedPreferences.Editor getPreferencesEditor() {
        return getSharedPreferences().edit();
    }
}
