package com.digitalwavefront.beepetc.parse.models;

import com.digitalwavefront.beepetc.interfaces.EventAccessLogLoadedListener;
import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by User on 9/18/17.
 */
@ParseClassName("EventAccessLog")
public class EventAccessLog extends ParseObject {

    public EventAccessLog() {}

    public String getUserId() {  return getString("userId");  }
    public void setUserId(String userId) {  put("userId", userId);  }
    public void setEventId(String userId) {  put("eventId", userId);  }
    public void setUserType(String userId) {  put("userType", userId);  }


    public static void getUsersForEVL(String evtId, final EventAccessLogLoadedListener evtLogLoadedlistener){

        ParseQuery<EventAccessLog> query = ParseQuery.getQuery(EventAccessLog.class);
        query.whereEqualTo("eventId",evtId);
        query.whereEqualTo("userType","ATTENDEE");

        query.findInBackground(new FindCallback<EventAccessLog>() {
            @Override
            public void done(List<EventAccessLog> objects, ParseException e) {
                if(e == null){

                    if(objects != null && !objects.isEmpty()) {

                        evtLogLoadedlistener.onEventAccessLoaded(objects);
                    }else{
                        evtLogLoadedlistener.onEventAccessLoaded(null);
                    }

                }else{
                        evtLogLoadedlistener.onEventAccessLoaded(null);
                }
            }
        });
    }
}
