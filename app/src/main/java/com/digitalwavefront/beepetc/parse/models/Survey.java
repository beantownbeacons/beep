package com.digitalwavefront.beepetc.parse.models;

import com.digitalwavefront.beepetc.interfaces.SurveyQuestionsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SurveysLoadedListener;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Wayne on 10/11/15.
 */
@ParseClassName("Survey")
public class Survey extends ParseObject {

    private List<SurveyQuestion> questions;

    public Survey() {}

    public String getTitle() {
        return getString("title");
    }
    public String getSpeakerId() { return getString("speakerId"); }

    /*
    public void getQuestions(final SurveyQuestionsLoadedListener listener) {
        if (questions != null) {
            listener.onSurveyQuestionsLoaded(questions);
        } else {
            SurveyQuestion.getSurveyQuestionsBySurveyId(getObjectId(), new SurveyQuestionsLoadedListener() {

                @Override
                public void onSurveyQuestionsLoaded(@Nullable List<SurveyQuestion> questions) {
                    Survey.this.questions = questions;
                    listener.onSurveyQuestionsLoaded(questions);
                }
            }, true);
        }
    }
*/
    public int getCompleteCount() {
        return getInt("completeCount");
    }

    /*********
     * Queries
     *********/
    public static void loadSurveysForUser(BeConUser beConUser, final SurveysLoadedListener listener) {
        List<String> sessionIds = beConUser.getSessionIds();

        /*if (sessionIds == null) {
            listener.onSurveysLoaded(new ArrayList<Survey>());
        } else {*/
            Date now = new Date();

            // Query for the surveys
            ParseQuery<Survey> query = ParseQuery.getQuery(Survey.class);
            //query.whereContainedIn("sessionId", sessionIds);
            query.whereLessThanOrEqualTo("startDate", now);
            query.whereEqualTo("eventId", beConUser.getCurrentEventId());

            query.findInBackground(new FindCallback<Survey>() {

                @Override
                public void done(List<Survey> surveys, ParseException e) {
                    listener.onSurveysLoaded(surveys);
                }
            });
        //}
    }

    // by Sarika [27/09/2017]

    public static void loadSurveyQuesForUser(BeConUser beConUser, final SurveyQuestionsLoadedListener listener) {

        Date now = new Date();

        // Query for the surveys
        ParseQuery<SurveyQuestion> query = ParseQuery.getQuery(SurveyQuestion.class);
        //query.whereLessThanOrEqualTo("startDate", now);
        query.whereEqualTo("eventId", beConUser.getCurrentEventId());
        query.findInBackground(new FindCallback<SurveyQuestion>() {
            @Override
            public void done(List<SurveyQuestion> objects, ParseException e) {

                ArrayList<String> sessIds = new ArrayList<String>();
                for(SurveyQuestion ques: objects) {

                }
                listener.onSurveyQuestionsLoaded(objects);
            }
        });

    }

    public static void getSurveysBySpeaker(Speaker speaker, final SurveysLoadedListener listener) {
        ParseQuery<Survey> query = ParseQuery.getQuery(Survey.class);
        query.whereEqualTo("speakerId", speaker.getObjectId());
        query.findInBackground(new FindCallback<Survey>() {

            @Override
            public void done(List<Survey> surveys, ParseException e) {
                listener.onSurveysLoaded(surveys);
            }
        });
    }

    /***********
     * Modifiers
     ***********/
    public static void incrementSurveyCompleteCountById(String surveyId) {
        ParseQuery<Survey> query = ParseQuery.getQuery(Survey.class);
        query.getInBackground(surveyId, new GetCallback<Survey>() {

            @Override
            public void done(Survey survey, ParseException e) {
                if (e == null) {
                    survey.increment("completeCount");
                    survey.saveInBackground();
                } else {
                    // TODO: Handle error
                }
            }
        });
    }
}
