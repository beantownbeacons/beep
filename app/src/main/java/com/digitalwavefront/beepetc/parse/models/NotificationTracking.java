package com.digitalwavefront.beepetc.parse.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Tarul on 02/09/2016.
 */
@ParseClassName("NotificationTracking")
public class NotificationTracking extends ParseObject {

    public NotificationTracking() {}

    public void setNotificationId(String notificationId) {
        put("NotificationId", notificationId);
    }

    public void setNotificationType(String notificationType) {
        put("NotificationType", notificationType);
    }

    public void setUserId(String userId) {
        put("UserId", userId);
    }
}
