package com.digitalwavefront.beepetc.parse.models;

import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by Wayne on 11/26/15.
 */
@ParseClassName("Company")
public class Company extends ParseObject {

    public Company() {}

    /*********
     * Queries
     *********/
    public static void getCompanyByCode(String code, FindCallback<Company> callback) {
        ParseQuery<Company> query = ParseQuery.getQuery(Company.class);
        query.whereEqualTo("code", code);
        query.setLimit(1);
        query.findInBackground(callback);
    }
}
