package com.digitalwavefront.beepetc.parse.models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.digitalwavefront.beepetc.interfaces.BitmapLoadedListener;
import com.digitalwavefront.beepetc.interfaces.ExhibitorsLoadedListener;
import com.digitalwavefront.beepetc.utils.StringUtils;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by sarika on 02/06/16.
 */
@ParseClassName("Exhibitor")
public class Exhibitor extends ParseObject {

    private Bitmap avatar;
    int rating;

    public Exhibitor() {

    }

    public String getName() {
        return getString("name");
    }

    public String getObjectId() {
        return getString("objectId");
    }
    public String getEventId(){
        return getString("eventId");
    }
    public String getBooth(){

        if (!StringUtils.isNullOrEmpty(getString("booth"))){
            return getString("booth");
        }
        return "";
    }
    public String getWebURL(){
        return getString("website");
    }
    public int getRating(){
        return this.rating;
    }

    public void setRating(int rating){
        this.rating = rating;
    }

    public void getAvatar(final BitmapLoadedListener bitmapLoadedListener) {
        if (avatar == null) {
            ParseFile avatarFile = getParseFile("logo");
            if(avatarFile != null) {
                avatarFile.getDataInBackground(new GetDataCallback() {

                    @Override
                    public void done(byte[] bytes, ParseException e) {
                        if (bytes != null) {
                            Bitmap avatar = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                            Exhibitor.this.avatar = avatar;
                            bitmapLoadedListener.onBitmapLoaded(avatar);
                        }
                    }
                });
            }else {
                bitmapLoadedListener.onBitmapLoaded(avatar);
            }
        } else {
            bitmapLoadedListener.onBitmapLoaded(avatar);
        }
    }

    public static void getExhibitorByEventId(String eventId, final ExhibitorsLoadedListener exhibitorsLoadedListener){

        ParseQuery<Exhibitor> query = ParseQuery.getQuery(Exhibitor.class);
        query.whereEqualTo("eventId",eventId);
        query.orderByAscending("name");
        query.setCachePolicy(ParseQuery.CachePolicy.CACHE_THEN_NETWORK); 
        query.findInBackground(new FindCallback<Exhibitor>() {
            @Override
            public void done(final List<Exhibitor> exhibitorList, ParseException e) {

                if (exhibitorList != null) {
                    /*

                    List<String> exhIdArr = new ArrayList<String>() {
                    };

                    for (int i = 0; i < exhibitorList.size(); i++) {
                        exhIdArr.add(exhibitorList.get(i).getObjectId());
                    }

                    ExhibitorRating.getExhibitorRatingByUserID(exhIdArr, new ExhibitorRatingLoadedListener() {
                        @Override
                        public void onExhibitorRatingLoaded(@Nullable List<ExhibitorRating> exhibitorRatingList) {

                            for (int j = 0; j < exhibitorList.size(); j++) {

                                Exhibitor exobj = exhibitorList.get(j);
                                for (int k = 0; k < exhibitorRatingList.size(); k++) {

                                    ExhibitorRating exrateObj = exhibitorRatingList.get(k);
                                    if (exrateObj.belongsExhibitor(exobj)) {
                                        exobj.setRating(exrateObj.getRating());
                                    }

                                }
                            }
                        }
                    });

                    */
                    exhibitorsLoadedListener.onExhibitorsLoaded(exhibitorList);
                }
            }
        });
    }
}
