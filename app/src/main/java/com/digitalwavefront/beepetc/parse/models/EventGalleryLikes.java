package com.digitalwavefront.beepetc.parse.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Praneet on 01/07/2016.
 */
@ParseClassName("EventGalleryLikes")
public class EventGalleryLikes extends ParseObject {

    public EventGalleryLikes() {}

    public String chkNull(String str)
    {
        if(str == null || str.equals(null))
        { return "";} return str;
    }

    public String getPictureId(){
        return getString("pictureId");
    }

    public String getUserId(){
        return getString("userId");
    }

    public void setPictureId(String pictureId){
        put("pictureId",pictureId);
    }

    public void setUserId(String userId){
         put("userId",userId);
    }

    /*********
     * Queries
     *********/

//    public static boolean likedByUser(String userId,String pictureId)
//    {
//        Log.e("Praneet","likedByuser "+pictureId);
//        Log.e("Praneet","in likedByuser"+userId);
//
//        ParseQuery<EventGalleryLikes> query = ParseQuery.getQuery(EventGalleryLikes.class);
//
//
//        query.whereEqualTo("pictureId", pictureId);
//        query.whereEqualTo("userId", userId);
//
//        query.findInBackground(new FindCallback<EventGallery>() {
//            @Override
//            public void done(List<EventGallery> gallery, ParseException e) {
//                Log.e("Praneet","inFind in background"+e);
//                galleryLoadedListener.onGalleryLoaded(gallery);
//            }
//        });
//
//    }


}
