package com.digitalwavefront.beepetc.parse.models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import com.digitalwavefront.beepetc.interfaces.BitmapLoadedListener;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.digitalwavefront.beepetc.interfaces.FileLoadedListener;
import com.digitalwavefront.beepetc.interfaces.NotificationsLoadedListener;
import com.digitalwavefront.beepetc.utils.PdfUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wayne on 9/28/15.
 */
@ParseClassName("Notification")
public class BeConNotification extends ParseObject {

    Bitmap image;

    public BeConNotification() {}

    public String getBeaconId() {
        return getString("beaconId");
    }

    public int getTriggerTimeInSeconds() {
        return getInt("triggerTimeInSecs");
    }

    public String getTitle() {
        return getString("title");
    }

    public String getMessage() {
        return getString("message");
    }

    public String getUrl() {
        return getString("url");
    }

    public ParseFile getPdfFile(){
        return getParseFile("pdfFile");
    }

    public ParseFile getImage() {  return getParseFile("image");  }

    public String getCompanyId() {  return getString("CompanyID");  }



    public void getImage(final BitmapLoadedListener bitmapLoadedListener) {

        if (image == null) {
            Log.e("if", "if");

            ParseFile profile_photo = getParseFile("Image");
            if (profile_photo != null) {
                profile_photo.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] bytes, ParseException e) {
                        if (bytes != null) {
                            Bitmap profile_img = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                            bitmapLoadedListener.onBitmapLoaded(profile_img);
                        }
                    }
                });
            }
        } else {
            Log.e("else", "else");
            bitmapLoadedListener.onBitmapLoaded(image);
        }
    }


    public void getPdf(final Context context, final FileLoadedListener fileLoadedListener) {
        final String pdfFilePath = getPdfFilePath(context);
        File pdfFile = new File(pdfFilePath);

        if (!pdfFile.exists()) {
            if (has("pdfFile")) {

                // Check if the pdf file has been saved to device
                ParseFile pdfParseFile = getParseFile("pdfFile");
                pdfParseFile.getDataInBackground(new GetDataCallback() {

                    @Override
                    public void done(byte[] pdfData, ParseException e) {
                        try {
                            PdfUtils.savePdfFromByteArray(pdfFilePath, pdfData);
                            fileLoadedListener.onFileLoaded(new File(pdfFilePath));
                        } catch (IOException e1) {
                            e1.printStackTrace();
                            fileLoadedListener.onFileLoaded(null);
                        }
                    }
                });
            } else {
                fileLoadedListener.onFileLoaded(null);
            }
        } else {
            fileLoadedListener.onFileLoaded(pdfFile);
        }
    }

    public boolean belongsToBeacon(BeConBeacon beacon) {
        return getBeaconId().equals(beacon.getObjectId());
    }

    public String getPdfFilePath(Context context) {
        try{
        String filesDirectoryPath = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS ).getAbsolutePath();
        String pdfFileName = String.format("%s.pdf", getObjectId());
        return String.format("%s/%s", filesDirectoryPath, pdfFileName);
    }catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    /*********
     * Queries
     *********/
    public static void getNotificationsForBeacons(List<BeConBeacon> beacons, final NotificationsLoadedListener notificationsLoadedListener) {
        ParseQuery<BeConNotification> query = ParseQuery.getQuery(BeConNotification.class);

        // Load sessions ids for query
        if (beacons != null && beacons.size() > 0) {
            List<String> beaconIds = new ArrayList<>();
            for (BeConBeacon beacon : beacons) {
                beaconIds.add(beacon.getObjectId());
            }

            query.whereContainedIn("beaconId", beaconIds);
            query.findInBackground(new FindCallback<BeConNotification>() {

                @Override
                public void done(List<BeConNotification> notifications, ParseException e) {
                    if (notifications != null && !notifications.isEmpty()) {
                        notificationsLoadedListener.onNotificationsLoaded(notifications);
                    } else {
                        notificationsLoadedListener.onNotificationsLoaded(null);
                    }
                }
            });
        } else {

            notificationsLoadedListener.onNotificationsLoaded(null);
        }
    }

    public static void getNotificationsByIds(List<String> notificationIds, final NotificationsLoadedListener listener) {
        ParseQuery<BeConNotification> query = ParseQuery.getQuery(BeConNotification.class);
        query.whereContainedIn("objectId", notificationIds);
        query.findInBackground(new FindCallback<BeConNotification>() {

            @Override
            public void done(List<BeConNotification> notifications, ParseException e) {

                listener.onNotificationsLoaded(notifications);
            }
        });
    }
}
