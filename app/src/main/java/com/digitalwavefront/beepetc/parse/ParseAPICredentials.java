package com.digitalwavefront.beepetc.parse;

import com.digitalwavefront.beepetc.BeConApp;

/**
 * Created by Wayne on 11/19/15.
 */
public class ParseAPICredentials {
    private static final String APPLICATION_ID_DEV = "APP_ID_BEEP_1_0";//"YCzMrwgIZqFcpt9gc1erWrwTnO6yv3pmFUOJ6OT5";
    //private static final String APPLICATION_ID_DEV ="2xdxNeM9YFoUIac6vo2t11duMvuqfbhN44jf0C2D";
    private static final String APPLICATION_ID = "YCzMrwgIZqFcpt9gc1erWrwTnO6yv3pmFUOJ6OT5";//"98cARrSpgNB7GsdZStrgrwGd4DnS2J3N91MeMM7R";
    private static final String CLIENT_KEY_DEV = "CLIENT_KEY_BEEP_1_0";//"1EM1SAq41UqitlJnX72RALdutbRQ2y4u1pvvVyXv";
    //private static final String CLIENT_KEY_DEV = "uOtSFD6CBTN0cWBD2K0r6nuqLsvAuY4kRNyADGV3";
    private static final String CLIENT_KEY = "PixDMyMgm1lGsa3FDMefCLEfBq3J93hsg4QYq4Jy";//"5d9yWMD5rbCuE5BsAqGmvYEyH73Il34DrNDtXI9s";

    private static final String SERVER_URL_DEV = "http://aws.beepetc.com:1338/parse/";
    private static final String SERVER_URL = "http://aws.beepetc.com:1337/parse/";
//MASTER_KEY_BEEP_1_0
    /*
    * ['app_id'] YCzMrwgIZqFcpt9gc1erWrwTnO6yv3pmFUOJ6OT5
['rest_key'] PixDMyMgm1lGsa3FDMefCLEfBq3J93hsg4QYq4Jy
['master_key'] Ml8ZoLEUf4fq3CnLtY6Y3J6AugYYwUycxiFV4muQ
URL: http://aws.beepetc.com:1337/parse
    * */

    public static String getApplicationId() {
        if (BeConApp.DEV_MODE) {
            return APPLICATION_ID_DEV;
        }

        return APPLICATION_ID;
    }

    public static String getClientKey() {
        if (BeConApp.DEV_MODE) {
            return CLIENT_KEY_DEV;
        }

        return CLIENT_KEY;
    }

    public static String getServer() {
        if (BeConApp.DEV_MODE) {
            return SERVER_URL_DEV;
        }

        return SERVER_URL;
    }
}
