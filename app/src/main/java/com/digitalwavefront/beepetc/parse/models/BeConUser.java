package com.digitalwavefront.beepetc.parse.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.digitalwavefront.beepetc.interfaces.BitmapLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SurveyQuestionsLoadedListener;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.digitalwavefront.beepetc.interfaces.EventLoadedListener;
import com.digitalwavefront.beepetc.interfaces.NotificationsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SessionsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SurveyResponseCallback;
import com.digitalwavefront.beepetc.interfaces.SurveysLoadedListener;
import com.digitalwavefront.beepetc.login.LoginType;
import com.digitalwavefront.beepetc.utils.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Wayne on 9/15/15.
 */
@ParseClassName("_User")
public class BeConUser extends ParseUser implements Comparable<BeConUser>{
    private static final String PREF_LOGIN_TYPE = "login_type";
    private static final String PREF_COMPANY_ID = "company_code";
    public Bitmap profile;
    static String eventCode;

    public BeConUser() {
    }

    public static BeConUser getCurrentUser() {
        return (BeConUser) ParseUser.getCurrentUser();
    }

    public void setName(String name) {
        if (!StringUtils.isNullOrEmpty(name)) {
            put("name", name);
        }
    }

    public void setUserName(String userName) {
        if (!StringUtils.isNullOrEmpty(userName)) {
            put("username", userName);
        }
    }

    public void setPhoneNumber(String phoneNumber) {
        if (!StringUtils.isNullOrEmpty(phoneNumber) && StringUtils.isValidPhoneNumber(phoneNumber)) {
            phoneNumber = StringUtils.convertToNumericPhoneNumber(phoneNumber);
            Log.e("converted_no1", "" + phoneNumber);
            put("phoneNumber", phoneNumber);
        }
    }

    public void setTitle(String title) {
        if (!StringUtils.isNullOrEmpty(title)) {
            put("title", title);
        }
    }

    public void setSeed(Boolean seed) {
        put("seed", seed);
    }

    public String getName() {

        if (!StringUtils.isNullOrEmpty(getString("name"))) {
            return getString("name");
        }
        return "";
    }

    public String getUserName() {
        if (!StringUtils.isNullOrEmpty(getString("username"))){
            return getString("username");
        }
        return "";
    }

    public String getTitle() {
        return getString("title");
    }

    public String getLinkedInProfile() {
        return getString("linkedInProfile");
    }

    public boolean hasLinkedIn() {
        return getLinkedInProfile() != null && !getLinkedInProfile().equals("");
    }

    public String getTwitterHandler() {
        return getString("twitterHandle");
    }

    public boolean hasTwitter() {
        /*UrlValidator urlValidator = StringUtils.getDefaultUrlValidator();
        return getTwitterHandler() != null && urlValidator.isValid(getTwitterHandler());*/
        return getTwitterHandler() != null && !getTwitterHandler().equals("");
    }

    public List getSavedNotIds() {
        return getList("savedNotificationIds");
    }

    public List getReceivedNotIds() {
        return getList("receivedNotificationIds");
    }

    public void setLinkedInProfile(String linkedIn) {
        if (!StringUtils.isNullOrEmpty(linkedIn)) {
            put("linkedInProfile", linkedIn);
        }
    }

    public void setTwitterHandler(String twitterhandle) {
        if (!StringUtils.isNullOrEmpty(twitterhandle)) {
            put("twitterHandle", twitterhandle);
        }
    }

    public void setProfilephoto(ParseFile file) {
        put("profilePhoto", file);
    }

    public String getPhoneNumber() {
        return getString("phoneNumber");
    }

    public void getProfilePhoto(final BitmapLoadedListener bitmapLoadedListener) {

        if (profile == null) {
            Log.e("if", "if");

            ParseFile profile_photo = getParseFile("profilePhoto");
            if (profile_photo != null) {
                profile_photo.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] bytes, ParseException e) {
                        if (bytes != null) {

                            Bitmap profile_img = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                            BeConUser.this.profile = profile_img;
                            bitmapLoadedListener.onBitmapLoaded(profile_img);
                        }
                    }
                });
            }
        } else {
            Log.e("else", "else");
            bitmapLoadedListener.onBitmapLoaded(profile);
        }
    }

    public void setCompany(String company) {
        if (!StringUtils.isNullOrEmpty(company)) {
            put("company", company);
        }
    }

    public void setDeviceDetails(String deviceDetails) {
        if (!StringUtils.isNullOrEmpty(deviceDetails)) {
            put("deviceDetails", deviceDetails);
        }
    }

    public void setDeviceOS(String os) {
        if (!StringUtils.isNullOrEmpty(os)) {
            put("deviceOS", os);
        }
    }

    public void setDeviceScreen(String screen) {
        if (!StringUtils.isNullOrEmpty(screen)) {
            put("screenSize", screen);
        }
    }


    public String getCompany() {
        return getString("company");
    }

    public void setIsVerified(boolean isVerified) {
        put("isVerified", isVerified);
    }

    public boolean isVerified() {
        return getBoolean("isVerified");
    }

    public boolean isSignedIntoEvent() {
        return has("currentEventId");
    }

    public void rsvpForSession(Session session, SaveCallback callback) {
        addUnique("sessionIds", session.getObjectId());

        List<ParseObject> saveList = new ArrayList<>();
        SessionRSVP rsvp = SessionRSVP.newInstance(this, session);
        saveList.add(rsvp);
        saveList.add(this);
        ParseObject.saveAllInBackground(saveList, callback);
    }

    public void unregisterForSession(Session session, final SaveCallback saveCallback) {
        Log.e("unregisterForSession", "unregisterForSession: " + session);
        List<String> sessionIds = new ArrayList<>();
        sessionIds.add(session.getObjectId());
        removeAll("sessionIds", sessionIds);
        SessionRSVP.deleteRSVPBySessionAndUser(session, this, new DeleteCallback() {

            @Override
            public void done(ParseException e) {
                // TODO: Handle the error deleting the RSVP
                saveInBackground(saveCallback);
            }
        });
    }

    public boolean isRegisteredForSession(Session session) {
        List<String> sessionIds = getSessionIds();
        if (sessionIds == null) {
            return false;
        }
        return sessionIds.contains(session.getObjectId());
    }

    public void getRegisteredSessions(SessionsLoadedListener listener) {
        List<String> sessionIds = getSessionIds();
        if (sessionIds == null) {
            listener.onSessionsLoaded(new ArrayList<Session>());
        } else {
            Session.getSessionsByObjectIds(getCurrentEventId(), sessionIds, listener);
        }
    }

    public List<String> getSessionIds() {
        return getList("sessionIds");
    }

    public String getCurrentEventId() {
        return getString("currentEventId");
    }

    public void getCurrentEvent(final EventLoadedListener eventLoadedListener) {
        if (!isSignedIntoEvent()) {
            eventLoadedListener.onEventLoaded(null);
        } else {
            String eventId = getCurrentEventId();
            Event.getEventByObjectId(eventId, eventLoadedListener);
        }
    }

    public void setCurrentEvent(Event currentEvent) {
        put("currentEventId", currentEvent.getObjectId());
    }

    public void removeCurrentEvent() {
        remove("currentEventId");
    }

    public void addReceivedNotification(BeConBeacon beacon, BeConNotification notification, SaveCallback callback) {
        addUnique("receivedNotificationIds", notification.getObjectId());

        List<ParseObject> saveList = new ArrayList<>();
        saveList.add(this);
        ReceivedNotification receivedNotification = ReceivedNotification.newInstance(this, notification, beacon.getCompanyId());
        saveList.add(receivedNotification);
        ParseObject.saveAllInBackground(saveList, callback);
    }

    public boolean hasReceivedNotification(BeConNotification notification) {
        List<String> receivedNotificationIds = getList("receivedNotificationIds");

        if (receivedNotificationIds == null) {
            return false;
        }
        return receivedNotificationIds.contains(notification.getObjectId());
    }

    public void saveNotification(String companyId, String notificationId, SaveCallback callback) {
        addUnique("savedNotificationIds", notificationId);

        List<ParseObject> saveList = new ArrayList<>();
        saveList.add(this);
        SavedNotification savedNotification = SavedNotification.newInstance(this, notificationId, companyId);
        saveList.add(savedNotification);
        ParseObject.saveAllInBackground(saveList, callback);
    }

    public boolean getSavedNotifications(final NotificationsLoadedListener listener) {
        //SavedNotification.getSavedNotificationsForUser(this, getCurrentEventId(), listener);
        List<String> savedIds = getSavedNotIds();


        Log.e("getSavedNotifications", "savedIds >> " + savedIds);
        if (savedIds != null) {
            Log.e("Praneet", "savedNotificationTable event id " + this.getCurrentEventId());
            Log.e("Praneet", "savedNotificationTable userid " + this.getObjectId());
            Log.e("Praneet", "savedNotificationTable savedId " + savedIds.toString());


            ParseQuery<SavedNotification> query = ParseQuery.getQuery(SavedNotification.class);

            query.whereEqualTo("eventId", this.getCurrentEventId());
            query.whereEqualTo("userId", this.getObjectId());
            query.whereContainedIn("notificationId", savedIds);

            query.findInBackground(new FindCallback<SavedNotification>() {
                @Override
                public void done(List<SavedNotification> objects, ParseException e) {
                    Log.e("Praneet", "savedNotificationTable objects list " + objects.toString());
                    Log.e("Praneet", "savedNotificationTable exception " + e);
                    String[] notificationId = new String[objects.size()];
                    List<String> notificationIdList;
                    for (int i = 0; i < objects.size(); i++) {
                        notificationId[i] = objects.get(i).getNotificationId();
                    }
                    notificationIdList = Arrays.asList(notificationId);
                    BeConNotification.getNotificationsByIds(notificationIdList, listener);
                }
            });
            ;
            return true;
        } else {
            return false;
        }
    }

    public void removeSavedNotification(BeConNotification notification, final SaveCallback saveCallback) {
        List<String> notificationIds = new ArrayList<>();
        notificationIds.add(notification.getObjectId());
        removeAll("savedNotificationIds", notificationIds);

        SavedNotification.deleteRSVPByNotificationAndUser(notification, this, new DeleteCallback() {
            @Override
            public void done(ParseException e) {
                // TODO: Handle delete error
                saveInBackground(saveCallback);
            }
        });
    }

    public void getSurveys(SurveysLoadedListener listener) {
        Survey.loadSurveysForUser(this, listener);
    }

    // by Sarika [27/09/2017]
    public void getSurveyQues(SurveyQuestionsLoadedListener listener) {
        Survey.loadSurveyQuesForUser(this, listener);
    }


    public void submitSurveyResponse(final SurveyQuestion question, int rating, final SurveyResponseCallback callback) {
        final SurveyResponse response = SurveyResponse.newInstance(this, question, rating);
        response.saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException e) {
                if (e == null) {
                    callback.onResponseRecorded(response);
                } else {
                    callback.onResponseError(e);
                }
            }
        });
    }

    // by Sarika [27-09-2017] submit surveyresponse with sessionId
    public void submitSurveyResponse(final SurveyQuestion question, int rating, String sessionId, String speakerd, final SurveyResponseCallback callback) {
        final SurveyResponse response = SurveyResponse.newInstance(this, question, sessionId, speakerd, rating);
        response.saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException e) {
                if (e == null) {
                    callback.onResponseRecorded(response);
                } else {
                    callback.onResponseError(e);
                }
            }
        });
    }

    // Login
    public void setLoginType(Context context, int loginType) {
        SharedPreferences preferences = getPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(PREF_LOGIN_TYPE, loginType);
        editor.commit();
    }

    public int getLoginType(Context context) {
        SharedPreferences preferences = getPreferences(context);
        int loginType = preferences.getInt(PREF_LOGIN_TYPE, LoginType.INVALID_VALUE);
        return loginType;
    }

    // Company Code
    public void setCompanyId(Context context, String companyCode) {
        SharedPreferences preferences = getPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_COMPANY_ID, companyCode);
        editor.commit();
    }

    public String getCompanyId(Context context) {
        SharedPreferences preferences = getPreferences(context);
        String companyCode = preferences.getString(PREF_COMPANY_ID, null);
        return companyCode;
    }

    public void clearCompanyId(Context context) {
        setCompanyId(context, null);
    }

    // Shared Preferences
    private SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    // Queries
    public static void getUsersByEmail(String email, FindCallback<BeConUser> findCallback) {
        ParseQuery<BeConUser> query = ParseQuery.getQuery(BeConUser.class);
        query.whereEqualTo("username", email);
        query.findInBackground(findCallback);
    }

    public static void getUsersByUsername(String userID, FindCallback<BeConUser> findCallback) {
        ParseQuery<BeConUser> query = ParseQuery.getQuery(BeConUser.class);
        query.whereEqualTo("username", userID);
        query.findInBackground(findCallback);
    }

    public static void getVerifiedUsersByPhoneNumber(String phoneNumber, FindCallback<BeConUser> findCallback) {
        ParseQuery<BeConUser> query = ParseQuery.getQuery(BeConUser.class);
        Log.e("verify uphn", "query " + phoneNumber);
        query.whereEqualTo("phoneNumber", phoneNumber);
        query.whereEqualTo("isVerified", true);
        query.findInBackground(findCallback);
    }

    @Override
    public int compareTo(@NonNull BeConUser o) {

        return this.getName().compareTo(o.getName());
    }


}
