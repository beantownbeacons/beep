package com.digitalwavefront.beepetc.parse.models;

import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.digitalwavefront.beepetc.interfaces.SurveyResponseLoadedListener;

import java.util.List;

/**
 * Created by Wayne on 10/16/15.
 */
@ParseClassName("SurveyResponse")
public class SurveyResponse extends ParseObject {

    public SurveyResponse() {}

    public static SurveyResponse newInstance(BeConUser beConUser, SurveyQuestion question, int rating) {
        SurveyResponse response = new SurveyResponse();
        response.setUser(beConUser);
        response.setQuestion(question);
        response.setRating(rating);
        return response;
    }

    // by Sarika [27-09-2017] add sessionId to response
    public static SurveyResponse newInstance(BeConUser beConUser, SurveyQuestion question, String sessionId, String speakerId, int rating) {
        SurveyResponse response = new SurveyResponse();
        response.setUser(beConUser);
        response.setQuestion(question);
        response.setRating(rating);
        response.setSession(sessionId);
        response.setSpeakerId(speakerId);
        return response;
    }

    public void setUser(BeConUser user) {
        put("userId", user.getObjectId());
    }
    public void setSpeakerId(String speakerId) {
        put("speakerId", speakerId);
    }

    public void setQuestion(SurveyQuestion question) {
        put("questionId", question.getObjectId());
        //put("speakerId", question.getSpeakerId());
    }

    // by Sarika [27-09-2017] set sessionId for response
    public void setSession(String sessionId) {
        put("sessionId", sessionId);
    }
    private String getQuestionId() {
        return getString("questionId");
    }
    private String getSpeakerId() {
        return getString("speakerId");
    }

    public boolean belongsToQuestion(SurveyQuestion question, String speakerId) {
        try {
            return ((getQuestionId().equals(question.getObjectId())) && (getSpeakerId().equals(speakerId)));
        }catch (Exception e){return false;}
    }

    public void setRating(int rating) {
        put("rating", rating);
    }

    public int getRating() {
        return getInt("rating");
    }


    /*********
     * Queries
     *********/
    public static void getSurveyResponsesByQuestionIds(List<String> questionIds, final SurveyResponseLoadedListener listener) {
        BeConUser currentUser = BeConUser.getCurrentUser();

        if (currentUser != null) {
            ParseQuery<SurveyResponse> query = ParseQuery.getQuery(SurveyResponse.class);
            query.whereContainedIn("questionId", questionIds);
            query.whereEqualTo("userId", currentUser.getObjectId());
            query.findInBackground(new FindCallback<SurveyResponse>() {

                @Override
                public void done(List<SurveyResponse> responses, ParseException e) {
                    listener.onSurveyResponsesLoaded(responses);
                }
            });
        } else {
            listener.onSurveyResponsesLoaded(null);
        }
    }

    public static void getSurveyResponseBySpeaker(Speaker speaker, final SurveyResponseLoadedListener listener) {
        ParseQuery<SurveyResponse> query = ParseQuery.getQuery(SurveyResponse.class);
        query.whereEqualTo("speakerId", speaker.getObjectId());
        query.findInBackground(new FindCallback<SurveyResponse>() {

            @Override
            public void done(List<SurveyResponse> responses, ParseException e) {
                listener.onSurveyResponsesLoaded(responses);
            }
        });
    }
}
