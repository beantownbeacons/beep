package com.digitalwavefront.beepetc.parse.models;

import android.support.annotation.Nullable;

import com.digitalwavefront.beepetc.R;
import com.digitalwavefront.beepetc.interfaces.PrefLoadedListener;
import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.HashMap;
import java.util.List;

/**
 * Created by User on 1/10/18.
 */
@ParseClassName("UserPreferences")
public class UserPreferences extends ParseObject {

    String title, prefTitle;

    public String getTitle () {
        return this.title;
    }

    public void setTitle(String title1) {
        this.title = title1;
    }

    public String getPrefTitle () {
        return this.prefTitle;
    }

    public String getPrefKey(String key) {
        return getString(key);
    }
    public void setPrefTitle(String title1) {
        this.prefTitle = title1;
    }

    public boolean getSessionreminder() {

        return getBoolean("Sessionreminder");
    }

    public boolean getPrefVal(String pref) {

        return getBoolean(pref);
    }

    public void setPrefVal(String pref, boolean prefval) {

        put(pref,prefval);
    }

    public void setSessionreminder(boolean sessReminder) {

        put("Sessionreminder",sessReminder);
    }

    public static void getPrefByUserId(String uid, final PrefLoadedListener prefLoaded){

        ParseQuery<UserPreferences> prefQry = ParseQuery.getQuery(UserPreferences.class);
        prefQry.whereEqualTo("UserId",uid);
        prefQry.findInBackground(new FindCallback<UserPreferences>() {
            @Override
            public void done(List<UserPreferences> objects, ParseException e) {
                if (objects != null && !objects.isEmpty()) {

                    UserPreferences navConf = (UserPreferences) objects.get(0);
                    prefLoaded.onPrefLoadedListener(navConf);
                }else {
                    prefLoaded.onPrefLoadedListener(null);
                }
            }
        });
    }

    public void setPrefForUser(String uid) {

        final String strUid = uid;
        final HashMap<String, Boolean> listPref = new HashMap<>();
        listPref.put("SessionReminder",true);

        getPrefByUserId(uid, new PrefLoadedListener() {
            @Override
            public void onPrefLoadedListener(@Nullable UserPreferences preferences) {

                if (preferences == null) {

                    UserPreferences pref = new UserPreferences();
                    pref.put("UserId",strUid);
                    for (Object key : listPref.keySet()) {

                        pref.setPrefVal(key.toString(),listPref.get(key));
                    }
                    pref.saveInBackground();

                } else {

                }
            }
        });



    }
}
