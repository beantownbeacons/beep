package com.digitalwavefront.beepetc.parse.models;

import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.digitalwavefront.beepetc.interfaces.BeaconInteractionsLoadedListener;

import java.util.Date;
import java.util.List;

/**
 * Created by Wayne on 10/18/15.
 */
@ParseClassName("BeaconInteraction")
public class BeaconInteraction extends ParseObject {

    public BeaconInteraction() {}

    public static BeaconInteraction newInstance(BeConUser user, BeConBeacon beacon, Date timeIn, Date timeOut) {
        BeaconInteraction interaction = new BeaconInteraction();
        interaction.setUser(user);
        //interaction.setSession(session);
        interaction.setBeacon(beacon,user.getCurrentEventId());
        interaction.setTimeIn(timeIn);
        interaction.setTimeOut(timeOut);
        interaction.setTimeSpentMillis(timeIn, timeOut);
        return interaction;
    }

    private String getSessionId() {
        return getString("sessionId");
    }

    private String getBeaconId() {
        return getString("beaconId");
    }

    private void setSession(Session session) {
        put("sessionId", session.getObjectId());
        put("beaconId", session.getBeaconId());
        put("companyId", session.getBeacon().getCompanyId());
        put("speakerId", session.getSpeakerId());
        put("eventId", session.getEventId());
    }
    private void setBeacon(BeConBeacon beacon,String eventId) {

        put("beaconId", beacon.getObjectId());
        put("eventId", eventId);
    }

    public String getUserId() {
        return getString("userId");
    }

    private void setUser(BeConUser user) {
        put("userId", user.getObjectId());
    }

    private Date getTimeIn() {
        return getDate("timeIn");
    }

    private void setTimeIn(Date timeIn) {
        put("timeIn", timeIn);
    }

    private Date getTimeOut() {
        return getDate("timeOut");
    }

    private void setTimeOut(Date timeOut) {
        put("timeOut", timeOut);
    }

    private void setTimeSpentMillis(Date timeIn, Date timeOut) {
        long timeInMillis = timeIn.getTime();
        long timeOutMillis = timeOut.getTime();
        long timeSpent = timeOutMillis - timeInMillis;
        put("timeSpentMillis", timeSpent);
    }

    public int getTimeSpentSeconds() {
        return getInt("timeSpentMillis") / 1000;
    }


    /***********
     * Queries
     ***********/
    public static void getInteractionCountForEvent(String eventId, CountCallback callback) {
        ParseQuery<BeaconInteraction> query = ParseQuery.getQuery(BeaconInteraction.class);
        query.whereEqualTo("eventId", eventId);
        query.countInBackground(callback);
    }

    public static void getInteractionCountForCompany(String eventId, String companyId, CountCallback callback) {
        ParseQuery<BeaconInteraction> query = ParseQuery.getQuery(BeaconInteraction.class);
        query.whereEqualTo("eventId", eventId);
        query.whereEqualTo("companyId", companyId);
        query.countInBackground(callback);
    }

    public static void getInteractionsByEventId(String eventId, final BeaconInteractionsLoadedListener listener) {
        ParseQuery<BeaconInteraction> query = ParseQuery.getQuery(BeaconInteraction.class);
        query.whereEqualTo("eventId", eventId);
        query.findInBackground(new FindCallback<BeaconInteraction>() {

            @Override
            public void done(List<BeaconInteraction> interactions, ParseException e) {
                listener.onInteractionsLoaded(interactions);
            }
        });
    }

    public static void getInteractionForCompany(String eventId, String companyId, final BeaconInteractionsLoadedListener listener) {
        ParseQuery<BeaconInteraction> query = ParseQuery.getQuery(BeaconInteraction.class);
        query.whereEqualTo("eventId", eventId);
        query.whereEqualTo("companyId", companyId);
        query.findInBackground(new FindCallback<BeaconInteraction>() {

            @Override
            public void done(List<BeaconInteraction> interactions, ParseException e) {
                listener.onInteractionsLoaded(interactions);
            }
        });
    }

    public static void getInteractionsBySpeakerId(String speakerId, final BeaconInteractionsLoadedListener listener) {
        ParseQuery<BeaconInteraction> query = ParseQuery.getQuery(BeaconInteraction.class);
        query.whereEqualTo("speakerId", speakerId);
        query.findInBackground(new FindCallback<BeaconInteraction>() {

            @Override
            public void done(List<BeaconInteraction> interactions, ParseException e) {
                listener.onInteractionsLoaded(interactions);
            }
        });
    }


    // added by supra 23 March 16

    public static void getUsersForInteractions(List<String> beaconIds, final BeaconInteractionsLoadedListener listener) {
        ParseQuery<BeaconInteraction> query = ParseQuery.getQuery(BeaconInteraction.class);
        if(beaconIds != null){

            BeConUser user = BeConUser.getCurrentUser();
            String evtId = user.getCurrentEventId();

            query.whereContainedIn("beaconId", beaconIds);
            //query.whereEqualTo("eventId",evtId);
            query.findInBackground(new FindCallback<BeaconInteraction>() {
                @Override
                public void done(List<BeaconInteraction> list, ParseException e) {
                    listener.onInteractionsLoaded(list);
                }
            });
        }else {
            query.findInBackground(new FindCallback<BeaconInteraction>() {
                @Override
                public void done(List<BeaconInteraction> list, ParseException e) {
                    listener.onInteractionsLoaded(null);
                }
            });
        }
    }
}
