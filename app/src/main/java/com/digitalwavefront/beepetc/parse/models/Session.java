package com.digitalwavefront.beepetc.parse.models;

import android.support.annotation.Nullable;
import android.util.Log;

import com.digitalwavefront.beepetc.BeConApp;
import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.digitalwavefront.beepetc.interfaces.BeaconLoadedListener;
import com.digitalwavefront.beepetc.interfaces.BeaconsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.EventLoadedListener;
import com.digitalwavefront.beepetc.interfaces.NotificationsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SessionsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SpeakerLoadedListener;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Wayne on 9/18/15.
 */

/**
 * Updated by Tarul on 10/15/16
 */
@ParseClassName("Session")
public class Session extends ParseObject {
    private Speaker speaker;
    private BeConBeacon beacon;
    private String speakerId;
    private int percent;
    private String spkeakerNames="";

    public Session() {
    }

    public String getTitle() {
        return getString("title");
    }

    public String getDetails() {
        return getString("details");
    }

    public String getRoomName() {
        String room = getString("roomName");
        if (room != null) {
            return room;
        }
        return "";
    }

    public void setSpkeakerNames(String names) { this.spkeakerNames = names;}
    public String getSpkNames() {
        return this.spkeakerNames;
    }

    //################### by Sarika [27/09/2017]############################
    public String getSessSpeakerId() {
        return this.speakerId;
    }
    public void setPercent(int percent) { this.percent = percent;}
    public int getPercent(){ return  this.percent;}
    //######################################

    public void setSpeakerId(String speakerId) { this.speakerId = speakerId;}

    public void setSpeaker(Speaker speaker) { this.speaker = speaker;}
    public Speaker getSpeaker(){ return this.speaker;}

    public static String convertTimeZones(String fromTimeZoneString, String toTimeZoneString, String fromDateTime) {
       /* DateTimeZone fromTimeZone = DateTimeZone.forID(fromTimeZoneString);
        DateTimeZone toTimeZone = DateTimeZone.forID(toTimeZoneString);
        DateTime dateTime = new DateTime(fromDateTime, fromTimeZone);*/

        DateTimeFormatter outputFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSSZ");

        DateTimeZone fromtimeZone = DateTimeZone.forID(fromTimeZoneString);
        DateTimeZone totimeZone = DateTimeZone.forID(toTimeZoneString);

        DateTime utc = DateTime.now(fromtimeZone);
        DateTime converted = utc.withZone(totimeZone);
        //  DateTime nowUtc = utc.withZone( DateTimeZone.UTC );

        return outputFormatter.print(converted);
    }

    public static Date convertTimeZone(Date date, TimeZone fromTimeZone, TimeZone toTimeZone) {
        long fromTimeZoneOffset = getTimeZoneUTCAndDSTOffset(date, fromTimeZone);
        long toTimeZoneOffset = getTimeZoneUTCAndDSTOffset(date, toTimeZone);

        return new Date(date.getTime() + (toTimeZoneOffset - fromTimeZoneOffset));
    }

    private static long getTimeZoneUTCAndDSTOffset(Date date, TimeZone timeZone) {
        long timeZoneDSTOffset = 0;
        if (timeZone.inDaylightTime(date)) {
            timeZoneDSTOffset = timeZone.getDSTSavings();
        }

        return timeZone.getRawOffset() + timeZoneDSTOffset;
    }

    public String getStartTime() {
        String expiry = "";
        Date startDate = getDate("startTime");
        Log.e("StartDateDb", String.valueOf(startDate));
        long epoch = 0;
        try {
            SimpleDateFormat simpleDate = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
            simpleDate.setTimeZone(TimeZone.getTimeZone("UTC"));
            String start = simpleDate.format(startDate);
            Log.e("startdatebeforeepoch", start);
            epoch = simpleDate.parse(start).getTime() / 1000;
            Log.e("datetoepoch", String.valueOf(epoch));
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        //SimpleDateFormat toDate = new SimpleDateFormat("EEE, MMM dd, hh:mm a");
        SimpleDateFormat toDate = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
        if (BeConApp.timeZone != null) {
            toDate.setTimeZone(TimeZone.getTimeZone(BeConApp.timeZone));
        }
        String date = toDate.format(new java.util.Date(epoch * 1000));
        Log.e("epochtodate", date);
        expiry = date;

        return expiry;
    }

    public Date getEndTime() {
        //Log.e("ENDTIME", getDate("endTime").toString());
        return getDate("endTime");
    }


    public String getStartTime1() {
        String expiry = "";
        Date startDate = getDate("startTime");
        Log.e("StartDateDb", String.valueOf(startDate));
        long epoch = 0;
        try {
            SimpleDateFormat simpleDate = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
            simpleDate.setTimeZone(TimeZone.getTimeZone("UTC"));
            String start = simpleDate.format(checkDayLight(startDate));
            Log.e("startdatebeforeepoch", start);
            epoch = simpleDate.parse(start).getTime() / 1000;
            Log.e("datetoepoch", String.valueOf(epoch));
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        //SimpleDateFormat toDate = new SimpleDateFormat("EEE, MMM dd, hh:mm a");
        SimpleDateFormat toDate = new SimpleDateFormat("EEE, MMM dd, hh:mm a");
        toDate.setTimeZone(TimeZone.getTimeZone(BeConApp.timeZone));
        String date = toDate.format(new java.util.Date(epoch * 1000));
        Log.e("epochtodate", date);
        expiry = date;

        return expiry;
    }

    public long getDuration() {

        Date startDate = getDate("startTime");
        Date endDate = getDate("endTime");
        long difference = Math.abs(endDate.getTime() - startDate.getTime());
        long differenceDates = difference / (60 * 1000);

        return differenceDates;
    }

    public int getNumOfDots() {

        int numDots = (int) getDuration()/15;
        return numDots;
    }

    public String getEndTime1() {
        //Log.e("ENDTIME", getDate("endTime").toString());
        String expiry = "";
        Date endDate = getDate("endTime");
        long epoch = 0;
        try {
            SimpleDateFormat simpleDate = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
            simpleDate.setTimeZone(TimeZone.getTimeZone("UTC"));
            String start = simpleDate.format(checkDayLight(endDate));
            Log.e("startdatebeforeepoch", start);
            epoch = simpleDate.parse(start).getTime() / 1000;
            Log.e("datetoepoch", String.valueOf(epoch));
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        //SimpleDateFormat toDate = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat toDate = new SimpleDateFormat("hh:mm a");
        toDate.setTimeZone(TimeZone.getTimeZone(BeConApp.timeZone));
        String date = toDate.format(new java.util.Date(epoch * 1000));
        Log.e("epochtodate", date);
        expiry = date;

        return expiry;
    }

    public String getFormattedTime(String strfield) {
        //Log.e("ENDTIME", getDate("endTime").toString());
        String expiry = "";
        Date endDate = getDate(strfield);
        long epoch = 0;
        try {
            SimpleDateFormat simpleDate = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
            simpleDate.setTimeZone(TimeZone.getTimeZone("UTC"));
            String start = simpleDate.format(checkDayLight(endDate));
            Log.e("startdatebeforeepoch", start);
            epoch = simpleDate.parse(start).getTime() / 1000;
            Log.e("datetoepoch", String.valueOf(epoch));
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        //SimpleDateFormat toDate = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat toDate = new SimpleDateFormat("hh:mm a");
        toDate.setTimeZone(TimeZone.getTimeZone(BeConApp.timeZone));
        String date = toDate.format(new java.util.Date(epoch * 1000));
        Log.e("epochtodate", date);
        expiry = date;

        return expiry;
    }
    public String getBeaconId() {
        return getString("beaconId");
    }


    public void getBeaconAsync(final BeaconLoadedListener beaconLoadedListener) {
        if (beacon == null) {
            BeConBeacon.getBeaconById(getBeaconId(), new BeaconLoadedListener() {

                @Override
                public void onBeaconLoaded(@Nullable BeConBeacon beacon) {
                    setBeacon(beacon);
                    beaconLoadedListener.onBeaconLoaded(beacon);
                }
            });
        } else {
            beaconLoadedListener.onBeaconLoaded(beacon);
        }
    }

    public BeConBeacon getBeacon() {
        return beacon;
    }

    private void setBeacon(BeConBeacon beacon) {
        this.beacon = beacon;
    }

    public String getEventId() {
        return getString("eventId");
    }

    public void getEvent(final EventLoadedListener eventLoadedListener) {
        Event.getEventByObjectId(getEventId(), eventLoadedListener);
    }

    public String getSpeakerId() {
        return getString("speakerId");
    }

    public List<String> getSpeakerIds() {
        return getList("speakerIds");
    }

    public void getSpeaker(final SpeakerLoadedListener speakerLoadedListener) {
        if (speaker == null) {
            Speaker.getSpeakerByObjectId(getSpeakerId(), new SpeakerLoadedListener() {

                @Override
                public void onSpeakerLoaded(@Nullable Speaker speaker) {
                    Session.this.speaker = speaker;
                    speakerLoadedListener.onSpeakerLoaded(speaker);
                }
            });
        } else {
            speakerLoadedListener.onSpeakerLoaded(speaker);
        }
    }

    Date checkDayLight(Date date) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a");
        String timeZone = BeConApp.timeZone;

        String startTime = dateFormat.format(date);

        if (timeZone.equals("EST")) {
            boolean tz = TimeZone.getTimeZone("America/Los_Angeles").inDaylightTime(date);
            Log.e("InDayLight", "" + tz);
            if (tz) {
                Calendar cal = Calendar.getInstance();
                try {
                    cal.setTime(dateFormat.parse(startTime));

                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                cal.add(Calendar.HOUR, 1);
                startTime = dateFormat.format(cal.getTime());
                Log.e("InTZ", startTime);

            }
        }

        if (timeZone.equals("CST")) {

            boolean tz = TimeZone.getTimeZone("America/Los_Angeles").inDaylightTime(date);
            Log.e("InDayLight", "" + tz);

            if (tz) {
                Calendar cal = Calendar.getInstance();
                try {
                    cal.setTime(dateFormat.parse(startTime));
                    cal.add(Calendar.HOUR, -5);
                    startTime = dateFormat.format(cal.getTime());

                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
            } else {
                Calendar cal = Calendar.getInstance();
                try {
                    cal.setTime(dateFormat.parse(startTime));
                    cal.add(Calendar.HOUR, -6);
                    startTime = dateFormat.format(cal.getTime());

                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
            }

        }

        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");

        try {
            date = format.parse(startTime);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        Log.e("Date", "" + date);
        return date;
    }

    public String getTimeRangeString() {

        SimpleDateFormat dateFormat1 = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");

        Date samDate = null;
        String timeZone = BeConApp.timeZone;
        String dt = getStartTime();
        Log.e("getStartTime", getStartTime());
        try {
            samDate = dateFormat1.parse(dt);
            Log.e("SamDate", "" + samDate);

        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));

        String startTime = dateFormat.format(samDate);
        String endTime;
        Log.e("GetEndTime", "" + getEndTime());
        if (getEndTime() != null && !getEndTime().equals(" ")) {
            endTime = dateFormat.format(getEndTime());
        } else {
            endTime = "";
        }


        if (timeZone.equals("EST")) {
            boolean tz = TimeZone.getTimeZone("America/Los_Angeles").inDaylightTime(samDate);
            Log.e("InDayLight", "" + tz);
            if (tz) {
                Calendar cal = Calendar.getInstance();
                Calendar cal1 = Calendar.getInstance();
                try {
                    cal.setTime(dateFormat.parse(startTime));
                    if (!endTime.equals("")) {
                        cal1.setTime(dateFormat.parse(endTime));
                        cal1.add(Calendar.HOUR, 1);
                        endTime = dateFormat.format(cal1.getTime());
                        Log.e("EndTime", endTime);
                    }

                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
                cal.add(Calendar.HOUR, 1);
                startTime = dateFormat.format(cal.getTime());
                Log.e("InTZ", startTime);

            }
        }

        if (timeZone.equals("CST")) {

            boolean tz = TimeZone.getTimeZone("America/Los_Angeles").inDaylightTime(samDate);
            Log.e("InDayLight", "" + tz);

            if (tz) {
                Calendar cal = Calendar.getInstance();
                try {
                    cal.setTime(dateFormat.parse(startTime));
                    cal.add(Calendar.HOUR, -5);
                    startTime = dateFormat.format(cal.getTime());
                    if (!endTime.equals("")) {
                        cal.setTime(dateFormat.parse(endTime));
                        cal.add(Calendar.HOUR, -5);
                        endTime = dateFormat.format(cal.getTime());
                    }

                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
            } else {
                Calendar cal = Calendar.getInstance();
                try {
                    cal.setTime(dateFormat.parse(startTime));
                    cal.add(Calendar.HOUR, -6);
                    startTime = dateFormat.format(cal.getTime());
                    if (!endTime.equals("")) {
                        cal.setTime(dateFormat.parse(endTime));
                        cal.add(Calendar.HOUR, -6);
                        endTime = dateFormat.format(cal.getTime());
                    }

                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
            }

        }

        Log.e("StartTime", startTime);
        Log.e("EndTime", endTime);
        return String.format("%s\n%s", startTime, endTime);
    }

    public String getSpeakerTimeRangeString() {
        String startTime = getStartTime1();// dateFormat.format(getStartTime());
        //String endTime = dateFormat.format(getEndTime());
        String endTime = getEndTime1();
        /*String startTime = dateFormat1.format(getStartTime());
        String endTime = dateFormat.format(getEndTime());*/

        return String.format("%s\t-\t%s", startTime, endTime);
    }


    public String FormattedStartDate() {
        Date startDate = null;
        SimpleDateFormat returnDate = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
        returnDate.setTimeZone(TimeZone.getTimeZone(BeConApp.timeZone));
        try {
            //toDate.setTimeZone(TimeZone.getTimeZone(BeConApp.timeZone));
            startDate = returnDate.parse(getStartTime());
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        //   Date startDate = getStartTime();
        //  Log.e("Praneet","FormattedStartDate"+startDate);
        SimpleDateFormat formatter = new SimpleDateFormat("d MMM");
        formatter.setTimeZone(TimeZone.getTimeZone(BeConApp.timeZone));
        String outDate = formatter.format(startDate);
        Log.e("outDate", outDate);
        return outDate; //formatter.format(getStartTime());
    }


    /*********
     * Queries
     *********/
    public static void getSessionsByEventId(String eventId, final SessionsLoadedListener sessionsLoadedListener, final boolean fetchData) {
        ParseQuery<Session> query = ParseQuery.getQuery(Session.class);
        query.whereEqualTo("eventId", eventId);
        query.orderByAscending("startTime");

        query.findInBackground((FindCallback<Session>) new FindCallback<Session>() {

            @Override
            public void done(final List<Session> sessions, ParseException e) {

                if (!fetchData) {
                    sessionsLoadedListener.onSessionsLoaded(sessions);
                } else {
                    if (sessions != null && !sessions.isEmpty()) {

                        // Fetch the beacons for the sessions
                        BeConBeacon.getBeaconsForSessions(sessions, new BeaconsLoadedListener() {

                            @Override
                            public void onBeaconsLoaded(@Nullable final List<BeConBeacon> beacons) {
                                if (beacons != null) {

                                    // Assign each beacon to it's respective session
                                    for (Session session : sessions) {

                                        for (BeConBeacon beacon : beacons) {

                                            if (beacon.belongsToSession(session)) {
                                                session.setBeacon(beacon);
                                            }
                                        }
                                    }
                                    // Fetch notifications for session
                                    BeConNotification.getNotificationsForBeacons(beacons, new NotificationsLoadedListener() {

                                        @Override
                                        public void onNotificationsLoaded(@Nullable List<BeConNotification> notifications) {
                                            if (notifications != null) {

                                                // Assign each notification to it's respective beacon
                                                for (BeConBeacon beacon : beacons) {

                                                    for (BeConNotification notification : notifications) {

                                                        if (notification.belongsToBeacon(beacon)) {
                                                            beacon.addNotification(notification);
                                                        }
                                                    }
                                                }
                                            }

                                            sessionsLoadedListener.onSessionsLoaded(sessions);
                                        }
                                    });
                                }
                            }
                        });
                    } else {
                        sessionsLoadedListener.onSessionsLoaded(null);
                    }
                }
            }
        });
    }

    public static void getSessionsByEventId(String eventId, final SessionsLoadedListener sessionsLoadedListener, final boolean fetchData, int limit) {
        ParseQuery<Session> query = ParseQuery.getQuery(Session.class);
        query.whereEqualTo("eventId", eventId);
        query.orderByAscending("startTime");
        query.setLimit(limit);

        query.findInBackground((FindCallback<Session>) new FindCallback<Session>() {

            @Override
            public void done(final List<Session> sessions, ParseException e) {

                if (!fetchData) {
                    sessionsLoadedListener.onSessionsLoaded(sessions);
                } else {
                    if (sessions != null && !sessions.isEmpty()) {

                        // Fetch the beacons for the sessions
                        BeConBeacon.getBeaconsForSessions(sessions, new BeaconsLoadedListener() {

                            @Override
                            public void onBeaconsLoaded(@Nullable final List<BeConBeacon> beacons) {
                                if (beacons != null) {

                                    // Assign each beacon to it's respective session
                                    for (Session session : sessions) {

                                        for (BeConBeacon beacon : beacons) {

                                            if (beacon.belongsToSession(session)) {
                                                session.setBeacon(beacon);
                                            }
                                        }
                                    }
                                    // Fetch notifications for session
                                    BeConNotification.getNotificationsForBeacons(beacons, new NotificationsLoadedListener() {

                                        @Override
                                        public void onNotificationsLoaded(@Nullable List<BeConNotification> notifications) {
                                            if (notifications != null) {

                                                // Assign each notification to it's respective beacon
                                                for (BeConBeacon beacon : beacons) {

                                                    for (BeConNotification notification : notifications) {

                                                        if (notification.belongsToBeacon(beacon)) {
                                                            beacon.addNotification(notification);
                                                        }
                                                    }
                                                }
                                            }

                                            sessionsLoadedListener.onSessionsLoaded(sessions);
                                        }
                                    });
                                }
                            }
                        });
                    } else {
                        sessionsLoadedListener.onSessionsLoaded(null);
                    }
                }
            }
        });
    }

    //getSessionBySpeakerWithBlock
    public static void getSessionsBySpeakerId(String speakerId, final SessionsLoadedListener sessionsLoadedListener) {
        ParseQuery<Session> query = ParseQuery.getQuery(Session.class);
        query.whereEqualTo("speakerId", speakerId);
        query.findInBackground((FindCallback<Session>) new FindCallback<Session>() {
            @Override
            public void done(List<Session> sessions, ParseException e) {
                if (sessions != null && !sessions.isEmpty()) {
                    sessionsLoadedListener.onSessionsLoaded(sessions);
                } else {
                    sessionsLoadedListener.onSessionsLoaded(null);
                }
            }
        });
    }

    public static void getSessionsBySpeakerIds(String speakerId, final SessionsLoadedListener sessionsLoadedListener) {
        ParseQuery<Session> query = ParseQuery.getQuery(Session.class);
        query.whereEqualTo("speakerIds", speakerId);
        //query.whereContains("speakerIds", speakerId);
        Log.e("SessionSpeakerIds", speakerId);
        query.findInBackground((FindCallback<Session>) new FindCallback<Session>() {
            @Override
            public void done(List<Session> sessions, ParseException e) {
                if (sessions != null && !sessions.isEmpty()) {
                    Log.e("S NotNull", sessions.toString());
                    sessionsLoadedListener.onSessionsLoaded(sessions);
                } else {
                    Log.e("S Null", "Session Null");
                    sessionsLoadedListener.onSessionsLoaded(null);
                }
            }
        });
    }



    public static void getSessionsByObjectIds(String eventId, List<String> sessionIds, final SessionsLoadedListener listener) {
        ParseQuery<Session> query = ParseQuery.getQuery(Session.class);
        query.whereContainedIn("objectId", sessionIds);
        query.whereEqualTo("eventId", eventId);
        query.findInBackground((FindCallback<Session>) new FindCallback<Session>() {
            @Override
            public void done(List<Session> sessions, ParseException e) {
                listener.onSessionsLoaded(sessions);
            }
        });
    }
}