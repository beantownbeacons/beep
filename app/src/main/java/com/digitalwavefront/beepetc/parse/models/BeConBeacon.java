package com.digitalwavefront.beepetc.parse.models;

import com.digitalwavefront.beepetc.interfaces.BeaconLoadedListener;
import com.digitalwavefront.beepetc.interfaces.BeaconsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.NotificationsLoadedListener;
import com.estimote.sdk.Region;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by Wayne on 9/27/15.
 */
@ParseClassName("Beacon")
public class BeConBeacon extends ParseObject {

    private HashMap<Integer, List<BeConNotification>> notificationMap;
    private boolean triggerBeacon;

    public BeConBeacon() {
    }

    public String getName() {
        return getString("name");
    }

    public UUID getUUID() {
        String uuid = getString("uuid");
        if (uuid != null)
        return UUID.fromString(uuid);
        return null;
    }

    public String getEventId() {
        return getString("eventId");
    }

    public int getMajor() {
        return getInt("major");
    }

    public int getMinor() {
        return getInt("minor");
    }

    public String getCompanyId() {
        if (getString("companyId") == null) {
            return " ";
        }else{
            return getString("companyId");
        }
    }


    private String getRegionIdentifier() {
        return getObjectId();
    }

    public Region getRegion() {
        return new Region(getRegionIdentifier(), getUUID(), getMajor(), getMinor());
    }

    public Region getRegionByUUID() {
        return new Region(getRegionIdentifier(), getUUID(), null, null);
    }

    public boolean isTriggerBeacon() {
        return triggerBeacon;
    }

    public void setIsTriggerBeacon(boolean flag) {
        triggerBeacon = flag;
    }

    public boolean ownsRegion(Region region) {
        return getRegionIdentifier().equals(region.getIdentifier());
    }

    public boolean belongsToSession(Session session) {
        return session.getBeaconId().equals(getObjectId());
    }

    public void addNotification(BeConNotification notification) {
        int triggerTime = notification.getTriggerTimeInSeconds();

        if (notificationMap == null) {
            notificationMap = new HashMap<>();
        }

        if (!notificationMap.containsKey(triggerTime)) {
            notificationMap.put(triggerTime, new ArrayList<BeConNotification>());
        }

        notificationMap.get(triggerTime).add(notification);
    }

    public List<BeConNotification> getNotificationsForTriggerTime(int triggerTime) {
        if (notificationMap == null || !notificationMap.containsKey(triggerTime)) {
            return null;
        }
        return notificationMap.get(triggerTime);
    }


    /*********
     * Queries
     *********/
    public static void getBeaconsByEventId(String eventId, final BeaconsLoadedListener beaconsLoadedListener) {
        ParseQuery<BeConBeacon> query = ParseQuery.getQuery(BeConBeacon.class);
        query.whereEqualTo("eventId", eventId);
        query.findInBackground(new FindCallback<BeConBeacon>() {

            @Override
            public void done(final List<BeConBeacon> beacons, ParseException e) {
                //beaconsLoadedListener.onBeaconsLoaded(beacons);

                // ######### added by Supra 2 April 16 #########
                // Fetch notifications for beacon
                if (beacons != null) {
                    BeConNotification.getNotificationsForBeacons(beacons, new NotificationsLoadedListener() {

                        @Override
                        public void onNotificationsLoaded(List<BeConNotification> notifications) {
                            if (notifications != null) {

                                // Assign each notification to it's respective beacon
                                for (BeConBeacon beacon : beacons) {

                                    for (BeConNotification notification : notifications) {

                                        if (notification.belongsToBeacon(beacon)) {
                                            beacon.addNotification(notification);
                                        }
                                    }
                                }
                            }

                            beaconsLoadedListener.onBeaconsLoaded(beacons);

                        }
                    });
                } else {

                    beaconsLoadedListener.onBeaconsLoaded(null);
                }
            }
            //######### END #########


        });
    }

    public static void getBeaconById(String beaconId, final BeaconLoadedListener beaconLoadedListener) {
        ParseQuery<BeConBeacon> query = ParseQuery.getQuery(BeConBeacon.class);
        query.getInBackground(beaconId, new GetCallback<BeConBeacon>() {
            @Override
            public void done(BeConBeacon beacon, ParseException e) {
                beaconLoadedListener.onBeaconLoaded(beacon);
            }
        });
    }

    public static void getBeaconsByIds(List<String> beaconIds, final BeaconsLoadedListener beaconsLoadedListener) {
        ParseQuery<BeConBeacon> query = ParseQuery.getQuery(BeConBeacon.class);
        query.whereContainedIn("objectId", beaconIds);
        query.findInBackground(new FindCallback<BeConBeacon>() {
            @Override
            public void done(List<BeConBeacon> beacons, ParseException e) {
                beaconsLoadedListener.onBeaconsLoaded(beacons);
            }
        });
    }

    public static void getBeaconsForSessions(List<Session> sessions, final BeaconsLoadedListener beaconsLoadedListener) {
        if (!sessions.isEmpty()) {
            ParseQuery<BeConBeacon> query = ParseQuery.getQuery(BeConBeacon.class);

            // Load the beacon ids for the query from the sessions
            List<String> beaconIds = new ArrayList<>();
            for (Session session : sessions) {

                // Each one should be unique
                beaconIds.add(session.getBeaconId());
            }
            query.whereContainedIn("objectId", beaconIds);
            query.findInBackground(new FindCallback<BeConBeacon>() {

                @Override
                public void done(List<BeConBeacon> beacons, ParseException e) {
                    beaconsLoadedListener.onBeaconsLoaded(beacons);
                }
            });
        } else {
            beaconsLoadedListener.onBeaconsLoaded(null);
        }
    }
}
