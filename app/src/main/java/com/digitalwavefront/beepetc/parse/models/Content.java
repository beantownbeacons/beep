package com.digitalwavefront.beepetc.parse.models;

import android.content.Context;
import android.os.Environment;

import com.digitalwavefront.beepetc.interfaces.FileLoadedListener;
import com.digitalwavefront.beepetc.utils.PdfUtils;
import com.parse.GetDataCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.io.File;
import java.io.IOException;

/**
 * Created by Tarul on 08/09/2016.
 */
@ParseClassName("Content")
public class Content extends ParseObject {

    public Content() {}

    private String getBeaconId() {
        return getString("beaconId");
    }

    //public int getTriggerTimeInSeconds() { return getInt("triggerTimeInSecs"); }

    public String getTitle() {
        return getString("title");
    }

    public String getMessage() {
        return getString("description");
    }

    public ParseFile getPdfFile() { return getParseFile("pdfFile"); }

    public String getUrl() { return getString("url"); }

    public void getPdf(final Context context, final FileLoadedListener fileLoadedListener) {
        final String pdfFilePath = getPdfFilePath(context);
        File pdfFile = new File(pdfFilePath);

        if (!pdfFile.exists()) {
            if (has("pdfFile")) {

                // Check if the pdf file has been saved to device
                ParseFile pdfParseFile = getParseFile("pdfFile");
                pdfParseFile.getDataInBackground(new GetDataCallback() {

                    @Override
                    public void done(byte[] pdfData, ParseException e) {
                        try {
                            PdfUtils.savePdfFromByteArray(pdfFilePath, pdfData);
                            fileLoadedListener.onFileLoaded(new File(pdfFilePath));
                        } catch (IOException e1) {
                            e1.printStackTrace();
                            fileLoadedListener.onFileLoaded(null);
                        }
                    }
                });
            } else {
                fileLoadedListener.onFileLoaded(null);
            }
        } else {
            fileLoadedListener.onFileLoaded(pdfFile);
        }
    }

    public String getPdfFilePath(Context context) {
        try{
            String filesDirectoryPath = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS ).getAbsolutePath();
            String pdfFileName = String.format("%s.pdf", getObjectId());
            return String.format("%s/%s", filesDirectoryPath, pdfFileName);
        }catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }


}
