package com.digitalwavefront.beepetc.parse.models;

import com.digitalwavefront.beepetc.interfaces.ReceivedNotificationsLoadedListener;
import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by Wayne on 10/13/15.
 */
@ParseClassName("ReceivedNotification")
public class ReceivedNotification extends ParseObject {

    public ReceivedNotification() {}

    public String getStatus() { return getString("status");}

    public static ReceivedNotification newInstance(BeConUser user, BeConNotification notification, String companyId) {
        ReceivedNotification receivedNotification = new ReceivedNotification();
        receivedNotification.setUser(user);
        receivedNotification.setNotification(notification);
        receivedNotification.setCompanyId(companyId);
        return receivedNotification;
    }

    private void setUser(BeConUser user) {
        put("userId", user.getObjectId());
        put("eventId", user.getCurrentEventId());
    }

    private void setNotification(BeConNotification notification) {
        put("notificationId", notification.getObjectId());
    }

    public String getNotificationId () {

        return getString("notificationId");
    }
    public void setStatus(String notifStat) {
        put("status", notifStat);
    }

    private void setCompanyId(String companyId) {
         put("companyId", companyId);
    }

    /*********
     * Queries
     *********/
    public static void getReceivedNotifByStatus(String status, final ReceivedNotificationsLoadedListener notifLoadedListener) {
        ParseQuery<ReceivedNotification> query = ParseQuery.getQuery(ReceivedNotification.class);
        query.whereEqualTo("status",status);
        query.findInBackground(new FindCallback<ReceivedNotification>() {
            @Override
            public void done(List<ReceivedNotification> objects, ParseException e) {
                if (e == null) {
                    if (objects != null) {
                        notifLoadedListener.onReceivedNotificationsLoaded(objects);
                    } else {
                        notifLoadedListener.onReceivedNotificationsLoaded(null);
                    }
                } else {
                    notifLoadedListener.onReceivedNotificationsLoaded(null);
                }
            }
        });
    }

    public static void getReceivedNotifByUserIdNotifId(String userId, String notifId, final ReceivedNotificationsLoadedListener notifLoadedListener) {
        ParseQuery<ReceivedNotification> query = ParseQuery.getQuery(ReceivedNotification.class);
        query.whereEqualTo("userId",userId);
        query.whereEqualTo("notificationId",notifId);
        query.findInBackground(new FindCallback<ReceivedNotification>() {
            @Override
            public void done(List<ReceivedNotification> objects, ParseException e) {
                if (e == null) {
                    if (objects != null) {
                        notifLoadedListener.onReceivedNotificationsLoaded(objects);
                    } else {
                        notifLoadedListener.onReceivedNotificationsLoaded(null);
                    }
                } else {
                    notifLoadedListener.onReceivedNotificationsLoaded(null);
                }
            }
        });
    }

    public static void getReceivedNotifByUserIdEvtIdNotifIds(String userId, String eventId, List <String> notifIds, final ReceivedNotificationsLoadedListener notifLoadedListener) {
        ParseQuery<ReceivedNotification> query = ParseQuery.getQuery(ReceivedNotification.class);
        query.whereEqualTo("userId",userId);
        query.whereEqualTo("eventId",eventId);
        query.whereContainedIn("notificationId",notifIds);
        query.findInBackground(new FindCallback<ReceivedNotification>() {
            @Override
            public void done(List<ReceivedNotification> objects, ParseException e) {
                if (e == null) {
                    if (objects != null) {
                        notifLoadedListener.onReceivedNotificationsLoaded(objects);
                    } else {
                        notifLoadedListener.onReceivedNotificationsLoaded(null);
                    }
                } else {
                    notifLoadedListener.onReceivedNotificationsLoaded(null);
                }
            }
        });
    }
}
