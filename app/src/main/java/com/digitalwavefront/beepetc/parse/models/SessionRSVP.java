package com.digitalwavefront.beepetc.parse.models;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by Wayne on 10/13/15.
 */
@ParseClassName("SessionRSVP")
public class SessionRSVP extends ParseObject {

    public SessionRSVP() {}

    public static SessionRSVP newInstance(BeConUser beConUser, Session session) {
        SessionRSVP rsvp = new SessionRSVP();
        rsvp.setUser(beConUser);
        rsvp.setSession(session);
        return rsvp;
    }

    private void setUser(BeConUser user) {
        put("userId", user.getObjectId());
    }

    private void setSession(Session session) {
        put("sessionId", session.getObjectId());
        if(session.getSpeakerId() != null) {
            put("speakerId", session.getSpeakerId());
        }
    }

    /****************
     * Queries
     ****************/
    public static void deleteRSVPBySessionAndUser(Session session, BeConUser user, final DeleteCallback callback) {
        ParseQuery<SessionRSVP> query = ParseQuery.getQuery(SessionRSVP.class);
        query.whereEqualTo("sessionId", session.getObjectId());
        query.whereEqualTo("userId", user.getObjectId());
        query.findInBackground(new FindCallback<SessionRSVP>() {

            public void done(List<SessionRSVP> rsvps, ParseException e) {
                ParseObject.deleteAllInBackground(rsvps, callback);
            }
        });
    }
}
