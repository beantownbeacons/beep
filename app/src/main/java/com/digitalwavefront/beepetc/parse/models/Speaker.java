package com.digitalwavefront.beepetc.parse.models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.util.Log;

import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.digitalwavefront.beepetc.interfaces.BeaconInteractionsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.BitmapLoadedListener;
import com.digitalwavefront.beepetc.interfaces.FloatLoadedCallback;
import com.digitalwavefront.beepetc.interfaces.IntegerLoadedCallback;
import com.digitalwavefront.beepetc.interfaces.SessionsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SpeakerLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SpeakersLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SurveyResponseLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SurveysLoadedListener;
import com.digitalwavefront.beepetc.utils.StringUtils;

//import org.apache.commons.validator.routines.UrlValidator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Wayne on 9/18/15.
 */
@ParseClassName("Speaker")
public class  Speaker extends ParseObject {

    private static final float INVALID_AVERAGE_SCORE = -1f;
    private Bitmap avatar;
    private float averageScore = INVALID_AVERAGE_SCORE;

    public Speaker() {}

    public String getFirstName() {

        String spkname = getString("firstName");

        if(!StringUtils.isNullOrEmpty(spkname)){
            return spkname;
        }
        return "";
    }

    public String getLastName() {
        String spkname = getString("lastName");
        if(!StringUtils.isNullOrEmpty(spkname)){
            return spkname;
        }
        return "";
    }

    public String getFullName() {
        return String.format("%s %s", getFirstName(), getLastName());
    }

    public String getAbout() {
        return getString("about");
    }

    public String getLinkedInURL() {
        return getString("linkedInURL");
    }

    public boolean hasLinkedIn() {
        return getLinkedInURL() != null;
    }

    public String getTwitterURL() {
        return getString("twitterURL");
    }

    public String getCompany(){ return getString("company"); }

    public String getTitle(){ return getString("title"); }

    public boolean hasTwitter() {
       // UrlValidator urlValidator = StringUtils.getDefaultUrlValidator();
        //return getTwitterURL() != null && urlValidator.isValid(getTwitterURL());
        return getTwitterURL() !=null && !getTwitterURL().equals("");
    }

    public void getAvatar(final BitmapLoadedListener bitmapLoadedListener) {
        if (avatar == null) {
            try {
                ParseFile avatarFile = getParseFile("avatar");
                if (avatarFile != null) {
                    avatarFile.getDataInBackground(new GetDataCallback() {

                        @Override
                        public void done(byte[] bytes, ParseException e) {
                            if (bytes != null) {
                                Bitmap avatar = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                Speaker.this.avatar = avatar;
                                bitmapLoadedListener.onBitmapLoaded(avatar);
                            }
                        }
                    });
                } else {
                    bitmapLoadedListener.onBitmapLoaded(null);
                }
            }catch (Exception e) {
                bitmapLoadedListener.onBitmapLoaded(null);
            }
        } else {
            bitmapLoadedListener.onBitmapLoaded(avatar);
        }
    }

    public void getAttendeeCount(final IntegerLoadedCallback callback) {
        ParseQuery<SessionRSVP> query = ParseQuery.getQuery(SessionRSVP.class);
        query.whereEqualTo("speakerId", getObjectId());
        query.countInBackground(new CountCallback() {

            @Override
            public void done(int count, ParseException e) {
                if (e != null) {
                    // TODO: Handle error
                    callback.onIntegerLoaded(0);
                } else {
                    callback.onIntegerLoaded(count);
                }
            }
        });
    }

    public void getAverageUserSessionTimeInSeconds(final FloatLoadedCallback callback) {
        BeaconInteraction.getInteractionsBySpeakerId(getObjectId(), new BeaconInteractionsLoadedListener() {

            @Override
            public void onInteractionsLoaded(@Nullable List<BeaconInteraction> interactions) {
                if (interactions == null || interactions.isEmpty()) {
                    callback.onFloatLoaded(0);
                } else {
                    float total = 0f;

                    for (BeaconInteraction interaction : interactions) {
                        total += interaction.getTimeSpentSeconds();
                    }

                    float averageSeconds = total / interactions.size();
                    callback.onFloatLoaded(averageSeconds);
                }
            }
        });
    }

    public void getSurveyCompleteCount(final IntegerLoadedCallback callback) {
        Survey.getSurveysBySpeaker(this, new SurveysLoadedListener() {

            @Override
            public void onSurveysLoaded(@Nullable List<Survey> surveys) {
                if (surveys == null || surveys.isEmpty()) {
                    callback.onIntegerLoaded(0);
                } else {

                    int completeCount = 0;
                    for (Survey survey : surveys) {
                        completeCount += survey.getCompleteCount();
                    }

                    callback.onIntegerLoaded(completeCount);
                }
            }
        });
    }

    public boolean hasCalculatedAverageScore() {
        return averageScore != INVALID_AVERAGE_SCORE;
    }

    public float getAverageScore() {
        return averageScore;
    }

    private void setAverageScore(float averageScore) {
        this.averageScore = averageScore;
    }

    public void calculateAverageScore(final FloatLoadedCallback callback) {
        SurveyResponse.getSurveyResponseBySpeaker(this, new SurveyResponseLoadedListener() {

            @Override
            public void onSurveyResponsesLoaded(@Nullable List<SurveyResponse> responses) {
                float averageScore;
                if (responses == null || responses.isEmpty()) {
                    averageScore = 0f;
                } else {
                    float total = 0f;
                    for (SurveyResponse response : responses) {
                        total += response.getRating();
                    }

                    averageScore = total / responses.size();
                }

                // Set the local value
                setAverageScore(averageScore);

                if (callback != null) {
                    callback.onFloatLoaded(averageScore);
                }
            }
        });
    }

    /*********
     * Queries
     *********/
    public static void getSpeakerByObjectId(String objectId, final SpeakerLoadedListener speakerLoadedListener) {
        ParseQuery<Speaker> query = ParseQuery.getQuery(Speaker.class);

        query.getInBackground(objectId, new GetCallback<Speaker>() {

            @Override
            public void done(Speaker speaker, ParseException e) {
                speakerLoadedListener.onSpeakerLoaded(speaker);
            }
        });
    }

    // ######### by Sarika ########
    // fetch speaker photo
    public static void getSpeakerPhoto(String objectId, final SpeakerLoadedListener speakerLoadedListener) {
        ParseQuery<Speaker> query = ParseQuery.getQuery(Speaker.class);
        query.whereEqualTo("objectId",objectId);
        query.selectKeys(Arrays.asList("avatar"));
        query.findInBackground(new FindCallback<Speaker>() {
            @Override
            public void done(List<Speaker> objects, ParseException e) {

                if (e == null) {
                    if (objects != null && objects.size() > 0) {
                        speakerLoadedListener.onSpeakerLoaded(objects.get(0));
                    }else {
                        speakerLoadedListener.onSpeakerLoaded(null);
                    }
                } else {
                    speakerLoadedListener.onSpeakerLoaded(null);
                }
            }
        });
    }

    public static void getSpeakerField(String objectId, ArrayList<String> fieldnameList, final SpeakerLoadedListener speakerLoadedListener) {
        ParseQuery<Speaker> query = ParseQuery.getQuery(Speaker.class);
        query.whereEqualTo("objectId",objectId);
        query.selectKeys(fieldnameList);
        /*
        try {
            List<Speaker> objects = query.find();
            speakerLoadedListener.onSpeakerLoaded(objects.get(0));
        }catch (ParseException e) {
            speakerLoadedListener.onSpeakerLoaded(null);
        }*/

            query.findInBackground(new FindCallback<Speaker>() {
            @Override
            public void done(List<Speaker> objects, ParseException e) {

                if (e == null) {
                    speakerLoadedListener.onSpeakerLoaded(objects.get(0));
                } else {
                    speakerLoadedListener.onSpeakerLoaded(null);
                }
            }
        });
    }

    // ######### END #########
    public static void getSpeakersByEventId(String eventId, final SpeakersLoadedListener listener) {
        Session.getSessionsByEventId(eventId, new SessionsLoadedListener() {

            @Override
            public void onSessionsLoaded(@Nullable List<Session> sessions) {
                if (sessions != null) {
                    final List<String> speakerIds = new ArrayList<>();
                    for (Session session : sessions) {
                        speakerIds.add(session.getSpeakerId());
                    }

                    Speaker.getSpeakersByObjectIds(speakerIds, new SpeakersLoadedListener() {

                        @Override
                        public void onSpeakersLoaded(@Nullable List<Speaker> speakers) {
                            listener.onSpeakersLoaded(speakers);
                        }
                    });
                } else {
                    listener.onSpeakersLoaded(null);
                }
            }
        }, false);
    }

    public static void getSpeakersByObjectIds(List<String> objectIds, final SpeakersLoadedListener speakersLoadedListener) {
        ParseQuery<Speaker> query = ParseQuery.getQuery(Speaker.class);
        query.whereContainedIn("objectId", objectIds);
        query.findInBackground(new FindCallback<Speaker>() {

            @Override
            public void done(List<Speaker> list, ParseException e) {
                speakersLoadedListener.onSpeakersLoaded(list);
            }
        });
    }

    public static void getSpeakersByEventIdNew(String eventId, final SpeakersLoadedListener speakersLoadedListener) {
        ParseQuery<Speaker> query = ParseQuery.getQuery(Speaker.class);
        query.whereEqualTo("eventId", eventId);
        query.addAscendingOrder("firstName");
        //query.fromLocalDatastore();
        query.setCachePolicy(ParseQuery.CachePolicy.CACHE_THEN_NETWORK);
        query.findInBackground(new FindCallback<Speaker>() {
            @Override
            public void done(List<Speaker> list, ParseException e) {

                if (e == null) {
                    if (list == null) {
                        speakersLoadedListener.onSpeakersLoaded(null);
                    } else {
                        speakersLoadedListener.onSpeakersLoaded(list);
                    }
                } else {
                    Log.d("Exception :",""+e.getLocalizedMessage());
                    System.out.println("Exception: "+e.getLocalizedMessage());
                        speakersLoadedListener.onSpeakersLoaded(null);
                }
            }
        });

    }

    // get cached results

    public static void getSpeakersLoc () {

        //ParseQuery<ParseObject> qry = ParseQuery.getQuery("Speakers");
        ParseQuery<Speaker> qry = ParseQuery.getQuery(Speaker.class);
        qry.fromLocalDatastore();
        qry.findInBackground(new FindCallback<Speaker>() {
            @Override
            public void done(List<Speaker> list, ParseException e) {


            }
        });

    }
}
