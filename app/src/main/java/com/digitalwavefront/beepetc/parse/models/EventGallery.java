package com.digitalwavefront.beepetc.parse.models;

import android.graphics.Bitmap;
import android.util.Log;

import com.digitalwavefront.beepetc.interfaces.GalleryLoadedListener;
import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by Praneet on 01/07/2016.
 */
@ParseClassName("EventGallery")
public class EventGallery extends ParseObject {

    public Bitmap bitmap;
    public Bitmap largebitmap;

    public EventGallery() {}



    public String chkNull(String str)
    {
        if(str == null || str.equals(null))
        { return "";} return str;
    }

    public String getUserId(){
        return getString("userId");
    }

   /* public String getObjectId()
    {
        return getString("objectId");
    }*/

    public String getEventId(){
        return getString("eventId");
    }

    public int getLikeCount()
    {
        return getInt("likeCount");
    }

    public String getUploaderName(){
        return getString("uploaderName");

    }

    public String getImageUrl(){

        ParseFile parseFile=getParseFile("image");
        return parseFile.getUrl();

    }

    public String getImageThumbnailUrl(){

        ParseFile parseFile=getParseFile("imageThumbnail");
        return parseFile.getUrl();

    }


    public void setUserId(String userId )
    {
        put("userId",userId);
    }
    public void setEventId(String eventId )
    {
        put("eventId",eventId);
    }
    public void setLikeCount(int likeCount )
    {
        put("likeCount",likeCount);
    }
    public void setUploaderName(String uploaderName )
    {
        put("uploaderName",uploaderName);
    }

    public void setImage(ParseFile image )
    {
        put("image",image);
    }
    public void setImageThumbnail(ParseFile imageThumbnail )
    {
        put("imageThumbnail",imageThumbnail);
    }







    /*********
     * Queries
     *********/

    public static void getImagesByEventId(String eventId,final GalleryLoadedListener galleryLoadedListener)
    {
        Log.e("Praneet","in getImagesByEventId Class name"+EventGallery.class);
        Log.e("Praneet","in getImagesByEventId event Id"+eventId);

        ParseQuery<EventGallery> query = ParseQuery.getQuery(EventGallery.class);


        query.whereEqualTo("eventId", eventId);
        query.orderByDescending("createdAt");
        query.findInBackground(new FindCallback<EventGallery>() {
            @Override
            public void done(List<EventGallery> gallery, ParseException e) {
                Log.e("Praneet","inFind in background"+e);
                galleryLoadedListener.onGalleryLoaded(gallery);
            }
        });


    }

}
