package com.digitalwavefront.beepetc.parse.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by User on 11/22/17.
 */
@ParseClassName("AttendeeChat")
public class AttendeeChat extends ParseObject {

    public void setSenderUserId(String senderId) {
        put("senderUserId", senderId);
    }
    public void setReceiverUserId(String receiverId) {
        put("receiverUserId", receiverId);
    }
    public void setStatus(boolean status) {
        put("status", status);
    }

    public String getSenderUserId() { return getString("senderUserId");}
    public String getReceiverUserId() { return getString("receiverUserId");}
    public String getStatus() { return getString("status");}


}
