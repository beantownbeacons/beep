package com.digitalwavefront.beepetc.parse.models;

import android.support.annotation.Nullable;

import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.digitalwavefront.beepetc.interfaces.SurveyQuestionsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.SurveyResponseLoadedListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wayne on 10/12/15.
 */
@ParseClassName("SurveyQuestion")
public class SurveyQuestion extends ParseObject {

    private SurveyResponse response;

    public SurveyQuestion() {}

    public String getQuestion() {
        return getString("question");
    }

    public int getPosition() {
        return getInt("position");
    }

    public boolean hasResponse() {
        return response != null;
    }

    public void setResponse(SurveyResponse response) {
        this.response = response;
    }

    public SurveyResponse getResponse() {
        return response;
    }

    public String getSpeakerId() {
        return getString("speakerId");
    }




    /*********
     * Queries
     **********/

    /*
    public static void getSurveyQuestionsBySurveyId(String surveyId, final SurveyQuestionsLoadedListener listener,
                                                    final boolean fetchResponses) {
        final ParseQuery<SurveyQuestion> query = ParseQuery.getQuery(SurveyQuestion.class);
        query.whereEqualTo("surveyId", surveyId);
        query.orderByAscending("position");
        query.findInBackground(new FindCallback<SurveyQuestion>() {

            @Override
            public void done(final List<SurveyQuestion> questions, ParseException e) {

                if (!fetchResponses || (questions == null)) {
                    listener.onSurveyQuestionsLoaded(questions);
                } else {

                    // Get the question ids
                    List<String> questionIds = new ArrayList<>();
                    for (SurveyQuestion question : questions) {
                        questionIds.add(question.getObjectId());
                    }

                    SurveyResponse.getSurveyResponsesByQuestionIds(questionIds, new SurveyResponseLoadedListener() {

                        @Override
                        public void onSurveyResponsesLoaded(@Nullable List<SurveyResponse> responses) {
                            if (responses != null) {

                                // Assign the responses to their corresponding question
                                for (SurveyQuestion question : questions) {
                                    for (SurveyResponse response : responses) {
                                        if (response.belongsToQuestion(question)) {
                                            question.setResponse(response);
                                        }
                                    }
                                }
                            }

                            listener.onSurveyQuestionsLoaded(questions);
                        }
                    });
                }
            }
        });
    }
    */
    public static void getSurveyQuestionsByEventId(String eventId, final String speakerId, final SurveyQuestionsLoadedListener listener,
                                                   final boolean fetchResponses) {
        final ParseQuery<SurveyQuestion> query = ParseQuery.getQuery(SurveyQuestion.class);
        query.whereEqualTo("eventId", eventId);
        query.orderByAscending("position");
        query.findInBackground(new FindCallback<SurveyQuestion>() {

            @Override
            public void done(final List<SurveyQuestion> questions, ParseException e) {

                if (!fetchResponses || (questions == null)) {
                    listener.onSurveyQuestionsLoaded(questions);
                } else {

                    // Get the question ids
                    List<String> questionIds = new ArrayList<>();
                    for (SurveyQuestion question : questions) {
                        questionIds.add(question.getObjectId());
                    }

                    SurveyResponse.getSurveyResponsesByQuestionIds(questionIds, new SurveyResponseLoadedListener() {

                        @Override
                        public void onSurveyResponsesLoaded(@Nullable List<SurveyResponse> responses) {
                            if (responses != null) {

                                // Assign the responses to their corresponding question
                                for (SurveyQuestion question : questions) {
                                    for (SurveyResponse response : responses) {
                                        if (response.belongsToQuestion(question, speakerId)) {
                                            question.setResponse(response);
                                        }
                                    }
                                }
                            }

                            listener.onSurveyQuestionsLoaded(questions);
                        }
                    });
                }
            }
        });
    }

    public static void checkQuestionResponse(String quesId, String userId, String sessionId, String speakerId, final SurveyResponseLoadedListener listener){
        ParseQuery<SurveyResponse> query = ParseQuery.getQuery(SurveyResponse.class);
        //query.whereEqualTo("questionId", quesId);
        query.whereEqualTo("userId", userId);
        query.whereEqualTo("sessionId", sessionId);
        query.whereEqualTo("speakerId", speakerId);
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int count, ParseException e) {

            }
        });
        /*
        query.findInBackground(new FindCallback<SurveyResponse>() {

            @Override
            public void done(List<SurveyResponse> responses, ParseException e) {
                listener.onSurveyResponsesLoaded(responses);
            }
        });
        */
    }
    /*
    public static void getSurveyQuestionsByEventSessId(String eventId, String sessionId, final SurveyQuestionsLoadedListener listener,
                                                   final boolean fetchResponses) {
        final ParseQuery<SurveyQuestion> query = ParseQuery.getQuery(SurveyQuestion.class);
        query.whereEqualTo("eventId", eventId);
        query.orderByAscending("position");
        query.findInBackground(new FindCallback<SurveyQuestion>() {

            @Override
            public void done(final List<SurveyQuestion> questions, ParseException e) {

                if (!fetchResponses || (questions == null)) {
                    listener.onSurveyQuestionsLoaded(questions);
                } else {

                    // Get the question ids
                    List<String> questionIds = new ArrayList<>();
                    for (SurveyQuestion question : questions) {
                        questionIds.add(question.getObjectId());
                    }

                    SurveyResponse.getSurveyResponsesByQuestionIds(questionIds, new SurveyResponseLoadedListener() {

                        @Override
                        public void onSurveyResponsesLoaded(@Nullable List<SurveyResponse> responses) {
                            if (responses != null) {

                                // Assign the responses to their corresponding question
                                for (SurveyQuestion question : questions) {
                                    for (SurveyResponse response : responses) {
                                        if (response.belongsToQuestion(question)) {
                                            question.setResponse(response);
                                        }
                                    }
                                }
                            }

                            listener.onSurveyQuestionsLoaded(questions);
                        }
                    });
                }
            }
        });
    }
    */
}
