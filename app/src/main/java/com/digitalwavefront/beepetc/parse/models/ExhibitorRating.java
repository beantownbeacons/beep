package com.digitalwavefront.beepetc.parse.models;

import android.graphics.Bitmap;

import com.digitalwavefront.beepetc.interfaces.ExhibitorRatingLoadedListener;
import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by sanjay on 02/06/16.
 */
@ParseClassName("ExhibitorRating")
public class ExhibitorRating extends ParseObject {

    private Bitmap avatar;
    int rating;

    public ExhibitorRating() {}

    public String getObjectId() {
        return getString("objectId");
    }
    public String getExhibitorId() {
        return getString("ExhibitorId");
    }
    public String getUserId() {
        return getString("UserId");
    }
    public int getRating() {
        return getInt("Rating");
    }

    public boolean belongsExhibitor(Exhibitor exhibitor){
        return getExhibitorId().equals(exhibitor.getObjectId());
    }
    public void setUser(String userid){
        put("userId",userid);
    }
    public void setExhibitor(String exhibitorId){
        put("ExhibitorId",exhibitorId);
    }
    public void setRating(int rating){
        put("Rating",rating);
    }

    public static void getExhibitorRatingByUserID(List<String > exhibitorIdArr, final ExhibitorRatingLoadedListener exhibitorRatingLoadedListener){

        BeConUser user = BeConUser.getCurrentUser();

        ParseQuery<ExhibitorRating> query = ParseQuery.getQuery(ExhibitorRating.class);
        query.whereEqualTo("UserId",user.getObjectId());
        query.whereContainedIn("ExhibitorId", exhibitorIdArr);

        query.findInBackground(new FindCallback<ExhibitorRating>() {
            @Override
            public void done(List<ExhibitorRating> list, ParseException e) {


                exhibitorRatingLoadedListener.onExhibitorRatingLoaded(list);
            }
        });

    }
}
