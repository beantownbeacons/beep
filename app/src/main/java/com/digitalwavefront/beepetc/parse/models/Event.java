package com.digitalwavefront.beepetc.parse.models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.util.Log;

import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.digitalwavefront.beepetc.interfaces.BeaconInteractionsLoadedListener;
import com.digitalwavefront.beepetc.interfaces.BitmapLoadedListener;
import com.digitalwavefront.beepetc.interfaces.EventLoadedListener;
import com.digitalwavefront.beepetc.interfaces.FloatLoadedCallback;
import com.digitalwavefront.beepetc.interfaces.IntegerLoadedCallback;
import com.digitalwavefront.beepetc.interfaces.SessionsLoadedListener;
import com.digitalwavefront.beepetc.login.LoginType;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Wayne on 9/16/15.
 */
@ParseClassName("Event")
public class Event extends ParseObject {

    public Event() {}

    public String chkNull(String str)
    { if(str == null || str.equals(null)){ return "";} return str;}

    public String getName() {
        return getString("name");
    }

    public int getCode() {
        return getInt("code");
    }
    public String getEvtCode() {
        return getString("eventCode");
    }


    private String getAddress() {
        return chkNull(getString("address"));
    }

    private String getCity() {
        return chkNull(getString("city"));
    }

    private String getState() {
        return chkNull(getString("state"));
    }

    private int getZipCode() {
        return getInt("zipCode");
    }

    public String getTimeZone() { return getString("timeZone"); }

    private String getZip() {
        return chkNull(getString("zipCode"));
    }

    //getTriggerBeaconIds
    public List<String> getTriggerBeaconIds() {
        return getList("triggerBeacons");
    }

    public String getDetailedAddress() {
        return String.format("%s, %s, %s %s", getAddress(), getCity(), getState(), getZip());
    }

    public String getLocationName() {
        return getString("location");
    }

    public void getLogo(final BitmapLoadedListener bitmapLoadedListener) {

   try {
           ParseFile logoFile = getParseFile("logo");
               if (logoFile != null) {
                   logoFile.getDataInBackground(new GetDataCallback() {
                       @Override
                       public void done(byte[] bytes, ParseException e) {

                           if (bytes != null) {
                               Log.e("logo_bytes", "" + bytes);
                               Bitmap logo = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                               bitmapLoadedListener.onBitmapLoaded(logo);
                           } else {
                               bitmapLoadedListener.onBitmapLoaded(null);
                           }
                       }
                   });
               }

       }catch (Exception e){
           e.printStackTrace();
       }

    }

    public Date getStartDate() {
        return getDate("startDate");
    }

    public String getStartDateString() {
        //2017-10-16T07:00:00.000Z

        Date startDate = getStartDate();
        String strDt = "";
        if (startDate != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("MMM d, yyyy @ hh:mm a");
            //SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSSZ");
            String timeZone = getTimeZone();
            formatter.setTimeZone(TimeZone.getTimeZone(timeZone));


            strDt = formatter.format(startDate);
            Log.e("StartDate", strDt);

            if (timeZone.equals("America/Los_Angeles")) {

                boolean tz = TimeZone.getTimeZone("America/Los_Angeles").inDaylightTime(getStartDate());
                Log.e("InDayLight", "" + tz);

                if(tz) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(getStartDate());
                    cal.add(Calendar.HOUR, 1);

                    return formatter.format(cal.getTime());
                }
            }
            if (timeZone.equals("EST")) {

                boolean tz = TimeZone.getTimeZone("America/Los_Angeles").inDaylightTime(getStartDate());
                Log.e("InDayLight", "" + tz);

                if(tz) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(getStartDate());
                    cal.add(Calendar.HOUR, 1);

                    return formatter.format(cal.getTime());
                }
            }
            if(timeZone.equals("CST")) {
                boolean tz = TimeZone.getTimeZone("America/Los_Angeles").inDaylightTime(getStartDate());
                Log.e("InDayLight", "" + tz);

                Calendar cal = Calendar.getInstance();
                cal.setTime(getStartDate());
                if(tz) {
                    cal.add(Calendar.HOUR, -5);
                    return formatter.format(cal.getTime());
                }
                else {
                    cal.add(Calendar.HOUR, -6);
                    return formatter.format(cal.getTime());
                }
            }

            return strDt; //formatter.format(startDate);
        }
        return "";
    }



    public Date getEndDate() {
        return getDate("endDate");
    }

    public void getSessions(final SessionsLoadedListener sessionsLoadedListener, boolean fetchBeaconData) {
        Session.getSessionsByEventId(getObjectId(), sessionsLoadedListener, fetchBeaconData);
    }

    /*public String getHashTag() {
        return getString("hashTag");
    }*/

    public List<String> getHash_Tag(){

        /*List<String> list=getList("twTags");
        if(list==null){
            list=new ArrayList<String>();
            list.add("");
            return list;
        }
        return list;*/

        return getList("twTags");
    }


    /*********
     * Queries
     *********/
    public static void getEventByObjectId(String eventId, final EventLoadedListener eventLoadedListener) {
        ParseQuery<Event> query = ParseQuery.getQuery(Event.class);
        query.getInBackground(eventId, new GetCallback<Event>() {

            @Override
            public void done(Event event, ParseException e) {
                Log.e("Praneet","in done"+event);
                if(e == null) {
                    eventLoadedListener.onEventLoaded(event);
                }else{
                    eventLoadedListener.onEventLoaded(null);
                }
            }
        });
    }

    public static void getEventByEventCode(String eventCode, final EventLoadedListener eventLoadedListener) {
        ParseQuery<Event> query = ParseQuery.getQuery(Event.class);
       // query.whereEqualTo("eventCode", eventCode);
        query.whereMatches("eventCode", "("+eventCode+")", "i");
        query.findInBackground(new FindCallback<Event>() {

            @Override
            public void done(List<Event> list, ParseException e) {
                if (list != null && !list.isEmpty()) {
                    eventLoadedListener.onEventLoaded(list.get(0));
                } else {
                    eventLoadedListener.onEventLoaded(null);
                }
            }
        });
    }

    public static void getEventByOrganizerCode(String organizerCode, final EventLoadedListener eventLoadedListener) {
        ParseQuery<Event> query = ParseQuery.getQuery(Event.class);
        query.whereEqualTo("organizerCode", organizerCode);
        query.findInBackground(new FindCallback<Event>() {

            @Override
            public void done(List<Event> list, ParseException e) {
                if (list != null && !list.isEmpty()) {
                    eventLoadedListener.onEventLoaded(list.get(0));
                } else {
                    eventLoadedListener.onEventLoaded(null);
                }
            }
        });
    }

    /*********
     * Metrics
     *********/


    // Interactions
    public static void getAverageTimePerInteraction(Context context, String eventId, final FloatLoadedCallback callback) {
        BeaconInteractionsLoadedListener listener = new BeaconInteractionsLoadedListener() {

            @Override
            public void onInteractionsLoaded(@Nullable List<BeaconInteraction> interactions) {
                if (interactions == null || interactions.isEmpty()) {
                    callback.onFloatLoaded(0f);
                } else {
                    // Calculate the average
                    float sumSeconds = 0f;
                    for (BeaconInteraction interaction : interactions) {
                        sumSeconds += interaction.getTimeSpentSeconds();
                    }

                    float average = sumSeconds / interactions.size();
                    callback.onFloatLoaded(average);
                }
            }
        };

        queryForBeaconInteractions(context, eventId, listener);
    }

    public static void getLongestInteractionForEvent(Context context, String eventId, final IntegerLoadedCallback callback) {
        BeaconInteractionsLoadedListener listener = new BeaconInteractionsLoadedListener() {

            @Override
            public void onInteractionsLoaded(@Nullable List<BeaconInteraction> interactions) {
                if (interactions == null || interactions.isEmpty()) {
                    callback.onIntegerLoaded(0);
                } else {
                    // Calculate the average
                    int largestInteraction = 0;
                    for (BeaconInteraction interaction : interactions) {
                        if (interaction.getTimeSpentSeconds() > largestInteraction) {
                            largestInteraction = interaction.getTimeSpentSeconds();
                        }
                    }
                    callback.onIntegerLoaded(largestInteraction);
                }
            }
        };

        queryForBeaconInteractions(context, eventId, listener);
    }

    public static void getUniqueInteractionCount(Context context, String eventId, final IntegerLoadedCallback callback) {
        BeaconInteractionsLoadedListener listener = new BeaconInteractionsLoadedListener() {

            @Override
            public void onInteractionsLoaded(@Nullable List<BeaconInteraction> interactions) {
                if (interactions == null || interactions.isEmpty()) {
                    callback.onIntegerLoaded(0);
                } else {
                    // Get the amount of unique interactions O(n) operation made need to be optimized
                    List<String> visitedList = new ArrayList<>();
                    int uniqueCount = 0;

                    for (BeaconInteraction interaction : interactions) {
                        String userId = interaction.getUserId();

                        if (!visitedList.contains(userId)) {
                            uniqueCount += 1;
                            visitedList.add(userId);
                        }
                    }

                    callback.onIntegerLoaded(uniqueCount);
                }
            }
        };

        queryForBeaconInteractions(context, eventId, listener);
    }

    public static void getAverageTimeInBoothInSeconds(Context context, String eventId, final FloatLoadedCallback callback) {
        BeaconInteractionsLoadedListener listener = new BeaconInteractionsLoadedListener() {

            @Override
            public void onInteractionsLoaded(@Nullable List<BeaconInteraction> interactions) {
                if (interactions == null || interactions.isEmpty()) {
                    callback.onFloatLoaded(0f);
                } else {
                    float totalTimeSeconds = 0f;

                    for (BeaconInteraction interaction : interactions) {
                        totalTimeSeconds += interaction.getTimeSpentSeconds();
                    }

                    float averageTime = totalTimeSeconds / interactions.size();
                    callback.onFloatLoaded(averageTime);
                }
            }
        };

        queryForBeaconInteractions(context, eventId, listener);
    }

    private static void queryForBeaconInteractions(Context context, String eventId, BeaconInteractionsLoadedListener listener) {

        // Check the login type
        BeConUser currentUser = BeConUser.getCurrentUser();
        int loginType = BeConUser.getCurrentUser().getLoginType(context);

        if (loginType != LoginType.EXHIBITOR) {
            BeaconInteraction.getInteractionsByEventId(eventId, listener);
        } else {
            BeaconInteraction.getInteractionForCompany(eventId, currentUser.getCompanyId(context),
                    listener);
        }
    }


    // Interaction Count
    public static void getTotalInteractionCount(Context context, String eventId, final IntegerLoadedCallback callback) {
        CountCallback countCallback = new CountCallback() {

            @Override
            public void done(int i, ParseException e) {

                if (e != null) {
                    callback.onIntegerLoaded(0);
                } else {
                    callback.onIntegerLoaded(i);
                }
            }
        };

        getInteractionCount(context, eventId, countCallback);
    }

    private static void getInteractionCount(Context context, String eventId, CountCallback countCallback) {

        // Get login type
        BeConUser currentUser = BeConUser.getCurrentUser();
        int loginType = currentUser.getLoginType(context);

        if (loginType != LoginType.EXHIBITOR) {
            BeaconInteraction.getInteractionCountForEvent(eventId, countCallback);
        } else {
            BeaconInteraction.getInteractionCountForCompany(eventId, currentUser.getCompanyId(context),
                    countCallback);
        }
    }


    // Notifications
    public static void getGeneratedNotificationsCountForEvent(Context context, String eventId, final IntegerLoadedCallback callback) {
        ParseQuery<ReceivedNotification> query = ParseQuery.getQuery(ReceivedNotification.class);
        query.whereEqualTo("eventId", eventId);

        BeConUser currentUser = BeConUser.getCurrentUser();
        int loginType = currentUser.getLoginType(context);

        if (loginType == LoginType.EXHIBITOR) {
            // Add the company to the query
            String companyId = currentUser.getCompanyId(context);
            query.whereEqualTo("companyId", companyId);
        }

        // Execute the query
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int numberOfSentNotifications, ParseException e) {
                if (e != null) {
                    callback.onIntegerLoaded(0);
                } else {
                    callback.onIntegerLoaded(numberOfSentNotifications);
                }
            }
        });
    }

    public static void getNumberOfSavedNotifications(Context context, String eventId, final IntegerLoadedCallback callback) {
        ParseQuery<SavedNotification> query = ParseQuery.getQuery(SavedNotification.class);
        query.whereEqualTo("eventId", eventId);

        BeConUser currentUser = BeConUser.getCurrentUser();
        int loginType = currentUser.getLoginType(context);

        if (loginType == LoginType.EXHIBITOR) {
            // Add the company to the query
            String companyId = currentUser.getCompanyId(context);
            query.whereEqualTo("companyId", companyId);
        }

        // Execute the query
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int numberOfSentNotifications, ParseException e) {
                if (e != null) {
                    callback.onIntegerLoaded(0);
                } else {
                    callback.onIntegerLoaded(numberOfSentNotifications);
                }
            }
        });
    }

    public static void getNumberOfDismissedNotifications(final Context context, final String eventId, final IntegerLoadedCallback callback) {
        getGeneratedNotificationsCountForEvent(context, eventId, new IntegerLoadedCallback() {

            @Override
            public void onIntegerLoaded(final int numGenNotifications) {

                getNumberOfSavedNotifications(context, eventId, new IntegerLoadedCallback() {

                    @Override
                    public void onIntegerLoaded(int numSavedNotifications) {
                        callback.onIntegerLoaded(numGenNotifications - numSavedNotifications);
                    }
                });
            }
        });
    }

    public static void getNumberOfNotificationClicks(Context context, String eventId, final IntegerLoadedCallback callback) {
        // TODO: Implement real results
        final float multiplier = .8f;
        getGeneratedNotificationsCountForEvent(context, eventId, new IntegerLoadedCallback() {

            @Override
            public void onIntegerLoaded(int numGenNotifications) {
                callback.onIntegerLoaded((int) (numGenNotifications * multiplier));
            }
        });
    }
}
