package com.digitalwavefront.beepetc.parse.models;

import com.digitalwavefront.beepetc.interfaces.SponsorsLoadedListener;
import com.digitalwavefront.beepetc.utils.StringUtils;
import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by Tarul on 06/10/2016.
 */
@ParseClassName("Sponsor")
public class Sponsor extends ParseObject {

    public Sponsor() {
    }

    public String getEventId() {
        return getString("eventId");
    }

    public String getName() {
        return getString("name");
    }

    public String getDescription() {
        return getString("description");
    }

    public String getWebUrl() {
        return getString("website");
    }

    public String getLevel() {

        if(!StringUtils.isNullOrEmpty(getString("level"))){
            return getString("level");
        }
        return "";
    }

    public static void getSponsorsByEventId(String eventId, final SponsorsLoadedListener sponsorsLoadedListener) {

        ParseQuery<Sponsor> query = ParseQuery.getQuery(Sponsor.class);
        query.whereEqualTo("eventId", eventId);
        query.orderByAscending("name");
        query.setCachePolicy(ParseQuery.CachePolicy.CACHE_THEN_NETWORK);
        query.findInBackground(new FindCallback<Sponsor>() {
            @Override
            public void done(List<Sponsor> objects, ParseException e) {
                if (objects != null) {
                    sponsorsLoadedListener.onSponsorsLoaded(objects);
                }
            }
        });

    }
}
