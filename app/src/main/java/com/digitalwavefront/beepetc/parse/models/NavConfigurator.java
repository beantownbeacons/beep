package com.digitalwavefront.beepetc.parse.models;

import com.digitalwavefront.beepetc.interfaces.NavConfLoadedListener;
import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by User on 9/18/17.
 */
@ParseClassName("NavConfigurator")
public class NavConfigurator extends ParseObject {

    /*
    * eventId
navMyContent
navContentLibrary
navGallery
navExhibitors
navSponsors
navSurvey
    * */
    public NavConfigurator() {}
    public String getEventId() {
        return getString("eventId");
    }
    public boolean getNavMyContent() {
        return getBoolean("navMyContent");
    }
    public boolean getNavContentLibrary() {
        return getBoolean("navContentLibrary");
    }
    public boolean getNavGallery() {
        return getBoolean("navGallery");
    }
    public boolean geNavExhibitors() {
        return getBoolean("navExhibitors");
    }
    public boolean geNavSponsors() {
        return getBoolean("navSponsors");
    }
    public boolean geNavSurvey() {
        return getBoolean("navSurvey");
    }
    public boolean getNewcontentlibrary() { return getBoolean("newcontentlibrary");}
    public boolean geNavAttendees() {
        return getBoolean("navNavAttendees");
    }

    public static void getNavConfiguratorByEventId(String evtId, final NavConfLoadedListener navConfLoaded){

        ParseQuery<NavConfigurator> navconfQry = ParseQuery.getQuery(NavConfigurator.class);
        navconfQry.whereEqualTo("eventId",evtId);
        navconfQry.findInBackground(new FindCallback<NavConfigurator>() {
            @Override
            public void done(List<NavConfigurator> objects, ParseException e) {
                if (objects != null && !objects.isEmpty()) {

                    NavConfigurator navConf = (NavConfigurator) objects.get(0);
                    navConfLoaded.onNavConfLoaded(navConf);
                }else {
                    navConfLoaded.onNavConfLoaded(null);
                }
            }
        });
    }
}
