<resources>
    <string name="app_name">BEEP</string>
    <string name="one_moment_please">One moment please...</string>
    <string name="okay">Okay</string>
    <string name="yes">Yes</string>
    <string name="cancel">Cancel</string>
    <string name="phone_number_taken">Phone number is already in use.</string>
    <string name="error_opening_webpage">Error opening webpage, please try again.</string>
    <string name="finding_event">Finding event...</string>
    <string name="no_network">Hmm...It appears that you\'re off the grid. Please try again once you\'re back from Asgard.</string>
    <string name="loc_access1">BEEP requires access to your location to deliver the best possible experience and in some cases, exclusive offers from our Sponsors and Exhibitors.</string>
    <string name="loc_access2">Click on Settings (below), then click BEEP --> Permissions and enable "Location".</string>
    <string name="settings">Settings</string>
    <string name="ok">Ok</string>
    <string name="error_signinEvent">Error signing in Event</string>

    <!-- Navigation -->
    <string name="event_info">Event Info</string>
    <string name="agenda">Agenda</string>
    <string name="my_agenda">My Agenda</string>
    <string name="attendee">Attendees</string>
    <string name="speakers">Speakers</string>
    <string name="exhibitors">Exhibitors</string>
    <string name="sponsors">Sponsors</string>
    <string name="surveys">Surveys</string>
    <string name="content_library">My Content</string>
    <string name="content">Content Library</string>
    <string name="dashboard">Dashboard</string>
    <string name="social_chatter">Social Chatter</string>
    <string name="about_us">About Us</string>
    <string name="gallery">Gallery</string>
    <string name="sign_out">Sign Out</string>

    <!-- Forgot Password -->
    <string name="forgot_password">Forgot Password</string>
    <string name="proceed">Proceed</string>
    <string name="verify">Verify</string>
    <string name="reset">Change Password</string>
    <string name="reset_password">Reset Password</string>
    <string name="phone_hint">Mobile Phone (Incl. Country Code)</string>
    <string name="confirm_password">Confirm Password</string>
    <string name="verify_phone">Verify Phone</string>

    <!-- Navigation Accessibility -->
    <string name="nav_drawer_open">Open Drawer</string>
    <string name="nav_drawer_close">Close Drawer</string>

    <!-- Registration -->
    <string name="registration_one_time">Registration (One Time)</string>
    <string name="name_hint">Name</string>
    <string name="email_hint">Email</string>
    <string name="username_hint">Username</string>
    <string name="password_hint">Password</string>
    <string name="password_confirmation_hint">Password Confirmation</string>
    <string name="phone_number_hint">Mobile Phone (Country code required)</string>
    <string name="company_hint">Your Company</string>
    <string name="title_hint">Your Title</string>
    <string name="accept_terms">I accept the</string>
    <string name="terms_conditions"> I accept the <u>Terms &amp; Conditions</u></string>
    <string name="register">Register</string>
    <string name="mobile_phone_reason">We\'ll send you a verification code.</string>
    <string name="error_signing_up">Error signing up, please try again later.</string>
    <string name="signing_in">Signing In</string>
    <string name="already_a_user">Already a user?</string>
    <string name="empty_fields">Empty fields.</string>
    <string name="invalid_email">Invalid username</string>
    <string name="passwords_dont_match">Passwords don\'t match.</string>
    <string name="password_must_be_at_least">Password must be at least</string>
    <string name="characters">characters</string>
    <string name="email_taken">Username is already in use.</string>

    <!-- Verify Code -->
    <string name="verify_code">Verify Code</string>
    <string name="edit_phone">Edit Phone?</string>
    <string name="code_hint">Enter code received via text message...</string>
    <string name="no_code_entered">No code entered.</string>
    <string name="verifying_code">Verifying Code</string>
    <string name="error_verifying_code">Error verifying code.</string>
    <string name="invalid_verification_code">Invalid verification code.</string>
    <string name="verification_code_on_the_way">The verification code is on the way!</string>
    <string name="error_sending_verification_code">Error sending verification code.</string>

    <!-- Event Login -->
    <string name="event_login">Event Login</string>
    <string name="event_code_hint">Enter event code here...</string>
    <string name="access_event">Access Event</string>
    <string name="enter_event_access_code">Enter the Event Access Code</string>
    <string name="no_event_code_entered">No event code entered.</string>
    <string name="no_event_found">No event could be found with the given code.</string>
    <string name="locating_event">Finding Event</string>
    <string name="event_login_error">Error logging into event.</string>

    <!-- Event Details -->
    <string name="loading_event_info">Loading Event Info</string>
    <string name="event_not_found">Event not found.</string>
    <string name="signing_out">Signing Out</string>
    <string name="loading_sessions">Loading Sessions</string>
    <string name="error_loading_sessions">Error loading sessions.</string>
    <string name="loading_attendee">Loading Attendees</string>
    <string name="error_loading_attendee">Error loading attendees</string>
    <string name="loading_speakers">Loading Speakers</string>
    <string name="loading_sponsors">Loading Sponsors</string>
    <string name="loading_gallery">Loading Gallery</string>
    <string name="loading_image">Loading Image</string>
    <string name="error_loading_speakers">Error loading speakers.</string>
    <string name="no_speakers">There\'s no content to see here right now! But, stay tuned, because you\'re going love the lineup of Speakers we have in store for you.</string>
    <string name="no_exhibitors">The Organizers have not setup the Exhibitors for this event yet. Please check back for any updates.
.</string>
    <string name="no_sponsors">The Organizers have not setup the Sponsors for this event yet. Please check back for any updates.
.</string>
    <string name="no_attendee">Your fellow attendees will show up here! Encourage them to download the App (BEEP ETC) so you can connect with them! After all, events are all about community engagement.</string>
    <!--<string name="no_hashtag">The Organizer hasn\'t assigned a #hashtag to this event. If you have any suggestions, please feel free to reach out!</string>-->
    <string name="no_hashtag">There\'s been no recent activity related to this hashtag. Please swipe-down to refresh or try again later.</string>
    <string name="enter_event">Enter Event</string>
    <string name="exit_event">Exit Event</string>

    <!-- Agenda -->
    <string name="filter_by_room">Filter by Room</string>
    <string name="all">All</string>
    <string name="going_question">Going?</string>
    <string name="delete">Delete</string>
    <string name="no_agenda">The Organizers have not setup the Agenda for the event yet.
</string>
    <string name="no_session">No Session Info Available</string>
    <string name="myagenda_errmsg">Build your personalized agenda by clicking on the  \"+\"  next to each session you plan to attend.</string>


    <!-- Notifications -->
    <string name="save">Save to My Content</string>
    <string name="saved_exclaim">Saved!</string>
    <string name="error_saving">Error saving.</string>
    <string name="not_interested">Not Interested</string>

    <!-- Content Library -->
    <string name="error_loading_content">There\'s no content to see here right now! But, you may be rewarded with content (perhaps even a special discount!) based on your interest. Just, save the notifications that do show up (they\'re personalized based on your interests) and they\'ll appear here. Enjoy the event!
</string>
    <string name="loading_content">Loading Content</string>
    <string name="pdf_error">Pdf could not be viewed at this time</string>
    <string name="loading_pdf">Loading PDF</string>
    <string name="pdf_viewer">PDF Viewer</string>
    <string name="please_install_pdf_viewer">Please install pdf viewer.</string>
    <string name="no_url">No website to view.</string>
    <string name="no_content">The Organizer has not made any content available for this event yet. Please check back later.</string>
    <string name="no_dismissed">You do not have any dismissed notifications. Please check back later.</string>
    <string name="no_pref">Nothing to see here. Just hit \"Save" when an Organizer/Exhibitor content notification arrives and it will be archived here. Forever.</string>

    <!-- Surveys -->
    <string name="error_loading_surveys">Patience Watson! Surveys will automatically appear when each session is scheduled to start. Before you know it, you\'ll have access to all the surveys by the end of the day.
</string>
    <string name="loading_surveys">Loading Surveys</string>
    <string name="survey">Survey</string>
    <string name="loading_questions">Loading Questions</string>
    <string name="error_loading_questions">Error loading questions.</string>
    <string name="modifying_rsvp">Modifying RSVP</string>
    <string name="no_rating_given">No rating given.</string>
    <string name="processing_response">Processing Response</string>
    <string name="rating_already_recorded">Rating already recorded.</string>
    <string name="thank_you">Thank You!</string>

    <!-- Dashboard -->
    <string name="event_snapshot">Event Snapshot</string>
    <string name="session_metrics">Session Metrics</string>
    <string name="speakers_by_rank">Speakers By Rank</string>
    <string name="booth_metrics">Booth Metrics</string>

    <array name="dashboard_titles">
        <item>@string/booth_metrics</item>
        <item>@string/session_metrics</item>
        <item>@string/speakers_by_rank</item>
        <item>@string/event_snapshot</item>
    </array>

    <array name="dashboard_icons">
        <item>-1</item>
        <item>-1</item>
        <item>-1</item>
        <item>-1</item>
    </array>

    <!-- Event Snapshot -->
    <string name="interactions">Interactions</string>
    <string name="average_time_per_interaction">Average Time Per Interaction</string>
    <string name="longest_measured_interaction">Longest (measured) Interaction</string>
    <string name="error_loading_event_info">Error loading event info.</string>

    <!-- Session Metrics -->
    <string name="average_score">Average Score</string>
    <string name="surveys_completed">Surveys Completed</string>
    <string name="average_attendee_time">Average Attendee Time</string>
    <string name="attendees">Attendees</string>
    <string name="error_loading_data">Error loading data.</string>
    <string name="loading_data">Loading Data</string>

    <!-- Booth Metrics -->
    <string name="total_interactions">Total Interactions</string>
    <string name="unique_interactions">Unique Interactions</string>
    <string name="average_time_in_booth">Average Time in Booth</string>
    <string name="notifications_generated">Notifications Generated</string>
    <string name="saved_notifications">Saved Notifications</string>
    <string name="dismissed_notifications">Dismissed Notifications</string>
    <string name="notification_clicks">Notification Clicks</string>

    <!-- Login Fragment -->
    <string name="login">Login</string>
    <string name="email_does_not_exist">Please check your credentials and try again.</string>
    <string name="logging_in">Logging In</string>

    <!-- Profile fragment-->
    <string name="linkedIn_txt_hint">http://www.linkedin.com/in/</string>
    <string name="twitter_txt_hint">Twitter Handle @</string>
    <string name="reset_txt"><u>Reset</u></string>
    <string name="camera_per">Please visit Settings --> Apps --> BEEP --> Permissions and enable Camera and Storage.</string>

    <!-- Delete Confirmation -->
    <string name="delete_confirmation_title">Delete?</string>
    <string name="delete_confirmation_prompt">Are you sure you want to delete this from your library? There’s no undo.</string>
    <string name="resend_code_prompt">Resend Code?</string>
    <string name="invalid_phone_number">Invalid phone number.</string>
    <string name="edit_phone_dialog_title">Phone Number?</string>

    <!-- Edit Phone -->
    <string name="phone_unchanged">Phone number unchanged.</string>
    <string name="error_updating_phone">Error updating phone.</string>
    <string name="updating_phone">Updating Phone Number</string>
    <string name="phone_update_success">Phone number updated!</string>
    <string name="error_logging_in">Error logging in, please try again.</string>
    <string name="organizer_prompt">Organizer?</string>
    <string name="exhibitor_prompt">Exhibitor?</string>

    <!-- Organizer Login -->
    <string name="event_organizer_login">Event Organizer Login</string>
    <string name="enter_organizer_access_code">Enter organizer access code.</string>
    <string name="organizer_code_hint">Enter organizer code for event</string>
    <string name="invalid_access_code">Invalid access code.</string>

    <!-- Exhibitor Login -->
    <string name="event_exhibitor_login">Event Exhibitor Login</string>
    <string name="exhibitor_code_hint">Enter exhibitor code for event</string>
    <string name="company_code_hint">Enter your company code</string>
    <string name="company_not_found">Company not found with that code.</string>

    <!-- Social Chatter -->
    <string name="loading_tweets">Loading Tweets...</string>
    <string name="error_loading_tweets">Error loading tweets, please try again later.</string>
    <string name="title_activity_maps">BEEP</string>
    <string name="no_recent_tweets">There\'s been no recent activity related to this hashtag. Please swipe-down to refresh or try again later.</string>

    <!-- Facebook App ID -->
    <string name="facebook_app_id">1109509782443828</string>
    <!--<string name="facebook_app_id">966996563439522</string>-->

    <!-- TODO: Remove or change this placeholder text -->
    <string name="hello_blank_fragment">Hello blank fragment</string>

    <!--Gallery-->
    <string name="no_gallery">Go ahead and share your event experience with others! Images from other Attendees will be shared here to bring you a 360-degree experience! Be the first to share a picture.</string>

    <!-- Preferences -->
    <string name="pref_sessreminder">Session Reminders</string>
    <string name="prefkey_sess">SessionReminder</string>
    <string name="loading_pref">Loading Preferences</string>
    <string name="updating_pref">Updating Preferences</string>
    <string name="updated_pref_success">Preferences updated successfully.</string>

    <!-- GCM -->
    <string name="gcm_project_number">1038199131079</string>
    <string name="gcm_pref_token">gcm_token</string>
    <string name="endpoint_arn">endpoint_arn</string>
    <!--arn:aws:sns:us-west-2:912347879959:app/GCM/Beep-->
    <string name="num_of_missed_messages">1</string>
    <string name="lines_of_message_count">1</string>
    <string name="notification_number">1</string>
    <string name="preferences">my_pref</string>
    <string name="first_launch">true</string>
</resources>
