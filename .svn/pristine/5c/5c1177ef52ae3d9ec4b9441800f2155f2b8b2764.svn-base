package com.digitalwavefront.beepetc.parse.models;

import android.util.Log;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.digitalwavefront.beepetc.interfaces.NotificationsLoadedListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wayne on 10/13/15.
 */
@ParseClassName("SavedNotification")
public class SavedNotification extends ParseObject {

    public SavedNotification() {}

    public static SavedNotification newInstance(BeConUser user, String notificationId, String companyId) {
        SavedNotification savedNotification = new SavedNotification();
        savedNotification.setUser(user);
        savedNotification.setNotificationId(notificationId);
        savedNotification.setCompanyId(companyId);
        return savedNotification;
    }

    private void setUser(BeConUser user) {
        put("userId", user.getObjectId());
        put("eventId", user.getCurrentEventId());
    }

    private void setCompanyId(String companyId) {
        put("companyId", companyId);
    }

    private void setNotificationId(String notificationId) {
        put("notificationId", notificationId);
    }

    public String getNotificationId() {
        return getString("notificationId");
    }

    /****************
     * Queries
     ****************/
    public static void deleteRSVPByNotificationAndUser(BeConNotification notification, BeConUser user, final DeleteCallback callback) {
        ParseQuery<SavedNotification> query = ParseQuery.getQuery(SavedNotification.class);
        query.whereEqualTo("notificationId", notification.getObjectId());
        query.whereEqualTo("userId", user.getObjectId());
        query.findInBackground(new FindCallback<SavedNotification>() {

            public void done(List<SavedNotification> notifications, ParseException e) {
                ParseObject.deleteAllInBackground(notifications, callback);
            }
        });
    }

    public static void deleteSavedNotification(BeConNotification notification, BeConUser user, final DeleteCallback callback) {
        ParseQuery<SavedNotification> query = ParseQuery.getQuery(SavedNotification.class);
        query.whereEqualTo("notificationId", notification.getObjectId());
        query.whereEqualTo("userId", user.getObjectId());
        query.whereEqualTo("eventId", user.getCurrentEventId());
        query.findInBackground(new FindCallback<SavedNotification>() {

            public void done(List<SavedNotification> notifications, ParseException e) {
                ParseObject.deleteAllInBackground(notifications, callback);
            }
        });
    }
    public static void getSavedNotificationsForUser(BeConUser user, String eventId, final NotificationsLoadedListener listener) {
        List<String> savedIds =  user.getSavedNotIds();
        Log.e("getSavedNotifications", "savedIds >> " + savedIds);

        ParseQuery<SavedNotification> query = ParseQuery.getQuery(SavedNotification.class);
        query.whereEqualTo("eventId", eventId);
        query.whereEqualTo("userId", user.getObjectId());
        //query.whereContainedIn("objectId", savedIds);
        query.findInBackground(new FindCallback<SavedNotification>() {

            @Override
            public void done(List<SavedNotification> notifications, ParseException e) {
                if (e != null || notifications == null) {
                    listener.onNotificationsLoaded(new ArrayList<BeConNotification>());
                } else {
                    List<String> savedNotificationIds = new ArrayList<>();
                    for (SavedNotification notification : notifications) {
                        savedNotificationIds.add(notification.getNotificationId());
                    }

                    BeConNotification.getNotificationsByIds(savedNotificationIds, listener);
                }
            }
        });
    }
}
