package com.digitalwavefront.beep.fragments;


import android.support.v4.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitalwavefront.beep.navigation.NavigationItemPositionManager;
import com.digitalwavefront.beep.R;
import com.digitalwavefront.beep.activities.MainActivity;
import com.digitalwavefront.beep.interfaces.FloatLoadedCallback;
import com.digitalwavefront.beep.interfaces.IntegerLoadedCallback;
import com.digitalwavefront.beep.parse.models.BeConUser;
import com.digitalwavefront.beep.parse.models.Event;
import com.digitalwavefront.beep.utils.StringUtils;
import com.digitalwavefront.beep.utils.TypefaceUtils;


public class BoothMetricsFragment extends Fragment {

    private BeConUser currentUser;
    private String eventId;
    private TextView totalInteractionsTextView, totalInteractionsTitleTextView, uniqueInteractionsTextView,
    uniqueInteractionsTitleTextView, averageTimeTextView, averageTimeTitleTextView, notificationsGeneratedTextView,
    notificationsGeneratedTitleTextView, savedNotificationsTextView, savedNotificationsTitleTextView,
    dismissedNotificationsTextView, dismissedNotificationsTitleTextView, notificationClicksTextView,
    notificationClicksTitleTextView;

    public BoothMetricsFragment() {}

    public static BoothMetricsFragment newInstance() {
        return new BoothMetricsFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentUser = BeConUser.getCurrentUser();
        eventId = currentUser.getCurrentEventId();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_booth_metrics, container, false);
        totalInteractionsTextView = (TextView) rootView.findViewById(R.id.total_interactions_text_view);
        totalInteractionsTitleTextView = (TextView) rootView.findViewById(R.id.total_interactions_title_text_view);
        uniqueInteractionsTextView = (TextView) rootView.findViewById(R.id.unique_interactions_text_view);
        uniqueInteractionsTitleTextView = (TextView) rootView.findViewById(R.id.unique_interactions_title_text_view);
        averageTimeTextView = (TextView) rootView.findViewById(R.id.average_time_text_view);
        averageTimeTitleTextView = (TextView) rootView.findViewById(R.id.average_time_title_text_view);
        notificationsGeneratedTextView = (TextView) rootView.findViewById(R.id.notifications_generated_text_view);
        notificationsGeneratedTitleTextView = (TextView) rootView.findViewById(R.id.notifications_generated_title_text_view);
        savedNotificationsTextView = (TextView) rootView.findViewById(R.id.saved_notifications_text_view);
        savedNotificationsTitleTextView = (TextView) rootView.findViewById(R.id.saved_notifications_title_text_view);
        dismissedNotificationsTextView = (TextView) rootView.findViewById(R.id.dismissed_notifications_text_view);
        dismissedNotificationsTitleTextView = (TextView) rootView.findViewById(R.id.dismissed_notifications_title_text_view);
        notificationClicksTextView = (TextView) rootView.findViewById(R.id.notification_clicks_text_view);
        notificationClicksTitleTextView = (TextView) rootView.findViewById(R.id.notification_clicks_title_text_view);

        // Set fonts
        Typeface lightTypeface = TypefaceUtils.getLightTypeface(getActivity());
        Typeface boldTypeface = TypefaceUtils.getBoldTypeface(getActivity());

        totalInteractionsTextView.setTypeface(boldTypeface);
        totalInteractionsTitleTextView.setTypeface(lightTypeface);
        uniqueInteractionsTextView.setTypeface(boldTypeface);
        uniqueInteractionsTitleTextView.setTypeface(lightTypeface);
        averageTimeTextView.setTypeface(boldTypeface);
        averageTimeTitleTextView.setTypeface(lightTypeface);
        notificationsGeneratedTextView.setTypeface(boldTypeface);
        notificationsGeneratedTitleTextView.setTypeface(lightTypeface);
        savedNotificationsTextView.setTypeface(boldTypeface);
        savedNotificationsTitleTextView.setTypeface(lightTypeface);
        dismissedNotificationsTextView.setTypeface(boldTypeface);
        dismissedNotificationsTitleTextView.setTypeface(lightTypeface);
        notificationClicksTextView.setTypeface(boldTypeface);
        notificationClicksTitleTextView.setTypeface(lightTypeface);

        loadMetrics();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.booth_metrics));
    //    ((MainActivity) getActivity()).setSelectedNavigationPosition(NavigationItemPositionManager.NO_POSITION);
    }

    private void loadMetrics() {
        loadTotalInteractions();
        loadUniqueInteractions();
        loadAverageTime();
        loadNotificationsGenerated();
        loadSavedNotifications();
        loadDismissedNotification();
        loadNotificationClicks();
    }

    private void loadTotalInteractions() {
        Event.getTotalInteractionCount(getActivity(), eventId, new IntegerLoadedCallback() {
            @Override
            public void onIntegerLoaded(int totalInteractions) {
                totalInteractionsTextView.setText(String.valueOf(totalInteractions));
            }
        });
    }

    private void loadUniqueInteractions() {
        Event.getUniqueInteractionCount(getActivity(), eventId, new IntegerLoadedCallback() {
            @Override
            public void onIntegerLoaded(int uniqueInteractions) {
                uniqueInteractionsTextView.setText(String.valueOf(uniqueInteractions));
            }
        });
    }

    private void loadAverageTime() {
        Event.getAverageTimeInBoothInSeconds(getActivity(), eventId, new FloatLoadedCallback() {

            @Override
            public void onFloatLoaded(float averageTime) {
                String timeString = StringUtils.getMinuteSecondStringFromSeconds(averageTime);
                averageTimeTextView.setText(timeString);
            }
        });
    }

    private void loadNotificationsGenerated() {
        Event.getGeneratedNotificationsCountForEvent(getActivity(), eventId, new IntegerLoadedCallback() {

            @Override
            public void onIntegerLoaded(int numGenNotifications) {
                notificationsGeneratedTextView.setText(String.valueOf(numGenNotifications));
            }
        });
    }

    private void loadSavedNotifications() {
        Event.getNumberOfSavedNotifications(getActivity(), eventId, new IntegerLoadedCallback() {

            @Override
            public void onIntegerLoaded(int numSavedNotifications) {
                savedNotificationsTextView.setText(String.valueOf(numSavedNotifications));
            }
        });
    }

    private void loadDismissedNotification() {
        Event.getNumberOfDismissedNotifications(getActivity(), eventId, new IntegerLoadedCallback() {

            @Override
            public void onIntegerLoaded(int numDismissedNotifications) {
                dismissedNotificationsTextView.setText(String.valueOf(numDismissedNotifications));
            }
        });
    }

    private void loadNotificationClicks() {
        Event.getNumberOfNotificationClicks(getActivity(), eventId, new IntegerLoadedCallback() {

            @Override
            public void onIntegerLoaded(int numNotificationClicks) {
                notificationClicksTextView.setText(String.valueOf(numNotificationClicks));
            }
        });
    }
}
