var twilio = require('twilio')('AC0bc06434b4528a6be316e388d700e130', '8ddb4a1fc1ffbfcbd9932f8914dfa35e');
Parse.Cloud.define("sendVerificationCode", function(request, response) {
    var verificationCode = Math.floor(Math.random()*999999);
    var user = Parse.User.current();
    user.set("phoneVerificationCode", verificationCode);
    user.save();
    
    twilio.sendSms({
        From: "+17813990674",
        To: request.params.phoneNumber,
        Body: "Your BEEP verification code is " + verificationCode + "."
    }, function(err, responseData) { 
        if (err) {
          response.error(err);
        } else { 
          response.success("Success");
        }
    });
});

Parse.Cloud.define("verifyUser", function(request, response) {
  var verificationCode = request.params.verificationCode;
  var user = Parse.User.current();
  var userVerificationCode = user.get("phoneVerificationCode");

  var verified = false;

  if (verificationCode == userVerificationCode) {
    verified = true;
  }
  
  user.set("isVerified", verified);
  user.save();

  // Create JSON with results
  response.success(verified);
});

